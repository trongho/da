﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Handlers;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
//using DeliveryHaPhan.Mail;
using DeliveryHaPhan.Models;
using DeliveryHaPhan.Requirements;
using DeliveryHaPhan.Services;
using DeliveryHaPhan.SignalR;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace DeliveryHaPhan
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services
                .AddControllersWithViews()
                .AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("WarehouseDeliveryConnectionString")));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderResultService, OrderResultService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
            services.AddScoped<IMapService, MapService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IWRRHeaderService, WRRHeaderService>();
            services.AddScoped<IWRRDetailService, WRRDetailService>();
            services.AddScoped<IWRDataHeaderService, WRDataHeaderService>();
            services.AddScoped<IWRDataGeneralService, WRDataGeneralService>();
            services.AddScoped<IGoodsDataService, GoodsDataService>();
            services.AddScoped<IRequestService, RequestService>();
            services.AddScoped<IUserNewService, UserNewService>();
            services.AddScoped<IRequestPaymentService, RequestPaymentService>();
            services.AddScoped<IRequestSuppliesService, RequestSuppliesService>();
            services.AddScoped<IRequestLendGoodsDemoService, RequestLendGoodsDemoService>();
            services.AddScoped<IRequestBookGrabService, RequestBookGrabService>();
            services.AddScoped<IRequestReimbursementService, RequestReimbursementService>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = TokenHelper.Issuer,
                            ValidAudience = TokenHelper.Audience,
                            IssuerSigningKey = new SymmetricSecurityKey(Convert.FromBase64String(TokenHelper.Secret))
                        };

                    });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("OnlyNonBlockedUser", policy =>
                {
                    policy.Requirements.Add(new UserStatusRequirement(false));
                });
                options.AddPolicy("RequireAdministratorRole",
                    policy => policy.RequireRole("Admin"));

            });

            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            //    .AddCookie(x => x.LoginPath = "/account/login");

            services.AddSingleton<IAuthorizationHandler, UserBlockedStatusHandler>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSession(options => {
                options.IdleTimeout = TimeSpan.FromMinutes(600);
            });
            //services.AddCors(); // Make sure you call this previous to AddMvc

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder => {
                builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed(origin => true) // allow any origin 
                    .AllowCredentials();
            }));
           
            services.AddMemoryCache();

            //var emailConfig = Configuration
            //    .GetSection("EmailConfiguration")
            //    .Get<EmailConfiguration>();
            //services.AddSingleton(emailConfig);
            //services.AddScoped<IEmailSender, EmailSender>();
            services.AddSignalR(o =>
            {
                o.EnableDetailedErrors = true;
            }).AddJsonProtocol(options => { options.PayloadSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;});
            services.AddScoped<LiveUpdateWRDHub>();
            services.AddSingleton<WRDTicker>();


            //services.AddIdentity<AppUser, IdentityRole>()
            //    .AddDefaultTokenProviders();
            // Truy cập IdentityOptions
            //services.Configure<IdentityOptions>(options => {
            //    // Thiết lập về Password
            //    options.Password.RequireDigit = false; // Không bắt phải có số
            //    options.Password.RequireLowercase = false; // Không bắt phải có chữ thường
            //    options.Password.RequireNonAlphanumeric = false; // Không bắt ký tự đặc biệt
            //    options.Password.RequireUppercase = false; // Không bắt buộc chữ in
            //    options.Password.RequiredLength = 3; // Số ký tự tối thiểu của password
            //    options.Password.RequiredUniqueChars = 1; // Số ký tự riêng biệt

            //    // Cấu hình Lockout - khóa user
            //    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5); // Khóa 5 phút
            //    options.Lockout.MaxFailedAccessAttempts = 5; // Thất bại 5 lầ thì khóa
            //    options.Lockout.AllowedForNewUsers = true;

            //    // Cấu hình về User.
            //    options.User.AllowedUserNameCharacters = // các ký tự đặt tên user
            //        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
            //    options.User.RequireUniqueEmail = true;  // Email là duy nhất

            //    // Cấu hình đăng nhập.
            //    options.SignIn.RequireConfirmedEmail = true;            // Cấu hình xác thực địa chỉ email (email phải tồn tại)
            //    options.SignIn.RequireConfirmedPhoneNumber = false;     // Xác thực số điện thoại

            //});

            //services.AddOptions();                                        // Kích hoạt Options
            //var mailsettings = Configuration.GetSection("MailSettings");  // đọc config
            //services.Configure<MailSettings>(mailsettings);               // đăng ký để Inject

            //services.AddTransient<IEmailSender, SendMailService>();        // Đăng ký dịch vụ Mail
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}


            app.UseDeveloperExceptionPage();

            app.UseSession();

            app.Use(async (context, next) =>
            {
                var token = context.Session.GetString("Token");
                if (!string.IsNullOrEmpty(token))
                {
                    context.Request.Headers.Add("Authorization", "Bearer " + token);
                }
                await next();
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "node_modules")),
                RequestPath = "/node_modules"
            });

            app.UseRouting();

            //app.UseCors(
            //    options => options.WithOrigins("http://localhost:61292/").AllowAnyMethod()
            //);

            app.UseCors("CorsPolicy");


            // global cors policy
            //app.UseCors(x => x
            //    .AllowAnyOrigin()
            //    .AllowAnyMethod()
            //    .AllowAnyHeader());

            app.UseStatusCodePages(async context =>
            {
                var request = context.HttpContext.Request;
                var response = context.HttpContext.Response;

                if (response.StatusCode == (int)HttpStatusCode.Unauthorized ||
                    response.StatusCode == (int)HttpStatusCode.Forbidden)
                    response.Redirect("/User/Login");
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseWebSockets();


            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<LiveUpdateWRDHub>("/liveUpdateWRDRHub");
            //});



            //app.UseForwardedHeaders(new ForwardedHeadersOptions
            //{
            //    ForwardedHeaders = ForwardedHeaders.XForwardedFor |
            //    ForwardedHeaders.XForwardedProto
            //});


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=App}/{action=Index}/{id?}");

                endpoints.MapHub<LiveUpdateWRDHub>("/liveUpdateWRDHub");
            });
        }
    }
}
