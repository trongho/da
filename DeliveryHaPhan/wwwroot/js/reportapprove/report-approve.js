﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.ReportApproves = function () {
    var currentReport;
    var waitingLength;
    var tabPanel;
    var listWaiting, listApproving;
    var popup;

    this.tabTitleTemplate = function (itemData, itemIndex, itemElement) {
        itemElement.append("<span style='font-size:small'>" + itemData.title + "</span>");

        $.getJSON("/api/RequestApi/GetQuantityRequest?text=" + itemData.title,
            function (result) {
                itemElement.append($("<span/>").text(result)
                    .attr("class", "w3-badge w3-orange"));
            });

        //tabPanel.option("items[0].badge", 9);
        //tabPanel.repaint();  

    };

    var tabTitleTemplate2 = function (itemData, itemIndex, itemElement) {
        itemElement.append("<span style='font-size:small'>" + itemData.title + "</span>");

        $.getJSON("/api/RequestApi/GetQuantityRequest?text=" + itemData.title,
            function (result) {
                itemElement.append($("<span/>").text(result)
                    .attr("class", "w3-badge w3-orange"));
            });
    };

    this.waitingBadge = function () {
        return waitingLength;
    };

    this.tabPanelTemplate = function (itemData, itemIndex, itemElement) {
        itemData.badge = 3
    };


    this.button_createNewRequest_clickHandle = async function () {
        const { value: report } = await Swal.fire({
            title: 'Chọn mẫu đề xuất',
            input: 'select',
            inputOptions: {
                'requestPayment': 'Đề nghị thanh toán',
                'requestSupplies': 'Đề xuất vật tư',
                'requestLendGoodsDemo': 'Đề xuất mượn hàng demo',
                'requestBookGrab': 'Đề xuất sử dụng thẻ taxi/ Xe công ty/ Grab',
                'requestReimbursement':'Đề nghị hoàn ứng'
            },
            inputPlaceholder: 'Chọn mẫu đề xuất',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                    if (value !== null) {
                        resolve()
                    } else {
                        resolve('Bạn chưa chọn mẫu tờ trình')
                    }
                })
            }
        })

        if (report === 'requestPayment') {
            openPopupCreateRequestPayment();
        }
        else if (report === 'requestSupplies') {
            openPopupCreateRequestSupplies();
        }
        else if (report === 'requestLendGoodsDemo') {
            openPopupCreateRequestLendGoodsDemo();
        }
        else if (report === 'requestBookGrab') {
            openPopupCreateRequestBookGrab();
        }
        else if (report === 'requestReimbursement') {
            openPopupCreateRequestReimbursement();
        }
    };

    var openPopupCreateRequestPayment = function () {
        popup = $("#popup-create-request-payment").dxPopup("instance");
        popup.show();

        var table = $('#payment-table').DataTable({
            destroy: true,
            paging: false,
            searching: false,
            ordering: false,
            data:[],
            columns: [
                { data: "PaymentContent" },
                { data: "Amount" },
                { data: "DocumentsNumber", defaultContent: "<i>Not set</i>" },
                { data: "Note", defaultContent: "<i>Not set</i>" },
                {
                    defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                },
            ],
            footerCallback: function (row, data, start, end, display) {
                var api = this.api();

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };

                // Total over all pages
                var total = api
                    .column(2)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                var pageTotal = api
                    .column(2, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $("#total-amount").dxTextBox("instance").option("value", total);
            }
        });

        $(document).on("click", "#add", function () {
            Swal.fire({
                title: 'Thêm nội dung thanh toán',
                html: `<input type="text" id="paymentContent" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Thêm',
                focusConfirm: false,
                preConfirm: () => {
                    const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                    const amount = Swal.getPopup().querySelector('#amount').value;
                    const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!paymentContent) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!amount) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(amount)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                }
            }).then((result) => {
                var row = {
                    "PaymentContent": result.value.paymentContent,
                    "Amount": parseInt(result.value.amount),
                    "DocumentsNumber": result.value.documentsNumber,
                    "Note": result.value.note,
                };
                table.row.add(row).draw(false);
            })

        });


        $('#payment-table tbody').on("click", ".edit", function () {
            var row = $(this).closest('tr');
            var data = table.row(row).data().PaymentContent;
            Swal.fire({
                title: 'Sửa nội dung thanh toán',
                html: `<input type="text" id="paymentContent" value="${table.row(row).data().PaymentContent}" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" type="number" value="${table.row(row).data().Amount}" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" value="${table.row(row).data().DocumentsNumber}" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Sửa',
                focusConfirm: false,
                preConfirm: () => {
                    const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                    const amount = Swal.getPopup().querySelector('#amount').value;
                    const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!paymentContent) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!amount) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(amount)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                }
            }).then((result) => {
                var rowData = {
                    "PaymentContent": result.value.paymentContent,
                    "Amount": parseInt(result.value.amount),
                    "DocumentsNumber": result.value.documentsNumber,
                    "Note": result.value.note,
                };
                table.row(row.attr('rowindex', row.index())).data(rowData).draw();
            })
        });


        $('#payment-table tbody').on("click", ".delete", function () {
            var row = $(this).closest('tr');
            table.row(row).remove().draw();
        });
    };

    //
    var openPopupCreateRequestSupplies = function () {
        popup = $("#popup-create-request-supplies").dxPopup("instance");
        popup.show();

        var table = $('#supplies-table').DataTable({
            destroy: true,
            paging: false,
            searching: false,
            ordering: false,
            data: [],
            columns: [
                { data: "Goods" },
                { data: "Specification" },
                { data: "Quantity" },
                { data: "Note", defaultContent: "<i>Not set</i>" },
                {
                    defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                },
            ]
        });

        $(document).on("click", "#add", function () {
            Swal.fire({
                title: 'Thêm nội dung thanh toán',
                html: `<input type="text" id="goods" class="swal2-input" placeholder="Hàng hóa">
                                <input type="text" id="specification" class="swal2-input" placeholder="Quy cách">
                                <input type="text" id="quantity" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Thêm',
                focusConfirm: false,
                preConfirm: () => {
                    const goods = Swal.getPopup().querySelector('#goods').value;
                    const specification = Swal.getPopup().querySelector('#specification').value;
                    const quantity = Swal.getPopup().querySelector('#quantity').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!goods) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!specification) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!quantity) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(quantity)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { goods: goods, specification: specification, quantity: quantity, note: note }
                }
            }).then((result) => {
                var row = {
                    "Goods": result.value.goods,
                    "Specification": result.value.specification,
                    "Quantity": result.value.quantity,
                    "Note": result.value.note,
                };
                table.row.add(row).draw(false);
            })

        });


        $('#supplies-table tbody').on("click", ".edit", function () {
            var row = $(this).closest('tr');
            Swal.fire({
                title: 'Sửa danh sách hàng hóa',
                html: `<input type="text" id="goods" value="${table.row(row).data().Goods}" class="swal2-input" placeholder="Hàng hóa">
                                <input type="text" id="specification" type="number" value="${table.row(row).data().Specification}" class="swal2-input" placeholder="Quy cách">
                                <input type="text" id="quantity" value="${table.row(row).data().Quantity}" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Sửa',
                focusConfirm: false,
                preConfirm: () => {
                    const goods = Swal.getPopup().querySelector('#goods').value;
                    const specification = Swal.getPopup().querySelector('#specification').value;
                    const quantity = Swal.getPopup().querySelector('#quantity').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!goods) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!specification) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!quantity) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(quantity)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { goods: goods, specification: specification, quantity: quantity, note: note }
                }
            }).then((result) => {
                var rowData = {
                    "Goods": result.value.goods,
                    "Specification": result.value.specification,
                    "Quantity": result.value.quantity,
                    "Note": result.value.note,
                };
                table.row(row.attr('rowindex', row.index())).data(rowData).draw();
            })
        });


        $('#supplies-table tbody').on("click", ".delete", function () {
            var row = $(this).closest('tr');
            table.row(row).remove().draw();
        });
    };

    //
    var openPopupCreateRequestLendGoodsDemo = function () {
        popup = $("#popup-create-request-lend-goods-demo").dxPopup("instance");
        popup.show();

        var table = $('#lend-goods-demo-table').DataTable({
            destroy: true,
            paging: false,
            searching: false,
            ordering: false,
            data: [],
            columns: [
                { data: "Description" },
                { data: "GoodsID" },
                { data: "Serial" },
                { data: "Quantity" },
                { data: "Note", defaultContent: "<i>Not set</i>" },
                {
                    defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                },
            ]
        });

        $(document).on("click", "#add", function () {
            Swal.fire({
                title: 'Thêm nội dung thanh toán',
                html: `<input type="text" id="description" class="swal2-input" placeholder="Mô tả">
                                <input type="text" id="goodsid" class="swal2-input" placeholder="Mã hàng hóa">
                                <input type="text" id="serial" class="swal2-input" placeholder="Số serial">
                                <input type="text" id="quantity" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Thêm',
                focusConfirm: false,
                preConfirm: () => {
                    const description = Swal.getPopup().querySelector('#description').value;
                    const goodsid = Swal.getPopup().querySelector('#goodsid').value;
                    const serial = Swal.getPopup().querySelector('#serial').value;
                    const quantity = Swal.getPopup().querySelector('#quantity').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!goodsid) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!serial) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!quantity) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(quantity)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { description: description, goodsid: goodsid, serial: serial, quantity: quantity, note: note }
                }
            }).then((result) => {
                var row = {
                    "Description": result.value.description,
                    "GoodsID": result.value.goodsid,
                    "Serial": result.value.serial,
                    "Quantity": result.value.quantity,
                    "Note": result.value.note,
                };
                table.row.add(row).draw(false);
            })

        });


        $('#lend-goods-demo-table tbody').on("click", ".edit", function () {
            var row = $(this).closest('tr');
            Swal.fire({
                title: 'Sửa danh sách hàng hóa',
                html: `<input type="text" id="description" value="${table.row(row).data().Description}" class="swal2-input" placeholder="Mô tả">
                                <input type="text" id="goodsid" value="${table.row(row).data().GoodsID}" class="swal2-input" placeholder="Mã hàng hóa">
                                <input type="text" id="serial" value="${table.row(row).data().Serial}" class="swal2-input" placeholder="Số serial">
                                <input type="text" id="quantity" type="number" value="${table.row(row).data().Quantity}" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Sửa',
                focusConfirm: false,
                preConfirm: () => {
                    const description = Swal.getPopup().querySelector('#description').value;
                    const goodsid = Swal.getPopup().querySelector('#goodsid').value;
                    const serial = Swal.getPopup().querySelector('#serial').value;
                    const quantity = Swal.getPopup().querySelector('#quantity').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!goodsid) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!serial) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!quantity) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(quantity)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { description: description, goodsid: goodsid, serial: serial, quantity: quantity, note: note }
                }
            }).then((result) => {
                var rowData = {
                    "Description": result.value.description,
                    "GoodsID": result.value.goodsid,
                    "Serial": result.value.serial,
                    "Quantity": result.value.quantity,
                    "Note": result.value.note,
                };
                table.row(row.attr('rowindex', row.index())).data(rowData).draw();
            })
        });


        $('#lend-goods-demo-table tbody').on("click", ".delete", function () {
            var row = $(this).closest('tr');
            table.row(row).remove().draw();
        });
    };

    //
    var openPopupCreateRequestBookGrab = function () {
        popup = $("#popup-create-request-book-grab").dxPopup("instance");
        popup.show();

        var table = $('#book-grab-table').DataTable({
            destroy: true,
            paging: false,
            searching: false,
            ordering: false,
            data: [],
            columns: [
                { data: "RequestContent" },
                { data: "StartAdress" },
                { data: "EndAdress" },
                { data: "ExecutionDate" },
                { data: "VehiceType", defaultContent: "<i>Not set</i>" },
                {
                    defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                },
            ]
        });

        $(document).on("click", "#add", function () {
            Swal.fire({
                title: 'Thêm nội dung thanh toán',
                html: `<input type="text" id="request-content" class="swal2-input" placeholder="Nội dung">
                                <input type="text" id="start-adress" class="swal2-input" placeholder="Địa chỉ đi">
                                <input type="text" id="end-adress" class="swal2-input" placeholder="Địa chỉ đến">
                                <input type="date" id="execution-date" class="swal2-input" placeholder="Thời gian thực hiện">
                                <select name="cars" id="vehice-type" class="swal2-select">
                                    <option value="Grab bike">Grab bike</option>
                                    <option value="Grab car">Grab car</option>
                                </select>`,
                confirmButtonText: 'Thêm',
                focusConfirm: false,
                preConfirm: () => {
                    const requestContent = Swal.getPopup().querySelector('#request-content').value;
                    const startAdress = Swal.getPopup().querySelector('#start-adress').value;
                    const endAdress = Swal.getPopup().querySelector('#end-adress').value;
                    const executionDate = Swal.getPopup().querySelector('#execution-date').value;
                    const vehiceType = Swal.getPopup().querySelector('#vehice-type').value;

                    if (!requestContent) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!startAdress) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!endAdress) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!executionDate) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!vehiceType) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    

                    return { requestContent: requestContent, startAdress: startAdress, endAdress: endAdress, executionDate: executionDate, vehiceType: vehiceType }
                }
            }).then((result) => {
                var row = {
                    "RequestContent": result.value.requestContent,
                    "StartAdress": result.value.startAdress,
                    "EndAdress": result.value.endAdress,
                    "ExecutionDate": result.value.executionDate,
                    "VehiceType": result.value.vehiceType,
                };
                table.row.add(row).draw(false);
            })

        });


        $('#book-grab-table tbody').on("click", ".edit", function () {
            var row = $(this).closest('tr');
            Swal.fire({
                title: 'Sửa danh sách hàng hóa',
                html: `<input type="text" id="request-content" value="${table.row(row).data().RequestContent}" class="swal2-input" placeholder="Nội dung">
                                <input type="text" id="start-adress" value="${table.row(row).data().StartAdress}" class="swal2-input" placeholder="Địa chỉ đi">
                                <input type="text" id="end-adress" value="${table.row(row).data().EndAdress}" class="swal2-input" placeholder="Địa chỉ đến">
                                <input type="date" id="execution-date" value="${table.row(row).data().ExecutionDate}" class="swal2-input" placeholder="Thời gian thực hiện">
                                <select name="cars" id="vehice-type" value="${table.row(row).data().VehiceType}" class="swal2-select">
                                    <option value="volvo">Volvo</option>
                                    <option value="saab">Saab</option>
                                    <option value="opel">Opel</option>
                                    <option value="audi">Audi</option>
                                </select>`,
                confirmButtonText: 'Thêm',
                focusConfirm: false,
                preConfirm: () => {
                    const requestContent = Swal.getPopup().querySelector('#request-content').value;
                    const startAdress = Swal.getPopup().querySelector('#start-adress').value;
                    const endAdress = Swal.getPopup().querySelector('#end-adress').value;
                    const executionDate = Swal.getPopup().querySelector('#execution-date').value;
                    const vehiceType = Swal.getPopup().querySelector('#vehice-type').value;

                    if (!requestContent) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!startAdress) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!endAdress) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!executionDate) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!vehiceType) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }


                    return { requestContent: requestContent, startAdress: startAdress, endAdress: endAdress, executionDate: executionDate, vehiceType: vehiceType }
                }
            }).then((result) => {
                var row = {
                    "RequestContent": result.value.requestContent,
                    "StartAdress": result.value.startAdress,
                    "EndAdress": result.value.endAdress,
                    "ExecutionDate": result.value.executionDate,
                    "VehiceType": result.value.vehiceType,
                };
                table.row(row.attr('rowindex', row.index())).data(rowData).draw();
            })
        });


        $('#book-grab-table tbody').on("click", ".delete", function () {
            var row = $(this).closest('tr');
            table.row(row).remove().draw();
        });
    };

  
    //
    var openPopupCreateRequestReimbursement = function () {
        popup = $("#popup-create-request-reimbursement").dxPopup("instance");
        popup.show();

        var table = $('#reimbursement-table').DataTable({
            destroy: true,
            paging: false,
            searching: false,
            ordering: false,
            data: [],
            //drawCallback: function () {
            //    var sum = $('#reimbursement-table').DataTable().column(1).data().sum();
            //    console.log(sum);
            //},   
            columns: [
                { data: "PaymentContent" },
                { data: "Amount" },
                { data: "DocumentsNumber", defaultContent: "<i>Not set</i>"},
                { data: "Note", defaultContent: "<i>Not set</i>" },
                {
                    defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                },
            ],
            footerCallback: function (row, data, start, end, display) {
                var api = this.api();

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                };

                // Total over all pages
                var total = api
                    .column(1)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                var pageTotal = api
                    .column(1, { page: 'current' })
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $("#total-amount").dxTextBox("instance").option("value", total);
            }
        });
       
        $(document).on("click", "#add", function () {
            Swal.fire({
                title: 'Thêm nội dung thanh toán',
                html: `<input type="text" id="paymentContent" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="number" id="amount" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Thêm',
                focusConfirm: false,
                preConfirm: () => {
                    const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                    const amount = Swal.getPopup().querySelector('#amount').value;
                    const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!paymentContent) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!amount) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(amount)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                }
            }).then((result) => {
                var row = {
                    "PaymentContent": result.value.paymentContent,
                    "Amount": parseInt(result.value.amount),
                    "DocumentsNumber": result.value.documentsNumber,
                    "Note": result.value.note,
                };
                table.row.add(row).draw(false);
            })

        });


        $('#reimbursement-table tbody').on("click", ".edit", function () {
            var row = $(this).closest('tr');
            var data = table.row(row).data().PaymentContent;
            Swal.fire({
                title: 'Sửa nội dung thanh toán',
                html: `<input type="text" id="paymentContent" value="${table.row(row).data().PaymentContent}" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" type="number" value="${table.row(row).data().Amount}" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" value="${table.row(row).data().DocumentsNumber}" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                confirmButtonText: 'Sửa',
                focusConfirm: false,
                preConfirm: () => {
                    const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                    const amount = Swal.getPopup().querySelector('#amount').value;
                    const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                    const note = Swal.getPopup().querySelector('#note').value;

                    if (!paymentContent) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (!amount) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                        )
                    }
                    if (isNaN(amount)) {
                        Swal.showValidationMessage(
                            '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                        )
                    }

                    return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                }
            }).then((result) => {
                var rowData = {
                    "PaymentContent": result.value.paymentContent,
                    "Amount": parseInt(result.value.amount),
                    "DocumentsNumber": result.value.documentsNumber,
                    "Note": result.value.note,
                };
                table.row(row.attr('rowindex', row.index())).data(rowData).draw();
            })
        });


        $('#reimbursement-table tbody').on("click", ".delete", function () {
            var row = $(this).closest('tr');
            table.row(row).remove().draw();
        });
    };

    this.selectFirstRow = function (e) {
        var listitems = e.element.find('.dx-item');
        var tooltip = $('#tooltip').dxTooltip().dxTooltip('instance');
        listitems.on('dxhoverstart', function (args) {
            tooltip.content().text($(this).data().dxListItemData.RequestTitle);
            tooltip.show(args.target);
        });

        listitems.on('dxhoverend', function () {
            tooltip.hide();
        });




        $.getJSON("/api/RequestApi/GetLastRequestApproving",
            function (result) {
                if (result.data.length==0) {
                    document.getElementById("content-detail").hidden = true;
                    document.getElementsByClassName("no-request")[0].hidden = false;
                } else {
                    currentReport = result.data[0];
                    document.getElementById("content-detail").hidden = false;
                    document.getElementsByClassName("no-request")[0].hidden = true;
                    updateRequestPanel();
                }
            });


    };

    this.itemClick = function (e) {
        var itemData = e.itemData;
        var itemElement = e.itemElement;
        var itemIndex = e.itemIndex;
        if (!itemData) return;
        currentReport = itemData;

        document.getElementById("content-detail").hidden = false;
        document.getElementsByClassName("no-request")[0].hidden =true;

        updateRequestPanel();

        $("#drawer2").dxDrawer("instance").option("opened", false);
    };

    var updateRequestPanel = function () {
        if (currentReport.ProcessingTime < new Date()) {
            document.getElementById("overdue-image").hidden = false;
            $("#accept-button").dxButton("instance").option("disabled", true);
            $("#fw-button").dxButton("instance").option("disabled", true);
            $("#refuse-button").dxButton("instance").option("disabled", true);
        }

        if (currentReport.Approvers.split(",").slice(-1).pop() !== $("#current-name").text()) {
            $("#accept-button").dxButton("instance").option("disabled", true);
            $("#fw-button").dxButton("instance").option("disabled", true);
            $("#refuse-button").dxButton("instance").option("disabled", true);
        }
        else {
            $("#accept-button").dxButton("instance").option("disabled", false);
            $("#fw-button").dxButton("instance").option("disabled", false);
            $("#refuse-button").dxButton("instance").option("disabled", false);
        }

        $("#request-id").text(currentReport.RequestID);
        $("#request-type").text(currentReport.RequestType);
        $("#request-title").text(currentReport.RequestTitle);
        $("#create-date").text(formatDate(new Date(currentReport.CreateDate)));
        $("#create-name").text(currentReport.CreateName);
        $("#create-email").text(currentReport.CreateEmail);
        $("#create-department").text(currentReport.CreateDepartment);
        $("#status").text(currentReport.Status);
        $("#processing-time").text(formatDate(new Date(currentReport.ProcessingTime)));
        $("#remaining-time").text(dateDiff(new Date(currentReport.ProcessingTime)));

        const dataApprovers = currentReport.Approvers.split(',').reverse();
        const listApprovers = document.getElementById('list-approvers');
        listApprovers.innerHTML = dataApprovers.map(i => `<li>${i}</li>`).join('');

        const dataFollowers = currentReport.Followers.split(',').reverse();
        const listFollowers = document.getElementById('list-followers');
        listFollowers.innerHTML = dataFollowers.map(i => `<li>${i}</li>`).join('');

        if (currentReport.RequestType === "Đề nghị thanh toán") {
            document.getElementsByClassName("request-payment")[0].hidden = false;
            document.getElementsByClassName("request-supplies")[0].hidden =true;
            document.getElementsByClassName("request-lend-goods-demo")[0].hidden = true;
            document.getElementsByClassName("request-book-grab")[0].hidden = true;
            document.getElementsByClassName("request-reimbursement")[0].hidden = true;
            loadRequestPayment();
        } 
        else if (currentReport.RequestType === "Đề xuất vật tư") {
            document.getElementsByClassName("request-payment")[0].hidden =true;
            document.getElementsByClassName("request-supplies")[0].hidden = false;
            document.getElementsByClassName("request-lend-goods-demo")[0].hidden = true;
            document.getElementsByClassName("request-book-grab")[0].hidden = true;
            document.getElementsByClassName("request-reimbursement")[0].hidden = true;
            loadRequestSupplies();
        } 
        else if (currentReport.RequestType === "Đề xuất mượn hàng demo") {
            document.getElementsByClassName("request-payment")[0].hidden =true;
            document.getElementsByClassName("request-supplies")[0].hidden =true;
            document.getElementsByClassName("request-lend-goods-demo")[0].hidden = false;
            document.getElementsByClassName("request-book-grab")[0].hidden = true;
            document.getElementsByClassName("request-reimbursement")[0].hidden = true;
            loadRequestLendGoodsDemo();
        } 
        else if (currentReport.RequestType === "Đề xuất book grab") {
            document.getElementsByClassName("request-payment")[0].hidden = true;
            document.getElementsByClassName("request-supplies")[0].hidden = true;
            document.getElementsByClassName("request-lend-goods-demo")[0].hidden = true;
            document.getElementsByClassName("request-book-grab")[0].hidden = false;
            document.getElementsByClassName("request-reimbursement")[0].hidden = true;
            loadRequestBookGrab();
        } 
        else if (currentReport.RequestType === "Đề nghị hoàn ứng") {
            document.getElementsByClassName("request-payment")[0].hidden = true;
            document.getElementsByClassName("request-supplies")[0].hidden = true;
            document.getElementsByClassName("request-lend-goods-demo")[0].hidden = true;
            document.getElementsByClassName("request-book-grab")[0].hidden =true;
            document.getElementsByClassName("request-reimbursement")[0].hidden = false;
            loadRequestReimbursement();
        } 
    };



    var loadRequestPayment = function () {
        $.getJSON("/api/RequestPaymentApi/GetByID?id=" + currentReport.RequestID,
            function (result) {
                $("#request-number").text(result.data[0].RequestNumber);
                $("#currency").text(result.data[0].Currency);
                $("#amount").text(result.data[0].Amount);
                $("#payment").text(result.data[0].Payment);
                $("#time-of-payment").text(result.data[0].TimeOfPayment);
                $("#customer-name").text(result.data[0].CustomerName);
                $("#beneficiary-account").text(result.data[0].BeneficiaryAccount);
                $("#beneficiary").text(result.data[0].Beneficiary);
                $("#bank-at").text(result.data[0].BankAt);
                $("#trading-department").text(result.data[0].TradingDepartment);

                var source = new DevExpress.data.CustomStore({
                    key: "RequestID",
                    load: function () {
                        return result.data;
                    }
                });

                $("#gridContainer-payment-detail").dxDataGrid("instance").option("dataSource", source);
            });
    };

    var loadRequestSupplies = function () {
        $.getJSON("/api/RequestSuppliesApi/GetByID?id=" + currentReport.RequestID,
            function (result) {
                $("#request-content").text(result.data[0].RequestContent);
                $("#reason").text(result.data[0].Reason);

                var source = new DevExpress.data.CustomStore({
                    key: "RequestID",
                    load: function () {
                        return result.data;
                    }
                });

                $("#gridContainer-supplies-detail").dxDataGrid("instance").option("dataSource", source);

            });
    };

    var loadRequestLendGoodsDemo = function () {
        $.getJSON("/api/RequestLendGoodsDemoApi/GetByID?id=" + currentReport.RequestID,
            function (result) {
                $("#lend-time").text(formatDate(new Date(result.data[0].LendTime)));
                $("#reason").text(result.data[0].Reason);

                var source = new DevExpress.data.CustomStore({
                    key: "RequestID",
                    load: function () {
                        return result.data;
                    }
                });

                $("#gridContainer-goods-demo").dxDataGrid("instance").option("dataSource", source);

               
            });
    };

    var loadRequestBookGrab = function () {
        $.getJSON("/api/RequestBookGrabApi/GetByID?id=" + currentReport.RequestID,
            function (result) {
                $("#book-type").text(result.data[0].BookType);

                var source = new DevExpress.data.CustomStore({
                    key: "RequestID",
                    load: function () {
                        return result.data;
                    }
                });

                $("#gridContainer-book-grab-detail").dxDataGrid("instance").option("dataSource", source);


            });
    };

    var loadRequestReimbursement  = function () {
        $.getJSON("/api/RequestReimbursementApi/GetByID?id=" + currentReport.RequestID,
            function (result) {
                $("#request-number").text(result.data[0].RequestNumber);
                $("#advance-amount").text(result.data[0].AdvanceAmount);
                $("#remaining-amount").text(result.data[0].RemainingAmount);
                $("#total-amount").text(result.data[0].TotalAmount);
                $("#excess-cash").text(result.data[0].TotalAmount - result.data[0].AdvanceAmount - result.data[0].ExcessCash);
                $("#currency").text(result.data[0].Currency);
                $("#payment").text(result.data[0].Payment);
                $("#time-of-payment").text(formatDate(new Date(result.data[0].TimeOfPayment)));
                $("#customer-name").text(result.data[0].CustomerName);
                $("#beneficiary").text(result.data[0].Beneficiary);
                $("#beneficiary-account").text(result.data[0].BeneficiaryAccount);
                $("#bank-at").text(result.data[0].BankAt);
                $("#trading-department").text(result.data[0].TradingDepartment);
                $("#accounting-payment").text(result.data[0].AccountingPayment);
                $("#chief-accountant").text(result.data[0].ChiefAccountant);
                $("#inspection-staff").text(result.data[0].InspectionStaff);
                $("#line-manager").text(result.data[0].LineManager);
                var source = new DevExpress.data.CustomStore({
                    key: "RequestID",
                    load: function () {
                        return result.data;
                    }
                });

                $("#gridContainer-reimbursement-detail").dxDataGrid("instance").option("dataSource", source);
            });
    };

    this.approve = function () {
        Swal.fire({
            title: 'Alert?',
            text: "Bạn chấp nhận duyệt đề xuất này?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Vâng, duyệt!',
            cancelButtonText:'Không'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/api/RequestApi/Approve?id=" + currentReport.RequestID,
                    type: 'PUT',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: {},
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Đã duyệt tờ trình đề xuất ' + currentReport.RequestID,
                            showConfirmButton: false,
                            timer: 2000
                        })
                        updateRequestPanel();
                        listWaiting.reload();
                        listApproving.reload();
                        listWaiting.repaint();
                        listApproving.repaint();
                        tabPanel.option("itemTitleTemplate", tabTitleTemplate2);
                    },
                    error: function (xhr) {
                        DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                    }
                });
            }
        })
    };
    this.openPopupForward = function () {
        const popup = $("#popup-choose-approver").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-choose-approver-template"),
        });
        popup.show();
    };

    this.forward = function () {
        var approvers = $("#approver-fw").dxSelectBox("instance").option("value");
        $.getJSON("/api/RequestApi/GetByID?id=" + currentReport.RequestID,
            function (result) {
                if (result.data[0].Approvers.includes(approvers)) {
                    DevExpress.ui.notify('Chọn người đề xuất khác', 'error', 2000);
                } else {
                    Swal.fire({
                        title: 'Alert?',
                        text: "Bạn có chấp nhận chuyển tiếp đề xuất này?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Vâng, chuyển tiếp!',
                        cancelButtonText: 'Không'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "api/RequestApi/Forward?id=" + currentReport.RequestID + "&approver=" + approvers,
                                type: 'PUT',
                                contentType: 'application/json; charset=utf-8',
                                dataType: 'json',
                                data: {},
                                success: function (data) {
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Tờ trình đề xuất ' + currentReport.RequestID + " đã được chuyển tiếp đến " + approvers,
                                        showConfirmButton: false,
                                        timer: 2000
                                    })
                                    const popup = $("#popup-choose-approver").dxPopup("instance");
                                    popup.hide();
                                    updateRequestPanel();
                                    listWaiting.reload();
                                    listApproving.reload();
                                    listWaiting.repaint();
                                    listApproving.repaint();
                                    tabPanel.option("itemTitleTemplate", tabTitleTemplate2);
                                    
                                },
                                error: function (xhr) {
                                    DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                                }
                            });
                        }
                    })
                }
            })
    };

    this.refuse = function () {
        Swal.fire({
            title: 'Alert?',
            text: "Bạn chắc chắn từ chối đề xuất này?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Từ chối!',
            cancelButtonText: 'Không'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "/api/RequestApi/Refuse?id=" + currentReport.RequestID,
                    type: 'PUT',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    data: {},
                    success: function (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warming',
                            title: 'Tờ trình đề xuất ' + currentReport.RequestID + " đã bị từ chối",
                            showConfirmButton: false,
                            timer: 2000
                        })
                        updateRequestPanel();
                        listWaiting.reload();
                        listApproving.reload();
                        listWaiting.repaint();
                        listApproving.repaint();
                        tabPanel.option("itemTitleTemplate", tabTitleTemplate2);
                    },
                    error: function (xhr) {
                        DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                    }
                });
            }
        })
        
    };

    this.openPreviewPopup = function () {
        
    };

    this.prevItem = function () {

    };

    this.nextItem = function () {

    };

    var dateDiff = function (secondDate) {
        var diffInDays = Math.floor((secondDate - new Date()) / (24 * 60 * 60 * 1000));
        var diffInHrs = Math.floor(Math.abs(((secondDate - new Date()) % 86400000) / 3600000));
        var diffInMins = Math.floor(Math.abs((((secondDate - new Date()) % 86400000) % 3600000) / 60000));
        var diffInSecs = Math.floor(Math.abs(((((secondDate - new Date()) % 86400000) % 3600000) % 60000) / 1000));
        var remainingTime = diffInDays + "d:" + diffInHrs + "h:" + diffInMins + "m:" + diffInSecs + "s";
        return remainingTime;
    };


    this.init = function () {
        tabPanel = $("#tab-panel").dxTabPanel("instance");
        listWaiting = $("#list").dxList("instance");
        listApproving = $("#list2").dxList("instance");
    };
}

DevAV.reportApproves = new DevAV.ReportApproves();

$(function () {
    DevAV.reportApproves.init();
});