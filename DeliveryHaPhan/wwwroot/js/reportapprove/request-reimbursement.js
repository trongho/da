﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestReimbursements = function () {
    var grid;
    var formSelector = "#request-form";
    var requestid;
    var requesttitle, requesttype, createname, createemail, createdepartment, createdate, processingtime, approvers, followers;
    var requestnumber, advanceamount, remainingamount,excesscash,totalamount,payment,currency, customername, timeofpayment, beneficiary, beneficiaryaccount,
        bankat, tradingdepartment, accountingpayment, chiefaccountant, inspectionstaff, linemanager;

    var paymentcontent, amount, documentsnumber, note;

    this.createNewRequest = function () {
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = new Date();
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        requestnumber = $("#request-number").dxTextBox("instance").option("value");
        advanceamount = $("#advance-amount").dxTextBox("instance").option("value");
        remainingamount = $("#remaining-amount").dxTextBox("instance").option("value");
        totalamount = $("#total-amount").dxTextBox("instance").option("value");
        
        payment = $("#payment").dxSelectBox("instance").option("value");
        currency = $("#currency").dxSelectBox("instance").option("value");
        customername = $("#customer-name").dxTextBox("instance").option("value");
        timeofpayment = $("#time-of-payment").dxDateBox("instance").option("value");
        beneficiary = $("#beneficiary").dxTextBox("instance").option("value");
        beneficiaryaccount = $("#beneficiary-account").dxTextBox("instance").option("value");
        bankat = $("#bank-at").dxTextBox("instance").option("value");
        tradingdepartment = $("#trading-department").dxTextBox("instance").option("value");
        accountingpayment = $("#accounting-payment").dxSelectBox("instance").option("value");
        chiefaccountant = $("#chief-accountant").dxSelectBox("instance").option("value");
        inspectionstaff = $("#inspection-staff").dxSelectBox("instance").option("value");
        linemanager = $("#line-manager").dxSelectBox("instance").option("value");
        if (!DevAV.isFormValid(formSelector)) return;

        createRequest();

    };

    var createRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Create",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                createRequestReimbursement(data.RequestID);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var createRequestReimbursement = function (requestID) {
        var table = $('#reimbursement-table').DataTable();
        var ordinal = 0;
        excesscash = 0;
        excesscash = totalamount - advanceamount - remainingamount;
        

        

        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var d = this.data();
            paymentcontent = d.PaymentContent;
            amount = d.Amount;
            documentsnumber = d.DocumentsNumber;
            note = d.Note;
            ordinal += 1;
            
            $.ajax({
                url: "/api/RequestReimbursementApi/Create",
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    RequestID: requestID,
                    Ordinal: ordinal,
                    RequestNumber: requestnumber,
                    AdvanceAmount: advanceamount,
                    RemainingAmount: remainingamount,
                    ExcessCash: excesscash,
                    TotalAmount: totalamount,
                    Payment: payment,
                    Currency: currency,
                    TimeOfPayment: timeofpayment,
                    CustomerName: customername,
                    Beneficiary: beneficiary,
                    BeneficiaryAccount: beneficiaryaccount,
                    BankAt: bankat,
                    TradingDepartment: tradingdepartment,
                    AccountingPayment: accountingpayment,
                    ChiefAccountant: chiefaccountant,
                    InspectionStaff: inspectionstaff,
                    LineManager: linemanager,
                    PaymentContent: paymentcontent,
                    Amount: amount,
                    DocumentsNumber: documentsnumber,
                    Note:note,
                }),
                beforeSend: function (request) {
                    request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                },
                success: function (data) {
                    const popup = $("#popup-create-request-reimbursement").dxPopup("instance");
                    popup.hide();
                    DevExpress.ui.notify('Tạo thành công đề xuất ' + requestID, 'success', 4000);
                },
                error: function (xhr) {
                    DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                }
            });
        });
    };

    this.editRequest = function () {
        requestid = $("#request-id").dxTextBox("instance").option("value");;
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = $("#create-date").dxDateBox("instance").option("value");
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        requestnumber = $("#request-number").dxTextBox("instance").option("value");
        advanceamount = $("#advance-amount").dxTextBox("instance").option("value");
        remainingamount = $("#remaining-amount").dxTextBox("instance").option("value");
        totalamount = $("#total-amount").dxTextBox("instance").option("value");
        payment = $("#payment").dxSelectBox("instance").option("value");
        currency = $("#currency").dxSelectBox("instance").option("value");
        customername = $("#customer-name").dxTextBox("instance").option("value");
        timeofpayment = $("#time-of-payment").dxDateBox("instance").option("value");
        beneficiary = $("#beneficiary").dxTextBox("instance").option("value");
        beneficiaryaccount = $("#beneficiary-account").dxTextBox("instance").option("value");
        bankat = $("#bank-at").dxTextBox("instance").option("value");
        tradingdepartment = $("#trading-department").dxTextBox("instance").option("value");
        accountingpayment = $("#accounting-payment").dxSelectBox("instance").option("value");
        chiefaccountant = $("#chief-accountant").dxSelectBox("instance").option("value");
        inspectionstaff = $("#inspection-staff").dxSelectBox("instance").option("value");
        linemanager = $("#line-manager").dxSelectBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        editRequest2();
    };

    var editRequest2 = function () {
        $.ajax({
            url: "/api/RequestApi/Update?id=" + requestid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestID: requestid,
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                editRequestReimbursement();
                const popup = $("#popup-edit-request-reimbursement").dxPopup("instance");
                popup.hide();
                DevExpress.ui.notify('Cập nhật thành công đề xuất ' + requestid, 'success', 4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var editRequestReimbursement = function () {
        $.ajax({
            url: "/api/RequestReimbursementApi/DeleteRequest?id=" + requestid,
            type: 'DELETE',
            success: function (data) {
                var table = $('#reimbursement-table').DataTable();
                var ordinal = 0;
                
                excesscash = totalamount - advanceamount - remainingamount;

                table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var d = this.data();
                    paymentcontent = d.PaymentContent;
                    amount = d.Amount;
                    documentsnumber = d.DocumentsNumber;
                    note = d.Note;
                    ordinal += 1;

                    $.ajax({
                        url: "/api/RequestReimbursementApi/Create",
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify({
                            RequestID: requestid,
                            Ordinal: ordinal,
                            RequestNumber: requestnumber,
                            AdvanceAmount: advanceamount,
                            RemainingAmount: remainingamount,
                            ExcessCash: excesscash,
                            TotalAmount: totalamount,
                            Payment: payment,
                            Currency: currency,
                            TimeOfPayment: timeofpayment,
                            CustomerName: customername,
                            Beneficiary: beneficiary,
                            BeneficiaryAccount: beneficiaryaccount,
                            BankAt: bankat,
                            TradingDepartment: tradingdepartment,
                            AccountingPayment: accountingpayment,
                            ChiefAccountant: chiefaccountant,
                            InspectionStaff: inspectionstaff,
                            LineManager: linemanager,
                            PaymentContent: paymentcontent,
                            Amount: amount,
                            DocumentsNumber: documentsnumber,
                            Note: note,
                        }),
                        beforeSend: function (request) {
                            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                        },
                        success: function (data) {

                        },
                        error: function (xhr) {
                            DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                        }
                    });
                });
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    this.deleteRequest = function (e) {
        let timerInterval
        Swal.fire({
            title: 'Bạn có muốn xóa đề xuất này',
            toast: true,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa!',
            cancelButtonText: 'Không!',
            html: '<strong></strong> seconds<br/><br/>',
            timer: 10000,
            didOpen: () => {
                const content = Swal.getHtmlContainer()
                const $ = content.querySelector.bind(content)

                timerInterval = setInterval(() => {
                    Swal.getHtmlContainer().querySelector('strong')
                        .textContent = (Swal.getTimerLeft() / 1000)
                            .toFixed(0)
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.isConfirmed) {
                if (e.row.data.Status == "Waiting") {
                    deleteRequest(e.row.data.RequestID);
                }
                else {
                    Swal.fire('Alert!!!', 'Không thể xóa đề xuất', 'error', 2000);
                }

            }
        })
    };

    var deleteRequest = function (requestID) {
        $.ajax({
            url: "/api/RequestApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                deleteRequestReimbursement(requestID);
            },
            error: function (error) {
                Swal.fire('Error!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestReimbursement = function (requestID) {
        $.ajax({
            url: "/api/RequestReimbursementApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                grid.refresh();
                Swal.fire('Success!', 'Xóa thành công đề xuất ' + requestID, 'success', 2000);
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.requestReimbursements = new DevAV.RequestReimbursements();

$(function () {
    DevAV.requestReimbursements.init();
});