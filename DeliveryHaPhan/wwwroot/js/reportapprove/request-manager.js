﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestManagers = function () {
    var formSelector = "#report-ct-form";
    var grid;
    

    this.onRowPrepared = function (e) {
        if (e.rowType == 'data' && e.data.Status == "Approved") {
            e.rowElement[0].style.color = 'red';
        }
        else if (e.rowType == 'data' && e.data.Status == "Refuse") {
            e.rowElement[0].style.backgroundColor = 'red';
            e.rowElement[0].style.color = 'white';
        }
    };

    this.selectBox_requestType_valueChanged = function (data) {
        if (data.value == "Tất cả")
            grid.clearFilter();
        else
            grid.filter(["RequestType", "=", data.value]);
    };

    this.selectBox_dates_valueChanged = function (data) {
        if (data.value == "Tất cả") {
            grid.clearFilter();
        } else if (data.value == "Còn hạn") {
            grid.filter(["ProcessingTime", ">", new Date()]);
        } else if (data.value == "Sắp hết hạn") {
            grid.filter([Math.floor(((new Date("ProcessingTime") - new Date()) % 86400000) / 3600000), "<", 1]);
        } else if (data.value == "Quá hạn") {
            grid.filter(["ProcessingTime", "<", new Date()]);
        }
    };

    this.filterDataByText = function (searchData) {
        grid.searchByText(searchData.value);
    };


    

    
    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.requestManagers = new DevAV.RequestManagers();

$(function () {
    DevAV.requestManagers.init();
});