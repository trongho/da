﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.MyReports = function () {
    var formSelector = "#request-form";
    var status, requestType;
    var grid;
    var popup;

    this.onRowPrepared = function (e) {
        
        if (e.rowType == 'data' && e.data.Status =="Approved") {
            e.rowElement[0].style.color = 'red';
        }
        else if (e.rowType == 'data' && e.data.Status == "Refuse") {
            e.rowElement[0].style.backgroundColor = 'red';
            e.rowElement[0].style.color = 'white';
        }

        if (e.rowType == 'data' && (e.data.Status == "Waiting" || e.data.Status == "Forwarding") && new Date(e.data.ProcessingTime) < new Date()) {
            e.rowElement[0].style.color = '#9f9f9f';
        }
    };

    this.openPopupEdit = function (e) {
        var status = e.row.data.Status;
        var requestType = e.row.data.RequestType;
        var requestID = e.row.data.RequestID;

        if (status === "Approved") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Đơn hàng đã duyệt xong, không thể chỉnh sửa!',
                timer: 2000,
            })
        }
        else if (status === "Forwarding") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Đơn hàng đã chuyển tiếp, không thể chỉnh sửa!',
                timer: 2000,
            })
        }
        else {
            if (requestType === "Đề nghị thanh toán") {
                popup = $("#popup-edit-request-payment").dxPopup("instance");
                popup.show();
                fetchRequest(e.row.data);
                fetchRequestPayment(e.row.data);
            }
            else if (requestType === "Đề xuất vật tư") {
                popup = $("#popup-edit-request-supplies").dxPopup("instance");
                popup.show();
                fetchRequest(e.row.data);
                fetchRequestSupplies(e.row.data);
            }
            else if (requestType === "Đề xuất mượn hàng demo") {
                popup = $("#popup-edit-request-lend-goods-demo").dxPopup("instance");
                popup.show();
                fetchRequest(e.row.data);
                fetchRequestLendGoodsDemo(e.row.data);
            }
            else if (requestType === "Đề xuất book grab") {
                popup = $("#popup-edit-request-book grab").dxPopup("instance");
                popup.show();
                fetchRequest(e.row.data);
                fetchRequestBookGrab(e.row.data);
            }
            else if (requestType === "Đề nghị hoàn ứng") {
                popup = $("#popup-edit-request-reimbursement").dxPopup("instance");
                popup.show();
                fetchRequest(e.row.data);
                fetchRequestReimbursement(e.row.data);
            }
        }
    };

     var fetchRequestPayment = function (data) {
         $.getJSON("/api/RequestPaymentApi/GetByID?id=" + data.RequestID,
             function (result) {
                 $("#request-number").dxTextBox("instance").option("value", result.data[0].RequestNumber);
                 $("#currency").dxSelectBox("instance").option("value", result.data[0].Currency);
                 $("#amount").dxTextBox("instance").option("value", result.data[0].Amount);
                 $("#payment").dxSelectBox("instance").option("value", result.data[0].Payment);
                 $("#time-of-payment").dxDateBox("instance").option("value", result.data[0].TimeOfPayment);
                 $("#customer-name").dxTextBox("instance").option("value", result.data[0].CustomerName);
                 $("#beneficiary-account").dxTextBox("instance").option("value", result.data[0].BeneficiaryAccount);
                 $("#beneficiary").dxTextBox("instance").option("value", result.data[0].Beneficiary);
                 $("#bank-at").dxTextBox("instance").option("value", result.data[0].BankAt);
                 $("#trading-department").dxTextBox("instance").option("value", result.data[0].TradingDepartment);

                 var list = []
                 if (result.data.length > 0) {
                     for (var i = 0; i < result.data.length; i++) {
                         var paymentContent = result.data[i].PaymentContent;
                         var amount = result.data[i].Amount;
                         var documentsNumber = result.data[i].DocumentsNumber;
                         var note = result.data[i].Note;
                         var obj = { "PaymentContent": paymentContent, "Amount": amount, "DocumentsNumber": documentsNumber, "Note": note };
                         list.push(obj);
                     }
                 }

                 var table = $('#payment-table').DataTable({
                     destroy: true,
                     paging: false,
                     searching: false,
                     ordering: false,
                     data: r.data,
                     columns: [
                         { data: "PaymentContent" },
                         { data: "Amount" },
                         { data: "DocumentsNumber", defaultContent: "<i>Not set</i>" },
                         { data: "Note", defaultContent: "<i>Not set</i>" },
                         {
                             defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                         },
                     ]
                 });

                 $(document).on("click", "#add", function () {
                     Swal.fire({
                         title: 'Thêm nội dung thanh toán',
                         html: `<input type="text" id="paymentContent" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                         confirmButtonText: 'Thêm',
                         focusConfirm: false,
                         preConfirm: () => {
                             const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                             const amount = Swal.getPopup().querySelector('#amount').value;
                             const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                             const note = Swal.getPopup().querySelector('#note').value;

                             if (!paymentContent) {
                                 Swal.showValidationMessage(
                                     '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                 )
                             }
                             if (!amount) {
                                 Swal.showValidationMessage(
                                     '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                 )
                             }
                             if (isNaN(amount)) {
                                 Swal.showValidationMessage(
                                     '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                 )
                             }

                             return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                         }
                     }).then((result) => {
                         var row = {
                             "PaymentContent": result.value.paymentContent,
                             "Amount": parseInt(result.value.amount),
                             "DocumentsNumber": result.value.documentsNumber,
                             "Note": result.value.note,
                         };
                         table.row.add(row).draw(false);
                     })

                 });


                 $('#payment-table tbody').on("click", ".edit", function () {
                     var row = $(this).closest('tr');
                     var data = table.row(row).data().PaymentContent;
                     Swal.fire({
                         title: 'Sửa nội dung thanh toán',
                         html: `<input type="text" id="paymentContent" value="${table.row(row).data().PaymentContent}" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" type="number" value="${table.row(row).data().Amount}" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" value="${table.row(row).data().DocumentsNumber}" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                         confirmButtonText: 'Thêm',
                         focusConfirm: false,
                         preConfirm: () => {
                             const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                             const amount = Swal.getPopup().querySelector('#amount').value;
                             const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                             const note = Swal.getPopup().querySelector('#note').value;

                             if (!paymentContent) {
                                 Swal.showValidationMessage(
                                     '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                 )
                             }
                             if (!amount) {
                                 Swal.showValidationMessage(
                                     '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                 )
                             }
                             if (isNaN(amount)) {
                                 Swal.showValidationMessage(
                                     '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                 )
                             }

                             return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                         }
                     }).then((result) => {
                         var rowData = {
                             "PaymentContent": result.value.paymentContent,
                             "Amount": parseInt(result.value.amount),
                             "DocumentsNumber": result.value.documentsNumber,
                             "Note": result.value.note,
                         };
                         table.row(row.attr('rowindex', row.index())).data(rowData).draw();
                     })
                 });


                 $('#payment-table tbody').on("click", ".delete", function () {
                     var row = $(this).closest('tr');
                     table.row(row).remove().draw();
                 });
             });
    };

    var fetchRequestSupplies = function (data) {
        $.getJSON("/api/RequestSuppliesApi/GetByID?id=" + data.RequestID,
            function (result) {
               
                $("#request-content").dxTextBox("instance").option("value", result.data[0].RequestContent);
                $("#reason").dxTextBox("instance").option("value", result.data[0].Reason);

                var list = []
                if (result.data.length > 0) {
                    for (var i = 0; i < result.data.length; i++) {
                        var goods = result.data[i].Goods;
                        var specification = result.data[i].Specification;
                        var quantity = result.data[i].Quantity;
                        var note = result.data[i].Note;
                        var obj = { "Goods": goods, "Specification": specification,"Quantity": quantity, "Note": note };
                        list.push(obj);
                    }
                }

                var table = $('#supplies-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "Goods" },
                        { data: "Specification" },
                        { data: "Quantity" },
                        { data: "Note", defaultContent: "<i>Not set</i>" },
                        {
                            defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                        },
                    ]
                });

                $(document).on("click", "#add", function () {
                    Swal.fire({
                        title: 'Thêm nội dung thanh toán',
                        html: `<input type="text" id="goods" class="swal2-input" placeholder="Hàng hóa">
                                <input type="text" id="specification" class="swal2-input" placeholder="Quy cách">
                                <input type="text" id="quantity" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const goods = Swal.getPopup().querySelector('#goods').value;
                            const specification = Swal.getPopup().querySelector('#specification').value;
                            const quantity = Swal.getPopup().querySelector('#quantity').value;
                            const note = Swal.getPopup().querySelector('#note').value;

                            if (!goods) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!specification) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!quantity) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (isNaN(quantity)) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                )
                            }

                            return { goods: goods, specification: specification, quantity: quantity, note: note }
                        }
                    }).then((result) => {
                        var row = {
                            "Goods": result.value.goods,
                            "Specification": parseInt(result.value.specification),
                            "Quantity": result.value.quantity,
                            "Note": result.value.note,
                        };
                        table.row.add(row).draw(false);
                    })

                });


                $('#supplies-table tbody').on("click", ".edit", function () {
                    var row = $(this).closest('tr');
                    Swal.fire({
                        title: 'Sửa danh sách hàng hóa',
                        html: `<input type="text" id="goods" value="${table.row(row).data().Goods}" class="swal2-input" placeholder="Hàng hóa">
                                <input type="text" id="specification" type="number" value="${table.row(row).data().Specification}" class="swal2-input" placeholder="Quy cách">
                                <input type="text" id="quantity" value="${table.row(row).data().Quantity}" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                        confirmButtonText: 'Sửa',
                        focusConfirm: false,
                        preConfirm: () => {
                            const goods = Swal.getPopup().querySelector('#goods').value;
                            const specification = Swal.getPopup().querySelector('#specification').value;
                            const quantity = Swal.getPopup().querySelector('#quantity').value;
                            const note = Swal.getPopup().querySelector('#note').value;

                            if (!goods) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!specification) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!quantity) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (isNaN(quantity)) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                )
                            }

                            return { goods: goods, specification: specification, quantity: quantity, note: note }
                        }
                    }).then((result) => {
                        var rowData = {
                            "Goods": result.value.goods,
                            "Specification": parseInt(result.value.specification),
                            "Quantity": result.value.quantity,
                            "Note": result.value.note,
                        };
                        table.row(row.attr('rowindex', row.index())).data(rowData).draw();
                    })
                });


                $('#supplies-table tbody').on("click", ".delete", function () {
                    var row = $(this).closest('tr');
                    table.row(row).remove().draw();
                });
            });
    };

    var fetchRequestLendGoodsDemo = function (data) {
        $.getJSON("/api/RequestLendGoodsDemoApi/GetByID?id=" + data.RequestID,
            function (result) {
               
                $("#lend-time").dxDateBox("instance").option("value", result.data[0].LendTime);
                $("#reason").dxTextBox("instance").option("value", result.data[0].Reason);

                var list = []
                if (result.data.length > 0) {
                    for (var i = 0; i < result.data.length; i++) {
                        var description = result.data[i].Description;
                        var goodID = result.data[i].GoodsID;
                        var serial = result.data[i].Serial;
                        var quantity = result.data[i].Quantity;
                        var note = result.data[i].Note;
                        var obj = { "Description": description, "GoodsID": goodID, "Serial": serial, "Quantity": quantity, "Note": note };
                        list.push(obj);
                    }
                }

                var table = $('#lend-goods-demo-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "Description" },
                        { data: "GoodsID" },
                        { data: "Serial" },
                        { data: "Quantity" },
                        { data: "Note", defaultContent: "<i>Not set</i>" },
                        {
                            defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                        },
                    ]
                });

                $(document).on("click", "#add", function () {
                    Swal.fire({
                        title: 'Thêm nội dung thanh toán',
                        html: `<input type="text" id="description" class="swal2-input" placeholder="Mô tả">
                                <input type="text" id="goodsid" class="swal2-input" placeholder="Mã hàng hóa">
                                <input type="text" id="serial" class="swal2-input" placeholder="Số serial">
                                <input type="text" id="quantity" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const description = Swal.getPopup().querySelector('#description').value;
                            const goodsid = Swal.getPopup().querySelector('#goodsid').value;
                            const serial = Swal.getPopup().querySelector('#serial').value;
                            const quantity = Swal.getPopup().querySelector('#quantity').value;
                            const note = Swal.getPopup().querySelector('#note').value;

                            if (!goodsid) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!serial) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!quantity) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (isNaN(quantity)) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                )
                            }

                            return { description: description, goodsid: goodsid, serial: serial, quantity: quantity, note: note }
                        }
                    }).then((r) => {
                        var row = {
                            "Description": r.value.description,
                            "GoodsID": r.value.goodsid,
                            "Serial": r.value.serial,
                            "Quantity": parseInt(r.value.quantity),
                            "Note": r.value.note,
                        };
                        table.row.add(row).draw(false);
                    })

                });


                $('#lend-goods-demo-table tbody').on("click", ".edit", function () {
                    var row = $(this).closest('tr');
                    Swal.fire({
                        title: 'Sửa danh sách hàng hóa',
                        html: `<input type="text" id="description" value="${table.row(row).data().Description}" class="swal2-input" placeholder="Mô tả">
                                <input type="text" id="goodsid" value="${table.row(row).data().GoodsID}" class="swal2-input" placeholder="Mã hàng hóa">
                                <input type="text" id="serial" value="${table.row(row).data().Serial}" class="swal2-input" placeholder="Số serial">
                                <input type="text" id="quantity" value="${table.row(row).data().Quantity}" class="swal2-input" placeholder="Số lượng">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const description = Swal.getPopup().querySelector('#description').value;
                            const goodsid = Swal.getPopup().querySelector('#goodsid').value;
                            const serial = Swal.getPopup().querySelector('#serial').value;
                            const quantity = Swal.getPopup().querySelector('#quantity').value;
                            const note = Swal.getPopup().querySelector('#note').value;

                            if (!goodsid) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!serial) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!quantity) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (isNaN(quantity)) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                )
                            }

                            return { description: description, goodsid: goodsid, serial: serial, quantity: quantity, note: note }
                        }
                    }).then((r) => {
                        var rowData = {
                            "Description": r.value.description,
                            "GoodsID": r.value.goodsid,
                            "Serial": r.value.serial,
                            "Quantity": parseInt(r.value.quantity),
                            "Note": r.value.note,
                        };
                        table.row(row.attr('rowindex', row.index())).data(rowData).draw();
                    })
                });


                $('#lend-goods-demo-table tbody').on("click", ".delete", function () {
                    var row = $(this).closest('tr');
                    table.row(row).remove().draw();
                });
            });
    };

    var fetchRequestBookGrab = function (data) {
        $.getJSON("/api/RequestBookGrabApi/GetByID?id=" + data.RequestID,
            function (result) {
               
                $("#book-type").dxSelectBox("instance").option("value", result.data[0].BookType);

                var list = []
                if (result.data.length > 0) {
                    for (var i = 0; i < result.data.length; i++) {
                        var requestContent = result.data[i].RequestContent;
                        var startAdress = result.data[i].StartAdress;
                        var endAdress = result.data[i].EndAdress;
                        var executionDate = result.data[i].ExecutionDate;
                        var vehiceType = result.data[i].VehiceType;
                        var obj = { "RequestContent": requestContent, "StartAdress": startAdress, "EndAdress": endAdress, "ExecutionDate": executionDate, "VehiceType": vehiceType };
                        list.push(obj);
                    }
                }

                var table = $('#book-grab-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "RequestContent" },
                        { data: "StartAdress" },
                        { data: "EndAdress" },
                        { data: "ExecutionDate" },
                        { data: "VehiceType", defaultContent: "<i>Not set</i>" },
                        {
                            defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                        },
                    ]
                });

                $(document).on("click", "#add", function () {
                    Swal.fire({
                        title: 'Thêm nội dung đề xuất',
                        html: `<input type="text" id="request-content" class="swal2-input" placeholder="Nội dung">
                                <input type="text" id="start-adress" class="swal2-input" placeholder="Địa chỉ đi">
                                <input type="text" id="end-adress" class="swal2-input" placeholder="Địa chỉ đến">
                                <input type="date" id="execution-date" class="swal2-input" placeholder="Thời gian thực hiện">
                                <select name="cars" id="vehice-type" class="swal2-select">
                                    <option value="Grab bike">Grab bike</option>
                                    <option value="Grab car">Grab car</option>
                                </select>`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const requestContent = Swal.getPopup().querySelector('#request-content').value;
                            const startAdress = Swal.getPopup().querySelector('#start-adress').value;
                            const endAdress = Swal.getPopup().querySelector('#end-adress').value;
                            const executionDate = Swal.getPopup().querySelector('#execution-date').value;
                            const vehiceType = Swal.getPopup().querySelector('#vehice-type').value;

                            if (!requestContent) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!startAdress) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!endAdress) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!executionDate) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!vehiceType) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }


                            return { requestContent: requestContent, startAdress: startAdress, endAdress: endAdress, executionDate: executionDate, vehiceType: vehiceType }
                        }
                    }).then((result) => {
                        var row = {
                            "RequestContent": result.value.requestContent,
                            "StartAdress": result.value.startAdress,
                            "EndAdress": result.value.endAdress,
                            "ExecutionDate": result.value.executionDate,
                            "VehiceType": result.value.vehiceType,
                        };
                        table.row.add(row).draw(false);
                    })

                });


                $('#book-grab-table tbody').on("click", ".edit", function () {
                    var row = $(this).closest('tr');
                    Swal.fire({
                        title: 'Sửa danh sách hàng hóa',
                        html: `<input type="text" id="request-content" value="${table.row(row).data().RequestContent}" class="swal2-input" placeholder="Nội dung">
                                <input type="text" id="start-adress" value="${table.row(row).data().StartAdress}" class="swal2-input" placeholder="Địa chỉ đi">
                                <input type="text" id="end-adress" value="${table.row(row).data().EndAdress}" class="swal2-input" placeholder="Địa chỉ đến">
                                <input type="date" id="execution-date" value="${table.row(row).data().ExecutionDate}" class="swal2-input" placeholder="Thời gian thực hiện">
                                <select name="cars" id="vehice-type" value="${table.row(row).data().VehiceType}" class="swal2-select">
                                    <option value="volvo">Volvo</option>
                                    <option value="saab">Saab</option>
                                    <option value="opel">Opel</option>
                                    <option value="audi">Audi</option>
                                </select>`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const requestContent = Swal.getPopup().querySelector('#request-content').value;
                            const startAdress = Swal.getPopup().querySelector('#start-adress').value;
                            const endAdress = Swal.getPopup().querySelector('#end-adress').value;
                            const executionDate = Swal.getPopup().querySelector('#execution-date').value;
                            const vehiceType = Swal.getPopup().querySelector('#vehice-type').value;

                            if (!requestContent) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!startAdress) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!endAdress) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!executionDate) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!vehiceType) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }


                            return { requestContent: requestContent, startAdress: startAdress, endAdress: endAdress, executionDate: executionDate, vehiceType: vehiceType }
                        }
                    }).then((result) => {
                        var row = {
                            "RequestContent": result.value.requestContent,
                            "StartAdress": result.value.startAdress,
                            "EndAdress": result.value.endAdress,
                            "ExecutionDate": result.value.executionDate,
                            "VehiceType": result.value.vehiceType,
                        };
                        table.row(row.attr('rowindex', row.index())).data(rowData).draw();
                    })
                });


                $('#book-grab-table tbody').on("click", ".delete", function () {
                    var row = $(this).closest('tr');
                    table.row(row).remove().draw();
                });
            });
    };

    var fetchRequest = function (data) {
        $("#request-id").dxTextBox("instance").option("value", data.RequestID);
        $("#request-title").dxTextBox("instance").option("value", data.RequestTitle);
        $("#request-type").dxSelectBox("instance").option("value", data.RequestType);
        $("#create-date").dxDateBox("instance").option("value", data.CreateDate);
        $("#create-name").dxTextBox("instance").option("value", data.CreateName);
        $("#create-email").dxTextBox("instance").option("value", data.CreateEmail);
        $("#create-department").dxTextBox("instance").option("value", data.CreateDepartment);
        $("#status").dxTextBox("instance").option("value", data.Status);
        $("#processing-time").dxDateBox("instance").option("value", data.ProcessingTime);
        $("#approvers").dxSelectBox("instance").option("value", data.Approvers.split(',').slice(-1).pop());
        $("#followers").dxTagBox("instance").option("value", data.Followers.split(','));
    };

    var fetchRequestReimbursement = function (data) {
        $.getJSON("/api/RequestReimbursementApi/GetByID?id=" + data.RequestID,
            function (result) {
                $("#request-number").dxTextBox("instance").option("value",result.data[0].RequestNumber);
                $("#advance-amount").dxTextBox("instance").option("value",result.data[0].AdvanceAmount);
                $("#remaining-amount").dxTextBox("instance").option("value",result.data[0].RemainingAmount);
                $("#total-amount").dxTextBox("instance").option("value",result.data[0].TotalAmount);
                $("#payment").dxSelectBox("instance").option("value",result.data[0].Payment);
                $("#customer-name").dxTextBox("instance").option("value",result.data[0].CustomerName);
                $("#time-of-payment").dxDateBox("instance").option("value",result.data[0].TimeOfPayment);
                $("#beneficiary-account").dxTextBox("instance").option("value",result.data[0].BeneficiaryAccount);
                $("#beneficiary").dxTextBox("instance").option("value",result.data[0].Beneficiary);
                $("#bank-at").dxTextBox("instance").option("value",result.data[0].BankAt);
                $("#trading-department").dxTextBox("instance").option("value",result.data[0].TradingDepartment);
                $("#accounting-payment").dxSelectBox("instance").option("value",result.data[0].AccountingPayment);
                $("#chief-accountant").dxSelectBox("instance").option("value",result.data[0].ChiefAccountant);
                $("#inspection-staff").dxSelectBox("instance").option("value",result.data[0].InspectionStaff);
                $("#line-manager").dxSelectBox("instance").option("value",result.data[0].LineManager);

                var list = []
                if (result.data.length > 0) {
                    for (var i = 0; i < result.data.length; i++) {
                        var paymentContent = result.data[i].PaymentContent;
                        var amount = result.data[i].Amount;
                        var documentsNumber = result.data[i].DocumentsNumber;
                        var note = result.data[i].Note;
                        var obj = { "PaymentContent": paymentContent, "Amount": amount, "DocumentsNumber": documentsNumber, "Note": note};
                        list.push(obj);
                    }
                }

                var table = $('#reimbursement-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "PaymentContent" },
                        { data: "Amount" },
                        { data: "DocumentsNumber", defaultContent: "<i>Not set</i>" },
                        { data: "Note", defaultContent: "<i>Not set</i>"},
                        {
                            defaultContent: '<input type="button" class="edit" value="Edit"/><input type="button" class="delete" value="Delete"/>'
                        },
                    ],
                    footerCallback: function (row, data, start, end, display) {
                        var api = this.api();

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
                        };

                        // Total over all pages
                        var total = api
                            .column(1)
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Total over this page
                        var pageTotal = api
                            .column(1, { page: 'current' })
                            .data()
                            .reduce(function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $("#total-amount").dxTextBox("instance").option("value", total);
                    }
                });

                $(document).on("click", "#add", function () {
                    Swal.fire({
                        title: 'Thêm nội dung thanh toán',
                        html: `<input type="text" id="paymentContent" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" class= "swal2-input" placeholder = "Ghi chú">`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                            const amount = Swal.getPopup().querySelector('#amount').value;
                            const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                            const note = Swal.getPopup().querySelector('#note').value;

                            if (!paymentContent) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!amount) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (isNaN(amount)) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                )
                            }

                            return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                        }
                    }).then((result) => {
                        var row = {
                            "PaymentContent": result.value.paymentContent,
                            "Amount": parseInt(result.value.amount),
                            "DocumentsNumber": result.value.documentsNumber,
                            "Note": result.value.note,
                        };
                        table.row.add(row).draw(false);
                    })

                });


                $('#reimbursement-table tbody').on("click", ".edit", function () {
                    var row = $(this).closest('tr');
                    var data = table.row(row).data().PaymentContent;
                    Swal.fire({
                        title: 'Sửa nội dung thanh toán',
                        html: `<input type="text" id="paymentContent" value="${table.row(row).data().PaymentContent}" class="swal2-input" placeholder="Nội dung thanh toán">
                                <input type="text" id="amount" type="number" value="${table.row(row).data().Amount}" class="swal2-input" placeholder="Số tiền">
                                <input type="text" id="documentsNumber" value="${table.row(row).data().DocumentsNumber}" class="swal2-input" placeholder="Số chứng từ">
                                <input type="text" id ="note" value="${table.row(row).data().Note}" class= "swal2-input" placeholder = "Ghi chú">`,
                        confirmButtonText: 'Thêm',
                        focusConfirm: false,
                        preConfirm: () => {
                            const paymentContent = Swal.getPopup().querySelector('#paymentContent').value;
                            const amount = Swal.getPopup().querySelector('#amount').value;
                            const documentsNumber = Swal.getPopup().querySelector('#documentsNumber').value;
                            const note = Swal.getPopup().querySelector('#note').value;

                            if (!paymentContent) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (!amount) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                                )
                            }
                            if (isNaN(amount)) {
                                Swal.showValidationMessage(
                                    '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                                )
                            }

                            return { paymentContent: paymentContent, amount: amount, documentsNumber: documentsNumber, note: note }
                        }
                    }).then((result) => {
                        var rowData = {
                            "PaymentContent": result.value.paymentContent,
                            "Amount": parseInt(result.value.amount),
                            "DocumentsNumber": result.value.documentsNumber,
                            "Note": result.value.note,
                        };
                        table.row(row.attr('rowindex', row.index())).data(rowData).draw();
                    })
                });


                $('#reimbursement-table tbody').on("click", ".delete", function () {
                    var row = $(this).closest('tr');
                    table.row(row).remove().draw();
                });
            });

    };

    this.deleteRequest = function (e) {
        let timerInterval
        Swal.fire({
            title: 'Bạn có muốn xóa đề xuất này',
            toast: true,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa!',
            cancelButtonText: 'Không!',
            html: '<strong></strong> seconds<br/><br/>',
            timer: 10000,
            didOpen: () => {
                const content = Swal.getHtmlContainer()
                const $ = content.querySelector.bind(content)

                timerInterval = setInterval(() => {
                    Swal.getHtmlContainer().querySelector('strong')
                        .textContent = (Swal.getTimerLeft() / 1000)
                            .toFixed(0)
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.isConfirmed) {
                if (e.row.data.Status == "Waiting") {
                    deleteRequest(e.row.data.RequestID);
                }
                else {
                    Swal.fire('Alert!!!', 'Không thể xóa đề xuất', 'error', 2000);
                }

            }
        })
    };

    var deleteRequest = function (data) {
        $.ajax({
            url: "/api/RequestApi/DeleteRequest?id=" + data.RequestID,
            type: 'DELETE',
            success: function (data) {
                if (data.RequestType === "Đề nghị thanh toán") {
                    deleteRequestPayment(data.RequestID);
                }
                else if (data.RequestType === "Đề nghị hoàn ứng") {
                    deleteRequestReimbursement(data.RequestID);
                }
                else if (data.RequestType === "Đề xuất vật tư") {
                    deleteRequestSupplies(data.RequestID);
                }
                else if (data.RequestType === "Đề xuất mượn hàng demo") {
                    deleteRequestLendGoodsDemo(data.RequestID);
                }
                else if (data.RequestType === "Đề xuất book grab") {
                    deleteRequestBookGrab(data.RequestID);
                }

            },
            error: function (error) {
                Swal.fire('Error!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestPayment = function (requestID) {
        $.ajax({
            url: "/api/RequestPaymentApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                Swal.fire('Success!', 'Xóa thành công', 'success', 2000);
                grid.refresh();
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestSupplies = function (requestID) {
        $.ajax({
            url: "/api/RequestSuppliesApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                Swal.fire('Success!', 'Xóa thành công', 'success', 2000);
                grid.refresh();
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestLendGoodsDemo = function (requestID) {
        $.ajax({
            url: "/api/RequestLendGoodsDemoApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                grid.refresh();
                Swal.fire('Success!', 'Xóa thành công đề xuất ' + requestID, 'success', 2000);
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestBookGrab = function (requestID) {
        $.ajax({
            url: "/api/RequestBookGrabApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                grid.refresh();
                Swal.fire('Success!', 'Xóa thành công đề xuất ' + requestID, 'success', 2000);
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestReimbursement = function (requestID) {
        $.ajax({
            url: "/api/RequestReimbursementApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                grid.refresh();
                Swal.fire('Success!', 'Xóa thành công đề xuất ' + requestID, 'success', 2000);
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    this.selectBox_requestType_valueChanged = function (data) {
        if (data.value == "Tất cả")
            grid.clearFilter();
        else
            grid.filter(["RequestType", "=", data.value]);
    };

    this.selectBox_dates_valueChanged = function (data) {
        if (data.value == "Tất cả") {
            grid.clearFilter();
        }else if (data.value == "Còn hạn") {
            grid.filter(["ProcessingTime", ">", new Date()]);
        } else if (data.value == "Sắp hết hạn") {
            grid.filter([Math.floor(((new Date("ProcessingTime") - new Date()) % 86400000) / 3600000), "<",1]);
        } else if (data.value == "Quá hạn") {
            grid.filter(["ProcessingTime", "<", new Date()]);
        }
    };

    this.filterDataByText = function (searchData) {
        grid.searchByText(searchData.value);
    };

    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.myReports = new DevAV.MyReports();

$(function () {
    DevAV.myReports.init();
});