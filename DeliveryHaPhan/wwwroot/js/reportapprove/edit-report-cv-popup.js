﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.EditReportCVPopups = function () {
    var formSelector = "#report-cv-form";

    this.editRequest = function () {
        var requestid = $("#request-id-edit-cv").dxTextBox("instance").option("value");
        var requesttitle = $("#request-title-edit-cv").dxTextBox("instance").option("value");;
        var requesttype = $("#request-type-edit-cv").dxSelectBox("instance").option("value").toString();
        var customername = $("#customer-name-edit-cv").dxTextBox("instance").option("value");
        var bankat = $("#bank-at-edit-cv").dxTextBox("instance").option("value");
        var beneficiaryaccount = $("#beneficiary-account-edit-cv").dxTextBox("instance").option("value");
        var beneficiary = $("#beneficiary-edit-cv").dxTextBox("instance").option("value").toString().toString();
        var paymentplace = $("#payment-place-edit-cv").dxTextBox("instance").option("value");
        var accompanyingdocuments = $("#accompanying-documents-edit-cv").dxTextBox("instance").option("value");
        var amount = parseFloat($("#amount-edit-cv").dxTextBox("instance").option("value"));
        var requestcontent = $("#request-content-edit-cv").dxTextBox("instance").option("value");
        var payment = $("#payment-edit-cv").dxSelectBox("instance").option("value").toString();
        var approvers = $("#approvers-edit-cv").dxSelectBox("instance").option("value");
        var followers = $("#followers-edit-cv").dxTagBox("instance").option("value").toString();
        var createname = $("#create-name-edit-cv").dxTextBox("instance").option("value");
        var createemail = $("#create-email-edit-cv").dxTextBox("instance").option("value");
        var createdepartment = $("#create-department-edit-cv").dxTextBox("instance").option("value");
        var createdate = $("#create-date-edit-cv").dxDateBox("instance").option("value");
        var processingtime = $("#processing-time-edit-cv").dxDateBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        $.ajax({
            url: "/api/RequestApi/Update?id=" + requestid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestID: requestid,
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                CustomerName: customername,
                BankAt: bankat,
                BeneficiaryAccount: beneficiaryaccount,
                Beneficiary: beneficiary,
                PaymentPlace: paymentplace,
                AccompanyingDocuments: accompanyingdocuments,
                Amount: amount,
                RequestContent: requestcontent,
                Payment: payment,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting",
                ProcessingTime: processingtime,
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                const popup = $("#popup-edit-report-cv").dxPopup("instance");
                popup.hide();
                DevExpress.ui.notify('Tờ trình đề xuất' + requestid + "đã chỉnh sửa", 'success', 4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };


    this.init = function () {

    };
}

DevAV.editReportCVPopups = new DevAV.EditReportCVPopups();

$(function () {
    DevAV.editReportCVPopups.init();
});