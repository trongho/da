﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestBookGrabs = function () {
    var grid;
    var formSelector = "#request-form";
    var requestid;
    var requesttitle, requesttype, createname, createemail, createdepartment, createdate, processingtime, approvers, followers;
    var booktype;
    var requestcontent, startadress, endadress, executiondate, vehicetype;

    this.createNewRequest = function () {
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = new Date();
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        booktype = $("#book-type").dxSelectBox("instance").option("value");


        if (!DevAV.isFormValid(formSelector)) return;

        createRequest();

    };

    var createRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Create",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                createRequestBookGrab(data.RequestID);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var createRequestBookGrab = function (requestID) {
        var table = $('#book-grab-table').DataTable();
        var ordinal = 0;

        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var d = this.data();
            requestcontent = d.RequestContent;
            startadress = d.StartAdress;
            endadress = d.EndAdress;
            executiondate = d.ExecutionDate;
            vehicetype = d.VehiceType;
            ordinal += 1;

            $.ajax({
                url: "/api/RequestBookGrabApi/Create",
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    RequestID: requestID,
                    Ordinal: ordinal,
                    BookType: booktype,
                    RequestContent: requestcontent,
                    StartAdress: startadress,
                    EndAdress: endadress,
                    ExecutionDate: executiondate,
                    VehiceType: vehicetype
                }),
                beforeSend: function (request) {
                    request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                },
                success: function (data) {
                    const popup = $("#popup-create-request-book-grab").dxPopup("instance");
                    popup.hide();
                    DevExpress.ui.notify('Tạo thành công đề xuất ' + requestID, 'success', 4000);
                },
                error: function (xhr) {
                    DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                }
            });
        });
    };

    this.editRequest = function () {
        requestid = $("#request-id").dxTextBox("instance").option("value");;
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = $("#create-date").dxDateBox("instance").option("value");
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        booktype = $("#book-type").dxSelectBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        editRequest();
    };

    var editRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Update?id=" + requestid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestID: requestid,
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                editRequestBookGrab();
                const popup = $("#popup-edit-request-book-grab").dxPopup("instance");
                popup.hide();
                grid.refresh();
                DevExpress.ui.notify('Cập nhật thành công đề xuất ' + requestid, 'success', 4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var editRequestBookGrab = function () {
        $.ajax({
            url: "/api/RequestBookGrabApi/DeleteRequest?id=" + requestid,
            type: 'DELETE',
            success: function (data) {
                var table = $('#book grabb-table').DataTable();
                var ordinal = 0;

                excesscash = totalamount - advanceamount - remainingamount;

                table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var d = this.data();
                    requestcontent = d.RequestContent;
                    startadress = d.StartAdress;
                    endadress = d.EndAdress;
                    vehicetype = d.VehiceType;
                    ordinal += 1;
                    $.ajax({
                        url: "/api/RequestBookGrabApi/DeleteRequest?id=" + requestID,
                        type: 'DELETE',
                        success: function (data) {
                            var table = $('#book-grab-table').DataTable();
                            var ordinal = 0;
                            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                                var d = this.data();
                                requestcontent = d.RequestContent;
                                startadress = d.StartAdress;
                                endadress = d.EndAdress;
                                executiondate = d.ExecutionDate;
                                vehicetype = d.VehiceType;
                                ordinal += 1;

                                $.ajax({
                                    url: "/api/RequestBookGrabApi/Create",
                                    type: 'POST',
                                    contentType: 'application/json; charset=utf-8',
                                    dataType: 'json',
                                    data: JSON.stringify({
                                        RequestID: requestID,
                                        Ordinal: ordinal,
                                        RequestType: requesttype,
                                        RequestTitle: requesttitle,
                                        CreateDate: createdate,
                                        CreateName: createname,
                                        CreateEmail: createemail,
                                        CreateDepartment: createdepartment,
                                        ProcessingTime: processingtime,
                                        Approvers: approvers,
                                        Followers: followers,
                                        Status: "Waiting",
                                        BookType: booktype,
                                        RequestContent: requestcontent,
                                        StartAdress: startadress,
                                        EndAdress: endadress,
                                        ExecutionDate: executiondate,
                                        VehiceType: vehicetype
                                    }),
                                    beforeSend: function (request) {
                                        request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                                    },
                                    success: function (data) {

                                    },
                                    error: function (xhr) {
                                        DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                                    }
                                });
                            });
                        },
                        error: function (error) {
                            Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
                        }
                    });
                });
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });    
    };

    this.deleteRequest = function (e) {
        let timerInterval
        Swal.fire({
            title: 'Bạn có muốn xóa đề xuất này',
            toast: true,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa!',
            cancelButtonText: 'Không!',
            html: '<strong></strong> seconds<br/><br/>',
            timer: 10000,
            didOpen: () => {
                const content = Swal.getHtmlContainer()
                const $ = content.querySelector.bind(content)

                timerInterval = setInterval(() => {
                    Swal.getHtmlContainer().querySelector('strong')
                        .textContent = (Swal.getTimerLeft() / 1000)
                            .toFixed(0)
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.isConfirmed) {
                if (e.row.data.Status == "Waiting") {
                    deleteRequest(e.row.data.RequestID);
                }
                else {
                    Swal.fire('Alert!!!', 'Không thể xóa đề xuất', 'error', 2000);
                }

            }
        })
    };

    var deleteRequest = function (requestID) {
        $.ajax({
            url: "/api/RequestApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                deleteRequestBookGrab(requestID);
            },
            error: function (error) {
                Swal.fire('Error!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestBookGrab = function (requestID) {
        $.ajax({
            url: "/api/RequestBookGrabApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                grid.refresh();
                Swal.fire('Success!', 'Xóa thành công đề xuất ' + requestID, 'success', 2000);
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.requestBookGrabs = new DevAV.RequestBookGrabs();

$(function () {
    DevAV.requestBookGrabs.init();
});