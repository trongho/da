﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestDetails = function () {
    var status, requestType;
    var formatDate = new Intl.DateTimeFormat("en-US").format;
    var currenRequest;
    

    this.openPopupDetail = function (e) {
        status = e.row.data.Status;
        requestType = e.row.data.RequestType;

        currenRequest = e.row.data;

        if (e.row.data.RequestType === "Đề nghị thanh toán") {
            const popup = $("#popup-request-payment-detail").dxPopup("instance");
            popup.show();
            fetchRequest(currenRequest);
            fetchRequestPayment(currenRequest);
        }
        else if (e.row.data.RequestType === "Đề xuất vật tư") {
            const popup = $("#popup-request-supplies-detail").dxPopup("instance");
            popup.show();
            fetchRequest(currenRequest);
            fetchRequestSupplies(currenRequest);
        }
        else if (e.row.data.RequestType === "Đề xuất mượn hàng demo") {
            const popup = $("#popup-request-lend-goods-demo-detail").dxPopup("instance");
            popup.show();
            fetchRequest(currenRequest);
            fetchRequestLendGoodsDemo(currenRequest);
        }
        else if (e.row.data.RequestType === "Đề xuất book grab") {
            const popup = $("#popup-request-book-grab-detail").dxPopup("instance");
            popup.show();
            fetchRequest(currenRequest);
            fetchRequestBookGrab(currenRequest);
        }
        else if (e.row.data.RequestType === "Đề nghị hoàn ứng") {
            const popup = $("#popup-request-reimbursement-detail").dxPopup("instance");
            popup.show();
            fetchRequest(currenRequest);
            fetchRequestReimbursement(currenRequest);
        }
    };

    this.openPrintPopup = function (e) {
        if (status === 'Waiting' || status === 'Forwarding') {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast'
                },
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true
            });

            Toast.fire({
                icon: 'warning',
                title: 'Đề xuất đang chờ duyệt, không thể in'
            });
            return;
        }
        if (status === 'Refuse') {
            const Toast2 = Swal.mixin({
                toast: true,
                position: 'top-right',
                iconColor: 'white',
                customClass: {
                    popup: 'colored-toast'
                },
                showConfirmButton: false,
                timer: 1500,
                timerProgressBar: true
            });

            Toast2.fire({
                icon: 'error',
                title: 'Đề xuất đã bị từ chối, không thể in'
            });
            return;
        }

        if (requestType === "Đề nghị thanh toán") {
            const popup = $("#popup-preview").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-preview-request-payment-template"),
            });
            popup.show();
            updateRichTextRequestPayment();
        }
        else if (requestType === "Đề xuất vật tư") {
            const popup = $("#popup-preview").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-preview-request-supplies-template"),
            });
            popup.show();
            updateRichTextRequestSupplies();
        }
        else if (requestType === "Đề xuất mượn hàng demo") {
            const popup = $("#popup-preview").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-preview-request-lend-goods-demo-template"),
            });
            popup.show();
            updateRichTextRequestLendGoodsDemo();
        }
        else if (requestType === "Đề xuất book grab") {
            const popup = $("#popup-preview").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-preview-request-book-grab-template"),
            });
            popup.show();
            updateRichTextRequestBookGrab();
        }
        else if (requestType === "Đề nghị hoàn ứng") {
            const popup = $("#popup-preview").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-preview-request-reimbursement-template"),
            });
            popup.show();
            updateRichTextRequestReimbursement();
        }
    };

    var fetchRequest = function (data) {
        $("#request-id").dxTextBox("instance").option("value", data.RequestID);
        $("#request-title").dxTextBox("instance").option("value", data.RequestTitle);
        $("#request-type").dxSelectBox("instance").option("value", data.RequestType);
        $("#create-date").dxDateBox("instance").option("value", data.CreateDate);
        $("#create-name").dxTextBox("instance").option("value", data.CreateName);
        $("#create-email").dxTextBox("instance").option("value", data.CreateEmail);
        $("#create-department").dxTextBox("instance").option("value", data.CreateDepartment);
        $("#status").dxTextBox("instance").option("value", data.Status);
        $("#processing-time").dxDateBox("instance").option("value", data.ProcessingTime);
        $("#approvers").dxSelectBox("instance").option("value", data.Approvers.split(',').slice(-1).pop());
        $("#followers").dxTagBox("instance").option("value", data.Followers.split(','));
    };

    var fetchRequestPayment = function (data) {
        $.getJSON("/api/RequestPaymentApi/GetByID?id=" + data.RequestID,
            function (result) {              
                $("#request-number").dxTextBox("instance").option("value", result.data[0].RequestNumber);
                $("#currency").dxSelectBox("instance").option("value", result.data[0].Currency);
                $("#total-amount").dxTextBox("instance").option("value", result.data[0].Amount);
                $("#payment").dxSelectBox("instance").option("value", result.data[0].Payment);
                $("#time-of-payment").dxDateBox("instance").option("value", result.data[0].TimeOfPayment);
                $("#customer-name").dxTextBox("instance").option("value", result.data[0].CustomerName);
                $("#beneficiary-account").dxTextBox("instance").option("value", result.data[0].BeneficiaryAccount);
                $("#beneficiary").dxTextBox("instance").option("value", result.data[0].Beneficiary);
                $("#bank-at").dxTextBox("instance").option("value", result.data[0].BankAt);
                $("#trading-department").dxTextBox("instance").option("value", result.data[0].TradingDepartment);

                var list = []
                for (var i = 0; i < result.data.length; i++) {
                    var PaymentContent = result.data[i].PaymentContent;
                    var Amount = result.data[i].Amount;
                    var DocumentsNumber = result.data[i].DocumentsNumber;
                    var Note = result.data[i].Note;
                    var obj = { "PaymentContent": PaymentContent, "Amount": Amount, "DocumentsNumber": DocumentsNumber, "Note": Note };
                    list.push(obj);
                }

                var table = $('#payment-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data:list,
                    columns: [
                        { data: "PaymentContent" },
                        { data: "Amount" },
                        { data: "DocumentsNumber", defaultContent: "<i>Not set</i>" },
                        { data: "Note", defaultContent: "<i>Not set</i>" },
                    ]
                });

            });
    };

    var fetchRequestSupplies = function (data) {
        $.getJSON("/api/RequestSuppliesApi/GetByID?id=" + data.RequestID,
            function (result) {
                $("#request-content").dxTextBox("instance").option("value", result.data[0].RequestContent);
                $("#reason").dxTextBox("instance").option("value", result.data[0].Reason);

                var list = []
                for (var i = 0; i < result.data.length; i++) {
                    var Goods = result.data[i].Goods;
                    var Specification = result.data[i].Specification;
                    var Quantity = result.data[i].Quantity;
                    var Note = result.data[i].Note;
                    var obj = { "Goods": Goods, "Specification": Specification, "Quantity": Quantity, "Note": Note };
                    list.push(obj);
                }

                var table = $('#supplies-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "Goods" },
                        { data: "Specification" },
                        { data: "Quantity" },
                        { data: "Note", defaultContent: "<i>Not set</i>" },
                    ]
                });
               
            });
    };

    var fetchRequestLendGoodsDemo = function (data) {
        $.getJSON("/api/RequestLendGoodsDemoApi/GetByID?id=" + data.RequestID,
            function (result) {
                $("#lend-time").dxDateBox("instance").option("value", result.data[0].LendTime);
                $("#reason").dxTextBox("instance").option("value", result.data[0].Reason);

                var list = []
                for (var i = 0; i < result.data.length; i++) {
                    var description = result.data[i].Description
                    var goodID = result.data[i].GoodsID;
                    var serial = result.data[i].Serial;
                    var quantity = result.data[i].Quantity;
                    var note = result.data[i].Note;
                    var obj = { "Description": description, "GoodsID": goodID, "Serial": serial, "Quantity": quantity, "Note": note };
                    list.push(obj);
                }

                var table = $('#lend-goods-demo-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "Description" },
                        { data: "GoodsID" },
                        { data: "Serial" },
                        { data: "Quantity" },
                        { data: "Note", defaultContent: "<i>Not set</i>" },
                    ]
                });

            });
    };

    var fetchRequestBookGrab = function (data) {
        $.getJSON("/api/RequestBookGrabApi/GetByID?id=" + data.RequestID,
            function (result) {
                $("#book-type").dxSelectBox("instance").option("value", result.data[0].BookType);

                var list = []
                for (var i = 0; i < result.data.length; i++) {
                    var RequestContent = result.data[i].RequestContent;
                    var StartAdress = result.data[i].StartAdress;
                    var EndAdress = result.data[i].EndAdress;
                    var ExecutionDate = result.data[i].ExecutionDate;
                    var VehiceType = result.data[i].VehiceType;
                    var obj = { "RequestContent": RequestContent, "StartAdress": StartAdress, "EndAdress": EndAdress, "ExecutionDate": ExecutionDate, "VehiceType": VehiceType };
                    list.push(obj);
                }

                var table = $('#book-grab-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "RequestContent" },
                        { data: "StartAdress" },
                        { data: "EndAdress" },
                        { data: "ExecutionDate" },
                        { data: "VehiceType", defaultContent: "<i>Not set</i>" },
                    ]
                });

            });
    };

    var fetchRequestReimbursement = function (data) {
        $.getJSON("/api/RequestBookGrabApi/GetByID?id="+ data.RequestID,
            function (result) {
                $("#request-number").dxTextBox("instance").option("value", result.data[0].RequestNumber);
                $("#advance-amount").dxTextBox("instance").option("value", result.data[0].AdvanceAmount);
                $("#remaining-amount").dxTextBox("instance").option("value", result.data[0].RemainingAmount);
                $("#excess-cash").dxTextBox("instance").option("value", result.data[0].ExcessCash);
                $("#total-amount").dxTextBox("instance").option("value", result.data[0].TotalAmount);
                $("#currency").dxSelectBox("instance").option("value", result.data[0].Currency);
                $("#payment").dxSelectBox("instance").option("value", result.data[0].Payment);
                $("#time-of-payment").dxDateBox("instance").option("value", result.data[0].TimeOfPayment);
                $("#beneficiary").dxTextBox("instance").option("value", result.data[0].Beneficiary);
                $("#beneficiary-account").dxTextBox("instance").option("value", result.data[0].BeneficiaryAccount);
                $("#bank-at").dxTextBox("instance").option("value", result.data[0].BankAt);
                $("#trading-department").dxTextBox("instance").option("value", result.data[0].TradingDepartment);
                $("#accounting-payment").dxSelectBox("instance").option("value", result.data[0].AccountingPayment);
                $("#chief-accountan").dxSelectBox("instance").option("value", result.data[0].ChiefAccountant);
                $("#inspection-staff").dxSelectBox("instance").option("value", result.data[0].InspectionStaff);
                $("#line-manager").dxSelectBox("instance").option("value", result.data[0].LineManager);
                var list = []
                for (var i = 0; i < result.data.length; i++) {
                    var PaymentContent = result.data[i].PaymentContent;
                    var Amount = result.data[i].Amount;
                    var DocumentsNumber = result.data[i].DocumentsNumber;
                    var Note = result.data[i].Note;
                    var obj = { "PaymentContent": PaymentContent, "Amount": Amount, "DocumentsNumber": DocumentsNumber, "Note": Note };
                    list.push(obj);
                }

                var table = $('#reimbursement-table').DataTable({
                    destroy: true,
                    paging: false,
                    searching: false,
                    ordering: false,
                    data: list,
                    columns: [
                        { data: "PaymentContent" },
                        { data: "Amount" },
                        { data: "DocumentsNumber", defaultContent: "<i>Not set</i>" },
                        { data: "Note", defaultContent: "<i>Not set</i>" },
                    ]
                });

            });
    };



    var updateRichTextRequestPayment = function () {
        var paymentDetail = []
        var table = $('#payment-table').DataTable();
        paymentDetail = table.row().data();

        var results = [{
            CreateName: $("#create-name").dxTextBox("instance").option("value"),
            CreateDepartment: $("#create-department").dxTextBox("instance").option("value"),
            RequestNumber: $("#request-number").dxTextBox("instance").option("value"),
            CreateDate: formatDate(new Date($("#create-date").dxDateBox("instance").option("value"))),
            Currency: $("#currency").dxSelectBox("instance").option("value"),
            Amount: $("#amount").dxTextBox("instance").option("value"),
            Payment: $("#payment").dxSelectBox("instance").option("value"),
            TimeOfPayment: formatDate(new Date($("#time-of-payment").dxDateBox("instance").option("value"))),
            AmountToWord: "(" + VNnum2words(Number($("#amount").dxTextBox("instance").option("value"))) + " VND)",
            CustomerName: $("#customer-name").dxTextBox("instance").option("value"),
            Beneficiary: $("#beneficiary").dxTextBox("instance").option("value"),
            BeneficiaryAccount: $("#beneficiary-account").dxTextBox("instance").option("value"),
            BankAt: $("#bank-at").dxTextBox("instance").option("value"),
            TradingDepartment: $("#trading-department").dxTextBox("instance").option("value"),
            Approvers: $("#approvers").dxSelectBox("instance").option("value"),

            PaymentContent0:table.row(0).data()[0],
            Amount0:table.row(0).data()[1],
            DocumentsNumber0:table.row(0).data()[2],
            Note0:table.row(0).data()[3],
            
        }];
        var selector = function (dataItem) {
            return {
                CreateName: dataItem.CreateName,
                CreateDepartment: dataItem.CreateDepartment,
                RequestNumber: dataItem.RequestNumber,
                CreateDate: dataItem.CreateDate,
                Currency: dataItem.Currency,
                Amount: dataItem.Amount,
                Payment: dataItem.Payment,
                TimeOfPayment: dataItem.TimeOfPayment,
                AmountToWord: dataItem.AmountToWord,
                CustomerName: dataItem.CustomerName,
                Beneficiary: dataItem.Beneficiary,
                BeneficiaryAccount: dataItem.BeneficiaryAccount,
                BankAt: dataItem.BankAt,
                TradingDepartment: dataItem.TradingDepartment,
                Approvers: dataItem.Approvers,

                PaymentContent0:dataItem.PaymentContent,
                Amount0:dataItem.Amount,
                DocumentsNumber0:dataItem.DocumentsNumber,
                Note0:dataItem.Note,
            };
        };
        var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
        richEditRequestPayment.mailMergeOptions.setDataSource(newDataSource);
    };


    var updateRichTextRequestSupplies = function () {
        $getJSON("/api/UserNewApi/GetByUsername", function (r) {
            var results = [{
                CreateName: r[0].FullName,
                Position: r[0].Position,
                RequestContent: $("#request-content").dxTextBox("instance").option("value"),
                Reason: $("#reason").dxTextBox("instance").option("value"),
                Approvers: $("#approvers").dxSelectBox("instance").option("value"),
            }];
            var selector = function (dataItem) {
                return {
                    CreateName: dataItem.CreateName,
                    CreateDepartment: dataItem.CreateDepartment,
                    CreateDate: dataItem.CreateDate,
                    Approvers: dataItem.Approvers,
                };
            };
            var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
            richEditRequestSupplies.mailMergeOptions.setDataSource(newDataSource);
        })
    };

    var updateRichTextRequestLendGoodsDemo = function () {
        $getJSON("/api/UserNewApi/GetByUsername", function (r) {
            var results = [{
                CreateName: r[0].FullName,
                Position: r[0].Position,
                RequestContent: $("#request-content").dxTextBox("instance").option("value"),
                Reason: $("#reason").dxTextBox("instance").option("value"),
                Approvers: $("#approvers").dxSelectBox("instance").option("value"),
            }];
            var selector = function (dataItem) {
                return {
                    CreateName: dataItem.CreateName,
                    CreateDepartment: dataItem.CreateDepartment,
                    CreateDate: dataItem.CreateDate,
                    Approvers: dataItem.Approvers,
                };
            };
            var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
            richEditRequestLendGoodsDemo.mailMergeOptions.setDataSource(newDataSource);
        })
    };

    var updateRichTextRequestBookGrab = function () {
        $getJSON("/api/UserNewApi/GetByUsername", function (r) {
            var results = [{
                CreateName: r[0].FullName,
                CreateDate: r[0].CreateDate,
                Department: r[0].Department,
                Approvers: $("#approvers").dxSelectBox("instance").option("value"),
            }];
            var selector = function (dataItem) {
                return {
                    CreateName: dataItem.CreateName,
                    CreateDepartment: dataItem.CreateDepartment,
                    CreateDate: dataItem.CreateDate,
                    Approvers: dataItem.Approvers,
                };
            };
            var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
            richEditRequestBookGrab.mailMergeOptions.setDataSource(newDataSource);

            r.data.forEach(r1 => {
                var results1 = [{
                    StartAdress: r1.StartAdress,
                    EndAdress: r1.EndAdress,
                    VehiceType=r1.VehiceType,
                    RequestContent=r1.RequestContent
                }];
                var selector1 = function (dataItem) {
                    return {
                        StartAdress: dataItem.StartAdress,
                        EndAdress=dataItem.EndAdress,
                        VehiceType=dataItem.VehiceType,
                        RequestContent=dataItem.RequestContent,
                    };
                };
                var newDataSource1 = new DevExpress.data.DataSource({ store: results1, select: selector1 });
                richEditRequestBookGrab.mailMergeOptions.setDataSource(newDataSource1);
            });
           
            
        })
    };

    var updateRichTextRequestReimbursement = function () {
        $getJSON("/api/UserNewApi/GetByUsername", function (r) {
            var results = [{
                CreateName: r[0].FullName,
                CreateDepartment: r[0].Department,
                CreateDate: formatDate(new Date($("#create-date").dxDateBox("instance").option("value"))),
                Currency: $("#currency").dxSelectBox("instance").option("value"),
                AdvanceAmount: $("#advance-amount").dxTextBox("instance").option("value"),
                RemainingAmount: $("#remaining-amount").dxTextBox("instance").option("value"),
                TotalAmount: $("#total-amount").dxTextBox("instance").option("value"),
                ExcessCash: $("#excess-cash").dxTextBox("instance").option("value"),
                AmountToWord: "(" + VNnum2words(Number($("#total-amount").dxTextBox("instance").option("value"))) + " VND)",
                CustomerName: $("#customer-name").dxTextBox("instance").option("value"),
                Beneficiary: $("#beneficiary").dxTextBox("instance").option("value"),
                BeneficiaryAccount: $("#beneficiaty-account").dxTextBox("instance").option("value"),
                BankAt: $("#bank-at").dxTextBox("instance").option("value"),
                TradingDepartment: $("#trading-department").dxTextBox("instance").option("value"),

                Approvers: $("#approvers").dxSelectBox("instance").option("value"),
                InspectionStaff: $("#inspection-staff").dxSelectBox("instance").option("value"),
                AccountingPayment: $("#accounting-payment").dxSelectBox("instance").option("value"),
                ChiefAccountant: $("#chief-accountant").dxSelectBox("instance").option("value"),
                LineManager: $("#line-manager").dxSelectBox("instance").option("value"),
            }];
            var selector = function (dataItem) {
                return {
                    CreateName: dataItem.CreateName,
                    CreateDepartment: dataItem.CreateDepartment,
                    CreateDate: dataItem.CreateDate,
                    Currency: dataItem.Currency,
                    AdvanceAmount: dataItem.AdvanceAmount,
                    RemainingAmount: dataItem.RemainingAmount,
                    TotalAmount: dataItem.TotalAmount,
                    ExcessCash: dataItem.ExcessCash,
                    AmountToWord: dataItem.AmountToWord,
                    CustomerName: dataItem.CustomerName,
                    Beneficiary: dataItem.Beneficiary,
                    BeneficiaryAccount: dataItem.BeneficiaryAccount,
                    BankAt: dataItem.BankAt,
                    TradingDepartment: dataItem.TradingDepartment,

                    Approvers: dataItem.Approvers,
                    InspectionStaff: dataItem.InspectionStaff,
                    AccountingPayment: dataItem.AccountingPayment,
                    ChiefAccountant: dataItem.ChiefAccountant,
                    LineManager: dataItem.LineManager

                };
            };
            var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
            richEditRequestReimbursement.mailMergeOptions.setDataSource(newDataSource);
        })
    };


    

    this.init = function () {

    };
}

DevAV.requestDetails = new DevAV.RequestDetails();

$(function () {
    DevAV.requestDetails.init();
});