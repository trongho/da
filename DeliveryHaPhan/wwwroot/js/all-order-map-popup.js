﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.AllOrderMap = function () {
    const mapLoadToken = 'SYASj2SLYfUvY5BtsZgDJgzGydMsDKZnBAgsQXP1';
    const mapAPIToken = 'R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS';
    var map;
    var results = [];
    var results_user = [];
    var filteringWidget = {};
   
    this.popupAllOrderMap = function (e) {
        //popup
        const popup = $("#popup-all-order-map").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-all-order-map-template"),
        });
        popup.show();

        fetchData();
        fetchDataUser();
    };

    var loadmap = function (pointArr) {
        goongjs.accessToken = mapLoadToken;
        if (map == null) {
            map = new goongjs.Map({
                container: 'map',
                style: 'https://tiles.goong.io/assets/goong_light_v2.json',
                center: pointArr,
                zoom: 9
            });
        }
        else {
            map.remove();
            map = new goongjs.Map({
                container: 'map',
                style: 'https://tiles.goong.io/assets/goong_light_v2.json',
                center: pointArr,
                zoom: 9
            });
        }
    };

    var fetchData = function () {
        $.getJSON("AdminHome/GetActiveOrders?status=New,Approved,Started,Pause,Continue",
            function (orders) {
                loadmap(orders.data[0].EndPoint.trim().split(',').reverse());
                orders.data.forEach(order => {
                    displayEndPoint(order.EndPoint.trim().split(',').reverse(), order.OrderID, order.Status)
                    if (order.CurrentPoint != null) {
                        displayCurrentPoint(order.CurrentPoint.trim().split(',').reverse(), order.OrderID, order.Employee1);
                        drawRouter(order.OrderID, order.CurrentPoint, order.EndPoint, order.Vehicle);
                    }
                });

                 //filter end point
                var lat1, lng1, lat2, lng2;
                var ordersNew = orders.data;

                for (var i = 0; i < ordersNew.length; i++) {
                    for (var j = 0; j < ordersNew.length; j++) {
                            
                        lat1 = ordersNew[i].EndPoint.split(',').reverse()[0];
                        lng1 = ordersNew[i].EndPoint.split(',').reverse()[1];
                        lat2 = ordersNew[j].EndPoint.split(',').reverse()[0];
                        lng2 = ordersNew[j].EndPoint.split(',').reverse()[1];

                        var obj = { "id": ordersNew[i].OrderID +"-"+ ordersNew[j].OrderID,"point1": ordersNew[i].OrderID, "point2":ordersNew[j].OrderID, "value": getDistanceFromLatLonInKm(lat1, lng1, lat2, lng2) };
                        results.push(obj);                       
                    }
                }

                for (var k = 0; k < results.length; k++) {
                    if (results[k].value==0) {
                            delete results[k];
                    }
                }

                results.sort(function (a, b) {
                    return a.point1 - b.point1;
                });

                var removeList = [];

                results = results.filter(function (currentObject) {
                    if (currentObject.value in removeList) {
                        return false;
                    } else {
                        removeList[currentObject.value] = true;
                        return true;
                    }
                });

                //for (var m = 0; m < results.length; m++) {
                //    if (compareList.indexOf(results[m]) >= 0) {
                //        removeList.push(results[m]);
                //        delete results[m]
                //    }
                //}

                var source = new DevExpress.data.CustomStore({
                    key: "id",
                    load: function () {
                        return results;
                    }
                });

                $("#grid-filter").dxDataGrid("instance").option("dataSource", source);               
            })
    };

    var fetchDataUser = function () {
        $.getJSON("AdminHome/GetActiveOrders?status=Started,Pause,Continue",
            function (orders) {
                //filter current point
                var lat1, lng1, lat2, lng2;
                var ordersNew = orders.data;

                for (var i = 0; i < ordersNew.length; i++) {
                    for (var j = 0; j < ordersNew.length; j++) {
                        if (ordersNew[i].OrderID != ordersNew[j].OrderID) {
                            lat1 = ordersNew[i].CurrentPoint.split(',').reverse()[0];
                            lng1 = ordersNew[i].CurrentPoint.split(',').reverse()[1];
                            lat2 = ordersNew[j].CurrentPoint.split(',').reverse()[0];
                            lng2 = ordersNew[j].CurrentPoint.split(',').reverse()[1];

                            var obj = { "id": ordersNew[i].Employee1 + "-" + ordersNew[j].Employee1, "point1": ordersNew[i].Employee1, "point2": ordersNew[j].Employee1, "value": getDistanceFromLatLonInKm(lat1, lng1, lat2, lng2) };
                            results_user.push(obj);
                        }
                    }
                }

            results_user = results_user.filter(x => x.point1 != x.point2);

                var splitlen = results_user;
                var uniqueArray = [];

                for (i = 0; i < splitlen.length; i++) {

                    if (!uniqueArray.find(x => x.point1 === splitlen[i].point2 && x.point2 === splitlen[i].point1)&& !uniqueArray.find(x => x.point1 === splitlen[i].point1 && x.point2 === splitlen[i].point2)) {
                        uniqueArray.push(splitlen[i]);
                    }
                }


                /*results_user = results_user.filter(x => x.point1 != x.point2);*/

                uniqueArray.sort(function (a, b) {
                    return a.id - b.id;
                });

                //results_user = results_user.filter(y => results_user.filter((z => z.point1 == y.point2 && z.point2 == y.point1)));

                var source = new DevExpress.data.CustomStore({
                    key: "id",
                    load: function () {
                        return uniqueArray;
                    }
                });

                $("#grid-filter-user").dxDataGrid("instance").option("dataSource", source);
            })
    };


    var displayEndPoint = function (pointArr, orderid,status) {
        var fillstyle1, fillstyle2;
        if (status == "New") {
            fillstyle1 = 'rgba(255, 200, 200,0.2)';
            fillstyle2 = 'rgba(255, 100, 100, 1)';
        }
        else if (status == "Approved") {
            fillstyle1 = 'rgba(150,255,100,0.2)';
            fillstyle2 = 'rgba(0,66,0, 1)';
        }
        else if (status == "Started" || status == "Continue" || status == "Pause") {
            fillstyle1 = 'rgba(150,150, 200,0.2)';
            fillstyle2 = 'rgba(0,0,200, 1)';
        }
        

        var size = 150;
        var canvas;
        var pulsingDot = {
            width: size,
            height: size,
            data: new Uint8Array(size * size * 4),

            // get rendering context for the map canvas when layer is added to the map
            onAdd: function () {
                canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                this.context = canvas.getContext('2d');
            },

            // called once before every frame where the icon will be used
            render: function () {
                var duration = 1000;
                var t = (performance.now() % duration) / duration;

                var radius = (size / 2) * 0.3;
                var outerRadius = (size / 2) * 0.7 * t + radius;
                var context = this.context;

                // draw outer circle
                context.clearRect(0, 0, this.width, this.height);

                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    outerRadius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = fillstyle1+(1 - t) + ')';
                context.fill();

                // draw inner circle
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    radius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = fillstyle2;
                context.strokeStyle = 'white';
                context.lineWidth = 2 + 4 * (1 - t);
                context.fill();
                context.stroke();

                // update this image's data with data from the canvas
                this.data = context.getImageData(
                    0,
                    0,
                    this.width,
                    this.height
                ).data;

                // continuously repaint the map, resulting in the smooth animation of the dot
                map.triggerRepaint();

                // return `true` to let the map know that the image was updated
                return true;
            }
        };
        
        map.on('load', function () {
            map.addImage('pulsing-dot' + orderid, pulsingDot, { pixelRatio: 2 });
            map.addSource('points' + orderid, {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': pointArr
                            },
                            'properties': {
                                'title': orderid,
                                'icon': 'company'
                            }
                        }
                    ]
                }
            });
            map.addLayer({
                'id': 'points' + orderid,
                'type': 'symbol',
                'source': 'points' + orderid,
                'layout': {
                    'icon-image': 'pulsing-dot' + orderid,
                    'text-field': ['get', 'title'],
                    'text-font': ['Roboto Regular'],
                    'text-offset': [0, 0.6],
                    'text-anchor': 'top',
                }
            });
           
        });

        map.on('click', 'points' + orderid, function (e) {
            const popup = $("#popup-assign").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-assign-template"),
            });
            popup.show();

            $("#order-id-assign").dxTextBox("instance").option("value", orderid);
            $.getJSON("Order/GetByID?id=" + orderid,
                function (data) {
                    $("#employee1-assign").dxSelectBox("instance").option("value", data.Employee1);
                    $("#employee2-assign").dxSelectBox("instance").option("value", data.Employee2);
                    $("#shipping-type-assign").dxSelectBox("instance").option("value", data.ShippingType);
                    $("#order-fee-assign").dxSelectBox("instance").option("value", data.OrderFee);
                    $("#vehice-assign").dxSelectBox("instance").option("value", data.Vehice);
                })

        });



        //var el = document.createElement('div');
        //el.className = 'markersp';
        //el.style.backgroundImage =
        //    "url('../image/startpoint16.png')";
        //el.style.width = '16px';
        //el.style.height = '16px';

        //const id = document.createElement("p");
        //id.style.color = 'red';
        //id.innerHTML = orderid;

        //const emp = document.createElement("p");
        //emp.style.color = 'green';
        //emp.innerHTML = employee1;

        //el.appendChild(id);
        //el.appendChild(emp);
       

        //// add marker to map
        //new goongjs.Marker(el)
        //    .setLngLat(pointArr)
        //    .addTo(map);
    };

    var displayCurrentPoint = function (pointArr, orderid, employee1) {       
        map.on('load', function () {
            map.loadImage(
                '../image/truck512.png',
                function (error, image) {
                    if (error) throw error;
                    map.addImage('cat' + orderid, image);
                    map.addSource('points2' + orderid, {
                        'type': 'geojson',
                        'data': {
                            'type': 'FeatureCollection',
                            'features': [
                                {
                                    'type': 'Feature',
                                    'geometry': {
                                        'type': 'Point',
                                        'coordinates': pointArr
                                    },
                                    'properties': {
                                        'title': employee1,
                                        'icon': 'company'
                                    }
                                }
                            ]
                        }
                    });
                    map.addLayer({
                        'id': 'points2' + orderid,
                        'type': 'symbol',
                        'source': 'points2' + orderid,
                        'layout': {
                            'icon-image': 'cat'+orderid,
                            'icon-size': 0.1,
                            'text-field': ['get', 'title'],
                            'text-font': ['Roboto Regular'],
                            'text-offset': [0, 0.6],
                            'text-anchor': 'top',
                        }
                    });
                }
            );
        });
    };

    var drawRouter = function (orderid,point1, point2, vh) {
        map.on('load', function () {
            var layers = map.getStyle().layers;
            // Find the index of the first symbol layer in the map style
            var firstSymbolId;
            for (var i = 0; i < layers.length; i++) {
                if (layers[i].type === 'symbol') {
                    firstSymbolId = layers[i].id;
                    break;
                }
            }
            // Initialize goongClient with an API KEY
            var goongClient = goongSdk({
                accessToken: mapAPIToken
            });
            // Get Directions
            goongClient.directions
                .getDirections({
                    origin: point1,
                    destination: point2,
                    vehicle: vh
                })
                .send()
                .then(function (response) {
                    var directions = response.body;
                    var route = directions.routes[0];

                    var geometry_string = route.overview_polyline.points;
                    var geoJSON = polyline.toGeoJSON(geometry_string);
                    map.addSource('route' + orderid, {
                        'type': 'geojson',
                        'data': geoJSON
                    });
                    // Add route layer below symbol layers
                    map.addLayer(
                        {
                            'id': 'route' + orderid,
                            'type': 'line',
                            'source': 'route' + orderid,
                            'layout': {
                                'line-join': 'round',
                                'line-cap': 'round'
                            },
                            'paint': {
                                'line-color': "#xxxxxx".replace(/x/g, y => (Math.random() * 16 | 0).toString(16)),
                                'line-width': 8,
                            }
                        },
                        firstSymbolId
                    );
                });
        });
    };

    var getFilteringWidget = function () {
        var filterList = $("#list").dxList("instance");

        return filterList;
    };

    this.filterDataByText = function (searchData) {
        filteringWidget.searchByText(searchData.value);
    };

    this.onRowPreparedGridFilterUser = function (e) {
        if (e.rowType == 'data' && e.data.value == 0) {
            e.rowElement[0].style.backgroundColor = 'red';
            e.rowElement[0].style.color ='white';
        }  
    };

    this.selectionChanged = function (e) {
        var order = e.addedItems[0];
        if (order.CurrentPoint != null) {
            map.flyTo({
                center: order.CurrentPoint.trim().split(',').reverse(),
                zoom: 15,
                essential: true
            });
        }
        else {
            map.flyTo({
                center: order.EndPoint.trim().split(',').reverse(),
                zoom: 15,
                essential: true
            });
        }
    };

    this.selectionChangedEndpoint = function (e) {
        var rowData = e.selectedRowsData[0];
        if (!rowData) return;
        map.flyTo({
            center: rowData.EndPoint.trim().split(',').reverse(),
            zoom: 15,
            essential: true
        });
    };

    this.selectionChangedCurrentpoint = function (e) {
        var rowData = e.selectedRowsData[0];
        if (!rowData) return;
        map.flyTo({
            center: rowData.CurrentPoint.trim().split(',').reverse(),
            zoom: 15,
            essential: true
        });
    };

    this.cellClick = function (e) {
        $.getJSON("Order/GetEndPoint?id=" +e.value,
            function (data) {
                map.flyTo({
                    center: data.trim().split(',').reverse(),
                    zoom: 15,
                    essential: true
                });
                console.log(data);
            })
    };


    this.selectBox_valueChanged = function (data) {
        var status;
        switch (data.value) {
            case 0:
                status = "New";
                break;
            case 1:
                status = "Approved";
                break;
            case 2:
                status = "Started";
                break;
            case 3:
                status = "Pause";
                break;
            case 4:
                status = "Continue";
                break;
            case 5:
                status = "New,Approved,Started,Pause,Continue";
                break;
            default:
                status = "Started";
        }

        $.getJSON("AdminHome/GetActiveOrders?status=" + status,
            function (orders) {
                var filterSource = new DevExpress.data.CustomStore({
                    key: "id",
                    load: function () {
                        return orders.data;
                    }
                });

                $("#list").dxList("instance").option("dataSource", filterSource);

                var countOrder = document.getElementById("countOrder");
                countOrder.innerHTML = orders.data.length + " đơn hàng";
            })

    };

    var getDistanceFromLatLonInKm = function (lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    var deg2rad = function (deg) {
        return deg * (Math.PI / 180)
    }

    this.init = function () {
        filteringWidget = getFilteringWidget();
    };
}

DevAV.allOrderMap = new DevAV.AllOrderMap();

$(function () {
    DevAV.allOrderMap.init();
});