﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.SearchWRR = function () {
    var grid;
    var branch, handlingStatus, fromDate, toDate;
    
    this.filterWRR = function () {
        branch = $("#branch").dxSelectBox("instance");
        grid = $("#gridContainerWRRHeader").dxDataGrid("instance");
        handlingStatus = $("#handling-status").dxSelectBox("instance");
        fromDate = $("#from-date").dxDateBox("instance");
        toDate = $("#to-date").dxDateBox("instance");
        $.getJSON("api/WRRHeaderApi/",
            function (data) {
                data = data.filter(x => x.BranchID == branch.option("value") && x.HandlingStatusID == handlingStatus.option("value")
                    && new Date(fromDate.option("value")).getDate() <= new Date(x.WRRDate).getDate() && new Date(x.WRRDate).getDate() <= new Date(toDate.option("value")).getDate());

                var source = new DevExpress.data.CustomStore({
                    key: "WRRNumber",
                    load: function () {
                        return data;
                    }
                });

                grid.option("dataSource", source);
            })       
    };

    this.refreshGrid = function () {
        grid = $("#gridContainerWRRHeader").dxDataGrid("instance");
        grid.refresh();
    };

    this.init = function () {
        
    };

};

DevAV.searchWRR  = new DevAV.SearchWRR();

$(function () {
    DevAV.searchWRR.init();
});