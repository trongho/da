﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.OrderMapAdmin = function () {
    const mapLoadToken = 'SYASj2SLYfUvY5BtsZgDJgzGydMsDKZnBAgsQXP1';
    const mapAPIToken = 'R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS';
    var map;
    var startPoint,startPointArr,startAdress;
    var currentPoint, currentPointArr;
    var endPoint,endPointArr,endAdress;
    var vehice;

    this.showStartPoint = function () {
        if (startPoint == "" || startPoint == null) {
            DevExpress.ui.notify("Không tìm thấy", "error", 1000);
        } else {
            startPointArr = startPoint.trim().split(',').reverse();
            map.flyTo({
                center: startPointArr,
                zoom: 16,
                essential: true
            });
        }
    };

    this.showCurrentPoint = function () {
        if (currentPoint == "" || currentPoint == null) {
            DevExpress.ui.notify("Không tìm thấy", "error", 1000);
        } else {
            currentPointArr = currentPoint.trim().split(',').reverse();
            map.flyTo({
                center: currentPointArr,
                zoom: 16,
                essential: true
            });
        }
    };

    this.showEndPoint = function () {
        map.flyTo({
            center: endPointArr,
            zoom: 14,
            essential: true
        });
    };

    var setVehice = function (vh) {
        if (vh != null) {
            if (vh.includes("xm")) {
                vehice = "bike";
            }
            else if (vh.includes("xt")) {
                vehice = "car";
            }
            else if (vh.includes("xbt")) {
                vehice = "car";
            }
            else if (vh.includes("tx")) {
                vehice = "taxi";
            }
            else {
                vehice = "bike"
            }

        }

        return vehice;

    }

    var getSpentTime = function () {
        var today = new Date();
        var startTime = new Date($("#start-time").dxTextBox("instance").option("value"));
        var stopTime = new Date($("#stop-time").dxTextBox("instance").option("value"));
        var diffMs = (stopTime - startTime); // milliseconds between now & Christmas
        var diffDays = Math.floor(diffMs / 86400000); // days
        var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
        var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        var diffSecs = Math.round((((diffMs % 86400000) % 3600000) % 60000) / 1000); // seconds
        var spentTime = diffDays + "d:" + diffHrs + "h:" + diffMins + "m:" + diffSecs + "s";
        if ($("#start-time").dxTextBox("instance").option("value") != null && $("#stop-time").dxTextBox("instance").option("value") != null) {
            $("#spent-time").dxTextBox("instance").option("value", spentTime);
        }
        else {
            $("#spent-time").dxTextBox("instance").option("value", "");
        }
    };

    this.popupOrderDetail = function (e) {
        //popup
        const popup = $("#popup-order-detail").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-order-detail-template"),
        });
        popup.show();

        //set value
        $("#order-id").dxTextBox("instance").option("value", e.row.data.OrderID);
        fetchData($("#order-id").dxTextBox("instance").option("value"));
    };

    var fetchData = function (orderID) {
        $.getJSON("Order/GetByID?id=" + orderID,
            function (data) {
                $("#status").dxTextBox("instance").option("value", data.Status);
                $("#employee1").dxTextBox("instance").option("value", data.Employee1);
                $("#employee2").dxTextBox("instance").option("value", data.Employee2);
                $("#start-point").dxTextBox("instance").option("value", data.StartPoint);
                $("#start-adress").dxTextBox("instance").option("value", data.StartAdress);
                $("#current-point").dxTextBox("instance").option("value", data.CurrentPoint);
                if (data.EndAdress) {
                    $("#end-point").dxTextBox("instance").option("value", data.EndPointUpdate);
                    $("#end-adress").dxTextBox("instance").option("value", data.EndAdress);
                } else {
                    $("#end-point").dxTextBox("instance").option("value", data.EndPoint);
                    $("#end-adress").dxTextBox("instance").option("value", data.CustomerAdress);
                }

                $("#vehice").dxTextBox("instance").option("value", data.Vehice);

                $("#result").dxTextBox("instance").option("value", data.Result);
                $("#reason").dxTextBox("instance").option("value", data.Reason);
                $("#start-time").dxTextBox("instance").option("value", data.StartTime);
                $("#stop-time").dxTextBox("instance").option("value", data.StopTime);
                $("#kilometers").dxTextBox("instance").option("value", data.Kilometers);

                startPoint = $("#start-point").dxTextBox("instance").option("value");
                startAdress = $("#start-adress").dxTextBox("instance").option("value");
                currentPoint = $("#current-point").dxTextBox("instance").option("value");
                endPoint = $("#end-point").dxTextBox("instance").option("value");
                endAdress = $("#end-adress").dxTextBox("instance").option("value");
                endPointArr = endPoint.trim().split(',').reverse();
                vehice = setVehice($("#vehice").dxTextBox("instance").option("value"));

                getSpentTime();

                
                loadmap(endPointArr);             
                userEndPoint(endPointArr, endAdress);
                if (startPoint != null && startPoint != "") {
                    startPointArr = startPoint.trim().split(',').reverse();
                    userStartPoint(startPointArr, startAdress);
                    drawRouter(startPoint, endPoint, vehice);
                }
                if (currentPoint != null && currentPoint != "") {
                    currentPointArr = currentPoint.trim().split(',').reverse();
                    userCurrentPoint(currentPointArr);
                }
            })
    };

    var loadmap = function (pointArr) {
        goongjs.accessToken = mapLoadToken;
        if (map == null) {
            map = new goongjs.Map({
                container: 'map-order-detail',
                style: 'https://tiles.goong.io/assets/goong_map_web.json',
                center: pointArr,
                zoom: 15
            });
        }
        else {
            map.remove();
            map = new goongjs.Map({
                container: 'map-order-detail',
                style: 'https://tiles.goong.io/assets/goong_map_web.json',
                center: pointArr,
                zoom: 15
            });
        }
    };

    this.dataRefresh = function () {
        var dataGrid = $("#gridContainer").dxDataGrid("instance");
        //dataGrid.refresh();
        dataGrid.getDataSource().reload();

        //document.getElementsByClassName("markersp")[0].remove();
        //document.getElementsByClassName("markerep")[0].remove();
        //if (map.getLayer('points')) map.removeLayer('points');
        //if (map.getSource('points')) map.removeSource('points');
        //if (map.getLayer('route')) map.removeLayer('route');
        //if (map.getSource('route')) map.removeSource('route');
        //window.top.location = window.top.location;

        //map.setLayoutProperty('route', 'visibility', 'none');
        loadmap(endPointArr);
        fetchData($("#order-id").dxTextBox("instance").option("value"));
    };

    var userEndPoint = function (pointArr, adress) {
        var el = document.createElement('div');
        el.className = 'markerep';
        el.style.backgroundImage =
            "url('../image/endpoint16.png')";
        el.style.width = '16px';
        el.style.height = '16px';

        //popup
        var popup = new goongjs.Popup({
            closeButton: false,
            closeOnClick: false
        });

        //mouse over
        el.addEventListener('mouseover', function () {
            el.style.backgroundImage =
                "url('../image/endpoint24.png')";
            el.style.width = '24px';
            el.style.height = '24px';
            map.getCanvas().style.cursor = 'pointer'
            popup.setLngLat(pointArr).setHTML(adress).addTo(map);
        });

        //mouse leave
        el.addEventListener('mouseleave', function () {
            el.style.backgroundImage =
                "url('../image/endpoint16.png')";
            el.style.width = '16px';
            el.style.height = '16px';
            map.getCanvas().style.cursor = '';
            popup.remove();
        });
        // add marker to map
        new goongjs.Marker(el)
            .setLngLat(pointArr)
            .addTo(map);
    };

    var userStartPoint = function (pointArr, adress) {
        var el = document.createElement('div');
        el.className = 'markersp';
        el.style.backgroundImage =
            "url('../image/startpoint16.png')";
        el.style.width = '16px';
        el.style.height = '16px';

        //popup
        var popup = new goongjs.Popup({
            closeButton: false,
            closeOnClick: false
        });

        //mouse over
        el.addEventListener('mouseover', function () {
            el.style.backgroundImage =
                "url('../image/startpoint24.png')";
            el.style.width = '24px';
            el.style.height = '24px';
            map.getCanvas().style.cursor = 'pointer';
            popup.setLngLat(pointArr).setHTML(adress).addTo(map);
        });

        //mouse leave
        el.addEventListener('mouseleave', function () {
            el.style.backgroundImage =
                "url('../image/startpoint16.png')";
            el.style.width = '16px';
            el.style.height = '16px';
            map.getCanvas().style.cursor = '';
            popup.remove();
        });
        // add marker to map
        new goongjs.Marker(el)
            .setLngLat(pointArr)
            .addTo(map);
    };

    var userCurrentPoint = function (pointArr) {
        var size = 200;
        var canvas;
        //popup
        var popup = new goongjs.Popup({
            closeButton: false,
            closeOnClick: false
        });

        // implementation of CustomLayerInterface to draw a pulsing dot icon on the map
        // see https://docs.goong.io/javascript/properties/#customlayerinterface for more info
        var pulsingDot = {
            width: size,
            height: size,
            data: new Uint8Array(size * size * 4),

            // get rendering context for the map canvas when layer is added to the map
            onAdd: function () {
                canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                this.context = canvas.getContext('2d');
            },

            // called once before every frame where the icon will be used
            render: function () {
                var duration = 1000;
                var t = (performance.now() % duration) / duration;

                var radius = (size / 2) * 0.3;
                var outerRadius = (size / 2) * 0.7 * t + radius;
                var context = this.context;

                // draw outer circle
                context.clearRect(0, 0, this.width, this.height);
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    outerRadius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 200, 200,' + (1 - t) + ')';
                context.fill();

                // draw inner circle
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    radius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 100, 100, 1)';
                context.strokeStyle = 'white';
                context.lineWidth = 2 + 4 * (1 - t);
                context.fill();
                context.stroke();

                // update this image's data with data from the canvas
                this.data = context.getImageData(
                    0,
                    0,
                    this.width,
                    this.height
                ).data;

                // continuously repaint the map, resulting in the smooth animation of the dot
                map.triggerRepaint();

                // return `true` to let the map know that the image was updated
                return true;
            }
        };

        map.on('load', function () {

            map.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });

            map.addSource('points', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': pointArr
                            }
                        }
                    ]
                }
            });
            map.addLayer({
                'id': 'points',
                'type': 'symbol',
                'source': 'points',
                'layout': {
                    'icon-image': 'pulsing-dot'
                }
            });
        });

        map.on('mouseenter', 'points', function (e) {
            // Change the cursor style as a UI indicator.
            map.getCanvas().style.cursor = 'pointer';

            var coordinates = e.features[0].geometry.coordinates.slice();


            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            // Populate the popup and set its coordinates
            // based on the feature found.
            $.getJSON("Map/GetAdress/" + coordinates,
                function (data) {
                    popup.setLngLat(coordinates).setHTML(data).addTo(map);
                });

        });

        map.on('mouseleave', 'points', function () {
            map.getCanvas().style.cursor = '';
            popup.remove();
        });
    };

    var drawRouter = function (point1, point2, vh) {
        map.on('load', function () {
            var layers = map.getStyle().layers;
            // Find the index of the first symbol layer in the map style
            var firstSymbolId;
            for (var i = 0; i < layers.length; i++) {
                if (layers[i].type === 'symbol') {
                    firstSymbolId = layers[i].id;
                    break;
                }
            }
            // Initialize goongClient with an API KEY
            var goongClient = goongSdk({
                accessToken: mapAPIToken
            });
            // Get Directions
            goongClient.directions
                .getDirections({
                    origin: point1,
                    destination: point2,
                    vehicle: vh
                })
                .send()
                .then(function (response) {
                    var directions = response.body;
                    var route = directions.routes[0];

                    var geometry_string = route.overview_polyline.points;
                    var geoJSON = polyline.toGeoJSON(geometry_string);
                    map.addSource('route', {
                        'type': 'geojson',
                        'data': geoJSON
                    });
                    // Add route layer below symbol layers
                    map.addLayer(
                        {
                            'id': 'route',
                            'type': 'line',
                            'source': 'route',
                            'layout': {
                                'line-join': 'round',
                                'line-cap': 'round'
                            },
                            'paint': {
                                'line-color': '#1e88e5',
                                'line-width': 8
                            }
                        },
                        firstSymbolId
                    );
                });
        });
    };


    this.init = function () {
        
    };
}

DevAV.orderMapAdmin = new DevAV.OrderMapAdmin();

$(function () {
    DevAV.orderMapAdmin.init();
});