﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Requirements
{
    public class UserStatusRequirement: IAuthorizationRequirement
    {
        public bool IsBlocked { get; }
        public UserStatusRequirement(bool isBlocked)
        {
            IsBlocked = isBlocked;
        }
    }
}
