﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class MapService : IMapService
    {
        private readonly AppDbContext context;

        public MapService(AppDbContext context)
        {
            this.context = context;
        }

        public decimal GetKilometers(string startPoint, string endPoint, string vehice)
        {
            //startPoint.Split(",").Reverse();
            //endPoint.Split(",").Reverse();
            Decimal kilometers = 0m;
            String firstString = "https://rsapi.goong.io/DistanceMatrix?origins=";
            String threeString = "&destinations=";
            String fiveString = "&vehicle=";
            String sevenString = "&api_key=";
            String APiKey = "R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS";
            var uri = firstString + startPoint + threeString + endPoint + fiveString + vehice + sevenString + APiKey;
            var json = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }
            dynamic data = JsonConvert.DeserializeObject(json);

            kilometers = data.rows[0].elements[0].distance.value;


            return kilometers/1000;

        }

        public decimal GetKilometersText(string startPoint, string endPoint, string vehice)
        {
            startPoint.Split(",").Reverse();
            endPoint.Split(",").Reverse();
            Decimal kilometers = 0m;
            String firstString = "https://rsapi.goong.io/DistanceMatrix?origins=";
            String threeString = "&destinations=";
            String fiveString = "&vehicle=";
            String sevenString = "&api_key=";
            String APiKey = "R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS";
            var uri = firstString + startPoint + threeString + endPoint + fiveString + vehice + sevenString + APiKey;
            var json = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }
            dynamic data = JsonConvert.DeserializeObject(json);

            kilometers = data.rows[0].elements[0].distance.text;


            return kilometers;
        }

        public string GetLatLng(string adress)
        {
            String firstString = "https://rsapi.goong.io/geocode?address=";
            String threeString = "&api_key=";
            String APiKey = "R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS";
            var uri = firstString + adress.Replace("/", "%2F") + threeString + APiKey;
            var json = "";
            String latlng = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }
            dynamic data = JsonConvert.DeserializeObject(json);
            String lng = data.results[0].geometry.location.lng;
            String lat = data.results[0].geometry.location.lat;
            latlng = lat + "," + lng;

            return latlng;
        }

        public string GetAdress(string coordinateString)
        {
            String firstString = "https://rsapi.goong.io/Geocode?latlng=";
            String threeString= "&api_key=";
            String APiKey = "R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS";
            var uri = firstString +coordinateString+ threeString + APiKey;
            var json = "";
            String adress = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }
            dynamic data = JsonConvert.DeserializeObject(json);
            adress = data.results[0].formatted_address;

            return adress;
        }

        public async Task<bool> UpdateCurrentLocation(string id,string currentpoint)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.CurrentPoint = currentpoint;
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Order>> GetAll()
        {
            var entrys = context.Orders;
            return await entrys.ToListAsync();
        }

        public async Task<List<Direction>> GetDirections(String startPoint, String endPoint, String vehice)
        {
            List<Direction> directions = new List<Direction>();
            String firstString = "https://rsapi.goong.io/Direction?origin=";
            String threeString = "&destination=";
            String fiveString = "&vehicle=";
            String sevenString = "&api_key=";
            String APiKey = "R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS";
            var uri = firstString + startPoint + threeString + endPoint + fiveString + vehice + sevenString + APiKey;
            var json = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }
            dynamic data = JsonConvert.DeserializeObject(json);

            var stepArr = data.routes[0].legs[0].steps;

            for (int i = 1; i <stepArr.Count ; i++)
            {
                Direction direction = new Direction();
                direction.Id = i;
                direction.Distance = data.routes[0].legs[0].steps[i].distance.text;
                direction.Duration = data.routes[0].legs[0].steps[i].duration.text;
                direction.Html_instructions = data.routes[0].legs[0].steps[i].html_instructions;
                directions.Add(direction);
            }
            return directions;
        }
    }
}
