﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class WRRHeaderService : IWRRHeaderService
    {
        private readonly AppDbContext warehouseDbContext;

        public WRRHeaderService(AppDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRRHeaders.FirstOrDefault(g => g.WRRNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRRHeader wRRHeader)
        {
            var entrys = warehouseDbContext.WRRHeaders.AddAsync(wRRHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRRHeaders.FirstOrDefault(o => o.WRRNumber.Equals(id));
            warehouseDbContext.WRRHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRRHeader>> Filter(string branchID, DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.BranchID.Equals(branchID)
               && u.WRRDate >= fromDate && u.WRRDate <= toDate
               && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRHeader>> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, string branchID)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.BranchID.Equals(branchID)
                && u.WRRDate >= fromDate && u.WRRDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRHeader>> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.WRRDate >= fromDate && u.WRRDate <= toDate
               && u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRHeader>> GetAll()
        {
            var entrys = warehouseDbContext.WRRHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRRHeaders.OrderByDescending(x => x.WRRNumber).Take(1).Select(x => x.WRRNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRRHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.WRRDate >= fromDate && u.WRRDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WRRHeaders.Where(u => u.WRRNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, WRRHeader wRRHeader)
        {
            var entrys = warehouseDbContext.WRRHeaders.FirstOrDefault(o => o.WRRNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRRHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
