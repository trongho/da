﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class ProductService: IProductService
    {
        private readonly AppDbContext context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProductService(AppDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<bool> Create(Product product,string photo)
        {
            product.ProductID = $"{GetID():D10}";
            product.Images = $"{GetID():D10}"+ photo.Substring(photo.IndexOf("."));
            product.Status = "new";
            var entrys = context.Products.AddAsync(product);
            context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = context.Products.FirstOrDefault(o => o.ProductID.Equals(id));
            context.Products.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Product>> GetAll()
        {
            var entrys = context.Products;
            return await entrys.ToListAsync();
        }

        public int GetID()
        {
            int id = 0;
            var lastID = context.Products.OrderByDescending(x => x.ProductID).Take(1).Select(x => x.ProductID).ToList().FirstOrDefault();
            if (lastID == null)
            {
                id = 1;
            }
            else
            {
                id = int.Parse(lastID) + 1;
            }
            return id;
        }

        public async Task<List<Product>> GetUnderId(string id)
        {
            var entry = context.Products.Where(u => u.ProductID.Equals(id));
            return await entry.ToListAsync();
        }

        public async Task<bool> Update(string id, Product product,string images)
        {
            var entrys = context.Products.FirstOrDefault(o => o.ProductID.Equals(id));
            product.Status = "updated";
            product.Images = images;
            context.Entry(entrys).CurrentValues.SetValues(product);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> UploadFile(IFormFile file)
        {
            string path = Path.Combine(_hostingEnvironment.ContentRootPath, "alpha_trace");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "alpha_trace/" + file.FileName);

            var form = new MultipartFormDataContent();
            using (var fileStream = file.OpenReadStream())
            {
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }
                form.Add(new StreamContent(fileStream), "file", file.FileName);
            }
            return true;
        }

    }
}
