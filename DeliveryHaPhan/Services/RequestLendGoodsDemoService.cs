﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class RequestLendGoodsDemoService : IRequestLendGoodsDemoService
    {
        private readonly AppDbContext _context;

        public RequestLendGoodsDemoService(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<bool> Create(RequestLendGoodsDemo entry)
        {
            var entrys = _context.RequestLendGoodsDemos.AddAsync(entry);
            _context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = _context.RequestLendGoodsDemos.Where(o => o.RequestID.Equals(id));
            if (entrys != null)
            {
                _context.RequestLendGoodsDemos.RemoveRange(entrys);
                _context.SaveChanges();
            }
            return true;
        }

        public async Task<List<RequestLendGoodsDemo>> GetAll()
        {
            var entrys = _context.RequestLendGoodsDemos;
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestLendGoodsDemo>> GetUnderId(string id)
        {
            var entrys = _context.RequestLendGoodsDemos.Where(u => u.RequestID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestLendGoodsDemo>> GetUnderParams(string id, int Ordinal)
        {
            var entrys = _context.RequestLendGoodsDemos.Where(u => u.RequestID.Equals(id) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, int Ordinal, RequestLendGoodsDemo entry)
        {
            var entrys = _context.RequestLendGoodsDemos.FirstOrDefault(o => o.RequestID.Equals(id) && o.Ordinal == Ordinal);
            _context.Entry(entrys).CurrentValues.SetValues(entry);
            _context.SaveChanges();
            return true;
        }
    }
}
