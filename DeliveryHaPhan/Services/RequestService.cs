﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class RequestService : IRequestService
    {
        private readonly AppDbContext _context;

        public RequestService(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<bool> Approve(string id)
        {
            var entrys = _context.Requests.FirstOrDefault(o => o.RequestID.Equals(id));
            entrys.Status = "Approved";
            _context.Entry(entrys).CurrentValues.SetValues(entrys);

            _context.SaveChanges();
            return true;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = _context.Requests.FirstOrDefault(g => g.RequestID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Request entry)
        {
            String year = DateTime.Now.Year.ToString();
            String month = int.Parse(DateTime.Now.Month.ToString()) < 10 ? ("0" + DateTime.Now.Month.ToString()) : DateTime.Now.Month.ToString();
            String day = int.Parse(DateTime.Now.Day.ToString()) < 10 ? ("0" + DateTime.Now.Day.ToString()) : DateTime.Now.Day.ToString();
            String id = year + month + day;

            String lastID = _context.Requests.OrderByDescending(x => x.RequestID).Take(1).Select(x => x.RequestID).ToList().FirstOrDefault();
            if (lastID != null)
            {
                if (lastID.Substring(0, 8).Equals(id))
                {
                    String lastNumber = "";
                    if((int.Parse(lastID.Substring(8, lastID.Length - 8)) + 1) < 10)
                    {
                        lastNumber ="00"+(int.Parse(lastID.Substring(8, lastID.Length - 8)) + 1);
                    }
                    else if ((int.Parse(lastID.Substring(8, lastID.Length - 8)) + 1) < 100)
                    {
                        lastNumber = "0" + (int.Parse(lastID.Substring(8, lastID.Length - 8)) + 1);
                    }
                    else
                    {
                        lastNumber = (int.Parse(lastID.Substring(8, lastID.Length - 8)) + 1).ToString();
                    }
                    id = lastID.Substring(0, 8)+lastNumber;
                }
                else
                {
                    id = id + "001";
                }
            }
            else
            {
                id = id + "001";
            }

            entry.RequestID = id;
            var entrys = _context.Requests.AddAsync(entry);
            _context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = _context.Requests.FirstOrDefault(o => o.RequestID.Equals(id));
            _context.Requests.Remove(entrys);

            //var requestSupplies = _context.RequestSuppliess.FirstOrDefault(o => o.RequestID.Equals(id));
            //_context.RequestSuppliess.Remove(requestSupplies);

            //var suppliesDetails = _context.SuppliesDetails.FirstOrDefault(o => o.RequestID.Equals(id));
            //_context.SuppliesDetails.RemoveRange(suppliesDetails);

            _context.SaveChanges();
            return true;
        }

        public async Task<bool> Forward(string id, string approver)
        {
            var entrys = _context.Requests.FirstOrDefault(o => o.RequestID.Equals(id));
            entrys.Approvers = entrys.Approvers + "," + approver;
            entrys.Status = "Forwarding";
            _context.Entry(entrys).CurrentValues.SetValues(entrys);
            _context.SaveChanges();
            return true;
        }

        public async Task<List<Request>> GetAll()
        {
            var entrys = _context.Requests;
            return await entrys.ToListAsync();
        }

        public async Task<List<Request>> GetLastRequestApproving(string approver)
        {
            var entry = _context.Requests.Where(u => u.Approvers.Contains(approver) && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding")) && u.ProcessingTime > DateTime.Now).OrderByDescending(x => x.RequestID).Take(1);
            return await entry.ToListAsync();
        }

        public async Task<List<Request>> GetOverdueRequest(string approvers)
        {
            var entry = _context.Requests.Where(u => u.Approvers.Contains(approvers)
                                            && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding"))
                                            && u.ProcessingTime < DateTime.Now);
            return await entry.ToListAsync();
        }

        public async Task<int> GetQuantityRequest(string name, string text)
        {
            int count = 0;
            if (text.Equals("Chờ phê duyệt"))
            {
                count = _context.Requests.Where(u => u.Approvers.Contains(name) && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding")) && u.ProcessingTime > DateTime.Now).Count();
            }
            else if (text.Equals("Theo dõi"))
            {
                count = _context.Requests.Where(u => u.Followers.Contains(name) && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding"))).Count();
            }
            else if (text.Equals("My Request"))
            {
                count = _context.Requests.Where(u => u.CreateName.Contains(name) && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding"))).Count();
            }
            else if (text.Equals("Quá hạn"))
            {
                count = _context.Requests.Where(u => u.Approvers.Contains(name) && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding")) && u.ProcessingTime < DateTime.Now).Count();
            }

            return count;
        }

        public async Task<List<Request>> GetUnderApprovers(string approvers)
        {
            var entry = _context.Requests.Where(u => u.Approvers.Contains(approvers)
                                            && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding"))
                                            && u.ProcessingTime > DateTime.Now);
            return await entry.ToListAsync();
        }

        public async Task<List<Request>> GetUnderCreateName(string createName)
        {
            var entry = _context.Requests.Where(u => u.CreateName.Equals(createName));
            return await entry.ToListAsync();
        }

        public async Task<List<Request>> GetUnderFollowers(string followers)
        {
            var entry = _context.Requests.Where(u => u.Followers.Contains(followers) && (u.Status.Equals("Waiting") || u.Status.Equals("Forwarding")));
            return await entry.ToListAsync();
        }

        public async Task<List<Request>> GetUnderId(string id)
        {
            var entrys = _context.Requests.Where(u => u.RequestID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Recycle(string id)
        {
            var entrys = _context.Requests.FirstOrDefault(o => o.RequestID.Equals(id));
            entrys.Status = "Recycle";
            _context.Entry(entrys).CurrentValues.SetValues(entrys);

            _context.SaveChanges();
            return true;
        }

        public async Task<bool> Refuse(string id)
        {
            var entrys = _context.Requests.FirstOrDefault(o => o.RequestID.Equals(id));
            entrys.Status = "Refuse";
            _context.Entry(entrys).CurrentValues.SetValues(entrys);

            _context.SaveChanges();
            return true;
        }

        public async Task<bool> Update(string id, Request entry)
        {
            var entrys = _context.Requests.FirstOrDefault(o => o.RequestID.Equals(id));
            _context.Entry(entrys).CurrentValues.SetValues(entry);
            _context.SaveChanges();
            return true;
        }
    }
}
