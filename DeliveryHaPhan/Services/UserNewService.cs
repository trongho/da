﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class UserNewService: IUserNewService
    {
        private readonly AppDbContext context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserNewService(AppDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<bool> changePassword(string id, ChangePassword changePassword)
        {
            var entry = context.UserNews.SingleOrDefault(user => user.UserID == id);
            entry.Password = HashingHelper.HashUsingPbkdf2(changePassword.NewPassword, "MTIzNDU2Nzg5MTIzNDU2Nw==");
            context.Entry(entry).CurrentValues.SetValues(entry);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Create(UserNew user)
        {
            int id = 0;
            String lastID = context.Users.OrderByDescending(x => x.UserID).Take(1).Select(x => x.UserID).ToList().FirstOrDefault();
            if (lastID == null)
            {
                id = 1;
            }
            else
            {
                id = int.Parse(lastID) + 1;
            }


            user.UserID = $"{int.Parse(id.ToString()):D5}"; ;
            user.Avatar = ("noimage.png");
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            var users = context.UserNews.AddAsync(user);
            context.SaveChanges();
            return users.IsCompleted;
        }

        public async Task<bool> Create2(UserNew user)
        {
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            var users = context.UserNews.AddAsync(user);
            context.SaveChanges();
            return users.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var users = context.UserNews.FirstOrDefault(o => o.UserID.Equals(id));
            context.UserNews.Remove(users);
            context.SaveChanges();
            return true;
        }

        public async Task<List<UserNew>> GetAll()
        {
            var users = context.UserNews;
            return await users.ToListAsync();
        }

        public async Task<List<UserNew>> GetApprovers(string loginDepartment,int loginRank)
        {
            var user = context.UserNews.Where(u =>(u.Department.Equals(loginDepartment)&&u.Rank>loginRank)||(u.Rank>loginRank));
            return await user.ToListAsync();
        }

        public async Task<List<UserNew>> GetFollowers(int loginRank)
        {
            var user = context.UserNews.Where(u => u.Rank > loginRank);
            return await user.ToListAsync();
        }

        public async Task<List<UserNew>> GetStaffUnderDepartmentAndRanks(string loginDepartment, int loginRank)
        {
            var user = context.UserNews.Where(u =>loginDepartment.Contains(u.Department)&& u.Rank.Equals(loginRank));
            if (loginDepartment.Equals("null"))
            {
                user = context.UserNews.Where(u => u.Rank.Equals(loginRank));
            }
            else if (loginRank==-1)
            {
                user = context.UserNews.Where(u =>loginDepartment.Contains(u.Department));
            }
            return await user.ToListAsync();
        }

        public async Task<List<UserNew>> GetUnderId(string id)
        {
            var user = context.UserNews.Where(u => u.UserID.Equals(id));
            return await user.ToListAsync();
        }

        public async Task<List<UserNew>> GetUnderUserName(string username)
        {
            var user = context.UserNews.Where(u => u.UserName.Equals(username));
            return await user.ToListAsync();
        }

        public async Task<LoginResponseNew> Login(LoginRequest loginRequest)
        {
            var user = context.UserNews.SingleOrDefault(user => user.Active && user.UserName == loginRequest.Username);
            if (user == null)
            {
                return null;
            }
            var passwordHash =HashingHelper.HashUsingPbkdf2(loginRequest.Password, user.PasswordSalt);
            if (user.Password != passwordHash)
            {
                return null;
            }

            user.LastLogin = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            context.Entry(user).CurrentValues.SetValues(user);
            context.SaveChanges();

            var token = await Task.Run(() => TokenHelperNew.GenerateToken(user));

            return new LoginResponseNew { UserID = user.UserID, Username = user.UserName, FullName = user.FullName,Email=user.Email,Position=user.Position,Department=user.Department,Rank=user.Rank,Avatar = user.Avatar, Role = user.Role,Active=user.Active, Blocked = user.Blocked, Token = token };
        }

        public async Task<bool> Update(string id, string updateid, UserNew user)
        {
            var users = context.UserNews.FirstOrDefault(o => o.UserID.Equals(id));
            user.UserID = id;
            user.UpdatedUserID = updateid;
            user.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            user.LastLogin = users.LastLogin;
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            context.Entry(users).CurrentValues.SetValues(user);
            context.SaveChanges();
            return true;
        }
    }
}
