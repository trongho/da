﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class LocationsService : ILocationsService
    {
        private readonly AppDbContext context;

        public LocationsService(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<List<Locations>> GetAll()
        {
            var entrys = context.Locations;
            return await entrys.ToListAsync();
        }

        public async Task<List<Locations>> GetUnderId(string id)
        {
            var entrys = context.Locations.Where(u => u.Latitude.Equals(id));
            return await entrys.ToListAsync();
        }
    }
}
