﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class RequestSuppliesService : IRequestSuppliesService
    {
        private readonly AppDbContext _context;

        public RequestSuppliesService(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<bool> Create(RequestSupplies entry)
        {
            var entrys = _context.RequestSuppliess.AddAsync(entry);
            _context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = _context.RequestSuppliess.Where(o => o.RequestID.Equals(id));
            if (entrys != null)
            {
                _context.RequestSuppliess.RemoveRange(entrys);
                _context.SaveChanges();
            }
            return true;
        }

        public async Task<List<RequestSupplies>> GetAll()
        {
            var entrys = _context.RequestSuppliess;
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestSupplies>> GetUnderId(string id)
        {
            var entrys = _context.RequestSuppliess.Where(u => u.RequestID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestSupplies>> GetUnderParams(string id, int Ordinal)
        {
            var entrys = _context.RequestSuppliess.Where(u => u.RequestID.Equals(id) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, int Ordinal, RequestSupplies entry)
        {
            var entrys = _context.RequestSuppliess.FirstOrDefault(o => o.RequestID.Equals(id) && o.Ordinal == Ordinal);
            _context.Entry(entrys).CurrentValues.SetValues(entry);
            _context.SaveChanges();
            return true;
        }
    }
}
