﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class UserService : IUserService
    {
        private readonly AppDbContext context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserService(AppDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task<bool> ChangeAvatar(string id, string imageUrl,User user)
        {
            var users = context.Users.FirstOrDefault(o => o.UserID.Equals(id));
            user.Avatar = imageUrl;
            user.Password = users.Password;
            user.PasswordSalt = users.PasswordSalt;
            user.CreatedUserID = users.CreatedUserID;
            user.CreatedDate = users.CreatedDate;
            user.UpdatedUserID =id;
            user.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            user.LastLogin = users.LastLogin;
            context.Entry(users).CurrentValues.SetValues(user);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> changePassword(string id,ChangePassword changePassword)
        {
            var entry = context.Users.SingleOrDefault(user => user.UserID ==id);
            entry.Password = HashingHelper.HashUsingPbkdf2(changePassword.NewPassword, "MTIzNDU2Nzg5MTIzNDU2Nw==");
            context.Entry(entry).CurrentValues.SetValues(entry);
            context.SaveChanges();
            return true;
        }

        public bool CheckOldPassword(string id, ChangePassword changePassword)
        {   
            var entry = context.Users.SingleOrDefault(user => user.Active && user.UserID == id);
            string passwordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            bool isValid = string.Equals(entry.Password, HashingHelper.HashUsingPbkdf2(changePassword.OldPassword, passwordSalt), StringComparison.OrdinalIgnoreCase);
            return isValid;
        }

        public async Task<bool> Create(User user)
        {
            int id = 0;
            String lastID = context.Users.OrderByDescending(x => x.UserID).Take(1).Select(x => x.UserID).ToList().FirstOrDefault();
            if (lastID == null)
            {
                id = 1;
            }
            else
            {
                id = int.Parse(lastID) + 1;
            }
            

            user.UserID = $"{int.Parse(id.ToString()):D5}";
            user.Avatar = ("noimage.png");
            user.PasswordSalt = "MTIzNDU2Nzg5MTIzNDU2Nw==";
            user.Password = HashingHelper.HashUsingPbkdf2(user.Password, user.PasswordSalt);
            var users = context.Users.AddAsync(user);
            context.SaveChanges();
            return users.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var users = context.Users.FirstOrDefault(o => o.UserID.Equals(id));
            context.Users.Remove(users);
            context.SaveChanges();
            return true;
        }

        public async Task<List<User>> GetAll()
        {
            var users = context.Users;
            return await users.ToListAsync();
        }

        public async Task<string> GetLastID()
        {
            var id = context.Users.OrderByDescending(x => x.UserID).Take(1).Select(x => x.UserID).ToList().FirstOrDefault();
            return id;
        }

        public async Task<List<User>> GetUnderId(string id)
        {
            var user = context.Users.Where(u =>u.UserID.Equals(id));
            return await user.ToListAsync();
        }

        public async Task<LoginResponse> Login(LoginRequest loginRequest)
        {
            var user = context.Users.SingleOrDefault(user => user.Active && user.UserName == loginRequest.Username);
            if (user == null)
            {
                return null;
            }
            var passwordHash = HashingHelper.HashUsingPbkdf2(loginRequest.Password, user.PasswordSalt);
            if (user.Password != passwordHash)
            {
                return null;
            }

            user.LastLogin = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            context.Entry(user).CurrentValues.SetValues(user);
            context.SaveChanges();

            var token = await Task.Run(() => TokenHelper.GenerateToken(user));

            //string dt = DateTimeOffset.Now.ToString("ddMMyyyy");
            //string t = DateTimeOffset.Now.ToString("HHmmss");

            //string path = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}");

            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}

            //string filePath = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}/{dt}.txt");


            //using (StreamWriter w = File.AppendText(filePath))
            //{
            //    w.WriteLine(t + "," +"người dùng "+ user.UserName + " đăng nhập"+ ";");
            //}

            return new LoginResponse { UserID=user.UserID, Username = user.UserName, Password =loginRequest.Password,FullName = user.FullName,Avatar=user.Avatar,Role=user.Role,Blocked=user.Blocked ,Token = token};
        }

        public async Task<bool> Update(string id,string updateid ,User user)
        {
            var users = context.Users.FirstOrDefault(o => o.UserID.Equals(id));
            user.UserID = id;
            user.UpdatedUserID = updateid;
            user.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            user.LastLogin = users.LastLogin;
            context.Entry(users).CurrentValues.SetValues(user);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Update2(string id, string updateid, UserModel userModel)
        {
            var users = context.Users.FirstOrDefault(o => o.UserID.Equals(id));
            userModel.UserID = id;
            userModel.UpdatedUserID = updateid;
            userModel.UpdatedDate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            userModel.LastLogin = users.LastLogin;
            context.Entry(users).CurrentValues.SetValues(userModel);
            context.SaveChanges();
            return true;
        }

    }
}
