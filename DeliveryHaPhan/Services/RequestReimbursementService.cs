﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class RequestReimbursementService: IRequestReimbursementService
    {
        private readonly AppDbContext _context;

        public RequestReimbursementService(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<bool> Create(RequestReimbursement entry)
        {
            var entrys = _context.RequestReimbursements.AddAsync(entry);
            _context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = _context.RequestReimbursements.Where(o => o.RequestID.Equals(id));
            if (entrys != null)
            {
                _context.RequestReimbursements.RemoveRange(entrys);
                _context.SaveChanges();
            }
            return true;
        }

        public async Task<List<RequestReimbursement>> GetAll()
        {
            var entrys = _context.RequestReimbursements;
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestReimbursement>> GetUnderId(string id)
        {
            var entrys = _context.RequestReimbursements.Where(u => u.RequestID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestReimbursement>> GetUnderParams(string id, int Ordinal)
        {
            var entrys = _context.RequestReimbursements.Where(u => u.RequestID.Equals(id) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, int Ordinal, RequestReimbursement entry)
        {
            var entrys = _context.RequestReimbursements.FirstOrDefault(o => o.RequestID.Equals(id) && o.Ordinal == Ordinal);
            _context.Entry(entrys).CurrentValues.SetValues(entry);
            _context.SaveChanges();
            return true;
        }
    }
}
