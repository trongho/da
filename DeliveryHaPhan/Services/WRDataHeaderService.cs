﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class WRDataHeaderService : IWRDataHeaderService
    {
        private readonly AppDbContext warehouseDbContext;

        public WRDataHeaderService(AppDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataHeaders.FirstOrDefault(g => g.WRDNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<string> checkTotalQuantity(string wRDNumber)
        {
            String light = "off";
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.WRDNumber.Equals(wRDNumber)).FirstOrDefault();
            if (entrys != null)
            {
                if (entrys.TotalQuantity < entrys.TotalQuantityOrg)
                {
                    light = "red";
                }
                if (entrys.TotalQuantity == entrys.TotalQuantityOrg)
                {
                    light = "green";
                }
                if (entrys.TotalQuantity > entrys.TotalQuantityOrg)
                {
                    light = "yellow";
                }
            }
            return light;
        }

        public async Task<bool> Create(WRDataHeader wRDataHeader)
        {
            var entrys = warehouseDbContext.WRDataHeaders.AddAsync(wRDataHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRDataHeaders.FirstOrDefault(o => o.WRDNumber.Equals(id));
            warehouseDbContext.WRDataHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDataHeader>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRDataHeaders.OrderByDescending(x => x.WRDNumber).Take(1).Select(x => x.WRDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRDataHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WRDataHeaders.Where(u => u.WRDNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, WRDataHeader wRDataHeader)
        {
            var entrys = warehouseDbContext.WRDataHeaders.FirstOrDefault(o => o.WRDNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRDataHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
