﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class WRDataGeneralService : IWRDataGeneralService
    {
        private readonly AppDbContext warehouseDbContext;

        public WRDataGeneralService(AppDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<string> checkQuantity(string wRDNumber, DateTime editedDateTime)
        {
            String light = "off";
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber)
            && (u.EditedDateTime.Value.CompareTo(editedDateTime) == 0
            || u.EditedDateTime.Value.AddSeconds(1).CompareTo(editedDateTime) == 0
            || u.EditedDateTime.Value.AddSeconds(-1).CompareTo(editedDateTime) == 0)).FirstOrDefault();

            if (entrys != null)
            {

                if (entrys.Quantity < entrys.QuantityOrg)
                {
                    light = "red";
                }
                if (entrys.Quantity == entrys.QuantityOrg)
                {
                    light = "green";
                }
                if (entrys.Quantity > entrys.QuantityOrg)
                {
                    light = "yellow";
                }
            }
            else
            {
                light = editedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }

            return light;
        }

        public async Task<bool> Create(WRDataGeneral wRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.AddAsync(wRDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(o => o.WRDNumber.Equals(wRDNumber));
            warehouseDbContext.WRDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string goodsID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public IEnumerable<WRDataGeneral> GetUnderIdSignalR(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber));
            return entrys.ToList();
        }

        public IEnumerable<WRDataGeneral> GetUnderIdSignalR(string wRDNumber, string goodsID)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(goodsID));
            return entrys.ToList();
        }

        public async Task<bool> Update(string wRDNumber, string goodsID, int Ordinal, WRDataGeneral wRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(o => o.WRDNumber.Equals(wRDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
