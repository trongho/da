﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{

    public class OrderService : IOrderService
    {
        private readonly AppDbContext context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderService(AppDbContext context, IHostingEnvironment hostingEnvironment)
        {
            this.context = context;
            _hostingEnvironment = hostingEnvironment;
        }


        public async Task<bool> Create(Order order)
        {
            var entrys = context.Orders.AddAsync(order);
            context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Create2(Order order)
        {
            order.OrderID = $"{int.Parse(getOrderID().ToString()):D10}";
            order.EndPoint = await GetLatLng(order.CustomerAdress);
            order.Kilometers = 0;
            var entrys = context.Orders.AddAsync(order);
            context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            context.Orders.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Ended(string id)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            if (entrys != null)
            {
                entrys.Status = "Ended";
                entrys.Result = "Success";
                entrys.StopTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                context.Entry(entrys).CurrentValues.SetValues(entrys);
                context.SaveChanges();
            }
            return true;
        }


        public async Task<bool> Export(List<Order> orders, string fileName)
        {
            bool flag = false;
            string rootFolder = _hostingEnvironment.WebRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));

            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = orders.Count;

                int i = 0;
                for (int row = 5; row <= totalRows + 4; row++)
                {
                    worksheet.Cells[row, 2].Value = orders[i].OrderID;
                    i++;
                }
                package.Save();
                flag = true;

            }
            return flag;
        }

        public async Task<List<Order>> GetAll()
        {
            var entrys = context.Orders;
            return await entrys.ToListAsync();
        }


        public async Task<string> GetLastID()
        {
            var orderID = context.Orders.OrderByDescending(x => x.OrderID).Take(1).Select(x => x.OrderID).ToList().FirstOrDefault();
            return orderID;
        }

        public async Task<List<Order>> GetUnderId(string id)
        {
            var entry = context.Orders.Where(u => u.OrderID.Equals(id));
            return await entry.ToListAsync();
        }


        public async Task<List<Order>> Import(string fileName)
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            List<Order> orders = new List<Order>();


            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                int orderId = getOrderID();
                for (int i = 5; i <= totalRows; i++)
                {
                    String selected = workSheet.Cells[i, 1].Text.Trim();
                    if (selected.Equals("x"))
                    {
                        orders.Add(new Order
                        {
                            OrderID = $"{orderId:D10}",
                            RequiredDate = DateTime.Parse("01-01-2021"),
                            WarehouseID = workSheet.Cells[i, 6].Text.Trim(),
                            ShippingMethod = workSheet.Cells[i, 11].Text.Trim(),
                            OrderValue = Decimal.Parse(workSheet.Cells[i, 12].Text.Trim()),
                            BussinesStaffID = workSheet.Cells[i, 13].Text.Trim(),
                            Note = workSheet.Cells[i, 14].Text.Trim(),
                            ShippingType = "Giao khách",
                            ShippedDate = null,
                            Employee1 = "",
                            Employee2 = "",
                            OrderFee = "Không",
                            StartTime = null,
                            StopTime = null,
                            Mass = 0,
                            Kilometers = 0,
                            Result = "",
                            Reason = "",
                            CustomerAdress = workSheet.Cells[i, 8].Text.Trim(),
                            CustomerName = workSheet.Cells[i, 7].Text.Trim(),
                            CustomerPhone = workSheet.Cells[i, 10].Text.Trim(),
                            CustomerContact = workSheet.Cells[i, 9].Text.Trim(),
                            StartPoint = "",
                            EndPoint = await GetLatLng(workSheet.Cells[i, 8].Text.Trim()),
                            Status = "New",

                        });
                        orderId++;
                    }
                }
                context.Orders.AddRange(orders);
                context.SaveChanges();
                return orders;
            }
        }

        public async Task<string> GetLatLng(string adress)
        {
            String firstString = "https://rsapi.goong.io/geocode?address=";
            String threeString = "&api_key=";
            String APiKey = "R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS";
            var uri = firstString + adress.Replace("/", "%2F") + threeString + APiKey;
            var json = "";
            String latlng = "";
            using (var client = new WebClient())
            {
                json = client.DownloadString(uri);
            }
            dynamic data = JsonConvert.DeserializeObject(json);
            String lng = data.results[0].geometry.location.lng;
            String lat = data.results[0].geometry.location.lat;
            latlng = lat + "," + lng;

            return latlng;
        }

        public int getOrderID()
        {
            int orderID = 0;
            String lastID = context.Orders.OrderByDescending(x => x.OrderID).Take(1).Select(x => x.OrderID).ToList().FirstOrDefault();
            if (lastID == null)
            {
                //orderID =$"{1:D10}";
                orderID = 1;
            }
            else
            {
                orderID = int.Parse(lastID) + 1;
                //orderID =$"{int.Parse(orderID):D10}";
            }
            return orderID;
        }


        public async Task<bool> Update(string id, Order order)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            context.Entry(entrys).CurrentValues.SetValues(order);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> UploadFile(IFormFile file)
        {
            string path = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile/"+file.FileName);

            var form = new MultipartFormDataContent();
            using (var fileStream = file.OpenReadStream())
            {
                //string path = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile/"+file.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create,FileAccess.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }
                form.Add(new StreamContent(fileStream), "file", file.FileName);


                //List<Order> orders = await Import(file.FileName);
                //foreach (Order order in orders)
                //{
                //    if (await Create(order) == true)
                //    {
                //        await Export(orders, file.FileName);
                //    }
                //}
            }
            return true;
        }

        public async Task<bool> CancelOrder(string id, string reason)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.Status = "Cancel";
            entrys.Result = "Failure";
            entrys.Reason = reason;
            context.Entry(entrys).CurrentValues.SetValues(reason);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Start(string id, string point,string adress, decimal kilometersOrg)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.StartPoint = point;
            entrys.CurrentPoint = point;
            entrys.StartAdress = adress;
            entrys.KilometersOrg = kilometersOrg;
            entrys.Status = "Started";
            entrys.StartTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            entrys.ShippedDate= DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd"));
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();

            return true;
        }
        public async Task<bool> Continue(string id, string point)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));


            //string dt = DateTimeOffset.Now.ToString("ddMMyyyy");
            //string t = DateTimeOffset.Now.ToString("HHmmss");

            //string path = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}");

            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}

            //string filePath = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}/{dt}.txt");


            //using (StreamWriter w = File.AppendText(filePath))
            //{
            //    w.WriteLine(t + "," + entrys.Employee1 + " tiếp tục đơn hàng " + id + ";");
            //}

            entrys.CurrentPoint = point;
            entrys.Status = "Continue";
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();

           

            return true;
        }

        public async Task<bool> Pause(string id, string point, decimal kilometers)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));


            //string dt = DateTimeOffset.Now.ToString("ddMMyyyy");
            //string t = DateTimeOffset.Now.ToString("HHmmss");

            //string path = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}");

            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}

            //string filePath = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}/{dt}.txt");
            

            //using (StreamWriter w = File.AppendText(filePath))
            //{
            //    w.WriteLine(t+","+entrys.Employee1+" tạm dừng đơn hàng "+id+";");
            //}

            entrys.Status = "Pause";
            entrys.CurrentPoint = point;
            entrys.Kilometers = entrys.Kilometers + kilometers;
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> End(string id, decimal kilometers)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.Status = "Ended";
            entrys.Result = "Success";
            entrys.Kilometers = entrys.Kilometers + kilometers;
            entrys.StopTime = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();

            return true;
        }

        private string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }

        public string getFilePath()
        {
            return System.IO.Path.Combine(_hostingEnvironment.WebRootPath + "Excel/");
        }

        public async Task<bool> UpdateStartPoint(string id, string startPoint)
        {
            var entrys = context.Orders.Where(e => e.OrderID.Equals(id)).FirstOrDefault();
            entrys.StartPoint = startPoint;
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Order>> GetUnderEmployee(string employee)
        {
            var entry = context.Orders.Where(u => u.Employee1.Equals(employee)&&(u.Status.Equals("Approved")|| u.Status.Equals("Started")|| u.Status.Equals("Pause") || u.Status.Equals("Continue")));
            return await entry.ToListAsync();
        }

        public async Task<bool> UpdateCurrentPoint(string id, string currentpoint)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.CurrentPoint = currentpoint;
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> UpdateEndPoint(string id, string endpoint,string adress)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.EndPointUpdate = endpoint;
            entrys.CustomerAdressUpdate = adress;

            if (!entrys.EndPoint.Contains("wrong"))
            {
                entrys.EndPoint = entrys.EndPoint + "(wrong)";
            }
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<string> GetCurrentPoint(string id)
        {
            String currentPoint = context.Orders.Where(o=>o.OrderID.Equals(id)).Select(x => x.CurrentPoint).ToList().FirstOrDefault();
            return currentPoint;
        }

        public async Task<List<User>> GetUsers()
        {
            var entrys = context.Users.Where(u=>u.Active==true&&u.Role.Equals("User"));
            return await entrys.ToListAsync();
        }

        public int CountOrdersUnderStatus(string status)
        {
            var entry = context.Orders.Where(u =>status.Contains(u.Status));
            return entry.Count();
        }


        public async Task<List<Order>> GetActiveOrders(string status)
        {
            var s = status.ToArray().ToList();
            var entry = context.Orders.Where(u =>status.Contains(u.Status));
            return await entry.ToListAsync();
        }

        public async Task<decimal?> GetSumKilometersByDate(DateTime date)
        {
            var result = context.Orders.Where(u=>u.ShippedDate==date).Sum(g => g.Kilometers);
            return result;
        }

        public async Task<List<KilometersData>> GetKilometers()
        {
            List<KilometersData> kilometerss = new List<KilometersData>();
            kilometerss = context.Orders.ToList().GroupBy(g=>g.RequiredDate).Select(s=>new KilometersData
            {
                Year= DateTime.Parse(s.First().RequiredDate.ToString()).Year,
                Month= DateTime.Parse(s.First().RequiredDate.ToString()).Month,
                Day= DateTime.Parse(s.First().RequiredDate.ToString()).Day,
                Kilometers= (decimal)s.Sum(sum=>sum.Kilometers),
            }).ToList();           
            return kilometerss;
            
        }

        public async Task<List<KilometersData>> GetCurrentYearKilometers()
        {
            List<KilometersData> kilometerss = new List<KilometersData>();
            int currentYear = DateTime.UtcNow.Year;
            kilometerss = context.Orders.Where(u => u.RequiredDate.Value.Year==currentYear).ToList().GroupBy(g => g.RequiredDate).Select(s => new KilometersData
            {
                Year = DateTime.Parse(s.First().RequiredDate.ToString()).Year,
                Month = DateTime.Parse(s.First().RequiredDate.ToString()).Month,
                Day = DateTime.Parse(s.First().RequiredDate.ToString()).Day,
                Kilometers = (decimal)s.Sum(sum => sum.Kilometers),
            }).ToList();
            return kilometerss;
        }

        public async Task<List<KilometersData>> GetCurrentYearKilometersWithEmployee()
        {
            List<KilometersData> kilometerss = new List<KilometersData>();
            int currentYear = DateTime.UtcNow.Year;
            kilometerss = context.Orders.Where(u =>u.ShippedDate!=null&&u.ShippedDate.Value.Year == currentYear&&u.Employee1!="").ToList().GroupBy(g =>g.Employee1).Select(s => new KilometersData
            {
                Year= s.First().ShippedDate.Value.Year,
                Month = DateTime.Parse(s.First().ShippedDate.ToString()).Month,
                Employee1 =s.First().Employee1,
                LicensePlates= s.First().Vehice,
                Kilometers = (decimal)s.Sum(sum => sum.Kilometers),
            }).ToList();
            return kilometerss;
        }

        public async Task<List<StatusStatistic>> GetCurrentYearStatusStatistic()
        {
            List<StatusStatistic> statusStatistics = new List<StatusStatistic>();
            int currentYear = DateTime.UtcNow.Year;
            statusStatistics = context.Orders.Where(u => u.RequiredDate.Value.Year == currentYear).ToList().GroupBy(g => g.Status).Select(s => new StatusStatistic
            {
                Status = s.First().Status,
                Count=s.Count(),
            }).ToList();
            return statusStatistics;
        }

        public async Task<string> GetLog(DateTime date,string filterString)
        {
            StringBuilder builtString = new StringBuilder();
            string dt = date.ToString("ddMMyyyy");
            string filePath = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}/{dt}.txt");

            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(filterString)||filterString.Equals("all"))
                    {
                        builtString.Insert(0, line + "\n");
                    }
                }
            }

            return builtString.ToString();
        }

        public async Task<List<OrderLog>> GetLog2(DateTime date)
        {
            StringBuilder builtString = new StringBuilder();
            string dt = date.ToString("ddMMyyyy");
            string filePath = Path.Combine(_hostingEnvironment.WebRootPath, $"Logs/{dt}/{dt}.txt");
            List<OrderLog> orderLogs = new List<OrderLog>();

            using (StreamReader reader = new StreamReader(filePath))
            {
                var arr = reader.ReadToEnd().Split(';');
                if (arr.Length > 0)
                {
                    arr.Reverse();
                    for(int i=1;i<arr.Length;i++)
                    {
                        var itemArr = arr[i].Split(',');
                        OrderLog orderLog = new OrderLog();
                        orderLog.id = i;
                        orderLog.logDate =itemArr[0].Replace("\r","").Replace("\n","");
                        orderLog.actionName =itemArr[1];
                        orderLogs.Add(orderLog);
                        i++;
                    }
                }
            }

            return orderLogs;
        }

        public async Task<List<KilometersData>> GetCurrentYearKilometersOfEmployee(string employee1)
        {
            List<KilometersData> kilometerss = new List<KilometersData>();
            int currentYear = DateTime.UtcNow.Year;
            kilometerss = context.Orders.Where(u =>u.ShippedDate!=null&& u.Employee1.Equals(employee1) && u.ShippedDate.Value.Year == currentYear).ToList().Select(s => new KilometersData
            {
                Year = s.ShippedDate.Value.Year,
                Month = s.ShippedDate.Value.Month,
                Day = s.ShippedDate.Value.Day,
                Employee1 = s.Employee1,
                LicensePlates = s.Vehice,
                Kilometers = s.Kilometers,
            }).ToList();
            return kilometerss;
        }

        public async Task<List<Order>> GetOrdersByEmployeeAndStatus(string employee1, string status)
        {
            var entry = context.Orders.Where(u =>u.Employee1.Equals(employee1)&&status.Contains(u.Status));
            return await entry.ToListAsync();
        }

        public async Task<decimal> GetSumKilometersByCurrentMonth()
        {
            decimal kilometers;
            int currentMonth = DateTime.UtcNow.Month;
            int currentYear = DateTime.UtcNow.Year;
            kilometers = (decimal)context.Orders.Where(u => u.ShippedDate.Value.Month == currentMonth&& u.ShippedDate.Value.Year ==currentYear).Sum(g => g.Kilometers);
            return kilometers;
        }

        public async Task<bool> UpdateMass(string id, decimal mass)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.Mass =mass;
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> UpdateSurcharge(string id, decimal surcharge)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.Surcharge = surcharge;
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }


        public async Task<bool> DeleteMore(string idString)
        {
            var entrys = context.Orders.FirstOrDefault(o =>o.OrderID==idString);           
            context.Orders.Remove(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Assign(string id,AssignOrder assignOrder)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            entrys.Employee1 = assignOrder.Employee1;
            entrys.Employee2 = assignOrder.Employee2;
            entrys.ShippingType =assignOrder.ShippingType;
            entrys.OrderFee = assignOrder.OrderFee;
            entrys.Vehice =assignOrder.Vehice;
            entrys.Status = "Approved";
            context.Entry(entrys).CurrentValues.SetValues(entrys);
            context.SaveChanges();
            return true;
        }

        public async Task<bool> Update2(string id, OrderModel orderModel)
        {
            var entrys = context.Orders.FirstOrDefault(o => o.OrderID.Equals(id));
            orderModel.OrderID = id;
            context.Entry(entrys).CurrentValues.SetValues(orderModel);
            context.SaveChanges();
            return true;
        }

        public async Task<List<Order>> GetActiveOrdersByThisMonth(string status)
        {
            int currentMonth = DateTime.UtcNow.Month;
            int currentYear = DateTime.UtcNow.Year;
            var entry = context.Orders.Where(u => status.Contains(u.Status)&&u.ShippedDate.Value.Month==currentMonth && u.ShippedDate.Value.Year == currentYear);
            return await entry.ToListAsync();
        }

        public async Task<List<Order>> GetNewOrdersByThisMonth(string status)
        {
            int currentMonth = DateTime.UtcNow.Month;
            int currentYear = DateTime.UtcNow.Year;
            var entry = context.Orders.Where(u => status.Contains(u.Status) && u.RequiredDate.Value.Month == currentMonth && u.RequiredDate.Value.Year == currentYear);
            return await entry.ToListAsync();
        }

        public async Task<string> GetEndPoint(string orderID)
        {
            var entry = context.Orders.Where(u => u.OrderID.Equals(orderID)).Select(x => x.EndPoint).ToList().FirstOrDefault();
            return entry;
        }

        public async Task<List<YearData>> GetYearData()
        {
            var entry = context.Orders;
            List<DateTime?> years = entry.Select(s => s.RequiredDate).ToList();
            List<YearData> yearDatas = years.GroupBy(s=>s.Value.Year).Select(s => new YearData
            {
                Name=s.Key+"",
            }).ToList();
            return yearDatas;
        }
    }
}
