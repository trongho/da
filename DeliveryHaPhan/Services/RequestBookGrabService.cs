﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class RequestBookGrabService : IRequestBookGrabService
    {
        private readonly AppDbContext _context;

        public RequestBookGrabService(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<bool> Create(RequestBookGrab entry)
        {
            var entrys = _context.RequestBookGrabs.AddAsync(entry);
            _context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = _context.RequestBookGrabs.Where(o => o.RequestID.Equals(id));
            if (entrys != null)
            {
                _context.RequestBookGrabs.RemoveRange(entrys);
                _context.SaveChanges();
            }
            return true;
        }

        public async Task<List<RequestBookGrab>> GetAll()
        {
            var entrys = _context.RequestBookGrabs;
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestBookGrab>> GetUnderId(string id)
        {
            var entrys = _context.RequestBookGrabs.Where(u => u.RequestID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestBookGrab>> GetUnderParams(string id, int Ordinal)
        {
            var entrys = _context.RequestBookGrabs.Where(u => u.RequestID.Equals(id) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, int Ordinal, RequestBookGrab entry)
        {
            var entrys = _context.RequestBookGrabs.FirstOrDefault(o => o.RequestID.Equals(id) && o.Ordinal == Ordinal);
            _context.Entry(entrys).CurrentValues.SetValues(entry);
            _context.SaveChanges();
            return true;
        }
    }
}
