﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class OrderResultService: IOrderResultService
    {
        private readonly AppDbContext context;

        public OrderResultService(AppDbContext context)
        {
            this.context = context;
        }

        public async Task<List<OrderResult>> GetAll()
        {
            var entrys = context.OrderResults;
            return await entrys.ToListAsync();
        }
    }
}
