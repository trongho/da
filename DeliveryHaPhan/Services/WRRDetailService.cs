﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class WRRDetailService : IWRRDetailService
    {
        private readonly AppDbContext warehouseDbContext;

        public WRRDetailService(AppDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }


        public async Task<bool> Create(WRRDetail wRRDetail)
        {
            var entrys = warehouseDbContext.WRRDetails.AddAsync(wRRDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(o => o.WRRNumber.Equals(id));
            warehouseDbContext.WRRDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRRDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WRRDetails;
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wRRNumber, String goodsID, int Ordinal, WRRDetail wRRDetail)
        {
            var entrys = warehouseDbContext.WRRDetails.FirstOrDefault(o => o.WRRNumber.Equals(wRRNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRRDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
