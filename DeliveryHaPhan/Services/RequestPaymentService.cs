﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Services
{
    public class RequestPaymentService : IRequestPaymentService
    {
        private readonly AppDbContext _context;

        public RequestPaymentService(AppDbContext context)
        {
            this._context = context;
        }

        public async Task<bool> Create(RequestPayment entry)
        {
            var entrys = _context.RequestPayments.AddAsync(entry);
            _context.SaveChanges();
            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = _context.RequestPayments.Where(o => o.RequestID.Equals(id));
            if (entrys != null)
            {
                _context.RequestPayments.RemoveRange(entrys);
                _context.SaveChanges();
            }
            return true;
        }

        public async Task<List<RequestPayment>> GetAll()
        {
            var entrys = _context.RequestPayments;
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestPayment>> GetUnderId(string id)
        {
            var entrys = _context.RequestPayments.Where(u => u.RequestID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<List<RequestPayment>> GetUnderParams(string id, int Ordinal)
        {
            var entrys = _context.RequestPayments.Where(u => u.RequestID.Equals(id) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, int Ordinal, RequestPayment entry)
        {
            var entrys = _context.RequestPayments.FirstOrDefault(o => o.RequestID.Equals(id) && o.Ordinal == Ordinal);
            _context.Entry(entrys).CurrentValues.SetValues(entry);
            _context.SaveChanges();
            return true;
        }
    }
}
