﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class UserNewHelper
    {
        public static List<UserNewModel> Coverts(List<UserNew> users)
        {
            var userModels = users.ConvertAll(user => new UserNewModel
            {
                UserID = user.UserID,
                UserName = user.UserName,
                Position=user.Position,
                Department=user.Department,
                Rank=user.Rank,
                Adress = user.Adress,
                Phone = user.Phone,
                Email = user.Email,
                FullName = user.FullName,
                Password = user.Password,
                PasswordSalt = user.PasswordSalt,
                CreatedUserID = user.CreatedUserID,
                CreatedDate = user.CreatedDate,
                UpdatedUserID = user.UpdatedUserID,
                UpdatedDate = user.UpdatedDate,
                Active = user.Active,
                Blocked = user.Blocked,
                Role = user.Role,
                Avatar = user.Avatar,
                LastLogin = user.LastLogin,
            });

            return userModels;
        }
    }
}
