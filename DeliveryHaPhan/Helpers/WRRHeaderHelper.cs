﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class WRRHeaderHelper
    {
        public static List<WRRHeaderModel> Covert(List<WRRHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRRHeaderModel
            {
                WRRNumber = sc.WRRNumber,
                WRRDate = sc.WRRDate,
                ReferenceNumber = sc.ReferenceNumber,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
