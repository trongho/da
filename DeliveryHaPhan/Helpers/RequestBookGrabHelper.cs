﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class RequestBookGrabHelper
    {
        public static List<RequestBookGrabModel> Covert(List<RequestBookGrab> entrys)
        {
            var models = entrys.ConvertAll(sc => new RequestBookGrabModel
            {
                RequestID = sc.RequestID,
                Ordinal=sc.Ordinal,
                RequestContent = sc.RequestContent,
                BookType=sc.BookType,
                StartAdress=sc.StartAdress,
                EndAdress=sc.EndAdress,
                ExecutionDate=sc.ExecutionDate,
                VehiceType=sc.VehiceType,
            });

            return models;
        }
    }
}
