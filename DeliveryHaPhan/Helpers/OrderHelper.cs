﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class OrderHelper
    {
        public static List<OrderModel> Covert(List<Order> entrys)
        {
            var models = entrys.ConvertAll(sc => new OrderModel
            {
                OrderID=sc.OrderID,
                ShippingType = sc.ShippingType,
                Employee1 =sc.Employee1,
                Employee2 = sc.Employee2,
                RequiredDate = sc.RequiredDate,
                ShippedDate = sc.ShippedDate,
                OrderFee = sc.OrderFee,
                StartTime = sc.StartTime,
                StopTime = sc.StopTime,
                OrderValue = sc.OrderValue,
                Mass = sc.Mass,
                KilometersOrg=sc.KilometersOrg,
                Kilometers = sc.Kilometers,
                Result =sc.Result,
                Reason = sc.Reason,
                ShippingMethod = sc.ShippingMethod,
                BussinesStaffID = sc.BussinesStaffID,
                StartPoint = sc.StartPoint,
                EndPoint = sc.EndPoint,
                CurrentPoint=sc.CurrentPoint,
                WarehouseID = sc.WarehouseID,
                Note = sc.Note,
                Status = sc.Status,
                CustomerName=sc.CustomerName,
                CustomerAdress=sc.CustomerAdress,
                CustomerPhone=sc.CustomerPhone,
                CustomerContact=sc.CustomerContact,
                Vehice=sc.Vehice,
                ExportAdress=sc.ExportAdress,
                EndPointUpdate=sc.EndPointUpdate,
                EndAdress=sc.EndAdress,
                CustomerAdressUpdate=sc.CustomerAdressUpdate,
                StartAdress=sc.StartAdress,
                Surcharge=sc.Surcharge
            });

            return models;
        }
    }
}
