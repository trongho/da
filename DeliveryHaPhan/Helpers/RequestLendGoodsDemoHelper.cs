﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class RequestLendGoodsDemoHelper
    {
        public static List<RequestLendGoodsDemoModel> Covert(List<RequestLendGoodsDemo> entrys)
        {
            var models = entrys.ConvertAll(sc => new RequestLendGoodsDemoModel
            {
                RequestID = sc.RequestID,
                Ordinal=sc.Ordinal,
                LendTime=sc.LendTime,
                Reason = sc.Reason,
                Description=sc.Description,
                GoodsID=sc.GoodsID,
                Serial=sc.Serial,
                Quantity=sc.Quantity,
                Note=sc.Note,
            });

            return models;
        }
    }
}
