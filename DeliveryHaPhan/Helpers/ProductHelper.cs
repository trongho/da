﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class ProductHelper
    {
        public static List<ProductModel> Covert(List<Product> e)
        {
            var userModels = e.ConvertAll(e => new ProductModel
            {
                ProductID=e.ProductID,
                ProductName=e.ProductName,
                Origin=e.Origin,
                Uses=e.Uses,
                UserManual=e.UserManual,
                Infomation=e.Infomation,
                Images=e.Images,
                Status=e.Status,
            });

            return userModels;
        }
    }
}
