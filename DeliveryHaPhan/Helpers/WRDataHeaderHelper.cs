﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class WRDataHeaderHelper
    {
        public static List<WRDataHeaderModel> Covert(List<WRDataHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRDataHeaderModel
            {
                WRDNumber = sc.WRDNumber,
                WRDDate = sc.WRDDate,
                ReferenceNumber = sc.ReferenceNumber,
                WRRNumber = sc.WRRNumber,
                WRRReference = sc.WRRReference,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
