﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class RequestHelper
    {
        public static List<RequestModel> Covert(List<Request> entrys)
        {
            var models = entrys.ConvertAll(sc => new RequestModel
            {
                RequestID = sc.RequestID,
                RequestType = sc.RequestType,
                RequestTitle = sc.RequestTitle,
                CreateDate = sc.CreateDate,
                CreateName = sc.CreateName,
                CreateEmail = sc.CreateEmail,
                CreateDepartment = sc.CreateDepartment,
                Status=sc.Status,
                ProcessingTime=sc.ProcessingTime,
                Approvers=sc.Approvers,
                Followers=sc.Followers,
            });

            return models;
        }
    }
}
