﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class GoodsDataHelper
    {
        public static List<GoodsDataModel> Covert(List<GoodsData> entrys)
        {
            var models = entrys.ConvertAll(sc => new GoodsDataModel
            {
                GoodsID = sc.GoodsID,
                ECNPart = sc.ECNPart,
                GoodsName = sc.GoodsName,
                GoodsNameEN = sc.GoodsNameEN,
                Description = sc.Description,
                Status = sc.Status,
                SLPart = sc.SLPart,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
