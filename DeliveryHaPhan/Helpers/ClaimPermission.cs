﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public static class ClaimPermission
    {
        public const string
            CanAddNewOrder = "Thêm đơn hàng",
            CanDeleteOrder = "Xóa đơn hàng",
            CanManageUser = "Quản lý người dùng";
    }
}
