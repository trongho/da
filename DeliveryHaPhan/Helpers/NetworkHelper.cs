﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class NetworkHelper
    {
        public static byte[] BitmapToByteArray(Bitmap bitmap)
        {

            //BitmapData bmpdata = null;

            //try
            //{
            //    bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            //    int numbytes = bmpdata.Stride * bitmap.Height;
            //    byte[] bytedata = new byte[numbytes];
            //    IntPtr ptr = bmpdata.Scan0;

            //    Marshal.Copy(ptr, bytedata, 0, numbytes);

            //    return bytedata;
            //}
            //finally
            //{
            //    if (bmpdata != null)
            //        bitmap.UnlockBits(bmpdata);
            //}

            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

        }
    }
}
