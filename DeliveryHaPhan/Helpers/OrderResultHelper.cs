﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class OrderResultHelper
    {
        public static List<OrderResultModel> Covert(List<OrderResult> entrys)
        {
            var models = entrys.ConvertAll( sc=> new OrderResultModel
            {
                ResultID=sc.ResultID,
                ResultName =sc.ResultName,
            });

            return models;
        }
    }
}
