﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class RequestReimbursementHelper
    {
        public static List<RequestReimbursementModel> Covert(List<RequestReimbursement> entrys)
        {
            var models = entrys.ConvertAll(sc => new RequestReimbursementModel
            {
                RequestID = sc.RequestID,
                Ordinal = sc.Ordinal,
                RequestNumber = sc.RequestNumber,
                AdvanceAmount = sc.AdvanceAmount,
                RemainingAmount = sc.RemainingAmount,
                ExcessCash = sc.ExcessCash,
                Currency = sc.Currency,
                TotalAmount = sc.TotalAmount,
                Payment = sc.Payment,
                TimeOfPayment = sc.TimeOfPayment,
                CustomerName = sc.CustomerName,
                BeneficiaryAccount = sc.BeneficiaryAccount,
                Beneficiary = sc.Beneficiary,
                BankAt = sc.BankAt,
                TradingDepartment = sc.TradingDepartment,
                AccountingPayment = sc.AccountingPayment,
                ChiefAccountant = sc.ChiefAccountant,
                InspectionStaff = sc.InspectionStaff,
                LineManager = sc.LineManager,
                PaymentContent = sc.PaymentContent,
                Amount = sc.Amount,
                DocumentsNumber = sc.DocumentsNumber,
                Note = sc.Note,
            });

            return models;
        }
    }
}
