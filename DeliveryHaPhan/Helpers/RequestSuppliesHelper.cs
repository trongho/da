﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class RequestSuppliesHelper
    {
        public static List<RequestSuppliesModel> Covert(List<RequestSupplies> entrys)
        {
            var models = entrys.ConvertAll(sc => new RequestSuppliesModel
            {
                RequestID = sc.RequestID,
                Ordinal=sc.Ordinal,
                RequestContent=sc.RequestContent,
                Reason=sc.Reason,
                Goods = sc.Goods,
                Specification = sc.Specification,
                Quantity = sc.Quantity,
                Note = sc.Note,
            });

            return models;
        }
    }
}
