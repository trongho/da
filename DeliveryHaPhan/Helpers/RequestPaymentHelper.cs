﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Helpers
{
    public class RequestPaymentHelper
    {
        public static List<RequestPaymentModel> Covert(List<RequestPayment> entrys)
        {
            var models = entrys.ConvertAll(sc => new RequestPaymentModel
            {
                RequestID = sc.RequestID,
                Ordinal=sc.Ordinal,
                RequestNumber=sc.RequestNumber,
                Currency=sc.Currency,
                TotalAmount=sc.TotalAmount,
                Payment=sc.Payment,
                TimeOfPayment=sc.TimeOfPayment,
                CustomerName=sc.CustomerName,
                BeneficiaryAccount=sc.BeneficiaryAccount,
                Beneficiary=sc.Beneficiary,
                BankAt=sc.BankAt,
                TradingDepartment=sc.TradingDepartment,
                PaymentContent = sc.PaymentContent,
                Amount = sc.Amount,
                DocumentsNumber = sc.DocumentsNumber,
                Note = sc.Note,
            });

            return models;
        }
    }
}
