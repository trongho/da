#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\UserManager.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5412f874754357bcebb2c870391c14e2646571c2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.ReportApprove.Views_ReportApprove_UserManager), @"mvc.1.0.view", @"/Views/ReportApprove/UserManager.cshtml")]
namespace DeliveryHaPhan.Models.ReportApprove
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\UserManager.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\UserManager.cshtml"
using DeliveryHaPhan.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5412f874754357bcebb2c870391c14e2646571c2", @"/Views/ReportApprove/UserManager.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_ReportApprove_UserManager : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\UserManager.cshtml"
  
    ViewData["Title"] = "Quản lý người dùng";
    Layout = "~/Views/Shared/_LayoutReportApprove.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<style>
    .left {
        float: right;
    }

    #gridContainer {
        height: 440px;
    }

    .header-toolbar {
        background-color: #e7e9ed;
    }
</style>

<script>
    DevAV.userManagers = DevAV.userManagers || {};
</script>

<div>
    ");
#nullable restore
#line 27 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\UserManager.cshtml"
Write(Html.DevExtreme().Toolbar()
        .ElementAttr("class","header-toolbar")
        .Items(items => {

            items.Add()
                    .Widget(w => w
                    .Button()
                    .Icon("save")
                    .Text("Lưu")
                    .UseSubmitBehavior(false)
                    .OnClick("DevAV.userManagers.createUser")
                    
                )
                .LocateInMenu(ToolbarItemLocateInMenuMode.Auto)
                .Location(ToolbarItemLocation.Before);

            items.Add()
                    .Widget(w => w
                    .Button()
                    .Icon("upload")
                    .Text("Nhập dữ liệu")
                    .UseSubmitBehavior(false)
                    .OnClick("DevAV.userManagers.uploadFile")
                )
                .LocateInMenu(ToolbarItemLocateInMenuMode.Auto)
                .Location(ToolbarItemLocation.Before);
        })
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n\r\n<div>\r\n    <div class=\"grid row mt-1\">\r\n        ");
#nullable restore
#line 59 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\UserManager.cshtml"
    Write(Html.DevExtreme().DataGrid<UserNewModel>()
                .ID("gridContainer")
                .ShowBorders(true)
                .ShowRowLines(true)
                .Scrolling(scrolling => scrolling.Mode(GridScrollingMode.Infinite))
                .RemoteOperations(true)
                .Sorting(sorting => sorting.Mode(GridSortingMode.None))
                .Editing(e => e.Mode(GridEditMode.Popup)
                    .AllowAdding(true)
                    .AllowUpdating(true)
                    .AllowDeleting(true)
                )
                .Columns(c => {
                c.Add().Caption("No.").CellTemplate(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n                                <p><%- rowIndex+1 %></p>\r\n                            ");
    PopWriter();
}
));
                    c.AddFor(m => m.UserID).Caption("Mã người dùng");
                    c.AddFor(m => m.UserName).Caption("Tên đăng nhập");
                    c.AddFor(m => m.FullName).Caption("Họ tên");
                    c.AddFor(m => m.Position).Caption("Chức vụ");
                    c.AddFor(m => m.Email).Caption("Email");
                    c.AddFor(m => m.Rank).Caption("Cấp bậc");
                    c.AddFor(m => m.Department).Caption("Bộ phận");

                    c.AddFor(m => m.Adress).Caption("Địa chỉ").Visible(false);
                    c.AddFor(m => m.Phone).Caption("Số điện thoại").Visible(false);
                    c.AddFor(m => m.Password).Caption("Mật khẩu").Visible(false);
                    c.AddFor(m => m.PasswordSalt).Caption("Khóa mật khẩu").Visible(false);
                    c.AddFor(m => m.CreatedUserID).Caption("Người tạo").Visible(false);
                    c.AddFor(m => m.CreatedDate).Caption("Ngày tạo").Visible(false);
                    c.AddFor(m => m.UpdatedUserID).Caption("Người cập nhật").Visible(false);
                    c.AddFor(m => m.UpdatedDate).Caption("Ngày cập nhật").Visible(false);
                    c.AddFor(m => m.Active).Caption("Đã kích hoạt").Visible(false);
                    c.AddFor(m => m.Blocked).Caption("Đã khóa").Visible(false);
                    c.AddFor(m => m.Role).Caption("Phân quyền").Visible(false);
                    c.AddFor(m => m.Avatar).Caption("Ảnh đại diện").Visible(false);
                    c.AddFor(m => m.LastLogin).Caption("Lần đăng nhập cuối").Visible(false);
                })
                .DataSource(d => d.Mvc()
                .Controller("UserNewApi")
                .LoadAction("Get")
                .InsertAction("Post")
                .UpdateAction("Put")
                .DeleteAction("Delete")
                .Key("UserID")
                .OnUpdated(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n                             function (e) {\r\n                                DevExpress.ui.notify(\"Cập nhật người dùng thành công\",\"success\", 600);\r\n                            }\r\n                        ");
    PopWriter();
}
))
                .OnInserted(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n                            function (e) {\r\n                                DevExpress.ui.notify(\"Thêm người dùng thành công+\",\"success\", 600);\r\n                            }\r\n                        ");
    PopWriter();
}
))
                .OnRemoved(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n                            function (e) {\r\n                                DevExpress.ui.notify(\"Xóa người dùng thành công\",\"success\", 600);\r\n                            }\r\n                        ");
    PopWriter();
}
))
            )
        );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n</div>\r\n\r\n<script>\r\n    \r\n</script>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
