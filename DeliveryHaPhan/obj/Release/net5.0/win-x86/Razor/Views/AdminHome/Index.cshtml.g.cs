#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\AdminHome\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0ae7f8eb2965987771fe76139dc4e50573ac7b8e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.AdminHome.Views_AdminHome_Index), @"mvc.1.0.view", @"/Views/AdminHome/Index.cshtml")]
namespace DeliveryHaPhan.Models.AdminHome
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\DeliveryApp\da\DeliveryHaPhan\Views\AdminHome\Index.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0ae7f8eb2965987771fe76139dc4e50573ac7b8e", @"/Views/AdminHome/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_AdminHome_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\AdminHome\Index.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    .card-counter {
        box-shadow: 2px 2px 10px #DADADA;
        margin: 5px;
        padding: 20px 10px;
        background-color: #fff;
        height: 100px;
        border-radius: 5px;
        transition: .3s linear all;
    }

        .card-counter:hover {
            box-shadow: 4px 4px 20px #DADADA;
            transition: .3s linear all;
        }

        .card-counter.primary {
            background-color: #007bff;
            color: #FFF;
        }

        .card-counter.danger {
            background-color: #ef5350;
            color: #FFF;
        }

        .card-counter.success {
            background-color: #66bb6a;
            color: #FFF;
        }

        .card-counter.info {
            background-color: #26c6da;
            color: #FFF;
        }

        .card-counter i {
            font-size: 5em;
            opacity: 0.2;
        }

        .card-counter .count-numbers {
            position: absolute;
            right: 35");
            WriteLiteral(@"px;
            top: 20px;
            font-size: 32px;
            display: block;
        }

        .card-counter .count-name {
            position: absolute;
            right: 35px;
            top: 65px;
            font-style: italic;
            text-transform: capitalize;
            opacity: 0.5;
            display: block;
            font-size: 18px;
        }
</style>

<div class=""container"">
    <div class=""row"">
        <div class=""col-md-3"">
");
            WriteLiteral(@"            <a id=""approved-click"">
                <div class=""card-counter primary"">
                    <i class=""fa fa-code-fork""></i>
                    <span class=""count-numbers"" id=""orders-approved""></span>
                    <span class=""count-name"">Đơn hàng đang giao</span>
                </div>
            </a>
        </div>


        <div class=""col-md-3"">
            <div class=""card-counter danger"">
                <i class=""fa fa-ticket""></i>
                <span class=""count-numbers"">10</span>
                <span class=""count-name"">Đơn hàng tạm dừng</span>
            </div>
        </div>

        <div class=""col-md-3"">
            <div class=""card-counter success"">
                <i class=""fa fa-database""></i>
                <span class=""count-numbers"">2</span>
                <span class=""count-name"">Nhân viên đang giao hàng</span>
            </div>
        </div>

        <div class=""col-md-3"">
            <div class=""card-counter info"">
              ");
            WriteLiteral(@"  <i class=""fa fa-users""></i>
                <span class=""count-numbers"">35</span>
                <span class=""count-name"">Tổng kết tháng</span>
            </div>
        </div>
    </div>
</div>
<script>
    //đơn hàng đang giao
    $.getJSON(""AdminHome/CountOrdersUnderStatus/Approved"",
        function (data) {
            $(""#orders-approved"").text(data);
        });
    //click
    $('#approved-click').click(function () {
        window.location.href='");
#nullable restore
#line 117 "D:\DeliveryApp\da\DeliveryHaPhan\Views\AdminHome\Index.cshtml"
                         Write(Url.Action("RedirectToApprovedOrder", "AdminHome"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\';\r\n    });\r\n</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
