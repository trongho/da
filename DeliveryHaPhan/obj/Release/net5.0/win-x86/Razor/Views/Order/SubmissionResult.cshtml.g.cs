#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "68b32c8955052545fcb50e6f3eaf4a112bfdbfde"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.Order.Views_Order_SubmissionResult), @"mvc.1.0.view", @"/Views/Order/SubmissionResult.cshtml")]
namespace DeliveryHaPhan.Models.Order
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"68b32c8955052545fcb50e6f3eaf4a112bfdbfde", @"/Views/Order/SubmissionResult.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_Order_SubmissionResult : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<h3>Data submitted</h3>\r\n\r\n<br />\r\n<p><b>First name:</b> ");
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml"
                 Write(ViewBag.FirstName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<p><b>Last name:</b> ");
#nullable restore
#line 5 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml"
                Write(ViewBag.LastName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<p><b>Photo file name:</b> ");
#nullable restore
#line 6 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml"
                      Write(ViewBag.Photo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<p><b>Photo file name:</b> ");
#nullable restore
#line 7 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml"
                      Write(ViewBag.Url);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n<br />\r\n<div>\r\n    <img");
            BeginWriteAttribute("src", " src=\"", 240, "\"", 258, 1);
#nullable restore
#line 10 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml"
WriteAttributeValue("", 246, ViewBag.Url, 246, 12, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 259, "\"", 265, 0);
            EndWriteAttribute();
            WriteLiteral("/>\r\n</div>\r\n\r\n");
#nullable restore
#line 13 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Order\SubmissionResult.cshtml"
Write(Html.DevExtreme().Button()
    .Text("Reload demo")
    .Type(ButtonType.Default)
    .Icon("refresh")
    .OnClick(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n        function() {\r\n            window.location = window.location;\r\n        }\r\n    ");
    PopWriter();
}
))
);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
