#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "970cc8b196561bcb900a3334f6d3183962baf642"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.User.Views_User_ChangePasswordAdmin), @"mvc.1.0.view", @"/Views/User/ChangePasswordAdmin.cshtml")]
namespace DeliveryHaPhan.Models.User
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
using DeliveryHaPhan.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"970cc8b196561bcb900a3334f6d3183962baf642", @"/Views/User/ChangePasswordAdmin.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_User_ChangePasswordAdmin : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
  
    ViewData["Title"] = "Index";
    Layout = "~/Views/Shared/_AdminLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 7 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
 using (Html.BeginForm("ChangePassword", "User", FormMethod.Post, new { id = "changepassword" }))
{

    using (Html.DevExtreme().ValidationGroup())
    {

        

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
   Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
    Write(Html.DevExtreme().Form<ChangePassword>()
            .ShowValidationSummary(true)
            .Items(items => {

                items.AddGroup()
                    .Caption("Đổi mật khẩu")
                    .Items(groupItems => {
                        groupItems.AddSimpleFor(m => m.UserID).Visible(false);
                        groupItems.AddSimpleFor(m => m.OldPassword)
                            .Editor(e => e.TextBox().Mode(TextBoxMode.Password));
                        groupItems.AddSimpleFor(m => m.NewPassword)
                            .Editor(e => e.TextBox().Mode(TextBoxMode.Password));
                        groupItems.AddSimpleFor(m => m.ConfirmPassword)
                            .Editor(e => e.TextBox().Mode(TextBoxMode.Password));
                    });

                items.AddButton()
                    .HorizontalAlignment(HorizontalAlignment.Left)

                    .ButtonOptions(b => b.Text("Save")
                        .Type(ButtonType.Success)
                        .UseSubmitBehavior(true)
                        .ID("changepassword")
                        .OnClick("callback")

                );

            })
            .FormData(Model)
        );

#line default
#line hidden
#nullable disable
#nullable restore
#line 44 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
         
    }
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<script>\r\n    function callback() {\r\n        if (\"");
#nullable restore
#line 50 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
        Write(ViewBag.Result);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" != null) {\r\n            var type = \"");
#nullable restore
#line 51 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
                   Write(ViewBag.Result);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" ? \"success\" : \"error\",\r\n                text = \"");
#nullable restore
#line 52 "D:\DeliveryApp\da\DeliveryHaPhan\Views\User\ChangePasswordAdmin.cshtml"
                   Write(ViewBag.Result);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" ? \"success\" : \"error\";\r\n            DevExpress.ui.notify(text, type, 600);\r\n        }\r\n    }\r\n</script>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
