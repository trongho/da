﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.Employees = function () {
    var grid;
    var currentEmployee;
    var currentOrder;
    var employeePopup;
    const url = 'http://localhost:61292/Order';

    this.sectionsData = [{
        id: 0,
        text: "Order",
        html: "<div id='order-grid'></div>"
    }];

    var formSelector = "#details-form";

    this.tabItemRendered = function (e) {
        //if (!currentOrder) return;      
        //$("#order-grid").dxDataGrid("instance").option("dataSource", gridDataOrderSource);
    };

    var updateEmployeePanel = function () {
        var imageUrl = currentEmployee.Avatar ? "avatar/" + currentEmployee.Avatar : "image/noimage.png";
        $("#employee-name").text(currentEmployee.FullName);
        $("#employee-title").text(currentEmployee.Role);
        $("#employee-image").css("background-image", "url('" + imageUrl + "')");
        $("#employee-adress").text(currentEmployee.Adress);
        $("#employee-phone").text(currentEmployee.Phone);
        $("#employee-mail").text(currentEmployee.Email);
    };

    var fetchOrderAssignedToEmployee = function () {
        $.getJSON("Order/GetByEmployee?employee=" + currentEmployee.FullName,
            function (d) {
                currentOrder = d;
            })
    };

    var gridDataOrderSource = new DevExpress.data.CustomStore({
        key: "OrderID",
        load: function () {
            return $.getJSON("Order/GetByEmployee?employee=" + currentEmployee.FullName);
        }
    });


    this.selectRow = function (data) {
        var rowData = data.selectedRowsData[0];
        if (!rowData) return;
        $("#employee-image")
            .css("background-image", "url('../image/loading.gif')");
        currentEmployee = rowData;

        //var $gridOrder = $("<div id='order-grid'>");
        //$gridOrder.dxDataGrid({
        //    dataSource: gridDataOrderSource,
        //});

        $("#order-grid").dxDataGrid("instance").option("dataSource", gridDataOrderSource);  
        updateEmployeePanel();
        
    };

    this.selectFirstRow = function () {
        grid.selectRowsByIndexes([0]);
    };

    this.exporting = function (e) {
        var workbook = new ExcelJS.Workbook();
        var worksheet = workbook.addWorksheet('Employees');

        DevExpress.excelExporter.exportDataGrid({
            component: e.component,
            worksheet: worksheet,
            autoFilterEnabled: true
        }).then(function () {
            workbook.xlsx.writeBuffer().then(function (buffer) {
                saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Employees.xlsx');
            });
        });
        e.cancel = true;
    }

    var showDetailsPopup = function () {
        var labelText;
        if ($("#UserID").dxTextBox("instance").option("value") != "") {
            labelText = "Sửa người dùng (" + $("#FullName").dxTextBox("instance").option("value") + ")";
        } else {
            labelText = "Thêm người dùng";
        }
        $("#details-form-popup").dxPopup("instance").option("title", labelText);

        var imageUrl
        if ($("#Avatar").dxTextBox("instance").option("value") !="") {
            imageUrl = "avatar/" + $("#Avatar").dxTextBox("instance").option("value");
            $("#user-image").css("background-image", "url('" + imageUrl + "')");
        }
        else {
            imageUrl = "avatar/noimage.png";
            $("#user-image").css("background-image", "url('" + imageUrl + "')");
        }
    };

    this.editEmployee = function () {    
        showDetailsPopup();
    };


    var updatePassword = function (e, result) {
        $.ajax({
            url: "User/AdminChangePassword?userid=" + e.row.data.UserID,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ NewPassword: result.value.NewPassword, ConfirmPassword: result.value.ConfirmPassword }),
            success: function (data) {
                Swal.fire({ title: 'Thành công', text: 'Cập nhật thành công mật khẩu ', icon: 'success', timer: 1500 })
            },
            error: function (error) {
                Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể cập nhật', icon: 'error', timer: 1500 })
            }
        });
    };

    this.popupChangePassword = function (e) {
        Swal.fire({
            title: 'Nhập mật khẩu',
            html: `<input type="password" id="NewPassword" class="swal2-input" placeholder="New Password">
                <input type="password" id="ConfirmPassword" class="swal2-input" placeholder="Confirm Password">`,
            confirmButtonText: 'OK',
            focusConfirm: false,
            preConfirm: () => {
                const NewPassword = Swal.getPopup().querySelector('#NewPassword').value
                const ConfirmPassword = Swal.getPopup().querySelector('#ConfirmPassword').value
                if (!NewPassword || !ConfirmPassword) {
                    Swal.showValidationMessage(`Không được để trống`)
                }
                if (NewPassword != ConfirmPassword) {
                    Swal.showValidationMessage(`Mật khẩu nhập lại không giống`)
                }
                return { NewPassword: NewPassword, ConfirmPassword: ConfirmPassword }
            }
        }).then((result) => {
            updatePassword(e, result);
        });
    };


    this.init = function () {
        employeePopup = $("#details-form-popup").dxPopup("instance");
        grid = $("#gridContainer").dxDataGrid("instance");
    };
};

DevAV.employees = new DevAV.Employees();

$(function () {
    DevAV.employees.init();
});