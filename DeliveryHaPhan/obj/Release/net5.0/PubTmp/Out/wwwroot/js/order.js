﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.Orderr = function () {
    var grid;
    
    const mapAPIToken = 'R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS';

    //var getLoadPanelInstance = function () {
    //    return $("#loadPanel2").dxLoadPanel("instance");
    //};

    //this.loadPanel_shown2 = function (e) {
    //    setTimeout(function () {
    //        e.component.hide();
    //    }, 1000);
    //};

    //this.loadPanel_hidden2 = function (e) {

    //};

    this.selectionChanged = function (selectedItems) {
        var data = selectedItems.selectedRowsData;

        if (data.length > 0) {
            $("#selectedItemsContainer").text(
                data
                    .map((value) => `${value.OrderID}`)
                    .join(",")
            );
        } else {
            $("#selectedItemsContainer").text("Nobody has been selected");
        }

        $("#btn-delete-multi").dxButton("instance").option('disabled', !data.length);
    };

    this.deleteSelectedRow = function () {
        var orderIDString = $("#selectedItemsContainer").text();
        var arr = orderIDString.split(',')
        Swal.fire({
            title: 'Bạn có muốn xóa những đơn hàng đã chọn ' + '[' + orderIDString + '] ?',
            showDenyButton: true,
            confirmButtonText: 'Có',
            denyButtonText: 'Không',
        }).then((result) => {
            if (result.isConfirmed) {
                for (let i = 0; i < arr.length; i++) {
                    $.ajax({
                        url: "Order/DeleteMore?key=" + arr[i],
                        type: 'DELETE',
                        data: {},
                        success: function (data) {
                            Swal.fire({
                                title: 'Thành công', text: 'Xóa thành công ' + arr.length + ' đơn hàng ' + '[' + orderIDString + ']', icon: 'success', timer: 1500,
                                backdrop: `
                                    rgba(0,0,123,0.2)
                                    url("/image/nyan-cat.gif")
                                    top center
                                    no-repeat
                                  `
                            })
                            grid.refresh();
                        },
                        error: function (error) {
                            Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể xóa', icon: 'error', timer: 2000 })
                        }
                    });
                }
            }
        })
    };

    this.uploadFile = async function () {
        const { value: file } = await Swal.fire({
            title: 'Select excel file',
            input: 'file',
            inputAttributes: {
                'accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'aria-label': 'Tải lên file excel của bạn'
            },
            preConfirm: (inp) => {
                if (!inp) {
                    Swal.showValidationMessage(`Chưa chọn file!!!`)
                }
            }
        })

        

        if (file) {    
            getLoadPanelInstance().show();

            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                url: "Order/UploadFile",
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,

                success: function (data) {
                    getLoadPanelInstance().hide();

                    Swal.fire({ title: 'Thành công', text: 'Tải lên thành công file ' + file.name + " có tổng số đơn hàng là:" + Object.keys(data).length, icon: 'success', timer: 2000 });
                    grid.refresh();    

                    $.ajax({
                        url: "Order/DownloadFile/" + file.name,
                        type: 'GET',
                        xhrFields: {
                            responseType: 'blob'
                        },
                        contentType: 'application/octetstream',
                        success: function (res) {
                            var a = document.createElement('a');
                            var url = window.URL.createObjectURL(res);
                            a.href = url;
                            a.download = file.name;
                            document.body.append(a);
                            a.click();
                            a.remove();
                            window.URL.revokeObjectURL(url);
                        }
                    });
                },
                error: function (err) {
                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tải lên ' + err.statusText, icon: 'error', timer: 2000 })
                }
            });
        }
    };

    //this.upload = function () {
    //    var form = $('#fileUploadForm')[0];
    //    var data = new FormData(form);
    //    $.ajax({
    //        url: "Order/UploadFile",
    //        type: 'POST',
    //        enctype: 'multipart/form-data',
    //        processData: false,
    //        contentType: false,
    //        cache: false,
    //        data: data,
    //        success: function (data) {
    //            Swal.fire({
    //                title: 'Thành công', text: 'Tải lên dữ liệu thành công ', icon: 'success', timer: 1500,
    //                backdrop: `
    //                                rgba(0,0,123,0.2)
    //                                url("/image/nyan-cat.gif")
    //                                top center
    //                                no-repeat
    //                              `
    //            })
    //            grid.refresh();
    //        },
    //        error: function (error) {
    //            Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể xóa', icon: 'error', timer: 2000 })
    //        }
    //    });
    //};

    this.exporting = function (e) {
        var workbook = new ExcelJS.Workbook();
        var worksheet = workbook.addWorksheet('Orders');

        DevExpress.excelExporter.exportDataGrid({
            component: e.component,
            worksheet: worksheet,
            autoFilterEnabled: true
        }).then(function () {
            workbook.xlsx.writeBuffer().then(function (buffer) {
                saveAs(new Blob([buffer], { type: 'application/octet-stream' }), 'Orders.xlsx');
            });
        });
        e.cancel = true;
    };

    this.refreshGrid = function () {
        grid.getDataSource().reload();
    };

    function getLoadPanelInstance() {
        return $("#loadPanel2").dxLoadPanel("instance");
    }

    this.popupAddSurcharge = function (e) {
        Swal.fire({
            title: 'Nhập phụ phí(VNĐ)',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'OK',
            showLoaderOnConfirm: true,
            preConfirm: (inp) => {
                if (!inp) {
                    Swal.showValidationMessage(
                        '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                    )
                }
                if (isNaN(inp)) {
                    Swal.showValidationMessage(
                        '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                    )
                }
                return { surcharge: inp, }
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            $.ajax({
                url: "Order/UpdateSurcharge/" + e.row.data.OrderID + "?surcharge=" + result.value.surcharge,
                type: 'PUT',
                data: {},
                success: function (data) {
                    Swal.fire({ title: 'Thành công', text: 'Cập nhật thành công phụ phí là ' + result.value.surcharge, icon: 'success', timer: 1500 })
                    grid.refresh();
                },
                error: function (error) {
                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể cập nhật', icon: 'error', timer: 1500 })
                }
            });
        });
    };

    this.autocompleteEndPoint = function () {
        
        var data;
        var latlng;
        var latlngArr;
        var geocoder = null;
        if (geocoder == null) {

            geocoder = new GoongGeocoder({
                accessToken: mapAPIToken
            });

            geocoder.addTo('#FindEndAdress');
            var results = $("#EndPointUpdate").dxTextBox("instance");
            var endAdress = $("#EndAdress").dxTextBox("instance");

            // Add geocoder result to container.
            geocoder.on('result', function (e) {
                data = JSON.parse(JSON.stringify(e.result, null, 2));
                var formatted_address = data.result.formatted_address;
                var lat = data.result.geometry.location.lat;
                var lng = data.result.geometry.location.lng;
                latlng = lat + "," + lng;
                latlngArr = latlng.split(',').reverse();

                results.option("value", latlng);
                endAdress.option("value", formatted_address);
            });

            // Clear results container when search is cleared.
            geocoder.on('clear', function () {
                results.innerText = '';
            });

        }
    };


    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.orderr = new DevAV.Orderr();

$(function () {
    DevAV.orderr.init();
});