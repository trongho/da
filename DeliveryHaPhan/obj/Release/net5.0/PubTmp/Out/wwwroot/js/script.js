﻿"use strict";

window.DevAV = window.DevAV || {};

DevAV.formatShortDate = function (date) {
    return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear().toString().substring(2);
};

    

DevAV.screenByWidth = function () {
    return "lg";
};

DevAV.isFormValid = function (selector) {
    var form = $(selector).dxForm("instance");
    return form.validate().isValid;
};

DevAV.sleep = async function (msec) {
    return new Promise(resolve => setTimeout(resolve, msec));
};