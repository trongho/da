﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.UserOrders = function () {
    var grid;

    this.popupEditMass = function (e) {
        Swal.fire({
            title: 'Nhập khối lượng đơn hàng(kg)',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'OK',
            showLoaderOnConfirm: true,
            preConfirm: (inp) => {
                if (!inp) {
                    Swal.showValidationMessage(
                        '<i class="fa fa-info-circle"></i> Không được để trống!!!'
                    )
                }
                if (isNaN(inp)) {
                    Swal.showValidationMessage(
                        '<i class="fa fa-info-circle"></i> Không phải là số!!!'
                    )
                }
                return { mass: inp, }
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            $.ajax({
                url: "UserOrder/UpdateMass/" + e.row.data.OrderID + "?mass=" + result.value.mass,
                type: 'PUT',
                data: {},
                success: function (data) {
                    Swal.fire({ title: 'Thành công', text: 'Cập nhật thành công khối lượng ' + result.value.mass, icon: 'success', timer: 1500 })
                    grid.refresh();
                },
                error: function (error) {
                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể cập nhật', icon: 'error', timer: 1500 })
                }
            });
        });
    };

   

    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.userOrders = new DevAV.UserOrders();

$(function () {
    DevAV.userOrders.init();
});