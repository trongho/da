﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.ReportManagers = function () {
    var grid;

    this.onRowPrepared = function (e) {
        if (e.rowType == 'data' && e.data.Status == "Approved") {
            e.rowElement[0].style.color = 'red';
        }
        else if (e.rowType == 'data' && e.data.Status == "Refuse") {
            e.rowElement[0].style.backgroundColor = 'red';
            e.rowElement[0].style.color = 'white';
        }
    };

    this.selectBox_requestType_valueChanged = function (data) {
        if (data.value == "Tất cả")
            grid.clearFilter();
        else
            grid.filter(["RequestType", "=", data.value]);
    };

    this.filterDataByText = function (searchData) {
        grid.searchByText(searchData.value);
    };


    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.reportManagers = new DevAV.ReportManagers();

$(function () {
    DevAV.reportManagers.init();
});