﻿var department = [
    { "id": 0,"category":"Hồ Chí Minh","text": "Phòng kỹ thuật HCM" },
    { "id": 1, "category": "Hồ Chí Minh", "text": "Phòng kinh doanh HCM" },
    { "id": 2, "category": "Hồ Chí Minh", "text": "Phòng kế toán HCM" },
    { "id": 3, "category": "Hồ Chí Minh", "text": "Phòng nhân sự HCM" },
    { "id": 4, "category": "Hồ Chí Minh", "text": "Phòng sale admin HCM" },
    { "id": 0, "category": "Hà Nội", "text": "Phòng kỹ thuật HN" },
    { "id": 1, "category": "Hà Nội", "text": "Phòng kinh doanh HN" },
    { "id": 2, "category": "Hà Nội", "text": "Phòng kế toán HN" },
    { "id": 3, "category": "Hà Nội", "text": "Phòng nhân sự HN" },
    { "id": 4, "category": "Hà Nội", "text": "Phòng sale admin HN" },

];