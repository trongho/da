﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestPayments = function () {
    var grid;
    var formSelector = "#request-payment-form";
    var requestid;
    var requesttitle, requesttype, createname, createemail, createdepartment, createdate, processingtime, approvers, followers;
    var totalamount, payment, requestNumber, currency, timeOfPayment, customername, beneficiary, beneficiaryaccount, bankat, tradingDepartment;
    var paymentcontent, amount, documentsnumber, note;

    this.createNewRequest = function () {
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = new Date();
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        totalamount = parseFloat($("#amount").dxTextBox("instance").option("value"));
        payment = $("#payment").dxSelectBox("instance").option("value").toString();

        requestNumber = $("#request-number").dxTextBox("instance").option("value");
        currency = $("#currency").dxSelectBox("instance").option("value");
        timeOfPayment = $("#time-of-payment").dxDateBox("instance").option("value");
        customername = $("#customer-name").dxTextBox("instance").option("value");
        beneficiary = $("#beneficiary").dxTextBox("instance").option("value").toString().toString();
        beneficiaryaccount = $("#beneficiary-account").dxTextBox("instance").option("value");
        bankat = $("#bank-at").dxTextBox("instance").option("value");
        tradingDepartment = $("#trading-department").dxTextBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        createRequest();

    };

    var createRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Create",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                createRequestPayment(data.RequestID);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var createRequestPayment = function (requestID) {
        var table = $('#payment-table').DataTable();
        var ordinal = 0;
        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var d = this.data();
            paymentcontent = d.PaymentContent;
            amount = d.Amount;
            documentsnumber = d.DocumentsNumber;
            note = d.Note;
            ordinal += 1;

            $.ajax({
                url: "/api/RequestPaymentApi/Create",
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    RequestID: requestID,
                    Ordinal: ordinal,
                    TotalAmount: totalamount,
                    Payment: payment,
                    RequestNumber: requestNumber,
                    Currency: currency,
                    TimeOfPayment: timeOfPayment,
                    CustomerName: customername,
                    Beneficiary: beneficiary,
                    BeneficiaryAccount: beneficiaryaccount,
                    BankAt: bankat,
                    TradingDepartment: tradingDepartment,
                    PaymentContent: paymentcontent,
                    Amount: amount,
                    DocumentsNumber: documentsnumber,
                    Note: note,
                }),
                beforeSend: function (request) {
                    request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                },
                success: function (data) {
                    const popup = $("#popup-create-request-payment").dxPopup("instance");
                    popup.hide();
                    DevExpress.ui.notify('Tạo thành công tờ trình đề nghị thanh toán', 'success', 4000);
                },
                error: function (xhr) {
                    DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                }
            });
        });
    };



    this.editRequest = function () {
        requestid = $("#request-id").dxTextBox("instance").option("value");;
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = $("#create-date").dxDateBox("instance").option("value");
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        totalamount = parseFloat($("#amount").dxTextBox("instance").option("value"));
        payment = $("#payment").dxSelectBox("instance").option("value").toString();

        requestNumber = $("#request-number").dxTextBox("instance").option("value");
        currency = $("#currency").dxSelectBox("instance").option("value");
        timeOfPayment = $("#time-of-payment").dxDateBox("instance").option("value");
        customername = $("#customer-name").dxTextBox("instance").option("value");
        beneficiary = $("#beneficiary").dxTextBox("instance").option("value").toString().toString();
        beneficiaryaccount = $("#beneficiary-account").dxTextBox("instance").option("value");
        bankat = $("#bank-at").dxTextBox("instance").option("value");
        tradingDepartment = $("#trading-department").dxTextBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        editRequest();
    };

    var editRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Update?id=" + requestid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestID: requestid,
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                editRequestPayment();
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var editRequestPayment = function () {
        $.ajax({
            url: "/api/RequestPaymentApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                var table = $('#payment-table').DataTable();
                var ordinal = 0;

                table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var d = this.data();
                    paymentcontent = d.PaymentContent;
                    amount = d.Amount;
                    documentsnumber = d.DocumentsNumber;
                    note = d.Note;
                    ordinal += 1;
                    $.ajax({
                        url: "/api/RequestPaymentApi/Create",
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify({
                            RequestID: requestid,
                            Ordinal: ordinal,
                            TotalAmount: totalamount,
                            Payment: payment,
                            RequestNumber: requestNumber,
                            Currency: currency,
                            TimeOfPayment: timeOfPayment,
                            CustomerName: customername,
                            Beneficiary: beneficiary,
                            BeneficiaryAccount: beneficiaryaccount,
                            BankAt: bankat,
                            TradingDepartment: tradingDepartment,
                            PaymentContent: paymentcontent,
                            Amount: amount,
                            DocumentsNumber: documentsnumber,
                            Note: note,
                        }),
                        beforeSend: function (request) {
                            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                        },
                        success: function (data) {
                            const popup = $("#popup-edit-request-payment").dxPopup("instance");
                            popup.hide();
                            grid.refresh();
                            DevExpress.ui.notify('Cập nhập thành công tờ trình đề nghị thanh toán', 'success', 4000);
                        },
                        error: function (xhr) {
                            DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                        }
                    });
                });
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };


    this.deleteRequest = function (e) {
        let timerInterval
        Swal.fire({
            title: 'Bạn có muốn xóa đề xuất này',
            toast: true,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa!',
            cancelButtonText: 'Không!',
            html: '<strong></strong> seconds<br/><br/>',
            timer: 10000,
            didOpen: () => {
                const content = Swal.getHtmlContainer()
                const $ = content.querySelector.bind(content)

                timerInterval = setInterval(() => {
                    Swal.getHtmlContainer().querySelector('strong')
                        .textContent = (Swal.getTimerLeft() / 1000)
                            .toFixed(0)
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.isConfirmed) {
                if (e.row.data.Status == "Waiting") {
                    deleteRequest(e.row.data.RequestID);
                }
                else {
                    Swal.fire('Alert!!!', 'Không thể xóa đề xuất', 'error', 2000);
                }

            }
        })
    };

    var deleteRequest = function (requestID) {
        $.ajax({
            url: "/api/RequestApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                deleteRequestPayment(requestID);
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestPayment = function (requestID) {
        $.ajax({
            url: "/api/RequestPaymentApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                Swal.fire('Success!', 'Xóa thành công', 'success', 2000);
                grid.refresh();
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };


    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.requestPayments = new DevAV.RequestPayments();

$(function () {
    DevAV.requestPayments.init();
});