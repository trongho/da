﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestDetailPopups = function () {
    var status, requestType;

    var formatDate = new Intl.DateTimeFormat("en-US").format;

    this.openPopupDetail = function (e) {      
        if (e.RequestType === "Đề xuất công tác") {
            const popup = $("#popup-report-ct-detail").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-report-ct-detail-template"),
            });
            popup.show();
            fetchReportCTData(e.RequestID);
        }
        else if (e.RequestType === "Đề xuất công vụ") {
            const popup2 = $("#popup-report-cv-detail").dxPopup("instance");
            popup2.option({
                contentTemplate: $("#popup-report-cv-detail-template"),
            });
            popup2.show();
            fetchReportCVData(e.RequestID);
        }
        
    };

    this.openPopupDetailMyReport = function (e) {
        if (e.row.data.RequestType === "Đề xuất công tác") {
            const popup = $("#popup-report-ct-detail").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-report-ct-detail-template"),
            });
            popup.show();
            fetchReportCTData(e.row.data.RequestID);
        }
        else if (e.row.data.RequestType === "Đề xuất công vụ") {
            const popup2 = $("#popup-report-cv-detail").dxPopup("instance");
            popup2.option({
                contentTemplate: $("#popup-report-cv-detail-template"),
            });
            popup2.show();
            fetchReportCVData(e.row.data.RequestID);
        }
    };

    var fetchReportCTData = function (requestID) {
        $.getJSON("/api/RequestApi/GetByID?id=" + requestID,
            function (result) {
                $("#request-id-detail-ct").dxTextBox("instance").option("value", result.data[0].RequestID);
                $("#status-detail-ct").dxTextBox("instance").option("value", result.data[0].Status);
                $("#request-title-detail-ct").dxTextBox("instance").option("value", result.data[0].RequestTitle);
                $("#request-type-detail-ct").dxSelectBox("instance").option("value", result.data[0].RequestType);
                $("#customer-name-detail-ct").dxTextBox("instance").option("value", result.data[0].CustomerName);
                $("#amount-detail-ct").dxTextBox("instance").option("value", result.data[0].Amount);
                $("#payment-detail-ct").dxSelectBox("instance").option("value", result.data[0].Payment);
                $("#approvers-detail-ct").dxSelectBox("instance").option("value", result.data[0].Approvers.split(',').slice(-1).pop());
                $("#followers-detail-ct").dxTagBox("instance").option("value", result.data[0].Followers.split(','));
                $("#create-name-detail-ct").dxTextBox("instance").option("value", result.data[0].CreateName);
                $("#create-date-detail-ct").dxDateBox("instance").option("value", result.data[0].CreateDate);
                $("#processing-time-detail-ct").dxDateBox("instance").option("value", result.data[0].ProcessingTime);

                $("#customer-id-detail-ct").dxTextBox("instance").option("value", result.data[0].CustomerID);
                $("#customer-adress-detail-ct").dxTextBox("instance").option("value", result.data[0].CustomerAdress);
                $("#execution-date-detail-ct").dxDateBox("instance").option("value", result.data[0].ExecutionDate);
                $("#business-staff-id-detail-ct").dxTextBox("instance").option("value", result.data[0].BusinessStaffID);
                $("#vehice-detail-ct").dxSelectBox("instance").option("value", result.data[0].Vehice);
                $("#vehicle-owner-detail-ct").dxTextBox("instance").option("value", result.data[0].VehicleOwner);
                $("#order-id-detail-ct").dxTextBox("instance").option("value", result.data[0].OrderID);
                $("#order-value-detail-ct").dxTextBox("instance").option("value", result.data[0].OrderValue);
                $("#business-fee-detail-ct").dxTextBox("instance").option("value", result.data[0].BusinessFee);

                status = $("#status-detail-ct").dxTextBox("instance").option("value");
                requestType = $("#request-type-detail-ct").dxSelectBox("instance").option("value");
            });

       
    };

    var fetchReportCVData = function (requestID) {
        $.getJSON("/api/RequestApi/GetByID?id=" + requestID,
            function (result) {
                $("#request-id-detail-cv").dxTextBox("instance").option("value", result.data[0].RequestID);
                $("#status-detail-cv").dxTextBox("instance").option("value", result.data[0].Status);
                $("#request-title-detail-cv").dxTextBox("instance").option("value", result.data[0].RequestTitle);
                $("#request-type-detail-cv").dxSelectBox("instance").option("value", result.data[0].RequestType);
                $("#customer-name-detail-cv").dxTextBox("instance").option("value", result.data[0].CustomerName);
                $("#amount-detail-cv").dxTextBox("instance").option("value", result.data[0].Amount);
                $("#payment-detail-cv").dxSelectBox("instance").option("value", result.data[0].Payment);
                $("#approvers-detail-cv").dxSelectBox("instance").option("value", result.data[0].Approvers.split(',').slice(-1).pop());
                $("#followers-detail-cv").dxTagBox("instance").option("value", result.data[0].Followers.split(','));
                $("#create-name-detail-cv").dxTextBox("instance").option("value", result.data[0].CreateName);
                $("#create-date-detail-cv").dxDateBox("instance").option("value", result.data[0].CreateDate);
                $("#processing-time-detail-cv").dxDateBox("instance").option("value", result.data[0].ProcessingTime);

                $("#request-content-detail-cv").dxTextBox("instance").option("value", result.data[0].RequestContent);
                $("#accompanying-documents-detail-cv").dxTextBox("instance").option("value", result.data[0].AccompanyingDocuments);
                $("#payment-place-detail-cv").dxTextBox("instance").option("value", result.data[0].PaymentPlace);
                $("#beneficiary-detail-cv").dxTextBox("instance").option("value", result.data[0].Beneficiary);
                $("#beneficiary-account-detail-cv").dxTextBox("instance").option("value", result.data[0].BeneficiaryAccount);
                $("#bank-at-detail-cv").dxTextBox("instance").option("value", result.data[0].BankAt);

                status = $("#status-detail-cv").dxTextBox("instance").option("value");
                requestType = $("#request-type-detail-cv").dxSelectBox("instance").option("value");
            });

        
    };

    //this.openPrintPopup = function (e) {
        

    //    if (status === 'Waiting' || status === 'Forwarding') {
    //        const Toast = Swal.mixin({
    //            toast: true,
    //            position: 'top-right',
    //            iconColor: 'white',
    //            customClass: {
    //                popup: 'colored-toast'
    //            },
    //            showConfirmButton: false,
    //            timer: 1500,
    //            timerProgressBar: true
    //        });

    //        Toast.fire({
    //            icon: 'warning',
    //            title: 'Đề xuất đang chờ duyệt, không thể in'
    //        });
    //        return;
    //    }
    //    if (status === 'Refuse') {
    //        const Toast2 = Swal.mixin({
    //            toast: true,
    //            position: 'top-right',
    //            iconColor: 'white',
    //            customClass: {
    //                popup: 'colored-toast'
    //            },
    //            showConfirmButton: false,
    //            timer: 1500,
    //            timerProgressBar: true
    //        });

    //        Toast2.fire({
    //            icon: 'error',
    //            title: 'Đề xuất đã bị từ chối, không thể in'
    //        });
    //        return;
    //    }

    //    if (requestType === "Đề xuất công tác") {
    //        const popup = $("#popup-preview").dxPopup("instance");
    //        popup.option({
    //            contentTemplate: $("#popup-preview-ct-template"),
    //        });
    //        popup.show();
    //        updateRichTextCT();
    //    }
    //    else if (requestType === "Đề xuất công vụ") {
    //        const popup2 = $("#popup-preview").dxPopup("instance");
    //        popup2.option({
    //            contentTemplate: $("#popup-preview-cv-template"),
    //        });
    //        popup2.show();
    //        updateRichTextCV();
    //    }
    //};

    var updateRichTextCT = function () {

        var results = [{
            CreateName: $("#create-name-detail-ct").dxTextBox("instance").option("value"),
            CreateEmail: $("#create-email-detail-ct").dxTextBox("instance").option("value"),
            CreateDepartment: $("#create-department-detail-ct").dxTextBox("instance").option("value"),
            Approver: $("#approvers-detail-ct").dxSelectBox("instance").option("value"),
            CreateDate: formatDate(new Date($("#create-date-detail-ct").dxDateBox("instance").option("value"))),
            CustomerName: $("#customer-name-detail-ct").dxTextBox("instance").option("value"),
            CustomerID: $("#customer-id-detail-ct").dxTextBox("instance").option("value"),
            CustomerAdress: $("#customer-adress-detail-ct").dxTextBox("instance").option("value"),
            Vehice: $("#vehice-detail-ct").dxSelectBox("instance").option("value"),
            VehicleOwner: $("#vehicle-owner-detail-ct").dxTextBox("instance").option("value"),
            ExecutionDate: formatDate(new Date($("#execution-date-detail-ct").dxDateBox("instance").option("value"))),
            Amount: $("#amount-detail-ct").dxTextBox("instance").option("value"),
            BusinessFee: $("#business-fee-detail-ct").dxTextBox("instance").option("value"),
            Payment: $("#payment-detail-ct").dxSelectBox("instance").option("value"),
            OrderID: $("#order-id-detail-ct").dxTextBox("instance").option("value"),
            BusinessStaffID: $("#business-staff-id-detail-ct").dxTextBox("instance").option("value"),
            OrderValue: $("#order-value-detail-ct").dxTextBox("instance").option("value"),
            Printer: $("#printer-detail-ct").dxTextBox("instance").option("value"),
            AmountToWord: "(" + VNnum2words(Number($("#amount-detail-ct").dxTextBox("instance").option("value"))) + " VND)",
        }];
        var selector = function (dataItem) {
            return {
                CreateName: dataItem.CreateName == null ? "" : dataItem.CreateName,
                CreateEmail: dataItem.CreateEmail == null ? "" : dataItem.CreateEmail ,
                CreateDepartment: dataItem.CreateDepartment == null ? "" : dataItem.CreateDepartment,
                Approver: dataItem.Approver == null ? "" : dataItem.Approver ,
                CreateDate: dataItem.CreateDate == null ? "" : dataItem.CreateDate ,
                CustomerName: dataItem.CustomerName == null ? "" : dataItem.CustomerName ,
                CustomerID: dataItem.CustomerID == null ? "" : dataItem.CustomerID ,
                CustomerAdress: dataItem.CustomerAdress == null ? "" : dataItem.CustomerAdress ,
                Vehice: dataItem.Vehice == null ? "" : dataItem.Vehice ,
                VehicleOwner: dataItem.VehicleOwner == null ? "" : dataItem.VehicleOwner ,
                ExecutionDate: dataItem.ExecutionDate == null ? "" : dataItem.ExecutionDate ,
                Amount: dataItem.Amount == null ? "" : dataItem.Amount ,
                BusinessFee: dataItem.BusinessFee == null ? "" : dataItem.BusinessFee,
                Payment: dataItem.Payment == null ? "" : dataItem.Payment,
                OrderID: dataItem.OrderID == null ? "" : dataItem.OrderID ,
                BusinessStaffID: dataItem.BusinessStaffID == null ? "" : dataItem.BusinessStaffID,
                OrderValue: dataItem.OrderValue == null ? "" : dataItem.OrderValue,
                Printer: dataItem.Printer == null ? "" : dataItem.Printer,
                AmountToWord: dataItem.AmountToWord == null ? "" : dataItem.AmountToWord ,
            };
        };
        var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
        richEdit.mailMergeOptions.setDataSource(newDataSource);
    };

    var updateRichTextCV = function () {
        var results = [{
            CreateName: $("#create-name-detail-cv").dxTextBox("instance").option("value"),
            CreateEmail: $("#create-email-detail-cv").dxTextBox("instance").option("value"),
            CreateDepartment: $("#create-department-detail-cv").dxTextBox("instance").option("value"),
            Approver: $("#approvers-detail-cv").dxSelectBox("instance").option("value"),
            CreateDate: formatDate(new Date($("#create-date-detail-cv").dxDateBox("instance").option("value"))),
            CustomerName: $("#customer-name-detail-cv").dxTextBox("instance").option("value"),
            Amount: $("#amount-detail-cv").dxTextBox("instance").option("value"),
            Payment: $("#payment-detail-cv").dxSelectBox("instance").option("value"),
            Printer: $("#printer-detail-cv").dxTextBox("instance").option("value"),
            AmountToWord: "(" + VNnum2words(Number($("#amount-detail-cv").dxTextBox("instance").option("value"))) + " VND)",

            RequestContent: $("#request-content-detail-cv").dxTextBox("instance").option("value"),
            AccompanyingDocuments: $("#accompanying-documents-detail-cv").dxTextBox("instance").option("value"),
            PaymentPlace: $("#payment-place-detail-cv").dxTextBox("instance").option("value"),
            Beneficiary: $("#beneficiary-detail-cv").dxTextBox("instance").option("value"),
            BeneficiaryAccount: $("#beneficiary-account-detail-cv").dxTextBox("instance").option("value"),
            BankAt: $("#bank-at-detail-cv").dxTextBox("instance").option("value"),

        }];
        var selector = function (dataItem) {
            return {
                CreateName: dataItem.CreateName == null ? "" : dataItem.CreateName,
                CreateEmail: dataItem.CreateEmail == null ? "" : dataItem.CreateEmail ,
                CreateDepartment: dataItem.CreateDepartment == null ? "" : dataItem.CreateDepartment ,
                Approver: dataItem.Approver == null ? "" : dataItem.Approver ,
                CreateDate: dataItem.CreateDate == null ? "" : dataItem.CreateDate,
                CustomerName: dataItem.CustomerName == null ? "" : dataItem.CustomerName,
                Amount: dataItem.Amount == null ? "" : dataItem.Amount ,
                Payment: dataItem.Payment == null ? "" : dataItem.Payment ,
                Printer: dataItem.Printer == null ? "" : dataItem.Printer,
                AmountToWord: dataItem.AmountToWord == null ? "" : dataItem.AmountToWord ,

                RequestContent: dataItem.RequestContent == null ? "" : dataItem.RequestContent ,
                AccompanyingDocuments: dataItem.AccompanyingDocuments == null ? "" : dataItem.AccompanyingDocuments,
                PaymentPlace: dataItem.PaymentPlace == null ? "" : dataItem.PaymentPlace ,
                Beneficiary: dataItem.Beneficiary == null ? "" : dataItem.Beneficiary,
                BeneficiaryAccount: dataItem.BeneficiaryAccount == null ? "" : dataItem.BeneficiaryAccount ,
                BankAt: dataItem.BankAt == null ? "" : dataItem.BankAt,
            };
        };
        var newDataSource = new DevExpress.data.DataSource({ store: results, select: selector });
        richEditCV.mailMergeOptions.setDataSource(newDataSource);
    };

    this.init = function () {

    };
}

DevAV.requestDetailPopups = new DevAV.RequestDetailPopups();

$(function () {
    DevAV.requestDetailPopups.init();
});