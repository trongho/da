﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.RequestSupplies = function () {
    var grid;
    var formSelector = "#request-form";
    var requestid;
    var requesttitle, requesttype, createname, createemail, createdepartment, createdate, processingtime, approvers, followers;
    var requestcontent, reason;
    var goods, specification, quantity, note;

    this.createNewRequest = function () {
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = new Date();
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        requestcontent = $("#request-content").dxTextBox("instance").option("value");
        reason = $("#reason").dxTextBox("instance").option("value").toString();

        if (!DevAV.isFormValid(formSelector)) return;

        createRequest();

    };

    var createRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Create",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                createRequestSupplies(data.RequestID);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var createRequestSupplies = function (requestID) {
        var table = $('#supplies-table').DataTable();
        var ordinal = 0;

        table.rows().every(function (rowIdx, tableLoop, rowLoop) {
            var d = this.data();
            goods = d.Goods;
            specification = d.Specification;
            quantity = d.Quantity;
            note = d.Note;
            ordinal += 1;

            $.ajax({
                url: "/api/RequestSuppliesApi/Create",
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                    RequestID: requestID,
                    Ordinal: ordinal,
                    RequestContent: requestcontent,
                    Reason: reason,
                    Goods: goods,
                    Specification: specification,
                    Quantity: quantity,
                    Note: note
                }),
                beforeSend: function (request) {
                    request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                },
                success: function (data) {
                    const popup = $("#popup-create-request-supplies").dxPopup("instance");
                    popup.hide();
                    DevExpress.ui.notify('Tạo thành công đề xuất vật tư, mã đề xuất là ' + requestID, 'success', 4000);
                },
                error: function (xhr) {
                    DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                }
            });
        });
    };

    this.editRequest = function () {
        requestid = $("#request-id").dxTextBox("instance").option("value");;
        requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        createname = $("#create-name").dxTextBox("instance").option("value");
        createemail = $("#create-email").dxTextBox("instance").option("value");
        createdepartment = $("#create-department").dxTextBox("instance").option("value");
        createdate = $("#create-date").dxDateBox("instance").option("value");
        processingtime = $("#processing-time").dxDateBox("instance").option("value");
        approvers = $("#approvers").dxSelectBox("instance").option("value");
        followers = $("#followers").dxTagBox("instance").option("value").toString();

        requestcontent = $("#request-content").dxTextBox("instance").option("value");
        reason = $("#reason").dxTextBox("instance").option("value").toString();

        if (!DevAV.isFormValid(formSelector)) return;

        editRequest();
    };

    var editRequest = function () {
        $.ajax({
            url: "/api/RequestApi/Update?id=" + requestid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestID: requestid,
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                ProcessingTime: processingtime,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting"
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                editRequestSupplies();
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };

    var editRequestSupplies = function () {
        $.ajax({
            url: "/api/RequestSuppliesApi/DeleteRequest?id=" + requestid,
            type: 'DELETE',
            success: function (data) {
                var table = $('#supplies-table').DataTable();
                var ordinal = 0;

                table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                    var d = this.data();
                    goods = d.Goods;
                    specification = d.Specification;
                    quantity = d.Quantity;
                    note = d.Note;
                    ordinal += 1;

                    $.ajax({
                        url: "/api/RequestSuppliesApi/Create",
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: JSON.stringify({
                            RequestID: requestid,
                            Ordinal: ordinal,
                            RequestContent: requestcontent,
                            Reason: reason,
                            Goods: goods,
                            Specification: specification,
                            Quantity: quantity,
                            Note: note
                        }),
                        beforeSend: function (request) {
                            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
                        },
                        success: function (data) {
                            const popup = $("#popup-edit-request-supplies").dxPopup("instance");
                            popup.hide();
                            grid.refresh();
                            DevExpress.ui.notify('Cập nhập thành công đề xuất vật tư', 'success', 4000);
                        },
                        error: function (xhr) {
                            DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
                        }
                    });
                });
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    this.deleteRequest = function (e) {
        let timerInterval
        Swal.fire({
            title: 'Bạn có muốn xóa đề xuất này',
            toast: true,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xóa!',
            cancelButtonText: 'Không!',
            html: '<strong></strong> seconds<br/><br/>',
            timer: 10000,
            didOpen: () => {
                const content = Swal.getHtmlContainer()
                const $ = content.querySelector.bind(content)

                timerInterval = setInterval(() => {
                    Swal.getHtmlContainer().querySelector('strong')
                        .textContent = (Swal.getTimerLeft() / 1000)
                            .toFixed(0)
                }, 100)
            },
            willClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            if (result.isConfirmed) {
                if (e.row.data.Status == "Waiting") {
                    deleteRequest(e.row.data.RequestID);
                }
                else {
                    Swal.fire('Alert!!!', 'Không thể xóa đề xuất', 'error', 2000);
                }

            }
        })
    };

    var deleteRequest = function (requestID) {
        $.ajax({
            url: "/api/RequestApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                deleteRequestSupplies(requestID);
            },
            error: function (error) {
                Swal.fire('Error!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    var deleteRequestSupplies = function (requestID) {
        $.ajax({
            url: "/api/RequestSuppliesApi/DeleteRequest?id=" + requestID,
            type: 'DELETE',
            success: function (data) {
                Swal.fire('Success!', 'Xóa thành công', 'success', 2000);
                grid.refresh();
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };

    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };
}

DevAV.requestSupplies = new DevAV.RequestSupplies();

$(function () {
    DevAV.requestSupplies.init();
});