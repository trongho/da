﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.CreateReportCVPopups = function () {
    var formSelector = "#report-cv-form";

    this.createNewRequest = function () {

        //var requestid = $("#request-id").dxTextBox("instance").option("value");
        var requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        var requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        var customername = $("#customer-name").dxTextBox("instance").option("value");
        var bankat = $("#bank-at").dxTextBox("instance").option("value");
        var beneficiaryaccount = $("#beneficiary-account").dxTextBox("instance").option("value");
        var beneficiary = $("#beneficiary").dxTextBox("instance").option("value").toString().toString();
        var paymentplace = $("#payment-place").dxTextBox("instance").option("value");
        var accompanyingdocuments = $("#accompanying-documents").dxTextBox("instance").option("value");
        var amount = parseFloat($("#amount").dxTextBox("instance").option("value"));
        var requestcontent = $("#request-content").dxTextBox("instance").option("value");
        var payment = $("#payment").dxSelectBox("instance").option("value").toString();
        var approvers = $("#approvers").dxSelectBox("instance").option("value");
        var followers = $("#followers").dxTagBox("instance").option("value").toString();
        var createname = $("#create-name").dxTextBox("instance").option("value");
        var createemail = $("#create-email").dxTextBox("instance").option("value");
        var createdepartment = $("#create-department").dxTextBox("instance").option("value");
        var createdate = $("#create-date").dxDateBox("instance").option("value");
        var processingtime = $("#processing-time").dxDateBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        $.ajax({
            url: "api/RequestApi/Create",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                CustomerName: customername,
                BankAt: bankat,
                BeneficiaryAccount: beneficiaryaccount,
                Beneficiary: beneficiary,
                PaymentPlace: paymentplace,
                AccompanyingDocuments: accompanyingdocuments,
                Amount: amount,
                RequestContent: requestcontent,
                Payment: payment,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting",
                ProcessingTime: processingtime,
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                const popup = $("#popup-create-report-ct").dxPopup("instance");
                popup.hide();
                DevExpress.ui.notify('Tạo thành công tờ trình đề xuất', 'success', 4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };


    this.init = function () {

    };
}

DevAV.createReportCVPopups = new DevAV.CreateReportCVPopups();

$(function () {
    DevAV.createReportCVPopups.init();
});