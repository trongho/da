﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.OrderAssigns = function () {
    var currentAssign = {};
    var formSelector = "#assign-form";

    this.popupAssign = function (e) {
        //popup
        const popup = $("#popup-assign").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-assign-template"),
        });
        popup.show();

        //currentAssign = {
        //    Employee1: e.row.data.Employee1, Employee2: e.row.data.Employee2, ShippingType: e.row.data.ShippingType,
        //    OrderFee: e.row.data.OrderFee, Vehice: e.row.data.Vehice
        //};
        //var form = $(formSelector).dxForm("instance");
        //form.option("formData", currentAssign);

        $("#order-id-assign").dxTextBox("instance").option("value", e.row.data.OrderID);
        $.getJSON("Order/GetByID?id=" + e.row.data.OrderID,
            function (data) {
                $("#employee1-assign").dxSelectBox("instance").option("value", data.Employee1);
                $("#employee2-assign").dxSelectBox("instance").option("value", data.Employee2);
                $("#shipping-type-assign").dxSelectBox("instance").option("value", data.ShippingType);
                $("#order-fee-assign").dxSelectBox("instance").option("value", data.OrderFee);

               

                $("#vehice-assign").dxSelectBox("instance").option("value", data.Vehice);
            })
    };

    this.assignedTo = function () {
        var formData = $("#assign-form").serialize();
        var orderid = $("#order-id-assign").dxTextBox("instance").option("value");
        var employee1 = $("#employee1-assign").dxSelectBox("instance").option("value");;
        var employee2 = $("#employee2-assign").dxSelectBox("instance").option("value");
        var shippingtype = $("#shipping-type-assign").dxSelectBox("instance").option("value");
        var orderfee = $("#order-fee-assign").dxSelectBox("instance").option("value");
        var vehice = $("#vehice-assign").dxSelectBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return; 

        $.ajax({
            url: "Order/AssignTo?orderid=" + orderid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            //contentType: 'application/x-www-form-urlencoded; charset=UTF-8',   
            dataType: 'json',
            data: JSON.stringify({
                Employee1: employee1, Employee2: employee2, ShippingType: shippingtype,
                OrderFee: orderfee, Vehice: vehice
            }),
            //data: formData,
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                const popup = $("#popup-assign").dxPopup("instance");
                popup.hide();
                var grid = $("#gridContainer").dxDataGrid("instance");
                grid.getDataSource().reload();
                DevExpress.ui.notify('Đơn hàng ' + orderid + " được phân công đến " + employee1, 'success',4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.status, 'error', 2000);
            }
        });
    };


    this.init = function () {

    };
}

DevAV.orderAssigns = new DevAV.OrderAssigns();

$(function () {
    DevAV.orderAssigns.init();
});