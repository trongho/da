﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.GoodsDatas = function () {
    var grid;
    var goodsIDCheck, checkResult;
    var createCount, updateCount;

    this.uploadFile = async function () {
        const { value: file } = await Swal.fire({
            title: 'Select excel file',
            input: 'file',
            inputAttributes: {
                'accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'aria-label': 'Tải lên file excel của bạn'
            },
            preConfirm: (inp) => {
                if (!inp) {
                    Swal.showValidationMessage(`Chưa chọn file!!!`)
                }
            }
        })

        if (file) {
            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                url: "GoodsData/UploadFile",
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                success: function (data) {
                    Swal.fire({ title: 'Thành công', text: 'Tải lên thành công file ' + file.name + " có tổng số mã hàng là:" + Object.keys(data).length, icon: 'success', timer: 10000 })

                    var gridDataSource = new DevExpress.data.CustomStore({
                        key: "GoodsID",
                        load: function () {
                            return data;
                        }
                    });

                    grid.option("dataSource", gridDataSource);
                    grid.option("noDataText", "");
                },
                error: function (err) {
                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tải lên ' + err.statusText, icon: 'error', timer: 1500 })
                }
            });
        }
    };

    this.createGoodsData = function () {
        createCount = 0;
        updateCount = 0;
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                goodsIDCheck.option("value", item.GoodsID);
                $.getJSON("api/GoodsDataApi/CheckExist/" + item.GoodsID,
                    function (data) {
                        checkResult.option("value", data + "");
                        if (data == false) {
                            $.ajax({
                                url: "api/GoodsDataApi/Post",
                                type: 'POST',
                                async: false,
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify(item),
                                success: function (data1) {
                                    console.log("post ok");
                                },
                                error: function (error) {
                                    console.log("post error " + error)
                                }
                            });
                            createCount++;
                        }
                        else {
                            updateGoodsData();
                            updateCount++;
                        }
                    })
            })

        });
    };

    var updateGoodsData = function () {
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                $.ajax({
                    url: "api/GoodsDataApi/Put/" + item.GoodsID,
                    type: 'PUT',
                    async: false,
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(item),
                    success: function (data) {
                        console.log("put ok");
                    },
                    error: function (error) {
                        console.log("put error " + error)
                    }
                });
            })

        });
    };

    
    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
        goodsIDCheck = $("#GoodsIDCheck").dxTextBox("instance");
        checkResult = $("#CheckResult").dxTextBox("instance");
    };

};

DevAV.goodsDatas = new DevAV.GoodsDatas();

$(function () {
    DevAV.goodsDatas.init();
});