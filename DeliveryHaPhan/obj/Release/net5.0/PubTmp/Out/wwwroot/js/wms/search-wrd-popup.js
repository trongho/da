﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.SearchWRD = function () {
    var grid;
    var branch, handlingStatus, fromDate, toDate,orderStatus;

    this.filterWRD = function () {
        branch = $("#branch").dxSelectBox("instance");
        grid = $("#gridContainerWRRHeader").dxDataGrid("instance");
        handlingStatus = $("#handling-status").dxSelectBox("instance");
        fromDate = $("#from-date").dxDateBox("instance");
        toDate = $("#to-date").dxDateBox("instance");
        orderStatus = $("#order-status").dxSelectBox("instance");

        //var order;
        //switch (data.value) {
        //    case 0:
        //        status = "New";
        //        break;
        //    case 1:
        //        status = "Approved";
        //        break;
        //    case 2:
        //        status = "Started";
        //        break;
        //    case 3:
        //        status = "Pause";
        //        break;
        //    case 4:
        //        status = "Continue";
        //        break;
        //    case 5:
        //        status = "New,Approved,Started,Pause,Continue";
        //        break;
        //    default:
        //        status = "Started";
        //}

        $.getJSON("api/WRDataHeaderApi/",
            function (data) {
                data = data.filter(x => x.BranchID == branch.option("value") && x.HandlingStatusID == handlingStatus.option("value")
                    && new Date(fromDate.option("value")).getDate() <= new Date(x.WRRDate).getDate() && new Date(x.WRRDate).getDate() <= new Date(toDate.option("value")).getDate()
                    );

                var source = new DevExpress.data.CustomStore({
                    key: "WRDNumber",
                    load: function () {
                        return data;
                    }
                });

                grid.option("dataSource", source);
            })
    };

    this.refreshGrid = function () {
        grid = $("#gridContainerWRDataHeader").dxDataGrid("instance");
        grid.refresh();
    };

    this.init = function () {

    };

};

DevAV.searchWRD = new DevAV.SearchWRD();

$(function () {
    DevAV.searchWRD.init();
});