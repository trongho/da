﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.ReceiptRequisitions = function () {
    var grid;
    var wrrNumber, wrrDate, branch, handlingStatus, totalQuantityByItem, totalQuantityByPack;
    var popup;
    var currentWRRNumber;
    var storageKey = "dx-devav-filter-";

    //this.uploadFile = function () {
    //    var formData = new FormData();
    //    //var attachment_data = $("#fileinfo").find("input")[0].files[0];
    //    //var fileInputElement = $("#myFile")[0];

    //    formData.append("file", $("#myFile").get(0).files[0]);


    //    $.ajax({
    //        url: "ReceiptRequisition/UploadFile",
    //        type: 'POST',
    //        data: formData,
    //        enctype:'multipart/form-data',
    //        contentType: false,
    //        processData: false,
    //        success: function (data) {
    //            Swal.fire({ title: 'Thành công', text: 'Tải lên thành công file ' + $("#myFile").get(0).files[0].name +" có tổng số mã hàng là:"+ Object.keys(data).length, icon: 'success', timer: 10000 })

    //            var gridDataSource = new DevExpress.data.CustomStore({
    //                key: "OrderID",
    //                load: function () {
    //                    return data;
    //                }
    //            });

    //            grid.option("dataSource", gridDataSource);
    //        },
    //        error: function (err) {
    //            Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tải lên ' + err.statusText, icon: 'error', timer: 1500 })
    //        }
    //    });
    //};

    this.loadDataGrid = function () {
        var gridDataSource = new DevExpress.data.CustomStore({
            key: "GoodsID",
            load: function () {
                return JSON.parse(window.localStorage.getItem(storageKey));
            }
        });

        if (window.localStorage.getItem(storageKey) != null) {
            grid = $("#gridContainer").dxDataGrid("instance");
            grid.option("dataSource", gridDataSource);
        }
    };

    this.uploadFile = async function () {
        const { value: file } = await Swal.fire({
            title: 'Select excel file',
            input: 'file',
            inputAttributes: {
                'accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'aria-label': 'Tải lên file excel của bạn'
            }
        })

        if (file) {
            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                url: "ReceiptRequisition/UploadFile",
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                success: function (data) {
                    window.localStorage.setItem(storageKey, JSON.stringify(data));

                    Swal.fire({ title: 'Thành công', text: 'Tải lên thành công file ' + file.name  + " có tổng số mã hàng là:" + Object.keys(data).length, icon: 'success', timer: 10000 })

                    var gridDataSource = new DevExpress.data.CustomStore({
                        key: "GoodsID",
                        load: function () {
                            return data;
                        }
                    });

                    grid.option("dataSource", gridDataSource);
                    grid.option("noDataText", "");
                    wrrNumber.option("value", data[0].WRRNumber);
                    wrrDate.option("value", new Date());
                    handlingStatus.option("value", 1);

                    var totalItem = 0;
                    data.forEach(function (element) {
                        totalItem += element.QuantityByItem;
                    });
                    totalQuantityByItem.option("value", totalItem);

                    var totalPack = 0;
                    data.forEach(function (element) {
                        totalPack += 1;
                    });
                    totalQuantityByPack.option("value", totalPack);

                },
                error: function (err) {
                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tải lên ' + err.statusText, icon: 'error', timer: 1500 })
                }
            });
        }
    };

    this.createWRRHeader = function () {
        $.getJSON("api/WRRHeaderApi/CheckExist/" + wrrNumber.option("value"),
            function (data) {
                if (data == false) {
                    Swal.fire({
                        title: 'Alert?',
                        text: "Bạn có muốn lưu yêu cầu nhập " + wrrNumber.option("value"),
                        icon: 'success',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Vâng, lưu!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "api/WRRHeaderApi/Post",
                                type: 'POST',
                                contentType: 'application/json; charset=utf-8',
                                data: JSON.stringify({
                                    WRRNumber: wrrNumber.option("value"),
                                    WRRDate: wrrDate.option("value"),
                                    ReferenceNumber: wrrNumber.option("value"),
                                    HandlingStatusID: handlingStatus.option("selectedItem").id + "",
                                    HandlingStatusName: handlingStatus.option("selectedItem").text,
                                    Note: "",
                                    BranchID: branch.option("selectedItem").id + "",
                                    BranchName: branch.option("selectedItem").text,
                                    TotalQuantity: totalQuantityByItem.option("value"),
                                    Status: "",
                                    CreatedUserID: $("#username").val(),
                                    CreatedDate: new Date(),
                                    UpdatedUserID: null,
                                    UpdatedDate: null
                                }),
                                success: function (data) {
                                    createWRRDetail();
                                    if (handlingStatus.option("value") == 3) {
                                        createWRDataHeader();
                                    }
                                },
                                error: function (error) {
                                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo', icon: 'error', timer: 1500 })
                                }
                            });
                        }
                    })
                }
                else {
                    Swal.fire({
                        title: 'Alert?',
                        text: "Conveyance Number đã tồn tại, Bạn muốn cập nhật???",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Vâng cập nhật!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            updateWRRHeader();
                        }
                    })
                }
            })
        
    };

    var createWRRDetail = function () {
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                $.ajax({
                    url: "api/WRRDetailApi/Post",
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(item),
                    success: function (data) {
                        Swal.fire({ title: 'success', text: 'Tạo thành công yêu cầu nhập', icon: 'success', timer: 2000 })
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo', icon: 'error', timer: 1500 })
                    }
                });
            })
            
        });        
    };

    var createWRDataHeader = function () {
        $.ajax({
            url: "api/WRDataHeaderApi/Post",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                WRDNumber: wrrNumber.option("value"),
                WRDDate: new Date(),
                ReferenceNumber: wrrNumber.option("value"),
                WRRNumber: wrrNumber.option("value"),
                WRRReference: wrrNumber.option("value"),
                HandlingStatusID: 1+"",
                HandlingStatusName:"Chưa duyệt",
                Note: "",
                BranchID: branch.option("selectedItem").id + "",
                BranchName: branch.option("selectedItem").text,
                TotalQuantity: 0,
                TotalQuantityOrg: totalQuantityByItem.option("value"),
                Status: "0",
                CreatedUserID: $("#username").val(),
                CreatedDate: new Date(),
                UpdatedUserID: null,
                UpdatedDate: null
            }),
            success: function (data) {
                createWRDataGeneral();
            },
            error: function (error) {
                Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo dữ liệu nhập', icon: 'error', timer: 1500 })
            }
        });
    };

    var createWRDataGeneral = function () {
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                $.ajax({
                    url: "api/WRDataGeneralApi/Post",
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        WRDNumber: wrrNumber.option("value"),
                        IDCode: "",
                        GoodsID: item.GoodsID,
                        GoodsName: item.GoodsName,
                        Ordinal: item.Ordinal,
                        LocationID: "",
                        Quantity: 0,
                        TotalQuantity: 0,
                        TotalGoods: 0,
                        PackingQuantity: 0,
                        QuantityOrg: item.QuantityByItem,
                        TotalQuantityOrg: totalQuantityByItem.option("value"),
                        TotalGoodsOrg: allData.length,
                        LocationIDOrg: item.LocationID,
                        CreatorID: $("#username").val(),
                        CreatedDateTime: new Date(),
                        EditerID: null,
                        EditedDateTime:null,
                        ReceiptStatus: item.ReceiptStatus,
                        SLPart: item.SLPart,
                        PackingVolume: item.PackingVolume,
                        QuantityByPack: item.QuantityByPack,
                        QuantityByItem: item.QuantityByItem,
                        ScanOption: item.ScanOption,
                        Note: "",
                        SupplierCode: item.SupplierCode,
                        ASNNumber: item.ASNNumber,
                        PackingSlip: item.PackingSlip,
                        Status:null
                    }),
                    success: function (data) {
                        Swal.fire({ title: 'success', text: 'Tạo thành công dữ liệu nhập', icon: 'success', timer: 2000 })
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo', icon: 'error', timer: 1500 })
                    }
                });
            })

        });
    };


    var updateWRRHeader = function () {
        $.getJSON("api/WRRHeaderApi/GetByID/" + wrrNumber.option("value"),
            function (data) {
                $.ajax({
                    url: "api/WRRHeaderApi/Put/" + wrrNumber.option("value"),
                    type: 'PUT',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        WRRNumber: wrrNumber.option("value"),
                        WRRDate: wrrDate.option("value"),
                        ReferenceNumber: wrrNumber.option("value"),
                        HandlingStatusID: handlingStatus.option("selectedItem").id + "",
                        HandlingStatusName: handlingStatus.option("selectedItem").text,
                        Note: "",
                        BranchID: branch.option("selectedItem").id + "",
                        BranchName: branch.option("selectedItem").text,
                        TotalQuantity: totalQuantityByItem.option("value"),
                        Status: "",
                        CreatedUserID: data.CreatedUserID,
                        CreatedDate: data.CreatedDate,
                        UpdatedUserID: $("#username").val(),
                        UpdatedDate: new Date()
                    }),
                    success: function (data) {
                        updateWRRDetail();
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể cập nhật', icon: 'error', timer: 1500 })
                    }
                });
            })
        
    };

    var updateWRRDetail = function () {
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                $.ajax({
                    url: "api/WRRDetailApi/Put/" + item.WRRNumber+"/"+ item.GoodsID + "/" + item.Ordinal,
                    type: 'PUT',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(item),
                    success: function (data) {
                        Swal.fire(
                            'Success!',
                            'Cập nhật thành công',
                            'success'
                        )
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo', icon: 'error', timer: 1500 })
                    }
                });
            })

        });
    };

    this.deleteWRRHeader = function () {
        Swal.fire({
            title: 'Alert!!!',
            text: "Bạn có muốn xóa yêu cầu nhập " + wrrNumber.option("value"),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Vâng, xóa!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "api/WRRHeaderApi/Delete/" + wrrNumber.option("value"),
                    type: 'DELETE',
                    data: {},
                    success: function (data) {
                        deleteWRRDetail();
                        grid.refresh();
                    },
                    error: function (error) {
                        Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
                    }
                });
            }
        })
    };

    var deleteWRRDetail = function () {
        $.ajax({
            url: "api/WRRDetailApi/Delete/" + wrrNumber.option("value"),
            type: 'DELETE',
            data: {},
            success: function (data) {
                Swal.fire('Success!', 'Xóa thành công', 'success', 2000);
                grid.refresh();
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };



    this.openPopupSearchWRR = function () {
        popup.show();
    };

    this.selectedRow = function (e) {
        if (!e.data) return;
        popup.hide();

        
        $.getJSON("api/WRRHeaderApi/GetByID/" + e.data.WRRNumber,
            function (data) {
                wrrNumber.option("value", data[0].WRRNumber);
                wrrDate.option("value", data[0].WRRDate);
                handlingStatus.option("value", parseInt(data[0].HandlingStatusID));
                branch.option("value", parseInt(data[0].BranchID));
            })
        $.getJSON("api/WRRDetailApi/GetByID/" + e.data.WRRNumber,
            function (data) {
                var gridDataSource = new DevExpress.data.CustomStore({
                    key: "GoodsID",
                    load: function () {
                        return data;
                    }
                });
                grid.option("dataSource", gridDataSource);

                var totalItem = 0;
                data.forEach(function (element) {
                    totalItem += element.QuantityByItem;
                });
                totalQuantityByItem.option("value", totalItem);

                var totalPack = 0;
                data.forEach(function (element) {
                    totalPack += 1;
                });
                totalQuantityByPack.option("value", totalPack);
            })
    };


    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
        wrrNumber = $("#WRRNumber").dxTextBox("instance");
        wrrDate = $("#WRRDate").dxDateBox("instance");
        totalQuantityByItem = $("#TotalQuantityByItem").dxTextBox("instance");
        branch = $("#Branch").dxSelectBox("instance");
        handlingStatus = $("#HandlingStatus").dxSelectBox("instance");
        totalQuantityByPack = $("#TotalQuantityByPack").dxTextBox("instance");

        popup = $("#popup-search-wrr").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-search-wrr-template"),
        });
    };

};

DevAV.receiptRequisitions = new DevAV.ReceiptRequisitions();

$(function () {
    DevAV.receiptRequisitions.init();
});