﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.CreateReportCTPopups = function () {
    var formSelector = "#report-ct-form";

    this.createNewRequest = function () {
        
        //var requestid = $("#request-id").dxTextBox("instance").option("value");
        var requesttitle = $("#request-title").dxTextBox("instance").option("value");;
        var requesttype = $("#request-type").dxSelectBox("instance").option("value").toString();
        var customername = $("#customer-name").dxTextBox("instance").option("value");
        var customerid = $("#customer-id").dxTextBox("instance").option("value");
        var customeradress = $("#customer-adress").dxTextBox("instance").option("value");
        var vehice = $("#vehice").dxSelectBox("instance").option("value").toString().toString();
        var vehicleowner = $("#vehicle-owner").dxTextBox("instance").option("value");
        var executiondate = $("#execution-date").dxDateBox("instance").option("value");
        var amount = parseFloat($("#amount").dxTextBox("instance").option("value"));
        var businessfee = parseFloat($("#business-fee").dxTextBox("instance").option("value"));
        var payment = $("#payment").dxSelectBox("instance").option("value").toString();
        var orderid = $("#order-id").dxTextBox("instance").option("value");
        var businessstaffid = $("#business-staff-id").dxTextBox("instance").option("value");
        var ordervalue = parseFloat($("#order-value").dxTextBox("instance").option("value"));
        var approvers = $("#approvers").dxSelectBox("instance").option("value");
        var followers = $("#followers").dxTagBox("instance").option("value").toString();
        var createname = $("#create-name").dxTextBox("instance").option("value");
        var createemail = $("#create-email").dxTextBox("instance").option("value");
        var createdepartment = $("#create-department").dxTextBox("instance").option("value");
        var createdate = new Date();
        var processingtime = $("#processing-time").dxDateBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return; 

        $.ajax({
            url: "api/RequestApi/Create",
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                CustomerName: customername,
                CustomerID: customerid,
                CustomerAdress: customeradress,
                Amount: amount,
                ExecutionDate: executiondate,
                Payment: payment,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting",
                BusinessFee: businessfee,
                BusinessStaffID: businessstaffid,
                Vehice: vehice,
                VehicleOwner: vehicleowner,
                OrderID: orderid,
                OrderValue: ordervalue,
                ProcessingTime: processingtime,
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                const popup = $("#popup-create-report-ct").dxPopup("instance");
                popup.hide();
                DevExpress.ui.notify('Tạo thành công tờ trình đề xuất', 'success', 4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };


    this.init = function () {
        
    };
}

DevAV.createReportCTPopups = new DevAV.CreateReportCTPopups();

$(function () {
    DevAV.createReportCTPopups.init();
});