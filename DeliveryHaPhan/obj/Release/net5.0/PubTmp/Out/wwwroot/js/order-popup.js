﻿"use strict";

window.DevAV = window.DevAV || {};

DevAV.OrderPopup = function () {
    var orderPopup;
    var currentOrder = {};
    var formSelector = "#order-form";

    this.onPopupShowing = function (currentTaskData) {
        var form = $(formSelector).dxForm("instance");
        form.option("formData", currentOrder);

        var $titleElement = $("#order-edit-label");

        $titleElement.text(currentOrder.OrderID ? "Sửa đơn hàng" : "Tạo đơn hàng");
    };

    this.selectAssignedEmployee = function (e) {
        var $titleElement = $("#order-edit-assigned-employee");
        var resultText = "";

        var displayValue = e.selectedItem && e.selectedItem.UserName;

        if (displayValue) {
            resultText = "(" + displayValue + ")";
        }

        $titleElement.text(resultText);
    };

    this.deleteOrder = function (data) {
        var orderID = data.OrderID;
        Swal.fire({
            title: 'Bạn có muốn xóa đơn hàng ' + orderID+' ?',
            text: "Đơn hàng sẽ bị xóa vĩnh viễn!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Vâng, xóa đơn hàng này!',
            cancelButtonText:'Không, cám ơn'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "Order/Delete2?key=" + orderID,
                    type: 'DELETE',
                    success: function (data) {
                        $("#order-grid").dxDataGrid("instance").refresh();
                        DevExpress.ui.notify('Xóa thành công đơn hàng' + orderID, 'success', 2000);
                    },
                    error: function (xhr) {
                        DevExpress.ui.dialog.alert('Thất bại ' + xhr.status, 'error');
                    }
                });
            }
        })
    };

    this.saveChanges = function () {
        if (!DevAV.isFormValid(formSelector)) return;

        console.log(currentOrder);

        orderPopup.hide();
        //var formData = $(formSelector).dxForm("instance").serialize();
        $.ajax({
            url: "Order/UpdateOrder?orderid=" + $("#OrderID").dxTextBox("instance").option("value"),
            type: 'PUT',
            //contentType:'application/x-www-form-urlencoded; charset=UTF-8',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            processData: false,
            data: JSON.stringify(currentOrder),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                DevExpress.ui.notify('Cập nhật thành công đơn hàng' + $("#OrderID").dxTextBox("instance").option("value"), 'success', 2000);
            },
            error: function (xhr) {
                DevExpress.ui.dialog.alert('Thất bại ' + xhr.status, 'error');
            }
        });
    };

    this.cancelChanges = function () {
        orderPopup.hide();
    };

    this.editOrder = function (data) {
        
        currentOrder = data;
        orderPopup.option("visible", true);
    };

    this.init = function () {
        orderPopup = $("#order-popup").dxPopup("instance");
    };
}

DevAV.orderPopup = new DevAV.OrderPopup();

$(function () {
    DevAV.orderPopup.init();
});