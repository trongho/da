﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.EditReportCTPopups = function () {
    var formSelector = "#report-ct-form";

    this.editRequest = function () {

        var requestid = $("#request-id-edit-ct").dxTextBox("instance").option("value");
        var requesttitle = $("#request-title-edit-ct").dxTextBox("instance").option("value");;
        var requesttype = $("#request-type-edit-ct").dxSelectBox("instance").option("value").toString();
        var customername = $("#customer-name-edit-ct").dxTextBox("instance").option("value");
        var customerid = $("#customer-id-edit-ct").dxTextBox("instance").option("value");
        var customeradress = $("#customer-adress-edit-ct").dxTextBox("instance").option("value");
        var vehice = $("#vehice-edit-ct").dxSelectBox("instance").option("value").toString().toString();
        var vehicleowner = $("#vehicle-owner-edit-ct").dxTextBox("instance").option("value");
        var executiondate = $("#execution-date-edit-ct").dxDateBox("instance").option("value");
        var amount = parseFloat($("#amount-edit-ct").dxTextBox("instance").option("value"));
        var businessfee = parseFloat($("#business-fee-edit-ct").dxTextBox("instance").option("value"));
        var payment = $("#payment-edit-ct").dxSelectBox("instance").option("value").toString();
        var orderid = $("#order-id-edit-ct").dxTextBox("instance").option("value");
        var businessstaffid = $("#business-staff-id-edit-ct").dxTextBox("instance").option("value");
        var ordervalue = parseFloat($("#order-value-edit-ct").dxTextBox("instance").option("value"));
        var approvers = $("#approvers-edit-ct").dxSelectBox("instance").option("value");
        var followers = $("#followers-edit-ct").dxTagBox("instance").option("value").toString();
        var createname = $("#create-name-edit-ct").dxTextBox("instance").option("value");
        var createemail = $("#create-email-edit-ct").dxTextBox("instance").option("value");
        var createdepartment = $("#create-department-edit-ct").dxTextBox("instance").option("value");
        var createdate = $("#create-date-edit-ct").dxDateBox("instance").option("value");
        var processingtime = $("#processing-time-edit-ct").dxDateBox("instance").option("value");

        if (!DevAV.isFormValid(formSelector)) return;

        $.ajax({
            url: "/api/RequestApi/Update?id=" + requestid,
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({
                RequestID: requestid,
                RequestType: requesttype,
                RequestTitle: requesttitle,
                CreateDate: createdate,
                CreateName: createname,
                CreateEmail: createemail,
                CreateDepartment: createdepartment,
                CustomerName: customername,
                CustomerID: customerid,
                CustomerAdress: customeradress,
                Amount: amount,
                ExecutionDate: executiondate,
                Payment: payment,
                Approvers: approvers,
                Followers: followers,
                Status: "Waiting",
                BusinessFee: businessfee,
                BusinessStaffID: businessstaffid,
                Vehice: vehice,
                VehiceOwner: vehicleowner,
                OrderID: orderid,
                OrderValue: ordervalue,
                ProcessingTime: processingtime,
            }),
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (data) {
                const popup = $("#popup-edit-report-ct").dxPopup("instance");
                popup.hide();
                DevExpress.ui.notify('Tờ trình đề xuất' + requestid + "đã chỉnh sửa", 'success', 4000);
            },
            error: function (xhr) {
                DevExpress.ui.notify('Thất bại ' + xhr.responseText, 'error', 2000);
            }
        });
    };


    this.init = function () {

    };
}

DevAV.editReportCTPopups = new DevAV.EditReportCTPopups();

$(function () {
    DevAV.editReportCTPopups.init();
});