﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.UserManagers = function () {
    var grid;

    this.uploadFile = async function () {
        const { value: file } = await Swal.fire({
            title: 'Select excel file',
            input: 'file',
            inputAttributes: {
                'accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'aria-label': 'Tải lên file excel của bạn'
            }
        })

        if (file) {
            var formData = new FormData();
            formData.append("file", file);
            $.ajax({
                url: "/api/UserNewApi/UploadFile",
                type: 'POST',
                data: formData,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                success: function (data) {
                    Swal.fire({ title: 'Thành công', text: 'Tải lên thành công file ' + file.name + " có tổng số người dùng là:" + Object.keys(data).length, icon: 'success', timer: 10000 })

                    var gridDataSource = new DevExpress.data.CustomStore({
                        key: "UserID",
                        load: function () {
                            return data;
                        }
                    });

                    grid.option("dataSource", gridDataSource);
                    grid.option("noDataText", "");
                },
                error: function (err) {
                    Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tải lên ' + err.statusText, icon: 'error', timer: 1500 })
                }
            });
        }
    };

    this.createUser = function () {
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                $.ajax({
                    url: "/api/UserNewApi/Create",
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(item),
                    success: function (data) {
                        Swal.fire({ title: 'success', text: 'Tạo thành công người dùng', icon: 'success', timer: 2000 })
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo', icon: 'error', timer: 1500 })
                    }
                });
            })

        });
    };

    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
    };

};

DevAV.userManagers = new DevAV.UserManagers();

$(function () {
    DevAV.userManagers.init();
});