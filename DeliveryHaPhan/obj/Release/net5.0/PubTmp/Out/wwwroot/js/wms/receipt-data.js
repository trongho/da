﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.ReceiptDatas = function () {
    var grid;
    var chart;
    var wrdNumber, wrdDate, handlingStatus, totalQuantityOrg,totalQuantity,totalGoodsOrg,totalGoods;
    var popup;
    var storageKey = "dx-devav-filter-receipt-data";
    var connection;
    var qrcode

    this.updateWRDataHeader = function () {
        $.getJSON("api/WRDataHeaderApi/GetByID/" + wrdNumber.option("value"),
            function (data) {
                $.ajax({
                    url: "api/WRDataHeaderApi/Put/" + wrdNumber.option("value"),
                    type: 'PUT',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({
                        WRDNumber: wrdNumber.option("value"),
                        WRDDate: wrdDate.option("value"),
                        ReferenceNumber: wrdNumber.option("value"),
                        WRRNumber: data.WRRNumber,
                        WRRReference: data.WRRReference,
                        HandlingStatusID: handlingStatus.option("selectedItem").id + "",
                        HandlingStatusName: handlingStatus.option("selectedItem").text,
                        Note: "",
                        BranchID: data.BranchID,
                        BranchName: data.BranchName,
                        TotalQuantity: totalQuantity.option("value"),
                        TotalQuantityOrg: totalQuantityOrg.option("value"),
                        Status: "",
                        CreatedUserID: data.CreatedUserID,
                        CreatedDate: data.CreatedDate,
                        UpdatedUserID: $("#username").val(),
                        UpdatedDate: new Date()
                    }),
                    success: function (data) {
                        updateWRDataGeneral();
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể cập nhật', icon: 'error', timer: 1500 })
                    }
                });
            })

    };

    var updateWRDataGeneral = function () {
        grid.getDataSource().store().load().done((allData) => {
            allData.forEach(item => {
                $.ajax({
                    url: "api/WRDataGeneralApi/Put/" + item.WRDNumber + "/" + item.GoodsID + "/" + item.Ordinal,
                    type: 'PUT',
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(item),
                    success: function (data) {
                        Swal.fire(
                            'Success!',
                            'Cập nhật thành công',
                            'success'
                        )
                    },
                    error: function (error) {
                        Swal.fire({ title: 'Thất bại', text: 'Có lỗi không thể tạo', icon: 'error', timer: 1500 })
                    }
                });
            })

        });
    };

    this.deleteWRDataHeader = function () {
        Swal.fire({
            title: 'Alert!!!',
            text: "Bạn có muốn xóa dữ liệu nhập " + wrdNumber.option("value"),
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Vâng, xóa!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "api/WRDataHeaderApi/Delete/" + wrdNumber.option("value"),
                    type: 'DELETE',
                    data: {},
                    success: function (data) {
                        deleteWRDataGeneral();
                        grid.refresh();
                    },
                    error: function (error) {
                        Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
                    }
                });
            }
        })
    };

    var deleteWRDataGeneral = function () {
        $.ajax({
            url: "api/WRDataGeneralApi/Delete/" + wrdNumber.option("value"),
            type: 'DELETE',
            data: {},
            success: function (data) {
                Swal.fire('Success!', 'Xóa thành công', 'success', 2000);
                grid.refresh();
            },
            error: function (error) {
                Swal.fire('Success!', 'Xóa không thành công', 'error', 2000);
            }
        });
    };



    this.openPopupSearchWRD = function () {
        popup.show();
    };

    this.selectedRow = function (e) {
        if (!e.data) return;
        popup.hide();

        qrcode.makeCode(e.data.WRDNumber);

        $.getJSON("api/WRDataHeaderApi/GetByID/" + e.data.WRDNumber,
            function (data) {
                wrdNumber.option("value", data[0].WRDNumber);
                wrdDate.option("value", data[0].WRDDate);
                handlingStatus.option("value", parseInt(data[0].HandlingStatusID));
            })
        $.getJSON("api/WRDataGeneralApi/GetByID/" + e.data.WRDNumber,
            function (data) {
                var gridDataSource = new DevExpress.data.CustomStore({
                    key: "GoodsID",
                    load: function () {
                        return data;
                    }
                });
                grid.option("dataSource", gridDataSource);
                chart.option("dataSource", gridDataSource);

                var store = $("#gridContainer").dxDataGrid("getDataSource").store();
                setInterval(function () {
                    updateWRD(store);
                }, 1000);

                var storeChart = $("#chart").dxPieChart("getDataSource").store();
                setInterval(function () {
                    updateWRD(storeChart);
                }, 1000);



                var totalItem = 0;
                data.forEach(function (element) {
                    totalItem += element.Quantity;
                });
                totalQuantity.option("value", totalItem);

                var totalItemOrg = 0;
                data.forEach(function (element) {
                    totalItemOrg += element.QuantityOrg;
                });
                totalQuantityOrg.option("value", totalItemOrg);

                var totalPack = 0;
                data.forEach(function (element) {
                    if (element.Quantity > 0) {
                        totalPack += 1;
                    }
                });
                totalGoods.option("value", totalPack);

                var totalPackOrg = 0;
                data.forEach(function (element) {
                    totalPackOrg += 1;
                });
                totalGoodsOrg.option("value", totalPackOrg);

                if (totalQuantity.option("value") < totalQuantityOrg.option("value") && totalQuantity.option("value")!=0) {
                    totalQuantity.option("elementAttr", { "class": "red .dx-texteditor-input" })
                }
                else if (totalQuantity.option("value") > totalQuantityOrg.option("value")) {
                    totalQuantity.option("elementAttr", { "class": "yellow .dx-texteditor-input" })
                }
                else if (totalQuantity.option("value")== totalQuantityOrg.option("value")) {
                    totalQuantity.option("elementAttr", { "class": "green .dx-texteditor-input" })
                }

            });

        

    };

    this.setPackingQuantityValue = function (rowData) {
        var packingQuantity = 0;
        var packingVolume = rowData.QuantityOrg / rowData.QuantityByPack
        if (rowData.Quantity % packingVolume != 0) {
            packingQuantity = Math.floor(rowData.Quantity / packingVolume) + 1;
        }
        else {
            packingQuantity = rowData.Quantity / packingVolume;
        }

        //var index = e.row.rowIndex;  
        //if (e.dataField === "PackingQuantity" && e.parentType === "dataRow") {
        //    grid.cellValue(index, "PackingQuantity", 100);  
        //}

        return packingQuantity;

    };

    this.onCellPreparedQuantity = function (e) {
        if (e.rowType == 'data') {
            if (e.data.Quantity < e.data.QuantityOrg && e.data.Quantity != 0) {
                if (e.column.dataField == 'Quantity') {
                    e.cellElement[0].style.backgroundColor = 'red';
                    e.cellElement[0].style.color = 'white';
                }
                if (e.column.dataField == 'PackingQuantity') {
                    e.cellElement[0].style.backgroundColor = 'red';
                    e.cellElement[0].style.color = 'white';
                }
            }
            if (e.data.Quantity>e.data.QuantityOrg) {
                if (e.column.dataField == 'Quantity') {
                    e.cellElement[0].style.backgroundColor = '#B8A72D';
                    e.cellElement[0].style.color = 'white';
                }
                if (e.column.dataField == 'PackingQuantity') {
                    e.cellElement[0].style.backgroundColor = '#B8A72D';
                    e.cellElement[0].style.color = 'white';
                }
            }
            if (e.data.Quantity==e.data.QuantityOrg) {
                if (e.column.dataField == 'Quantity') {
                    e.cellElement[0].style.backgroundColor = 'green';
                    e.cellElement[0].style.color = 'white';
                }
                if (e.column.dataField == 'PackingQuantity') {
                    e.cellElement[0].style.backgroundColor = 'green';
                    e.cellElement[0].style.color = 'white';
                }
            }

        }
    };

    var updateWRD = function (wrdStore) {
        var wrdNumberArg = $("#WRDNumber").dxTextBox("instance").option("value");
        $.getJSON("api/WRDataGeneralApi/GetByID/" + wrdNumberArg,
            function (data) {
                data.forEach(function (element) {
                    wrdStore.push([{
                        type: "update",
                        key: element.GoodsID,
                        data: element
                    }]);
                });

                var totalItem = 0;
                data.forEach(function (element) {
                    totalItem += element.Quantity;
                });
                totalQuantity.option("value", totalItem);

                var totalItemOrg = 0;
                data.forEach(function (element) {
                    totalItemOrg += element.QuantityOrg;
                });
                totalQuantityOrg.option("value", totalItemOrg);

                var totalPack = 0;
                data.forEach(function (element) {
                    if (element.Quantity > 0) {
                        totalPack += 1;
                    }
                });
                totalGoods.option("value", totalPack);

                var totalPackOrg = 0;
                data.forEach(function (element) {
                    totalPackOrg += 1;
                });
                totalGoodsOrg.option("value", totalPackOrg);

                if (totalQuantity.option("value") < totalQuantityOrg.option("value") && totalQuantity.option("value") != 0) {
                    totalQuantity.option("elementAttr", { "class": "red .dx-texteditor-input" })
                }
                else if (totalQuantity.option("value") > totalQuantityOrg.option("value")) {
                    totalQuantity.option("elementAttr", { "class": "yellow .dx-texteditor-input" });
                    //Swal.fire(
                    //    'Good job!',
                    //    'Đơn hàng này nhập dư',
                    //    'warning'
                    //)
                }
                else if (totalQuantity.option("value") == totalQuantityOrg.option("value")) {
                    totalQuantity.option("elementAttr", { "class": "green .dx-texteditor-input" });
                    //Swal.fire(
                    //    'Good job!',
                    //    'Đơn hàng này đã nhập đủ',
                    //    'success'
                    //)
                }

            });
    };


    //this.customizePoint = function () {
    //    var seriesQuantity = $("#chart").dxChart("getSeriesByName", "Quantity");
    //    var seriesQuantityOrg = $("#chart").dxChart("getSeriesByName", "QuantityOrg");
    //    if (seriesQuantity.value > seriesQuantityOrg.value) {
    //        return { color: "#ff7c7c", hoverStyle: { color: "#ff7c7c" } };
    //    } else if (seriesQuantity.value < seriesQuantityOrg.value) {
    //        return { color: "#8c8cff", hoverStyle: { color: "#8c8cff" } };
    //    }
    //};

    //this.customizeLabel = function () {
    //    if (this.value > highAverage) {
    //        return {
    //            visible: true,
    //            backgroundColor: "#ff7c7c",
    //            customizeText: function () {
    //                return this.valueText + "&#176F";
    //            }
    //        };
    //    }
    //};


    this.init = function () {
        grid = $("#gridContainer").dxDataGrid("instance");
        chart = $("#chart").dxPieChart("instance");
        wrdNumber = $("#WRDNumber").dxTextBox("instance");
        wrdDate = $("#WRDDate").dxDateBox("instance");
        totalQuantity = $("#TotalQuantity").dxTextBox("instance");
        totalQuantityOrg = $("#TotalQuantityOrg").dxTextBox("instance");
        totalGoods = $("#TotalGoods").dxTextBox("instance");
        totalGoodsOrg = $("#TotalGoodsOrg").dxTextBox("instance");
        handlingStatus = $("#HandlingStatus").dxSelectBox("instance");

        qrcode = new QRCode("qrcode", {
            text: "http://jindo.dev.naver.com/collie",
            width: 80,
            height: 80,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });

        popup = $("#popup-search-wrd").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-search-wrd-template"),
        });
    };

   

};

DevAV.receiptDatas = new DevAV.ReceiptDatas();

$(function () {
    DevAV.receiptDatas.init();
});