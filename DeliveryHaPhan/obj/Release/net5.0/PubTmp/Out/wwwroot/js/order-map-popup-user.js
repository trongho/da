﻿"use strict";

window.DevAV = window.DevAV || {};
DevAV.OrderMapUser = function () {
    const mapLoadToken = 'SYASj2SLYfUvY5BtsZgDJgzGydMsDKZnBAgsQXP1';
    const mapAPIToken = 'R0YBSqv8kl3JvbSFvQwkvWlGkbnTKjhDm3YRlcBS';
    var map, geocoder, c;
    var startPoint, startPointArr, startAdress;
    var currentPoint, currentPointArr, currentAdress;
    var endPoint, endPointArr, endAdress;
    var prePoint;
    var vehice;
    var orderID, status;
    var kilometersOrg, kilometers;

    this.popupMap = function (e) {
        if (e.row.data.Status == "Ended") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Đơn hàng đã hoàn thành!',
                timer: 2000,
            })
        } else if (e.row.data.Status == "Cancel") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Đơn hàng đã bị hủy!',
                timer: 2000,
            })
        } else {
            //popup
            const popup = $("#order-map-popup").dxPopup("instance");
            popup.option({
                contentTemplate: $("#popup-order-map-template"),
            });
            popup.show();

        }
        $("#order-id").dxTextBox("instance").option("value", e.row.data.OrderID);
        fetchData($("#order-id").dxTextBox("instance").option("value"));
    };

    this.tooltipShow = function () {
        $("#tt-b-start-adress").text($("#start-adress").dxTextBox("instance").option("value"));
        $("#tt-b-end-adress").text($("#end-adress").dxTextBox("instance").option("value"));
    }

    var fetchData = function (orderID) {
        getLocation();

        $.getJSON("Order/GetByID?id=" + orderID,
            function (data) {
                $("#status").dxTextBox("instance").option("value", data.Status);
                $("#employee1").dxTextBox("instance").option("value", data.Employee1);
                $("#employee2").dxTextBox("instance").option("value", data.Employee2);
                $("#start-point").dxTextBox("instance").option("value", data.StartPoint);
                $("#current-point").dxTextBox("instance").option("value", data.CurrentPoint);
                $("#start-adress").dxTextBox("instance").option("value", data.StartAdress);
                $("#pre-point").dxTextBox("instance").option("value", data.CurrentPoint);
                if (data.EndAdress) {
                    $("#end-point").dxTextBox("instance").option("value", data.EndPointUpdate);
                    $("#end-adress").dxTextBox("instance").option("value", data.EndAdress);
                } else {
                    $("#end-point").dxTextBox("instance").option("value", data.EndPoint);
                    $("#end-adress").dxTextBox("instance").option("value", data.CustomerAdress);
                }


                status = $("#status").dxTextBox("instance").option("value");

                if (status === "Approved") {
                    $("#status").dxTextBox({
                        elementAttr: {
                            class: "approved"
                        }
                    });
                }
                else if (status === "Started") {
                    $("#status").dxTextBox({
                        elementAttr: {
                            class: "started"
                        }
                    });
                }
                else if (status === "Pause") {
                    $("#status").dxTextBox({
                        elementAttr: {
                            class: "pause"
                        }
                    });
                }
                else if (status === "Continue") {
                    $("#status").dxTextBox({
                        elementAttr: {
                            class: "continue"
                        }
                    });
                }

                if (status === "Pause" || status==="Approved") {
                    $("#start_button").dxButton({
                        visible:true,
                    });
                    $("#pause_button").dxButton({
                        visible: false,
                    });

                } else {
                    $("#pause_button").dxButton({
                        visible: true,
                    });
                    $("#start_button").dxButton({
                        visible:false,
                    });
                }

                $("#vehice").dxTextBox("instance").option("value", data.Vehice);

                $("#result").dxTextBox("instance").option("value", data.Result);
                $("#reason").dxTextBox("instance").option("value", data.Reason);
                $("#start-time").dxTextBox("instance").option("value", data.StartTime);
                $("#stop-time").dxTextBox("instance").option("value", data.StopTime);
                $("#stop-time").dxTextBox("instance").option("value", data.StopTime);
                $("#kilometers-org").text(data.KilometersOrg);
                $("#kilometers").text(data.Kilometers);
                //$("#mass").dxTextBox("instance").option("value", data.Mass);
                $("#order-fee").dxTextBox("instance").option("value", data.OrderFee);
                //$("#order-value").dxTextBox("instance").option("value", data.OrderValue);

                startPoint = $("#start-point").dxTextBox("instance").option("value");
                startAdress = $("#start-adress").dxTextBox("instance").option("value");
                currentPoint = $("#current-point").dxTextBox("instance").option("value");
                endPoint = $("#end-point").dxTextBox("instance").option("value");
                endAdress = $("#end-adress").dxTextBox("instance").option("value");
                endPointArr = endPoint.trim().split(',').reverse();
                vehice = setVehice($("#vehice").dxTextBox("instance").option("value"));

                loadmap(endPointArr);



                if (startPoint != null && startPoint != "") {
                    startPointArr = startPoint.trim().split(',').reverse();
                    currentPointArr = currentPoint.trim().split(',').reverse();
                    loadmap(currentPointArr);
                    displayStartPoint(startPointArr, startAdress);
                    displayEndPoint(endPointArr, endAdress);
                    displayCurrentPoint(currentPointArr);
                    fly(currentPointArr);
                    drawRouter(startPoint, endPoint, vehice)
                    
                    displayEndPoint(endPointArr, endAdress);
                    fly(endPointArr);
                }

                currentLocation();
            })
    };

    this.popupDirection = function () {
        //popup
        const popup = $("#direction-popup").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-direction-template"),
        });
        popup.show();
    };

    this.popupOrderCancel = function () {
        //popup
        const popup = $("#order-cancel-popup").dxPopup("instance");
        popup.option({
            contentTemplate: $("#popup-order-cancel-template"),
        });
        popup.show();
        $("#cancel-order-id").dxTextBox("instance").option("value", $("#order-id").dxTextBox("instance").option("value"));
    };

    this.popupDirectionParams = function () {
        var param = {};
        param["startpoint"] = $("#start-point").dxTextBox("instance").option("value");
        param["endpoint"] = $("#end-point").dxTextBox("instance").option("value");
        param["vehice"] = setVehice($("#vehice").dxTextBox("instance").option("value"));
        return param;
    };

    var getLoadPanelInstance = function () {
        return $("#loadPanel").dxLoadPanel("instance");
    };

    this.loadPanel_shown = function (e) {
        setTimeout(function () {
            e.component.hide();
        }, 2000);
    };

    this.loadPanel_hidden = function (e) {

    };

    var setVehice = function (vh) {
        if (vh != null) {
            if (vh.includes("xm")) {
                vehice = "bike";
            }
            else if (vh.includes("xt")) {
                vehice = "car";
            }
            else if (vh.includes("xbt")) {
                vehice = "car";
            }
            else if (vh.includes("tx")) {
                vehice = "taxi";
            }
            else {
                vehice = "bike"
            }
        }

        return vehice;

    }

    this.startbutton_onClick = async function (e) {
       
        endPoint = $("#end-point").dxTextBox("instance").option("value");
        vehice = setVehice($("#vehice").dxTextBox("instance").option("value"));
        orderID = $("#order-id").dxTextBox("instance").option("value");
        currentPoint = $("#current-point").dxTextBox("instance").option("value");
        status = $("#status").dxTextBox("instance").option("value");


        if (currentPoint !=null) {
            currentPointArr = currentPoint.trim().split(',').reverse();
            if (status === "Approved") {
                getLocation();
                var result = confirm("Bắt đầu vận chuyển đơn hàng" + "[" + orderID + "] ");
                if (result) {
                    $.getJSON("Map/GetAdress/" + currentPoint,
                        function (currentAdressData) {
                            $.getJSON("Map/GetKilometers/" + currentPoint + "/" + endPoint + "/" + vehice,
                                function (kilometerOrgData) {
                                    if (currentAdressData != "" && kilometerOrgData != 0) {
                                        $.ajax({
                                            url: "UserOrder/Start/" + orderID + "?point=" + currentPoint + "&&adress=" + currentAdressData + "&&kilometersOrg=" + kilometerOrgData,
                                            method: 'PUT',
                                            data: {},
                                            success: function (data) {
                                                map.remove();
                                                map = new goongjs.Map({
                                                    container: 'map-order-detail',
                                                    style: 'https://tiles.goong.io/assets/goong_map_web.json',
                                                    center: currentPointArr,
                                                    zoom: 15
                                                });
                                                getLoadPanelInstance().show();
                                                fetchData(orderID);
                                                $("#status").dxTextBox("instance").option("value", "Started");
                                                DevExpress.ui.notify("Đơn hàng " + orderID + " bắt đầu vận chuyển", "success", "top right", 500);
                                            }
                                        });
                                    } else {
                                        DevExpress.ui.notify("Có lỗi, vui lòng thử lại", "error", 500);
                                    }
                                });
                        });
                }
            }
            else {
                var result = confirm("Tiếp tục vận chuyển đơn hàng" + "[" + orderID + "]");
                if (result) {
                    $.ajax({
                        url: "UserOrder/Continue/" + orderID + "?point=" + currentPoint,
                        method: 'PUT',
                        data: {},
                        success: function (data) {
                            map.remove();
                            map = new goongjs.Map({
                                container: 'map-order-detail',
                                style: 'https://tiles.goong.io/assets/goong_map_web.json',
                                center: currentPointArr,
                                zoom: 15
                            });
                            getLoadPanelInstance().show();
                            fetchData(orderID);
                            $("#status").dxTextBox("instance").option("value", "Continue");
                            DevExpress.ui.notify("Đơn hàng " + orderID + " tiếp tục vận chuyển", "success", 500);
                        }
                    });
                }
            }
        } else {
            DevExpress.ui.notify("Không lấy được vị trí hiện tại,vui lòng thử lại", "error", 500);
        }
    };

    this.pausebutton_onClick = async function (e) {
        
        orderID = $("#order-id").dxTextBox("instance").option("value");
        currentPoint = $("#current-point").dxTextBox("instance").option("value");
        vehice = setVehice($("#vehice").dxTextBox("instance").option("value"));

        if (currentPoint != "") {
            $.getJSON("UserOrder/GetCurrentPoint/" + orderID,
                function (prePointData) {
                    $.getJSON("Map/GetKilometers/" + prePointData + "/" + currentPoint + "/" + vehice,
                        function (data) {
                            var result = confirm("Tạm dừng vận chuyển đơn hàng" + "[" + orderID + "] ");
                            if (result) {
                                $.ajax({
                                    url: "UserOrder/Pause/" + orderID + "?point=" + currentPoint + "&&kilometers=" + data,
                                    method: 'PUT',
                                    data: {},
                                    success: function (data1) {
                                        fetchData(orderID);
                                        $("#status").dxTextBox("instance").option("value", "Pause");
                                        DevExpress.ui.notify("Đơn hàng " + orderID + " tạm dừng, đã đi được " + data + " kms", "success", 500);
                                    }
                                });
                            }
                        })
                })
        } else {
            DevExpress.ui.notify("Không lấy được vị trí hiện tại,vui lòng thử lại", "error", 500);
        }
    };

    this.endbutton_onClick = async function (e) {
       
        orderID = $("#order-id").dxTextBox("instance").option("value");
        currentPoint = $("#current-point").dxTextBox("instance").option("value");
        startPoint = $("#start-point").dxTextBox("instance").option("value");
        endPoint = $("#end-point").dxTextBox("instance").option("value");
        vehice = setVehice($("#vehice").dxTextBox("instance").option("value"));

        if (startPoint === null || startPoint === "") {
            DevExpress.ui.notify("Đơn hàng " + orderID + " chưa bắt đầu, không thể hoàn thành", "error", 2000);
        } else {
            if (currentPoint != "") {
                $.getJSON("UserOrder/GetCurrentPoint/" + orderID,
                    function (prePointData) {
                        $.getJSON("Map/GetKilometers/" + prePointData + "/" + currentPoint + "/" + vehice,
                            function (data) {
                                $.getJSON("Map/GetKilometers/" + currentPoint + "/" + endPoint + "/" + vehice,
                                    function (dataCompare) {
                                        if (dataCompare < 1) {
                                            var result = confirm("Hoàn thành đơn hàng " + "[" + orderID + "]" + "?");
                                            if (result) {
                                                $.ajax({
                                                    url: "UserOrder/End/" + orderID + "?kilometers=" + dataCompare,
                                                    method: 'PUT',
                                                    data: {},
                                                    success: function (data) {
                                                        fetchData(orderID);
                                                        kilometers = $("#kilometers").text();
                                                        DevExpress.ui.notify("Đơn hàng " + orderID + " đã hoàn thành, số kms cuối cùng là " + kilometers, "success", 500);
                                                        const popup = $("#order-map-popup").dxPopup("instance");
                                                        popup.hide();
                                                        const dataGrid = $("#gridContainer").dxDataGrid("instance");
                                                        dataGrid.getDataSource().reload();
                                                    }
                                                });
                                            }
                                        } else {
                                            var result = confirm("Sai địa điểm đến,địa chỉ hiện tại cách điểm đến " + dataCompare + " kms, bạn muốn hoàn thành đơn hàng" + "[" + orderID + "]" + "?");
                                            if (result) {
                                                $.ajax({
                                                    url: "UserOrder/End/" + orderID + "?kilometers=" + data,
                                                    method: 'PUT',
                                                    data: {},
                                                    success: function (data) {
                                                        DevExpress.ui.notify("Đơn hàng " + orderID + " đã hoàn thành", "success", 500);
                                                        const popup = $("#order-map-popup").dxPopup("instance");
                                                        popup.hide();
                                                        const dataGrid = $("#gridContainer").dxDataGrid("instance");
                                                        dataGrid.getDataSource().reload();
                                                    }
                                                });
                                            }
                                        }
                                    })
                            })
                    })
            } else {
                DevExpress.ui.notify("Không lấy được vị trí hiện tại,vui lòng thử lại", "error", 500);
            }
        }
    };

    this.cancelOrder = function () {
        orderID = $("#order-id").dxTextBox("instance").option("value");
        var reason = $("#reason-cancel").dxTextBox("instance").option("value");
        var result = confirm("Bạn muốn hủy đơn hàng " + "[" + orderID + "]" + " với lý do " + "[" + reason + "]");
        if (result) {
            $.ajax({
                url: "UserOrder/Cancel/" + orderID + "?Reason=" + reason,
                method: 'PUT',
                data: {},
                success: function (data) {
                    DevExpress.ui.notify("Đơn hàng " + orderID + " đã bị hủy", "success", 2000);
                    const popupcancel = $("#order-cancel-popup").dxPopup("instance");
                    popupcancel.hide();
                    const popupdetail = $("#order-map-popup").dxPopup("instance");
                    popupdetail.hide();
                    const dataGrid = $("#gridContainer").dxDataGrid("instance");
                    dataGrid.getDataSource().reload();
                }
            });
        }
    };

    var loadmap = function (pointArr) {
        goongjs.accessToken = mapLoadToken;
        if (map == null) {
            map = new goongjs.Map({
                container: 'map-order-detail',
                style: 'https://tiles.goong.io/assets/goong_map_web.json',
                center: pointArr,
                zoom: 11.5
            });

            map.addControl(new goongjs.FullscreenControl());
        }
        else {
            map.remove();
            map = new goongjs.Map({
                container: 'map-order-detail',
                style: 'https://tiles.goong.io/assets/goong_map_web.json',
                center: pointArr,
                zoom: 11.5
            });

            map.addControl(new goongjs.FullscreenControl());
        }
    };

    var drawRouter = function (p1, p2, vh) {
        map.on('load', function () {
            var layers = map.getStyle().layers;
            // Find the index of the first symbol layer in the map style
            var firstSymbolId;
            for (var i = 0; i < layers.length; i++) {
                if (layers[i].type === 'symbol') {
                    firstSymbolId = layers[i].id;
                    break;
                }
            }
            // Initialize goongClient with an API KEY
            var goongClient = goongSdk({
                accessToken: mapAPIToken
            });
            // Get Directions
            goongClient.directions
                .getDirections({
                    origin: p1,
                    destination: p2,
                    vehicle: vh
                })
                .send()
                .then(function (response) {
                    var directions = response.body;
                    var route = directions.routes[0];

                    var geometry_string = route.overview_polyline.points;
                    var geoJSON = polyline.toGeoJSON(geometry_string);
                    map.addSource('route', {
                        'type': 'geojson',
                        'data': geoJSON
                    });
                    // Add route layer below symbol layers
                    map.addLayer(
                        {
                            'id': 'route',
                            'type': 'line',
                            'source': 'route',
                            'layout': {
                                'line-join': 'round',
                                'line-cap': 'round'
                            },
                            'paint': {
                                'line-color': '#1e88e5',
                                'line-width': 8
                            }
                        },
                        firstSymbolId
                    );
                });
        });
    };

    var currentLocation = function () {
        if (c == null) {
            c = new goongjs.GeolocateControl({
                positionOptions: { enableHighAccuracy: true, timeout: 20000 },
                trackUserLocation: false,
                showUserLocation: true
            });
            // Add geolocate control to the map.
            map.addControl(
                c
            );
            c.on('geolocate', showPosition);
        }
    };

    var getLocation = function () {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(showPosition, showError, options);
        } else {
            document.getElementById("map-status").value = "Geolocation is not supported by this browser.";
        }
    };

    var showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                document.getElementById("map-status").value = "User denied the request for Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                document.getElementById("map-status").value = "Location information is unavailable.";
                break;
            case error.TIMEOUT:
                document.getElementById("map-status").value = "The request to get user location timed out.";
                break;
            case error.UNKNOWN_ERROR:
                document.getElementById("map-status").value = "An unknown error occurred.";
                break;
        }
    };

    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };

    var showPosition =  function (position) {
        var lng = position.coords.longitude;
        var lat = position.coords.latitude;
        $("#current-point").dxTextBox("instance").option("value", lat + "," + lng);
        
    };

    this.selectBox_valueChanged = function (data) {
        if (data.value == "Lý do khác") {
            $("#reason-cancel").dxTextBox("instance").option("visible", true);
            $("#reason-cancel").dxTextBox("instance").option("value", "");
        }
        else {
            $("#reason-cancel").dxTextBox("instance").option("visible", false);
            $("#reason-cancel").dxTextBox("instance").option("value", data.value);
        }
    };

    //this.editEndPoint = function (e) {
    //    e.component.option("visible", false);
    //    $("#confirm-end-point").dxButton({
    //        visible: true,
    //    });
    //    document.getElementById("geocoder").hidden = false;
    //    autocompleteEndPoint();
    //};

    //this.confirmEndPoint = function (e) {
    //    e.component.option("visible", false);
    //    $("#edit-end-point").dxButton({
    //        visible: true,
    //    });
    //    document.getElementById("geocoder").hidden = true;

    //    orderID = $("#order-id").dxTextBox("instance").option("value");
    //    endPoint = $("#end-point").dxTextBox("instance").option("value");
    //    endPointArr = endPoint.trim().split(',').reverse();
    //    endAdress = $("#end-adress").dxTextBox("instance").option("value");
    //    $.ajax({
    //        url: "UserOrder/UpdateEndPoint/" + orderID + " ?endpoint=" + endPoint + "&&adress=" + endAdress,
    //        method: 'PUT',
    //        data: {},
    //        success: function (data) {
    //            map.remove();
    //            map = new goongjs.Map({
    //                container: 'map-order-detail',
    //                style: 'https://tiles.goong.io/assets/goong_map_web.json',
    //                center: endPointArr,
    //                zoom: 15
    //            });
    //            fetchData(orderID);
    //            DevExpress.ui.notify("Cập nhật thành công điểm đến", "success", 500);
    //        }
    //    });
    //};

    var autocompleteEndPoint = function () {
        var data;
        var latlng;
        var latlngArr;
        if (geocoder == null) {

            geocoder = new GoongGeocoder({
                accessToken: mapAPIToken
            });

            geocoder.addTo('#geocoder');

            // Add geocoder result to container.
            geocoder.on('result', function (e) {
                data = JSON.parse(JSON.stringify(e.result, null, 2));
                var lat = data.result.geometry.location.lat;
                var lng = data.result.geometry.location.lng;
                latlng = lat + "," + lng;
                latlngArr = latlng.split(',').reverse();

                $("#end-point").dxTextBox("instance").option("value", latlng);
                //lấy địa chỉ điểm đến
                $.getJSON("Map/GetAdress/" + latlng,
                    function (data) {
                        $("#end-adress").dxTextBox("instance").option("value", data);
                    })


                var el = document.createElement('div');
                el.className = 'markercppp';
                el.style.backgroundImage =
                    "url('../image/startpoint24.png')";
                el.style.width = '24px';
                el.style.height = '24px';

                var marker = new goongjs.Marker(el)
                    .setLngLat(latlngArr)
                    .addTo(map);

                map.flyTo({
                    center: latlngArr,
                    zoom: 15,
                    essential: true
                });
            });

            // Clear results container when search is cleared.
            geocoder.on('clear', function () {
                results.innerText = '';
            });

        }
    };

    var fly = function (pointArr) {
        map.flyTo({
            center: pointArr,
            zoom: 15,
            essential: true
        });
    };

    var displayCurrentPoint = function (pointArr) {
        var size = 200;
        var canvas;
        //popup
        var popup = new goongjs.Popup({
            closeButton: false,
            closeOnClick: false
        });

        // implementation of CustomLayerInterface to draw a pulsing dot icon on the map
        // see https://docs.goong.io/javascript/properties/#customlayerinterface for more info
        var pulsingDot = {
            width: size,
            height: size,
            data: new Uint8Array(size * size * 4),

            // get rendering context for the map canvas when layer is added to the map
            onAdd: function () {
                canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                this.context = canvas.getContext('2d');
            },

            // called once before every frame where the icon will be used
            render: function () {
                var duration = 1000;
                var t = (performance.now() % duration) / duration;

                var radius = (size / 2) * 0.3;
                var outerRadius = (size / 2) * 0.7 * t + radius;
                var context = this.context;

                // draw outer circle
                context.clearRect(0, 0, this.width, this.height);
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    outerRadius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 200, 200,' + (1 - t) + ')';
                context.fill();

                // draw inner circle
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    radius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 100, 100, 1)';
                context.strokeStyle = 'white';
                context.lineWidth = 2 + 4 * (1 - t);
                context.fill();
                context.stroke();

                // update this image's data with data from the canvas
                this.data = context.getImageData(
                    0,
                    0,
                    this.width,
                    this.height
                ).data;

                // continuously repaint the map, resulting in the smooth animation of the dot
                map.triggerRepaint();

                // return `true` to let the map know that the image was updated
                return true;
            }
        };

        map.on('load', function () {
            map.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });

            map.addSource('points', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': [
                        {
                            'type': 'Feature',
                            'geometry': {
                                'type': 'Point',
                                'coordinates': pointArr
                            }
                        }
                    ]
                }
            });
            map.addLayer({
                'id': 'points',
                'type': 'symbol',
                'source': 'points',
                'layout': {
                    'icon-image': 'pulsing-dot'
                }
            });
        });
    };

    var displayStartPoint = function (pointArr, adress) {
        var el = document.createElement('div');
        el.className = 'markersp';
        el.style.backgroundImage =
            "url('../image/startpoint16.png')";
        el.style.width = '16px';
        el.style.height = '16px';

        //popup
        var popup = new goongjs.Popup({
            closeButton: false,
            closeOnClick: false
        });

        //mouse over
        el.addEventListener('mouseover', function () {
            el.style.backgroundImage =
                "url('../image/startpoint24.png')";
            el.style.width = '24px';
            el.style.height = '24px';
            map.getCanvas().style.cursor = 'pointer'
            popup.setLngLat(pointArr).setHTML(adress).addTo(map);
        });

        //mouse leave
        el.addEventListener('mouseleave', function () {
            el.style.backgroundImage =
                "url('../image/startpoint16.png')";
            el.style.width = '16px';
            el.style.height = '16px';
            map.getCanvas().style.cursor = '';
            popup.remove();
        });
        // add marker to map
        new goongjs.Marker(el)
            .setLngLat(pointArr)
            .addTo(map);
    };

    var displayEndPoint = function (pointArr, adress) {
        var el = document.createElement('div');
        el.className = 'markerep';
        el.style.backgroundImage =
            "url('../image/endpoint16.png')";
        el.style.width = '16px';
        el.style.height = '16px';

        //popup
        var popup = new goongjs.Popup({
            closeButton: false,
            closeOnClick: false
        });

        //mouse over
        el.addEventListener('mouseover', function () {
            el.style.backgroundImage =
                "url('../image/endpoint24.png')";
            el.style.width = '24px';
            el.style.height = '24px';
            map.getCanvas().style.cursor = 'pointer'
            popup.setLngLat(pointArr).setHTML(adress).addTo(map);
        });

        //mouse leave
        el.addEventListener('mouseleave', function () {
            el.style.backgroundImage =
                "url('../image/endpoint16.png')";
            el.style.width = '16px';
            el.style.height = '16px';
            map.getCanvas().style.cursor = '';
            popup.remove();
        });
        // add marker to map
        var marker = new goongjs.Marker(
            {
                element: el,
                draggable: true
            }
        )
            .setLngLat(pointArr)
            .addTo(map);

        function onDragEnd() {
            var coordinates = document.getElementById('coordinates');
            var lngLat = marker.getLngLat();
            coordinates.style.display = 'block';
            coordinates.innerHTML =
                'Longitude: ' + lngLat.lng + '<br />Latitude: ' + lngLat.lat;
        }

        marker.on('dragend', onDragEnd);
    };

    this.toggleSidebar = function (id) {
        var elem = document.getElementById(id);
        var classes = elem.className.split(' ');
        var collapsed = classes.indexOf('collapsed') !== -1;

        var padding = {};

        if (collapsed) {
            // Remove the 'collapsed' class from the class list of the element, this sets it back to the expanded state.
            classes.splice(classes.indexOf('collapsed'), 1);

            padding[id] = 300; // In px, matches the width of the sidebars set in .sidebar CSS class
            map.easeTo({
                padding: padding,
                duration: 1000 // In ms, CSS transition duration property for the sidebar matches this value
            });
        } else {
            padding[id] = 0;
            // Add the 'collapsed' class to the class list of the element
            classes.push('collapsed');

            map.easeTo({
                padding: padding,
                duration: 1000
            });
        }

        // Update the class list on the element
        elem.className = classes.join(' ');
    }

    this.test = function () {
        if (window.history && window.history.pushState) {

            $(window).on('popstate', function () {
                var hashLocation = location.hash;
                var hashSplit = hashLocation.split("#!/");
                var hashName = hashSplit[1];

                if (hashName !== '') {
                    var hash = window.location.hash;


                    if (hash === "") {
                        const popup = $("#order-map-popup").dxPopup("instance");
                        popup.hide();
                        window.history.pushState('back', null, null);
                    }
                }
            });

            window.history.pushState('forward', null, './#forward');
        }
    }

    this.init = function () {

    };
}

DevAV.orderMapUser = new DevAV.OrderMapUser();

$(function () {
    DevAV.orderMapUser.init();
});