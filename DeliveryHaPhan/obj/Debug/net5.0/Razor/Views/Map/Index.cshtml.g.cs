#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fb413fcfa0cae6fe7ec909022a6a199ca2eb5f3a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.Map.Views_Map_Index), @"mvc.1.0.view", @"/Views/Map/Index.cshtml")]
namespace DeliveryHaPhan.Models.Map
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fb413fcfa0cae6fe7ec909022a6a199ca2eb5f3a", @"/Views/Map/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_Map_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<DeliveryHaPhan.Models.OrderModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"
<link rel=""stylesheet"" type=""text/css"" href=""https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"" />
<style>
    #icon-back, #icon-disabled-back {
        margin-left: 4px;
    }

    .dx-viewport:not(.dx-theme-ios7) .dx-fieldset {
        width: 520px;
        margin: 30px auto;
    }

        .dx-viewport:not(.dx-theme-ios7) .dx-fieldset:first-of-type {
            margin-top: 120px;
        }

    .dx-viewport:not(.dx-theme-ios7) .dx-fieldset-header {
        font-size: 16px;
    }

    .dx-viewport:not(.dx-theme-ios7) .dx-field {
        display: inline-block;
        margin-right: 20px;
    }

    .dx-viewport:not(.dx-theme-ios7) .dx-field-value:not(.dx-widget) > .dx-button {
        float: none;
    }

    .dx-viewport:not(.dx-theme-ios7) .dx-field-value:not(.dx-switch):not(.dx-checkbox):not(.dx-button),
    .dx-viewport:not(.dx-theme-ios7) .dx-field-label {
        float: none;
        width: 100%;
    }

    .dx-viewport:not(.dx-theme-ios7) .");
            WriteLiteral(@"dx-field-label {
        padding-left: 0;
    }

    .send .dx-button-content .dx-icon {
        font-size: 18px;
    }

    .fields-container {
        display: flex;
        align-items: baseline;
    }

    .dx-field-value {
        display: flex;
    }
</style>

<div class=""dx-fieldset"">
    <div class=""fields-container"">
        <div class=""dx-field"">
            <div class=""dx-field-label"">Built-in icon</div>
            <div class=""dx-field-value"">
                ");
#nullable restore
#line 60 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml"
            Write(Html.DevExtreme().Button()
                    .Icon("check")
                    .Text("Done")
                    .Type(ButtonType.Success)
                    .OnClick("notifylatlng")
                );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n        <div class=\"dx-field\">\r\n            <div class=\"dx-field-label\">Image icon</div>\r\n            <div class=\"dx-field-value\">\r\n                ");
#nullable restore
#line 71 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml"
            Write(Html.DevExtreme().Button()
                    .Icon(Url.Content("~/images/button/weather.png"))
                    .Text("Weather")
                    .OnClick("notify")
                );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n        <div class=\"dx-field\">\r\n            <div class=\"dx-field-label\">External icon</div>\r\n            <div class=\"dx-field-value\">\r\n                ");
#nullable restore
#line 81 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml"
            Write(Html.DevExtreme().Button()
                    .Icon("fa fa-envelope-o")
                    .Text("Send")
                    .ElementAttr("class", "send")
                    .OnClick("notify")
                );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </div>\r\n        </div>\r\n        <div class=\"dx-field\">\r\n            <div class=\"dx-field-label\">Icon only</div>\r\n            <div class=\"dx-field-value\">\r\n                ");
#nullable restore
#line 92 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml"
            Write(Html.DevExtreme().Button()
                    .Icon("plus")
                    .OnClick("notify")
                );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                ");
#nullable restore
#line 96 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml"
            Write(Html.DevExtreme().Button()
                    .ID("icon-back")
                    .Icon("back")
                    .OnClick("notify")
                );

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
            </div>
        </div>
    </div>
</div>
<script>
    function notify(data) {
        var buttonText = data.component.option(""text"");

        DevExpress.ui.notify(""The "" + buttonText + "" button was clicked"");

    }
    function notifylatlng() {
        DevExpress.ui.notify('");
#nullable restore
#line 113 "D:\DeliveryApp\da\DeliveryHaPhan\Views\Map\Index.cshtml"
                         Write(ViewBag.EndPoint);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"');
    }

    var lng = """";
    var lat = """";

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = ""Geolocation is not supported by this browser."";
        }
    }

    function showPosition(position) {
        lng = position.coords.longitude;
        lat = position.coords.latitude;
        window.alert(""Latitude: "" + lat +
            ""<br>Longitude: "" + lng);
    }
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<DeliveryHaPhan.Models.OrderModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
