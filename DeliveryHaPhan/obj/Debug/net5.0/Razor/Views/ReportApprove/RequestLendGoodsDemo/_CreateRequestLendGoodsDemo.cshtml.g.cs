#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7ae05060f6dd712e7e58347360dd767915f8a3ec"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.ReportApprove.RequestLendGoodsDemo.Views_ReportApprove_RequestLendGoodsDemo__CreateRequestLendGoodsDemo), @"mvc.1.0.view", @"/Views/ReportApprove/RequestLendGoodsDemo/_CreateRequestLendGoodsDemo.cshtml")]
namespace DeliveryHaPhan.Models.ReportApprove.RequestLendGoodsDemo
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7ae05060f6dd712e7e58347360dd767915f8a3ec", @"/Views/ReportApprove/RequestLendGoodsDemo/_CreateRequestLendGoodsDemo.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_ReportApprove_RequestLendGoodsDemo__CreateRequestLendGoodsDemo : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/reportapprove/request-type.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/reportapprove/request-lend-goods-demo.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<link rel=""stylesheet"" href=""https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans"">
<link rel=""stylesheet"" href=""https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"">
<link rel=""stylesheet"" href=""https://fonts.googleapis.com/icon?family=Material+Icons"">
<link rel=""stylesheet"" href=""https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"">
z
<script src=""https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js""></script>
<script src=""https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js""></script>
<style>
    .table-wrapper {
        width: 700px;
        margin: 30px auto;
        background: #fff;
        padding: 20px;
        box-shadow: 0 1px 1px rgba(0,0,0,.05);
    }

    .table-title {
        padding-bottom: 10px;
        margin: 0 0 10px;
    }

        .table-title h2 {
            margin: 6px 0 0;
            font-size: 22px;
        }

        .table-title .add-new {
            float:");
            WriteLiteral(@" right;
            height: 30px;
            font-weight: bold;
            font-size: 12px;
            text-shadow: none;
            min-width: 100px;
            border-radius: 50px;
            line-height: 13px;
        }

            .table-title .add-new i {
                margin-right: 4px;
            }

    table.table {
        table-layout: fixed;
    }

        table.table tr th, table.table tr td {
            border-color: #e9e9e9;
        }

        table.table th i {
            font-size: 13px;
            margin: 0 5px;
            cursor: pointer;
        }

        table.table th:last-child {
            width: 100px;
        }

        table.table td a {
            cursor: pointer;
            display: inline-block;
            margin: 0 5px;
            min-width: 24px;
        }

            table.table td a.add {
                color: #27C46B;
            }

            table.table td a.edit {
                color: #FFC107;
          ");
            WriteLiteral(@"  }

            table.table td a.delete {
                color: #E34724;
            }

        table.table td i {
            font-size: 19px;
        }

        table.table td a.add i {
            font-size: 24px;
            margin-right: -1px;
            position: relative;
            top: 3px;
        }

        table.table .form-control {
            height: 32px;
            line-height: 32px;
            box-shadow: none;
            border-radius: 2px;
        }

            table.table .form-control.error {
                border-color: #f50000;
            }

        table.table td .add {
            display: none;
        }
</style>
");
#nullable restore
#line 107 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
Write(Html.DevExtreme().Popup()
        .ID("popup-create-request-lend-goods-demo")
        .Width("auto")
        .Height("600")
        .FullScreen(false)
        .Position(PositionAlignment.Center)
        .Container(".dx-viewport")
        .ShowTitle(true)
        .Title("Tạo đề xuất mượn hàng demo")
        .DragEnabled(false)
        .CloseOnOutsideClick(false)
        .ShowCloseButton(false)
        .ContentTemplate(new TemplateName("popup-create-request-lend-goods-demo-template"))
        .DeferRendering(true)
        .ToolbarItems(barItems => {
            barItems.Add()
                .Toolbar(Toolbar.Bottom)
                .Location(ToolbarItemLocation.After)
                .Widget(widget => widget.Button()
                    .Text("Thoát")
                    .Type(ButtonType.Danger)
                    .StylingMode(ButtonStylingMode.Outlined)
                    .OnClick(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral(@"
                                function hideInfo(data) {
                                    const popup = $(""#popup-create-request-lend-goods-demo"").dxPopup(""instance"");
                                    popup.hide();
                                }
                            ");
    PopWriter();
}
))
                );
            barItems.Add()
                .Toolbar(Toolbar.Bottom)
                .Location(ToolbarItemLocation.After)
                .Widget(widget => widget.Button()
                    .Text("Tạo đề xuất mới")
                    .Type(ButtonType.Success)
                    .StylingMode(ButtonStylingMode.Outlined)
                    .UseSubmitBehavior(false)
                    .OnClick("DevAV.requestLendGoodsDemos.createNewRequest")
                );
        })
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n");
#nullable restore
#line 150 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
 using (Html.DevExtreme().NamedTemplate("popup-create-request-lend-goods-demo-template"))
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 152 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
Write(Html.DevExtreme().ScrollView()
        .Width("100%")
        .Height("100%")
        .Content(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n\r\n");
#nullable restore
#line 157 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
     using (Html.BeginForm())
    {

        using (Html.DevExtreme().ValidationGroup())
        {

            

#line default
#line hidden
#nullable disable
#nullable restore
#line 163 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
       Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
#nullable restore
#line 165 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
        Write(Html.DevExtreme().Form()
            .ID("request-form")
            .ShowColonAfterLabel(true)
            .Height("auto")
            .ShowValidationSummary(false)
            .ScrollingEnabled(true)
            .Items(items => {


                items.AddGroup()
                        .Items(groupItems => {

                            groupItems.AddSimple().DataField("RequestTitle").Label(l => l.Text("Tiêu đề đề xuất"))
                                .Editor(e => e
                                    .TextBox()
                                    .ID("request-title")
                                );
                            groupItems.AddSimple().DataField("RequestType").Label(l => l.Text("Nhóm đề xuất"))
                                .Editor(e => e
                                    .SelectBox()
                                    .ID("request-type")
                                    .DataSource(new JS("request_type"))
                                    .ValueExpr("text")
                                    .DisplayExpr("text")
                                    .Value("Đề xuất mượn hàng demo")
                                );
                            groupItems.AddSimple().DataField("CreateDate").Label(l => l.Text("Ngày trình"))
                             .Editor(e => e
                                 .DateBox()
                                 .ID("create-date")
                                 .Type(DateBoxType.Date)
                                 .DisplayFormat("yyyy/MM/dd")
                                 .Value(DateTime.Now)
                                 .ReadOnly(true)
                             );
                            groupItems.AddSimple().DataField("CreateName").Label(l => l.Text("Người trình"))
                                .Editor(e => e
                                    .TextBox()
                                    .ID("create-name")
                                    .Value(HttpContextAccessor.HttpContext.Session.GetString("Username"))
                                    .Visible(true)
                                    .ReadOnly(true)
                                );
                            groupItems.AddSimple().DataField("CreateEmail").Label(l => l.Text("Email"))
                              .Editor(e => e
                                  .TextBox()
                                  .ID("create-email")
                                  .Value(HttpContextAccessor.HttpContext.Session.GetString("Email"))
                                  .ReadOnly(true)
                              );

                            groupItems.AddSimple().DataField("CreateDepartment").Label(l => l.Text("Bộ phận"))
                             .Editor(e => e
                                 .TextBox()
                                 .ID("create-department")
                                 .Value(HttpContextAccessor.HttpContext.Session.GetString("Department"))
                                 .ReadOnly(true)
                             );
                            groupItems.AddSimple().DataField("Approvers").Label(l => l.Text("Người duyệt"))
                             .Editor(e => e
                                 .SelectBox()
                                 .ID("approvers")
                                 .DataSourceOptions(o => o.Group("Department"))
                                .Grouped(true)
                                 .DataSource(d => d
                                   .Mvc()
                                   .Controller("UserNewApi")
                                   .LoadAction("GetApprovers")
                                   .LoadMode(DataSourceLoadMode.Raw)
                                   .Key("UserID")
                               )
                               .DisplayExpr(new JS("displayExpr"))
                               .ValueExpr("UserName")
                               .SearchEnabled(true)
                             );
                            groupItems.AddSimple().DataField("Followers").Label(l => l.Text("Người theo dõi"))
                              .Editor(e => e
                                  .TagBox()
                                  .ShowMultiTagOnly(false)
                                  .ID("followers")
                                  .DataSourceOptions(o => o.Group("Department"))
                                  .Grouped(true)
                                  .HideSelectedItems(true)
                                  .DataSource(d => d
                                    .Mvc()
                                    .Controller("UserNewApi")
                                    .LoadAction("GetFollowers")
                                    .LoadMode(DataSourceLoadMode.Raw)
                                    .Key("UserID")
                                )
                                .DisplayExpr(new JS("displayExpr"))
                                .ValueExpr("UserName")
                                .SearchEnabled(true)
                              );
                            groupItems.AddSimple().DataField("ProcessingTime").Label(l => l.Text("Thời hạn xử lý"))
                              .Editor(e => e
                                  .DateBox()
                                  .ID("processing-time")
                                  .Type(DateBoxType.DateTime)
                              );


                           groupItems.AddSimple().Template(new TemplateName("request-lend-goods-demo-detail-template"));



                            groupItems.AddSimple().DataField("LendTime").Label(l => l.Text("Thời gian mượn"))
                              .Editor(e => e
                                  .DateBox()
                                  .ID("lend-time")
                                  .Type(DateBoxType.DateTime)
                              );

                            groupItems.AddSimple().DataField("Reason").Label(l => l.Text("Lý do"))
                              .Editor(e => e
                                  .TextBox()
                                  .ID("reason")
                              );
                        });
            })
        );

#line default
#line hidden
#nullable disable
#nullable restore
#line 285 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
         
        }
    }

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n");
    PopWriter();
}
))
    );

#line default
#line hidden
#nullable disable
#nullable restore
#line 290 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
     

}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 294 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
 using (Html.DevExtreme().NamedTemplate("request-lend-goods-demo-detail-template"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <div class=""col-md-12 mt-1 mb-2""><button type=""button"" id=""add"" class=""btn btn-success"">Add</button></div>
    <table id=""lend-goods-demo-table"" class=""display nowrap"" style=""width:100%"">
        <thead>
            <tr>
                <th>Mô tả sản phẩm</th>
                <th>Mã sản phẩm</th>
                <th>Serial sản phẩm</th>
                <th>Số lượng</th>
                <th>Ghi chú</th>
                <th></th>
            </tr>
        </thead>
    </table>
");
#nullable restore
#line 309 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestLendGoodsDemo\_CreateRequestLendGoodsDemo.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<script>\r\n    function displayExpr(item) {\r\n        return item && item.UserName + \'(\' + item.FullName + \'- \' +item.Position + \')\';\r\n    }\r\n\r\n</script>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7ae05060f6dd712e7e58347360dd767915f8a3ec18948", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7ae05060f6dd712e7e58347360dd767915f8a3ec19988", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
