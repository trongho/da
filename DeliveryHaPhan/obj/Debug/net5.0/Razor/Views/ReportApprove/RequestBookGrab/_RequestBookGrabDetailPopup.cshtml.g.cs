#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "836527944071a27bc21463b11f3b1ee58df2701e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.ReportApprove.RequestBookGrab.Views_ReportApprove_RequestBookGrab__RequestBookGrabDetailPopup), @"mvc.1.0.view", @"/Views/ReportApprove/RequestBookGrab/_RequestBookGrabDetailPopup.cshtml")]
namespace DeliveryHaPhan.Models.ReportApprove.RequestBookGrab
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"836527944071a27bc21463b11f3b1ee58df2701e", @"/Views/ReportApprove/RequestBookGrab/_RequestBookGrabDetailPopup.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_ReportApprove_RequestBookGrab__RequestBookGrabDetailPopup : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/reportapprove/request-type.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/reportapprove/book-type.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/reportapprove/request-detail.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
Write(Html.DevExtreme().Popup()
        .ID("popup-request-book-grab-detail")
        .Width("auto")
        .Height("600")
        .FullScreen(false)
        .Position(PositionAlignment.Center)
        .Container(".dx-viewport")
        .ShowTitle(true)
        .Title("Chi tiết đề xuất")
        .DragEnabled(false)
        .ShowCloseButton(false)
        .ContentTemplate(new TemplateName("popup-request-book-grab-detail-template"))
        .DeferRendering(true)
        .ToolbarItems(barItems => {
            barItems.Add()
                .Toolbar(Toolbar.Bottom)
                .Location(ToolbarItemLocation.After)
                .Widget(widget => widget.Button()
                    .Text("Thoát")
                    .Type(ButtonType.Danger)
                    .StylingMode(ButtonStylingMode.Outlined)
                    .OnClick(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral(@"
                                function hideInfo(data) {
                                    const popup = $(""#popup-request-book-grab-detail"").dxPopup(""instance"");
                                    popup.hide();
                                }
                            ");
    PopWriter();
}
))
                );
            barItems.Add()
              .Toolbar(Toolbar.Bottom)
              .Location(ToolbarItemLocation.After)
              .Widget(widget => widget.Button()
                  .Icon("print")
                  .Type(ButtonType.Default)
                  .StylingMode(ButtonStylingMode.Outlined)
                  .OnClick("DevAV.requestDetails.openPrintPopup")
              );
        })
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n");
#nullable restore
#line 44 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
 using (Html.DevExtreme().NamedTemplate("popup-request-book-grab-detail-template"))
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 46 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
Write(Html.DevExtreme().ScrollView()
        .Width("100%")
        .Height("100%")
        .Content(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral("\r\n\r\n");
#nullable restore
#line 51 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
     using (Html.BeginForm())
    {

        using (Html.DevExtreme().ValidationGroup())
        {

            

#line default
#line hidden
#nullable disable
#nullable restore
#line 57 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
       Write(Html.AntiForgeryToken());

#line default
#line hidden
#nullable disable
#nullable restore
#line 59 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
        Write(Html.DevExtreme().Form()
            .ID("request-form")
            .ShowColonAfterLabel(true)
            .Height("auto")
            .ShowValidationSummary(false)
            .ScrollingEnabled(true)
            .Items(items => {


                items.AddGroup()
                        .Items(groupItems => {

                            groupItems.AddSimple().DataField("RequestID").Label(l => l.Text("Mã đề xuất"))
                               .Editor(e => e
                                   .TextBox()
                                   .ID("request-id")
                                   .ReadOnly(true)
                               );

                            groupItems.AddSimple().DataField("RequestTitle").Label(l => l.Text("Tiêu đề đề xuất"))
                                .Editor(e => e
                                    .TextBox()
                                    .ID("request-title")
                                    .ReadOnly(true)
                                );
                            groupItems.AddSimple().DataField("RequestType").Label(l => l.Text("Nhóm đề xuất"))
                                .Editor(e => e
                                    .SelectBox()
                                    .ID("request-type")
                                    .DataSource(new JS("request_type"))
                                    .ValueExpr("text")
                                    .DisplayExpr("text")
                                     .Value("Đề xuất book grab")
                                    .ReadOnly(true)
                                );
                            groupItems.AddSimple().DataField("Status").Label(l => l.Text("Trạng thái"))
                              .Editor(e => e
                                  .TextBox()
                                  .ID("status")
                                  .ReadOnly(true)
                              );
                            groupItems.AddSimple().DataField("CreateDate").Label(l => l.Text("Ngày trình"))
                             .Editor(e => e
                                 .DateBox()
                                 .ID("create-date")
                                 .Type(DateBoxType.Date)
                                 .DisplayFormat("yyyy/MM/dd")
                                 .Value(DateTime.Now)
                                 .ReadOnly(true)
                             );
                            groupItems.AddSimple().DataField("CreateName").Label(l => l.Text("Người trình"))
                                .Editor(e => e
                                    .TextBox()
                                    .ID("create-name")
                                    .Value(HttpContextAccessor.HttpContext.Session.GetString("Username"))
                                    .Visible(true)
                                    .ReadOnly(true)
                                );
                            groupItems.AddSimple().DataField("CreateEmail").Label(l => l.Text("Email"))
                              .Editor(e => e
                                  .TextBox()
                                  .ID("create-email")
                                  .Value(HttpContextAccessor.HttpContext.Session.GetString("Email"))
                                  .ReadOnly(true)
                              );

                            groupItems.AddSimple().DataField("CreateDepartment").Label(l => l.Text("Bộ phận"))
                             .Editor(e => e
                                 .TextBox()
                                 .ID("create-department")
                                 .Value(HttpContextAccessor.HttpContext.Session.GetString("Department"))
                                 .ReadOnly(true)
                             );
                            groupItems.AddSimple().DataField("Approvers").Label(l => l.Text("Người duyệt"))
                             .Editor(e => e
                                 .SelectBox()
                                 .ID("approvers")
                                 .DataSourceOptions(o => o.Group("Department"))
                                .Grouped(true)
                                 .DataSource(d => d
                                   .Mvc()
                                   .Controller("UserNewApi")
                                   .LoadAction("GetApprovers")
                                   .LoadMode(DataSourceLoadMode.Raw)
                                   .Key("UserID")
                               )
                               .DisplayExpr(new JS("displayExpr"))
                               .ValueExpr("UserName")
                               .SearchEnabled(true)
                               .ReadOnly(true)
                             );
                            groupItems.AddSimple().DataField("Followers").Label(l => l.Text("Người theo dõi"))
                              .Editor(e => e
                                  .TagBox()
                                  .ShowMultiTagOnly(false)
                                  .ID("followers")
                                  .DataSourceOptions(o => o.Group("Department"))
                                  .Grouped(true)
                                  .HideSelectedItems(true)
                                  .DataSource(d => d
                                    .Mvc()
                                    .Controller("UserNewApi")
                                    .LoadAction("GetFollowers")
                                    .LoadMode(DataSourceLoadMode.Raw)
                                    .Key("UserID")
                                )
                                .DisplayExpr(new JS("displayExpr"))
                                .ValueExpr("UserName")
                                .SearchEnabled(true)
                                .ReadOnly(true)
                              );
                            groupItems.AddSimple().DataField("ProcessingTime").Label(l => l.Text("Thời hạn xử lý"))
                              .Editor(e => e
                                  .DateBox()
                                  .ID("processing-time")
                                  .Type(DateBoxType.DateTime)
                                  .ReadOnly(true)
                              );


                            groupItems.AddSimple().Template(new TemplateName("request-book-grab-detail-template"));

                            groupItems.AddSimple().DataField("BookType").Label(l => l.Text("Hình thức book xe"))
                            .Editor(e => e
                                   .SelectBox()
                                   .ID("book-type")
                                   .DataSource(new JS("booktype"))
                                   .ValueExpr("text")
                                   .DisplayExpr("text")
                                   .Value("Grab")
                                   .ReadOnly(true)
                               );

                        });

            })
        );

#line default
#line hidden
#nullable disable
#nullable restore
#line 195 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
         
        }
    }

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n");
    PopWriter();
}
))
    );

#line default
#line hidden
#nullable disable
#nullable restore
#line 200 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
     

}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 204 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
 using (Html.DevExtreme().NamedTemplate("request-book-grab-detail-template"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral(@"    <table id=""book-grab-table"" class=""display nowrap"" style=""width:100%"">
        <thead>
            <tr>
                <th>Nội dung đề xuất</th>
                <th>Địa chỉ đi</th>
                <th>Địa chỉ đến</th>
                <th>Thời gian thực hiện</th>
                <th>Số thẻ taxi/ Hình thức grab</th>
                <th></th>
            </tr>
        </thead>
    </table>
");
#nullable restore
#line 218 "D:\DeliveryApp\da\DeliveryHaPhan\Views\ReportApprove\RequestBookGrab\_RequestBookGrabDetailPopup.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<script>\r\n    function displayExpr(item) {\r\n        return item && item.UserName + \'(\' + item.FullName + \')\';\r\n    }\r\n\r\n</script>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "836527944071a27bc21463b11f3b1ee58df2701e16946", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "836527944071a27bc21463b11f3b1ee58df2701e17986", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "836527944071a27bc21463b11f3b1ee58df2701e19026", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
