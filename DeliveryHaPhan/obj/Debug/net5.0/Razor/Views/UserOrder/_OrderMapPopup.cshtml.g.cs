#pragma checksum "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "586926bd79753f56301629f6d560f4a81dd59543"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(DeliveryHaPhan.Models.UserOrder.Views_UserOrder__OrderMapPopup), @"mvc.1.0.view", @"/Views/UserOrder/_OrderMapPopup.cshtml")]
namespace DeliveryHaPhan.Models.UserOrder
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DeliveryHaPhan;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "D:\DeliveryApp\da\DeliveryHaPhan\Views\_ViewImports.cshtml"
using DevExtreme.AspNet.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"586926bd79753f56301629f6d560f4a81dd59543", @"/Views/UserOrder/_OrderMapPopup.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ed451b47b1e684cb67de74559539f4fe2d82bcdf", @"/Views/_ViewImports.cshtml")]
    public class Views_UserOrder__OrderMapPopup : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/js/order-map-popup-user.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n\r\n<style>\r\n\r\n    ");
            WriteLiteral("@media only screen and (max-width: 2000px) and (min-width: 1140px) {\r\n        .map-load-panel {\r\n            height: 70vh;\r\n            overflow: hidden;\r\n        }\r\n    }\r\n\r\n    ");
            WriteLiteral("@media only screen and (max-width: 1140px) and (min-width: 960px) {\r\n        .map-load-panel {\r\n            height: 70vh;\r\n            overflow: hidden;\r\n        }\r\n    }\r\n\r\n    ");
            WriteLiteral("@media only screen and (max-width: 960px) and (min-width: 720px) {\r\n        .map-load-panel {\r\n            height: 30vh;\r\n            overflow: hidden;\r\n        }\r\n    }\r\n\r\n    ");
            WriteLiteral("@media only screen and (max-width: 720px) and (min-width:540px) {\r\n        .map-load-panel {\r\n            height: 30vh;\r\n            overflow: hidden;\r\n        }\r\n    }\r\n\r\n\r\n    ");
            WriteLiteral(@"@media only screen and (max-width: 540px) {
        .map-load-panel {
            height: 30vh;
            overflow: hidden;
        }
    }



    .map-order-detail {
        height: 100%;
    }

    .approved .dx-texteditor-input {
        color: white;
        border: none;
        background-color: #e1711c;
        text-align: center;
    }

    .started .dx-texteditor-input {
        color: white;
        border: none;
        background-color: #1ea114;
        text-align: center;
    }

    .pause .dx-texteditor-input {
        color: white;
        border: none;
        background-color: #db2222;
        text-align: center;
    }

    .continue .dx-texteditor-input {
        color: white;
        border: none;
        background-color: #0eb1b1;
        text-align: center;
    }

    .coordinates {
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        position: absolute;
        bottom: 40px;
        left: 10px;
        padding: 5px 10px;
 ");
            WriteLiteral(@"       margin: 0;
        font-size: 11px;
        line-height: 18px;
        border-radius: 3px;
        display: none;
    }

    .kilo {
        background: rgba(0, 0, 0, 0.5);
        color: #fff;
        position: absolute;
        top: 2px;
        left: 5px;
        padding: 5px 10px;
        margin: 0;
        font-size: 11px;
        line-height: 18px;
        border-radius: 3px;
    }

        .kilo p {
        }

    .time .dx-texteditor-input {
        background: #000000;
        color: #0eb1b1;
        font-size: 12px;
        font-family: Orbitron;
        letter-spacing: 2px;
        border: none;
    }

    .field-value .dx-texteditor-input {
        border: none;
        color: #2b0a5b;
        background: #a79a9a;
        font-family: Ebrima;
    }


    /*Sidebar*/
    .rounded-rect {
        background: white;
        border-radius: 10px;
        box-shadow: 0 0 50px -25px black;
    }

    .flex-center {
        position: absolute;
       ");
            WriteLiteral(@" display: flex;
        justify-content: center;
        align-items: center;
    }

        .flex-center.left {
            left: 0px;
        }

        .flex-center.right {
            right: 0px;
        }

        .flex-center.bottom {
            bottom: 0px;
        }

    .sidebar-content {
        position: absolute;
        width: 95%;
        height: 95%;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 32px;
        color: gray;
    }

    .sidebar-toggle {
        position: absolute;
        width: 1.3em;
        height: 1.3em;
        overflow: visible;
        display: flex;
        justify-content: center;
        align-items: center;
    }

        .sidebar-toggle.left {
            right: -1.5em;
        }

        .sidebar-toggle.right {
            left: -1.5em;
        }

        .sidebar-toggle.bottom {
            top: -1.5em;
        }

        .sidebar-toggle:hover {
            color: #0aa1cf;
            cursor: ");
            WriteLiteral(@"pointer;
        }

    .sidebar {
        transition: transform 1s;
        z-index: 1;
        width: 300px;
        height: 100%;
    }

    /*
    The sidebar styling has them ""expanded"" by default, we use CSS transforms to push them offscreen
    The toggleSidebar() function removes this class from the element in order to expand it.
    */
    .left.collapsed {
        transform: translateX(-295px);
    }

    .right.collapsed {
        transform: translateX(295px);
    }

    .bottom.collapsed {
        transform: translateY(295px);
    }
</style>
");
#nullable restore
#line 207 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
Write(await Html.PartialAsync("~/Views/UserOrder/_OrderCancelPopup.cshtml"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 208 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
Write(await Html.PartialAsync("~/Views/UserOrder/_DirectionPopup.cshtml"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 210 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
Write(Html.DevExtreme().Popup()
    .ID("order-map-popup")
    .ElementAttr("class","popup")
    .FullScreen(true)
    .MinHeight(280)
    .MinWidth(300)
    .Container(".dx-viewport")
    .DragEnabled(false)
    .CloseOnOutsideClick(false)
   
    .ShowCloseButton(false)
    .ToolbarItems(barItems => {
    barItems.Add()
            .Toolbar(Toolbar.Top)
            .Location(ToolbarItemLocation.After)
            .Widget(widget => widget.Button()
                .Icon("close")
                .Type(ButtonType.Danger)
                .OnClick(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral(@"
                        function hideInfo(data) {
                            if (true) {
                                const popup = $(""#order-map-popup"").dxPopup(""instance"");
                                popup.hide();
                                const dataGrid = $(""#gridContainer"").dxDataGrid(""instance"");
                                dataGrid.refresh();
                            }
");
    WriteLiteral("                        }\r\n                    ");
    PopWriter();
}
))
                );

        barItems.Add()
                .Toolbar(Toolbar.Bottom)
                .Location(ToolbarItemLocation.Center)
                .Widget(widget => widget.Button()
                    .ID("start_button")
                    .Icon("fas fa-play-circle")
                    .Type(ButtonType.Default)
                    .Visible(true)
                .OnClick("DevAV.orderMapUser.startbutton_onClick")
                );
        barItems.Add()
                .Toolbar(Toolbar.Bottom)
                .Location(ToolbarItemLocation.Center)
                .Widget(widget => widget.Button()
                    .ID("pause_button")
                    .Icon("fas fa-pause-circle")
                    .Type(ButtonType.Danger)
                    .Visible(false)
                .OnClick("DevAV.orderMapUser.pausebutton_onClick")
                );
        barItems.Add()
               .Toolbar(Toolbar.Bottom)
               .Location(ToolbarItemLocation.Center)
               .Widget(widget => widget.Button()
                   .ID("end_button")
                   .Icon("far fa-check-circle")
                   .Type(ButtonType.Success)
                   .Visible(true)
               .OnClick("DevAV.orderMapUser.endbutton_onClick")
               );
        barItems.Add()
               .Toolbar(Toolbar.Bottom)
               .Location(ToolbarItemLocation.Center)
               .Widget(widget => widget.Button()
                   .ID("cancel_button")
                   .Icon("fas fa-window-close")
                   .Type(ButtonType.Danger)
                   .Visible(true)
               .OnClick("DevAV.orderMapUser.popupOrderCancel")
               );
        barItems.Add()
             .Toolbar(Toolbar.Top)
             .Location(ToolbarItemLocation.Before)
             .Widget(widget => widget.TextBox()
                 .ID("order-id")
                 .ElementAttr("class", "order-id")
                 .ReadOnly(true)
             );
    })
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 302 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
 using (Html.DevExtreme().NamedTemplate("popup-order-map-template"))
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 304 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
Write(Html.DevExtreme().ScrollView()
        .Width("100%")
        .Height("100%")
        .Content(item => new global::Microsoft.AspNetCore.Mvc.Razor.HelperResult(async(__razor_template_writer) => {
    PushWriter(__razor_template_writer);
    WriteLiteral(@"
    <div class=""container-fluid"" >
        <div class=""row"" style=""background-color:#e7e9ed"">
            <div class=""map-load-panel col-xl-6 p-1"" style=""background-color:white"">
                <div id=""map-order-detail"" class=""map-order-detail"">

");
    WriteLiteral(@"
                    <div class=""kilo"">
                        <p>
                            Kms org:
                            <span id=""kilometers-org""></span>
                        </p>
                        <p>
                            Kms:
                            <span id=""kilometers""></span>
                        </p>
                        ");
#nullable restore
#line 357 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                    Write(Html.DevExtreme().Button()
                                .ID("direction")
                                .Icon("fas fa-directions")
                                .Type(ButtonType.Success)
                            .OnClick("DevAV.orderMapUser.popupDirection")
                            );

#line default
#line hidden
#nullable disable
    WriteLiteral(@"

                    </div>
                </div>


                <pre id=""coordinates"" class=""coordinates""></pre>
                <div id=""map-status""></div>
            </div>

            <div class=""col-xl-6 p-1"" style=""background-color:white"">
                <div class=""row"">
                    <div class=""dx-field col-sm-12"">
                         ");
#nullable restore
#line 375 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                                .ElementAttr("class", "status")
                                .ID("status")
                                .ReadOnly(true)
                                .Visible(true)
                            );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                        ");
#nullable restore
#line 386 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                    Write(Html.DevExtreme().TextBox()
                                //.ElementAttr("class", "field-value")
                                .ID("order-fee")
                                .ReadOnly(true)
                                .Visible(true).Label("Thu phí").LabelMode(EditorLabelMode.Floating)
                            );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                         ");
#nullable restore
#line 397 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                        .ID("start-adress")
                        .Label("Điểm đi").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 403 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().Tooltip()
                                .ID("tt-start-adress")
                                .Target("#start-adress")
                                .ShowEvent("mouseenter")
                                .HideEvent("mouseleave")
                                .CloseOnOutsideClick(false)
                                .Position(Position.Right)
                                .OnShowing("DevAV.orderMapUser.tooltipShow")
                                .ContentTemplate(new TemplateName("tooltip-start-adress-template"))
                            );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 413 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().TextBox()
                        .ID("start-point")
                        .Visible(false)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 417 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().TextBox()
                        .ID("current-point")
                        .Visible(false)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 421 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().TextBox()
                        .ID("current-adress")
                        .Visible(false)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 425 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().TextBox()
                        .ID("pre-point")
                        .Visible(false)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n\r\n                            ");
#nullable restore
#line 437 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().TextBox()
                        .ID("end-adress")
                        .Label("Điểm đến").LabelMode(EditorLabelMode.Floating)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 442 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().Tooltip()
                                .ID("tt-end-adress")
                                .Target("#end-adress")
                                .ShowEvent("mouseenter")
                                .HideEvent("mouseleave")
                                .CloseOnOutsideClick(false)
                                .Position(Position.Right)
                                .OnShowing("DevAV.orderMapUser.tooltipShow")
                                .ContentTemplate(new TemplateName("tooltip-end-adress-template"))
                            );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                            ");
#nullable restore
#line 452 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                        Write(Html.DevExtreme().TextBox()
                        .ID("end-point")
                        .ReadOnly(true)
                        .Visible(false)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                         ");
#nullable restore
#line 462 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                        .ID("vehice")
                        .Label("Phương tiện").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                        ");
#nullable restore
#line 472 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                    Write(Html.DevExtreme().TextBox()
                        .ID("employee1")
                        .Placeholder("Nhân viên lái xe")
                        .Label("Nhân viên lái xe").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                         ");
#nullable restore
#line 483 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                        .ID("employee2")
                        .Label("Nhân viên phụ xe").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                        ");
#nullable restore
#line 494 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                    Write(Html.DevExtreme().TextBox()
                        .ID("start-time")
                        .ElementAttr("class","time")
                        .Label("Thời gian bắt đầu").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                         ");
#nullable restore
#line 505 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                        .ID("stop-time")
                        .ElementAttr("class", "time")
                        .Label("Thời gian kết thúc").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                         ");
#nullable restore
#line 517 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                        .ID("result")
                        .Label("Kết quả").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"dx-field col-sm-12\">\r\n                         ");
#nullable restore
#line 527 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
                     Write(Html.DevExtreme().TextBox()
                        .ID("reason")
                        .Label("Lý do").LabelMode(EditorLabelMode.Floating)
                        .ReadOnly(true)
                        .Visible(true)
                        );

#line default
#line hidden
#nullable disable
    WriteLiteral("\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n");
    PopWriter();
}
))
    );

#line default
#line hidden
#nullable disable
#nullable restore
#line 540 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
     
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 543 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
 using (Html.DevExtreme().NamedTemplate("tooltip-end-adress-template"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <b id=\"tt-b-end-adress\">\r\n\r\n    </b>\r\n");
#nullable restore
#line 548 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 550 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
 using (Html.DevExtreme().NamedTemplate("tooltip-start-adress-template"))
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <b id=\"tt-b-start-adress\">\r\n\r\n    </b>\r\n");
#nullable restore
#line 555 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
#nullable restore
#line 557 "D:\DeliveryApp\da\DeliveryHaPhan\Views\UserOrder\_OrderMapPopup.cshtml"
Write(Html.DevExtreme().LoadPanel()
        .ID("loadPanel")
        .ShadingColor("rgba(0,0,0,0.4)")
        .Position(p => p.Of("#map-load-panel"))
        .Visible(false)
        .ShowIndicator(true)
        .ShowPane(true)
        .Shading(true)
        .CloseOnOutsideClick(false)
    .OnShown("DevAV.orderMapUser.loadPanel_shown")
    .OnHidden("DevAV.orderMapUser.loadPanel_hidden")
    );

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n<script>\r\n   \r\n</script>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "586926bd79753f56301629f6d560f4a81dd5954327242", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
