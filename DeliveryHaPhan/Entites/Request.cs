﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class Request
    {
       public string RequestID{get;set;}
       public string RequestType{get;set;}
       public string RequestTitle{get;set;}
       public DateTime? CreateDate{get;set;}
       public string CreateName{get;set;}
       public string CreateEmail{get;set;}
       public string CreateDepartment{get;set;}
       public string Status{get;set;}
       public DateTime? ProcessingTime {get;set;}
       public string Approvers { get; set; }
       public string Followers { get; set; }
    }
}
