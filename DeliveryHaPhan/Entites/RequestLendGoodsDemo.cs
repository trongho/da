﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class RequestLendGoodsDemo
    {
        public string RequestID { get; set; }
        public int Ordinal { get; set; }
        public DateTime? LendTime { get; set; }
        public string Reason { get; set; }
        public string Description { get; set; }
        public string GoodsID { get; set; }
        public string Serial { get; set; }
        public decimal? Quantity { get; set; }
        public string Note { get; set; }
    }
}
