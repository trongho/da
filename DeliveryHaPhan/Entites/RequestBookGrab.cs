﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class RequestBookGrab
    {
        public string RequestID { get; set; }
        public int Ordinal { get; set; }
        public string BookType { get; set; }
        public string StartAdress { get; set; }
        public string EndAdress { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public string VehiceType { get; set; }
        public string RequestContent { get; set; }
    }
}
