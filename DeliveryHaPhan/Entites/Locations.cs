﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class Locations
    {
        public String Latitude { get; set; }
        public String Longitude { get; set; }
    }
}
