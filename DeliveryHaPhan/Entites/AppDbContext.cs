﻿
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class AppDbContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderResult> OrderResults { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<Product> Products { get; set; }

        public DbSet<WRRHeader> WRRHeaders { get; set; }
        public DbSet<WRRDetail> WRRDetails { get; set; }
        public DbSet<WRDataHeader> WRDataHeaders { get; set; }
        public DbSet<WRDataGeneral> WRDataGenerals { get; set; }
        public DbSet<GoodsData> GoodsDatas { get; set; }
        public DbSet<UserNew> UserNews { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<RequestPayment> RequestPayments { get; set; }
        public DbSet<RequestSupplies> RequestSuppliess { get; set; }
        public DbSet<RequestLendGoodsDemo> RequestLendGoodsDemos { get; set; }
        public DbSet<RequestBookGrab> RequestBookGrabs { get; set; }
        public DbSet<RequestReimbursement> RequestReimbursements { get; set; }
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>().ToTable("User").HasKey(e => e.UserID);

            modelBuilder.Entity<Order>().ToTable("Order").HasKey(e => e.OrderID);
            modelBuilder.Entity<Order>().Property(p => p.OrderValue).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.Mass).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.Kilometers).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.KilometersOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<Order>().Property(p => p.Surcharge).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<OrderResult>().ToTable("OrderResult").HasKey(e => e.ResultID);

            modelBuilder.Entity<Warehouse>().ToTable("Warehouse").HasKey(e => e.WarehouseID);

            modelBuilder.Entity<Locations>().ToTable("Locations").HasNoKey();

            modelBuilder.Entity<Product>().ToTable("Product").HasKey(e => e.ProductID);

            modelBuilder.Entity<WRRHeader>().ToTable("WRRHeader").HasKey(e => e.WRRNumber);
            modelBuilder.Entity<WRRHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRRDetail>().ToTable("WRRDetail").HasKey(e => new { e.WRRNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<WRRDetail>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRRDetail>().Property(p => p.QuantityReceived).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataHeader>().ToTable("WRDataHeader").HasKey(e => e.WRDNumber);
            modelBuilder.Entity<WRDataHeader>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataHeader>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<WRDataGeneral>().ToTable("WRDataGeneral").HasKey(e => new { e.WRDNumber, e.GoodsID, e.Ordinal });
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalQuantity).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalGoods).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalQuantityOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.TotalGoodsOrg).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.PackingVolume).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityByPack).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.QuantityByItem).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<WRDataGeneral>().Property(p => p.PackingQuantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<GoodsData>().ToTable("GoodsData").HasKey(e => e.GoodsID);

            modelBuilder.Entity<Request>().ToTable("Request").HasKey(e => e.RequestID);

            modelBuilder.Entity<UserNew>().ToTable("UserNew").HasKey(e => e.UserID);

            modelBuilder.Entity<RequestPayment>().ToTable("RequestPayment").HasKey(e => new { e.RequestID, e.Ordinal });
            modelBuilder.Entity<RequestPayment>().Property(p => p.Amount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<RequestPayment>().Property(p => p.TotalAmount).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<RequestSupplies>().ToTable("RequestSupplies").HasKey(e => new { e.RequestID, e.Ordinal });
            modelBuilder.Entity<RequestSupplies>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<RequestLendGoodsDemo>().ToTable("RequestLendGoodsDemo").HasKey(e => new { e.RequestID, e.Ordinal });
            modelBuilder.Entity<RequestLendGoodsDemo>().Property(p => p.Quantity).HasColumnType("decimal(18,3)");

            modelBuilder.Entity<RequestBookGrab>().ToTable("RequestBookGrab").HasKey(e => new { e.RequestID,e.Ordinal});

            modelBuilder.Entity<RequestReimbursement>().ToTable("RequestReimbursement").HasKey(e => new { e.RequestID, e.Ordinal });
            modelBuilder.Entity<RequestReimbursement>().Property(p => p.Amount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<RequestReimbursement>().Property(p => p.AdvanceAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<RequestReimbursement>().Property(p => p.RemainingAmount).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<RequestReimbursement>().Property(p => p.ExcessCash).HasColumnType("decimal(18,3)");
            modelBuilder.Entity<RequestReimbursement>().Property(p => p.TotalAmount).HasColumnType("decimal(18,3)");

        }
    }
}
