﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class RequestPayment
    {
        public string RequestID { get; set; }
        public int Ordinal { get; set; }
        public string RequestNumber { get; set; }
        public string Currency { get; set; }
        public decimal? TotalAmount { get; set; }
        public string Payment { get; set; }
        public DateTime? TimeOfPayment { get; set; }
        public string CustomerName { get; set; }
        public string BeneficiaryAccount { get; set; }
        public string Beneficiary { get; set; }
        public string BankAt { get; set; }
        public string TradingDepartment { get; set; }
        public string PaymentContent { get; set; }

        public decimal? Amount { get; set; }

        public string DocumentsNumber { get; set; }

        public string Note { get; set; }
    }
}
