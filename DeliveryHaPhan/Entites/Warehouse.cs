﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class Warehouse
    {
        public String WarehouseID { get; set; }
        public String WarehouseName { get; set; }
    }
}
