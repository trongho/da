﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Entites
{
    public class RequestSupplies
    {
        public string RequestID { get; set; }
        public int Ordinal { get; set; }
        public string RequestContent { get; set; }
        public string Reason { get; set; }
        public string Goods { get; set; }
        public string Specification { get; set; }
        public decimal? Quantity { get; set; }
        public string Note { get; set; }
    }
}
