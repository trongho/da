﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Menu
{
    public class MenuItem
    {
        public string text { get; set; }
        public bool disabled { get; set; }
        public string icon { get; set; }
        public string controller { get; set; }
        public string action { get; set; }
        public IEnumerable<MenuItem> items { get; set; }
    }
}
