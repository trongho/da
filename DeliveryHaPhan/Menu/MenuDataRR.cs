﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Menu
{
    public static class MenuDataRR
    {
        public static readonly IEnumerable<MenuItem> Menus = new[] {
            new MenuItem {
                text = "Nhập xuất tồn",
                icon = "../image/receipt-svgrepo-com.svg",
                items = new[] {
                    new MenuItem {
                        text = "Yêu cầu nhập kho",
                        controller ="ReceiptRequisition",
                        action="Index",
                        icon = "../image/goodsdata.svg"
                    },
                    new MenuItem {
                        text = "Dữ liệu nhập kho",
                        controller ="ReceiptData",
                        action="Index",
                        icon = "../image/goodsdata.svg"
                    }
                }
            },
            new MenuItem {
                text = "Danh mục",
                icon = "../image/invoice-receipt-svgrepo-com.svg",
                items = new[] {
                    new MenuItem {
                        text = "Dữ liêu hàng hóa",
                        controller ="GoodsData",
                        action="Index",
                        icon = "../image/goodsdata.svg"
                    }
                }
            }
        };
    }
}
