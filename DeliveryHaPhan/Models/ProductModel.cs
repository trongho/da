﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class ProductModel
    {
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string Origin { get; set; }
        public string Uses { get; set; }
        public string UserManual { get; set; }
        public string Infomation { get; set; }
        public string Images { get; set; }
        public string Status { get; set; }
    }
}
