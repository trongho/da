﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class OrderResultModel
    {
        public String ResultID { get; set; }
        public String ResultName { get; set; }
    }
}
