﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class GoodsDemoDetailModel
    {
        public string Description { get; set; }
        public string GoodsID { get; set; }
        public string Serial { get; set; }
        public decimal? Quantity { get; set; }
        public string Note { get; set; }
    }
}
