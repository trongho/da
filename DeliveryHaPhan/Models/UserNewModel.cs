﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class UserNewModel
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public int Rank { get; set; }
        public string FullName { get; set; }
        public string Adress { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Active { get; set; }
        public bool Blocked { get; set; }
        public string Role { get; set; }
        public string Avatar { get; set; }
        public DateTime? LastLogin { get; set; }
    }
}
