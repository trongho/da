﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class GroupOrder
    {
        public String listOrders { get; set; }
        public Decimal? distanceAverage { get; set; }
    }
}
