﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class MonthName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public partial class MonthsData
    {
        public static readonly IEnumerable<MonthName> MonthNames = new[]{
            new MonthName {
                Id = 1,
                Name = "Tháng 1"
            },
            new MonthName {
                Id = 2,
                Name = "Tháng 2"
            },
            new MonthName {
                Id = 3,
                Name = "Tháng 3"
            },
            new MonthName {
                Id = 4,
                Name = "Tháng 4"
            },
            new MonthName {
                Id = 5,
                Name = "Tháng 5"
            },
            new MonthName {
                Id = 6,
                Name = "Tháng 6"
            },
            new MonthName {
                Id = 7,
                Name = "Tháng 7"
            },
            new MonthName {
                Id = 8,
                Name = "Tháng 8"
            },
            new MonthName {
                Id = 9,
                Name = "Tháng 9"
            },
            new MonthName {
                Id = 10,
                Name = "Tháng 10"
            },
            new MonthName {
                Id = 11,
                Name = "Tháng 11"
            },
            new MonthName {
                Id = 12,
                Name = "Tháng 12"
            }
        };
    }
}
