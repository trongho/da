﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class QuantityRequest
    {
        public int ApprovingQuantity { get; set; }
        public int FollowingQuantity { get; set; }
        public int MyQuantity { get; set; }
    }
}
