﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class WRDataGeneral2
    {
        public String WRDNumber { get; set; }
        public int Ordinal { get; set; }
        public String GoodsID { get; set; }
        public int Quantity { get; set; }

        public void Update()
        {
            Quantity += GenerateChange();
        }

        static Random random = new Random();

        int GenerateChange()
        {
            return (int)random.Next(0, 10);
        }
    }
}
