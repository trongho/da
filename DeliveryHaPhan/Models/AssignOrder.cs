﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class AssignOrder
    {
        [Required(ErrorMessage = "Nhân viên phụ trách là bắt buộc")]
        public string Employee1 { get; set; }
 
        public string Employee2 { get; set; }

        [Required(ErrorMessage = "Loại vận chuyển là bắt buộc")]
        public string ShippingType { get; set; }

        [Required(ErrorMessage = "Phí giao hàng là bắt buộc")]
        public string OrderFee { get; set;}

        [Required(ErrorMessage = "Phương tiện vận chuyển là bắt buộc")]
        public string Vehice { get; set; }
    }
}
