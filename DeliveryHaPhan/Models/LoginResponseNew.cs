﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class LoginResponseNew
    {
        public string UserID { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public int Rank { get; set; }
        public string Avatar { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public bool Active { get; set; }
        public bool Blocked { get; set; }
    }
}
