﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class Direction
    {
        public int Id { get; set; }
        public string Distance { get; set; }
        public string Duration { get; set; }
        public string Html_instructions { get; set; }
    }
}
