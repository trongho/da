﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class GoodsDataModel
    {
        [Required(ErrorMessage = "Mã đơn hàng là bắt buộc")]
        public String GoodsID { get; set; }
        public String ECNPart { get; set; }
        public String GoodsName { get; set; }
        public String GoodsNameEN { get; set; }
        public String SLPart { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
        public String CreatedUserID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public String UpdatedUserID { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
