﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class StatusStatistic
    {
        public String Status { get; set; }
        public int Count { get; set; }
    }
}
