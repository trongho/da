﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class ChangePassword
    {
        public String UserID { get; set; }

        public String OldPassword { get; set; }

        [Required(ErrorMessage = "New password is required")]
        public String NewPassword { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "'Password' and 'Confirm Password' do not match.")]
        public String ConfirmPassword { get; set; }
    }
}
