﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class OrderLog
    {
        public int id { get; set;}
        public string logDate { get; set;}
        public String actionName { get; set; }

    }
}
