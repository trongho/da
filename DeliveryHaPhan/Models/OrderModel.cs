﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class OrderModel
    {
        public String OrderID { get; set; }
        
        public String ShippingType { get; set; }
        public String Vehice { get; set; }
        public String Employee1 { get; set; }
        public String Employee2 { get; set; }

        [Required(ErrorMessage = "Ngày phát sinh vận đơn là bắt buộc")]
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        public String OrderFee { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? StopTime { get; set; }
        public Decimal? OrderValue { get; set; }
        public Decimal? Mass { get; set; }
        public Decimal? KilometersOrg { get; set; }
        public Decimal? Kilometers { get; set; }
        public String Result { get; set; }
        public String Reason { get; set; }
        public String StartPoint { get; set; }
        public String StartAdress { get; set; }
        public String EndPoint { get; set; }
        public String EndPointUpdate { get; set; }
        public String EndAdress { get; set; }
        public String CurrentPoint { get; set; }

        [Required(ErrorMessage = "Trạng thái là bắt buộc")]
        public String Status { get; set; }
        public String ShippingMethod { get; set; }
        public String WarehouseID { get; set; }
        public String ExportAdress { get; set; }
        public String BussinesStaffID { get; set; }
        public String CustomerName { get; set; }

        [Required(ErrorMessage = "Địa chỉ khách hàng là bắt buộc")]
        public String CustomerAdress { get; set; }
        public String CustomerAdressUpdate { get; set; }
        public String CustomerPhone { get; set; }
        public String CustomerContact { get; set; }
        public String Note { get; set; }
        public Decimal? Surcharge { get; set; }
    }
}
