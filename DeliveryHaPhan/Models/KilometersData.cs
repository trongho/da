﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Models
{
    public class KilometersData
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string Employee1 { get; set; }
        public string LicensePlates { get; set; }
        public Decimal? Kilometers { get; set; }
    }
}
