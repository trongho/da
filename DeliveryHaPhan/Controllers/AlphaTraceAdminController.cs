﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class AlphaTraceAdminController : Controller
    {
        private readonly IProductService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public AlphaTraceAdminController(IProductService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<object> Get(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = ProductHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }


        [HttpGet]
        public async Task<JsonResult> GetByID([FromQuery]String id)
        {
            var entrys = await service.GetUnderId(id);
            var models = ProductHelper.Covert(entrys);
            return Json(models[0]);
        }

        [HttpPost]
        [EnableCors("CorsPolicy")]
        public async Task<ActionResult> Upload([FromForm] Product product, IFormFile photo)
        {

            if (photo != null)
            {
                
                if (product.ProductID != ""&&product.ProductID!=null)
                {
                    await SaveFile(photo, product.ProductID);
                    await service.Update(product.ProductID, product, product.ProductID +photo.FileName.Substring(photo.FileName.IndexOf(".")));
                }
                else
                {
                    await service.Create(product,photo.FileName);
                    await SaveFile(photo, product.ProductID);
                }
            }
            return RedirectToAction("Index");
        }

        public async Task<string> SaveFile(IFormFile file,String productID)
        {
            try
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "alpha_trace/"+productID);

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                using (var fileStream = System.IO.File.Create(Path.Combine(path,productID+file.FileName.Substring(file.FileName.IndexOf(".")))))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }
            return file.FileName;
        }

        //[HttpPost]
        //public async Task<IActionResult> UploadFile(IFormFile file, [FromQuery] string id, [FromBody] Product product)
        //{
        //    using (var httpClient = new HttpClient())
        //    {
        //        var form = new MultipartFormDataContent();
        //        using (var fileStream = file.OpenReadStream())
        //        {
        //            form.Add(new StreamContent(fileStream), "file", file.FileName);
        //            var result = await service.UploadFile(file);
        //            if (file != null)
        //            {
        //                await service.Update(id, product);
        //            }
        //        }
        //    }
        //    return Ok(product);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> UpdateProduct(IFormFile photo,[FromQuery] string id, [FromBody]Product product)
        //{
        //    if (photo != null)
        //    {
        //        await SaveFile(photo, product.ProductID);
        //        await service.Update(id, product);
        //    }
        //    return Ok(product);
        //}


        [HttpPost]
        public async Task<IActionResult> Post(string values)
        {
            var newProduct = new Product();
            JsonConvert.PopulateObject(values,newProduct);

            if (!TryValidateModel(newProduct))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Create(newProduct, HttpContext.Session.GetString("image"));

            return Ok(newProduct);
        }

        [HttpPut]
        public async Task<IActionResult> Put(string key, string values,string images)
        {
            var product = await service.GetUnderId(key);
            JsonConvert.PopulateObject(values, product[0]);

            if (!TryValidateModel(product[0]))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Update(key,product[0],images);

            return Ok(product);
        }

        [HttpDelete]
        public async Task Delete(string key)
        {
            var order = await service.GetUnderId(key);
            await service.Delete(key);
        }
    }
}
