﻿using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    
    public class AdminHomeController : Controller
    {
        private readonly IOrderService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public AdminHomeController(IOrderService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            ViewBag.CurrentYear = DateTime.UtcNow.Year;
            return View();
        }

        [HttpGet("AdminHome/CountOrdersUnderStatus/{status}")]
        public JsonResult CountOrdersUnderStatus(string status)
        {
            int count =service.CountOrdersUnderStatus(status);
            return Json(count);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult RedirectToApprovedOrder()
        {
            ViewBag.status = "Approved";
            return View("../Order/Index");
        }

        [HttpGet]
        public async Task<object> GetActiveOrders(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetActiveOrders(Request.Query["status"].ToString());
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        public async Task<object> GetActiveOrdersByThisMonth(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetActiveOrdersByThisMonth(Request.Query["status"].ToString());
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        public async Task<object> GetNewOrdersByThisMonth(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetNewOrdersByThisMonth(Request.Query["status"].ToString());
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        public object GetMonthsData(DataSourceLoadOptions loadOptions)
        {
            var entrys = MonthsData.MonthNames;
            return DataSourceLoader.Load(entrys, loadOptions);
        }

        [HttpGet]
        public async Task<JsonResult> GetYearsData(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetYearData();
            return Json(entrys);
        }

        [HttpGet("AdminHome/GetSumKilometersByDate/{date}")]
        public async Task<JsonResult> GetSumKilometersByDate(DateTime date)
        {
            decimal? count = await service.GetSumKilometersByDate(date);
            return Json(count);
        }

        [HttpGet("AdminHome/GetSumKilometersByCurrentMonth")]
        public async Task<JsonResult> GetSumKilometersByCurrentMonth()
        {
            decimal count = await service.GetSumKilometersByCurrentMonth();
            return Json(count);
        }

        [HttpGet("AdminHome/GetKilometers")]
        public async Task<JsonResult> GetKilometers()
        {
            var entry = await service.GetKilometers();
            return Json(entry);
        }

        [HttpGet("AdminHome/GetCurrentYearKilometers")]
        public async Task<JsonResult> GetCurrentYearKilometers()
        {
            var entry = await service.GetCurrentYearKilometers();
            return Json(entry);
        }

        [HttpGet("AdminHome/GetCurrentYearKilometersWithEmployee")]
        public async Task<JsonResult> GetCurrentYearKilometersWithEmployee()
        {
            var entry = await service.GetCurrentYearKilometersWithEmployee();
            return Json(entry);
        }

        [HttpGet("AdminHome/GetCurrentYearStatusStatistic")]
        public async Task<JsonResult> GetCurrentYearStatusStatistic()
        {
            var entry = await service.GetCurrentYearStatusStatistic();
            return Json(entry);
        }
    }
}
