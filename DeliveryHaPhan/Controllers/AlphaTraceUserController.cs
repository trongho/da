﻿using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DevExpress.XtraPrinting.BarCode;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace DeliveryHaPhan.Controllers
{
    public class AlphaTraceUserController : Controller
    {
        private readonly IProductService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public AlphaTraceUserController(IProductService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Index1()
        {
            //List<KeyValuePair<string, Byte[]>> fileData = new List<KeyValuePair<string, byte[]>>();
            //KeyValuePair<string, Byte[]> data;
            //string[] files = Directory.GetFiles("wwwroot/qrcode");
            //foreach (string file in files)
            //{
            //    QRCodeData qrCodeData = new QRCodeData(file, QRCodeData.Compression.Uncompressed);
            //    QRCode qrCode = new QRCode(qrCodeData);
            //    Bitmap qrCodeImage = qrCode.GetGraphic(20);


            //    Byte[] byteData = NetworkHelper.BitmapToByteArray(qrCodeImage);
            //    data = new KeyValuePair<string, Byte[]>(Path.GetFileName(file), byteData);
            //    fileData.Add(data);
            //}

            //QRCoder.QRCodeGenerator qrGenerator = new QRCoder.QRCodeGenerator();
            //QRCodeData qrCodeData = qrGenerator.CreateQrCode("hihi", QRCoder.QRCodeGenerator.ECCLevel.Q);
            ////QRCodeData qrCodeData2 = qrGenerator.CreateQrCode(NetworkHelper.BitmapToByteArray(image), QRCoder.QRCodeGenerator.ECCLevel.Q);

            ////string fileGuid = Guid.NewGuid().ToString().Substring(0, 4);
            //qrCodeData.SaveRawData("wwwroot/qrcode/" + "file001.qrr", QRCodeData.Compression.Uncompressed);
            //QRCodeData qrCodeData1 = new QRCodeData("wwwroot/qrcode/" + "file001.qrr", QRCodeData.Compression.Uncompressed);

            //QRCode qrCode = new QRCode(qrCodeData1);
            //Bitmap qrCodeImage = qrCode.GetGraphic(20);
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetByID([FromQuery] String id)
        {
            var entrys = await service.GetUnderId(id);
            var models = ProductHelper.Covert(entrys);
            return Json(models[0]);
        }

        public ActionResult WriteQR(string qrText,Bitmap image)
        {
            QRCoder.QRCodeGenerator qrGenerator = new QRCoder.QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(qrText, QRCoder.QRCodeGenerator.ECCLevel.Q);
            //QRCodeData qrCodeData2 = qrGenerator.CreateQrCode(NetworkHelper.BitmapToByteArray(image), QRCoder.QRCodeGenerator.ECCLevel.Q);

            string fileGuid = Guid.NewGuid().ToString().Substring(0, 4);
            qrCodeData.SaveRawData("wwwroot/alpha_trace/file-" + fileGuid + ".qrr", QRCodeData.Compression.Uncompressed);
            QRCodeData qrCodeData1 = new QRCodeData("wwwroot/alpha_trace/file-" + fileGuid + ".QRR", QRCodeData.Compression.Uncompressed);

            QRCode qrCode = new QRCode(qrCodeData1);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            return View(NetworkHelper.BitmapToByteArray(qrCodeImage));
        }

        public ActionResult ReadQR()
        {
            List<KeyValuePair<string, Byte[]>> fileData = new List<KeyValuePair<string, byte[]>>();
            KeyValuePair<string, Byte[]> data;
            string[] files = Directory.GetFiles("wwwroot/alpha_trace");
            foreach (string file in files)
            {
                QRCodeData qrCodeData = new QRCodeData(file, QRCodeData.Compression.Uncompressed);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(20);


                Byte[] byteData = NetworkHelper.BitmapToByteArray(qrCodeImage);
                data = new KeyValuePair<string, Byte[]>(Path.GetFileName(file), byteData);
                fileData.Add(data);
            }
            return View(fileData);
        }
    }
}
