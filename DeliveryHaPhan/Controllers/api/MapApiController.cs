﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeliveryHaPhan.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapApiController : ControllerBase
    {
        private readonly IMapService service;

        public MapApiController(IMapService service)
        {
            this.service = service;
        }

        [HttpGet("GetLngLat/{*adress}")]
        public async Task<IActionResult> GetLngLat(String adress)
        {
            var entrys =service.GetLatLng(adress);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }

        [HttpGet("GetAdress/{coordinate}")]
        public async Task<IActionResult> GetAdress(String coordinate)
        {
            var entrys = service.GetAdress(coordinate);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }

        [HttpGet("GetKilometers/{startpoint}/{endpoint}/{vehice}")]
        public async Task<IActionResult> GetKilometers(String startpoint,String endpoint,String vehice)
        {
            var entrys = service.GetKilometers(startpoint,endpoint,vehice);
            if (entrys == null)
            {
                return NotFound();
            }

            return Ok(entrys);
        }

        [HttpPut]
        [Route("UpdateCurrentLocation/{orderId}/{currentpoint}")]
        public async Task<IActionResult> UpdateCurrentLocation(String orderId,String currentpoint)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }          
            await service.UpdateCurrentLocation(orderId,currentpoint);

            return new NoContentResult();
        }
    }
}
