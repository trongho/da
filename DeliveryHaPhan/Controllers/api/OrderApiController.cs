﻿using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeliveryHaPhan.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderApiController : Controller
    {
        private readonly IOrderService service;

        public OrderApiController(IOrderService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }

            var models = OrderHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByID/{orderId}")]
        public async Task<IActionResult> GetUnderId(String orderId)
        {
            var entrys = await service.GetUnderId(orderId);
            if (entrys == null)
            {
                return NotFound();
            }

            var models = OrderHelper.Covert(entrys);

            return Ok(models);
        }

        [HttpGet]
        [Route("GetByEmployee/{employee}")]
        public async Task<JsonResult> GetByEmployee(String employee)
        {
            var entrys = await service.GetUnderEmployee(employee);
            var models = OrderHelper.Covert(entrys);

            return Json(models);
        }

        // GET api/<OrderApiController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<OrderApiController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<OrderApiController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<OrderApiController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
