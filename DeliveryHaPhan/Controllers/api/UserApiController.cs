﻿using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.api
{
    public class UserApiController : Controller
    {
        private readonly IUserService service;

        public UserApiController(IUserService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("api/users")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Get()
        {
            var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
            var claimRole = claimsIdentity.FindFirst(ClaimTypes.Role);

            if (claim == null)
            {
                return Unauthorized("You not signin");
            }
            if (claimRole==null)
            {
                return Unauthorized("You not have permission to access this");
            }

            var users = await service.GetAll();
            if (users == null)
            {
                return NotFound();
            }

            var userModels = UserHelper.CovertUsers(users);

            return Ok(userModels);
        }

    }
}
