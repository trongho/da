﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserController(IUserService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<IActionResult> ChangePasswordUser()
        {
            if (TempData["status"] != null)
            {
                ViewBag.Status = "Success";
                TempData.Remove("status");
            }
            return View();
        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> ChangePasswordAdmin()
        {
            if (TempData["status"] != null)
            {
                ViewBag.Status = "Success";
                TempData.Remove("status");
            }
            return View();
        }

        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<IActionResult> UserDetail()
        {
            string id = HttpContext.Session.GetString("UserID");
            var entrys = await service.GetUnderId(id);
            ViewBag.Avatar = "../avatar/" + entrys[0].Avatar;
            return View(entrys[0]);
        }

        [HttpGet]
        public async Task<IActionResult> GetUnderId([FromQuery]string id)
        {
            var entrys = await service.GetUnderId(id);
            return Json(entrys);
        }

        //[HttpGet]
        //public async Task<object> Get(DataSourceLoadOptions loadOptions)
        //{
        //    var entrys = await service.GetAll();
        //    //var models = UserHelper.CovertUsers(entrys);
        //    return DataSourceLoader.Load(entrys, loadOptions);
        //}

        [HttpGet]
        public IActionResult Login()
        {
            string token = HttpContext.Session.GetString("Token");
            string role = HttpContext.Session.GetString("Role");
            if (token != null)
            {
                if (role == "User")
                {
                    return RedirectToAction("Index", "UserHome");
                }
                else
                {
                    return RedirectToAction("Index", "AdminHome");
                }

            }

            bool rememberMe;
            if (Request.Cookies["rememberme"] != null)
            {
                rememberMe = Boolean.Parse(Request.Cookies["rememberme"]);
            }
            else
            {
                rememberMe = false;
            }

            return View(new LoginRequest()
            {
                Username = Request.Cookies["username"],
                Password = Request.Cookies["password"],
                RememberMe = rememberMe
            });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            if (loginRequest.RememberMe == true)
            {
                Set("username", loginRequest.Username, 10000);
                Set("password", loginRequest.Password, 10000);
                Set("rememberme", loginRequest.RememberMe.ToString(), 10000);
            }
            else
            {
                Remove("username");
                Remove("password");
                Remove("rememberme");
            }

            var loginResponse = await service.Login(loginRequest);

            if (loginResponse == null)
            {

                ViewBag.R = "Thông tin đăng nhập không đúng";
                return View(loginRequest);
                //return BadRequest($"Invalid credentials");
            }
            else
            {
                if (loginResponse.Blocked == true)
                {
                    ModelState.AddModelError("CustomError", "Tài khoản này đang bị khóa");
                    return View();
                }
                if (ModelState.IsValid)
                {
                    HttpContext.Session.SetString("Token", loginResponse.Token);
                    HttpContext.Session.SetString("Role", loginResponse.Role);
                    HttpContext.Session.SetString("UserID", loginResponse.UserID);
                    HttpContext.Session.SetString("Avatar",loginResponse.Avatar);
                    HttpContext.Session.SetString("Username", loginResponse.Username);
                    HttpContext.Session.SetString("Fullname", loginResponse.FullName);
                    ViewBag.Username = HttpContext.Session.GetString("Username");
                    if (loginResponse.Role.Equals("Admin"))
                    {                    
                        return RedirectToAction("Index", "AdminHome");
                    }
                    else
                    {
                        return RedirectToAction("Index", "UserHome");
                    }
                }
            }
            return View(loginRequest);
        }

        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Remove("Token");
            HttpContext.Session.Remove("Username");
            HttpContext.Session.Remove("Role");
            HttpContext.Session.Remove("UserID");
            HttpContext.Session.Remove("Fullname");
            return RedirectToAction("Login", "User");
        }


        //[HttpPut("User/Update/{id}/{updateid}")]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Update([FromForm] User user)
        {
            if (ModelState.IsValid)
            {
                await service.Update(Request.Query["ID"].ToString(), Request.Query["UPDATEID"].ToString(), user);
            }
            return NoContent();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePasswordAdmin([FromForm] ChangePassword changePassword)
        {
            string id = HttpContext.Session.GetString("UserID");
            if (ModelState.IsValid)
            {
                await service.changePassword(id, changePassword);
                TempData["status"] = "Success";
                return RedirectToAction("ChangePasswordAdmin");
            }
            return View(changePassword);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePasswordUser([FromForm] ChangePassword changePassword)
        {
            string id = HttpContext.Session.GetString("UserID");
            if (ModelState.IsValid)
            {
                await service.changePassword(id, changePassword);
                TempData["status"] = "Success";
                return RedirectToAction("ChangePasswordUser");
            }

            return View(changePassword);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AdminChangePassword([FromBody] ChangePassword changePassword, [FromQuery] string userid)
        {


            var result = await service.changePassword(userid, changePassword);

            return NoContent();
        }

        [HttpPost]
        public JsonResult CheckOldPassword([FromForm] ChangePassword changePassword)
        {
            string id = HttpContext.Session.GetString("UserID");
            if (changePassword.OldPassword == null || changePassword.OldPassword.Equals(""))
            {
                return Json(false);
            }
            var entrys = service.CheckOldPassword(id, changePassword);
            return Json(entrys);
        }

        [HttpPost]
        public async Task<ActionResult> Upload(IFormFile photo,[FromForm]User user)
        {

            ViewBag.Photo = "[No photo]";

            if (photo != null)
            {
                await SaveFile(photo);
                ViewBag.AvatarUpdate = "../avatar/" + photo.FileName;
                await service.ChangeAvatar(HttpContext.Session.GetString("UserID"), photo.FileName,user);
                HttpContext.Session.SetString("Avatar",photo.FileName);
            }
            return RedirectToAction("UserDetail");
        }

        public async Task<string> SaveFile(IFormFile file)
        {
            try
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "avatar");

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                using (var fileStream = System.IO.File.Create(Path.Combine(path, file.FileName)))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }
            return file.FileName;
        }

        [HttpGet]
        public async Task<object> Get(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = UserHelper.CovertUsers(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }


        [HttpPost]
        public async Task<IActionResult> Post(string values)
        {
            var newUser = new User();
            JsonConvert.PopulateObject(values, newUser);

            if (!TryValidateModel(newUser))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Create(newUser);

            return Ok(newUser);
        }

        [HttpPut]
        public async Task<IActionResult> Put(string key, string values)
        {
            string updateid = HttpContext.Session.GetString("UserID");
            var user = await service.GetUnderId(key);
            JsonConvert.PopulateObject(values, user[0]);

            if (!TryValidateModel(user[0]))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Update(key, updateid, user[0]);

            return Ok(user);
        }

        [HttpDelete]
        public async Task Delete(string key)
        {
            var order = await service.GetUnderId(key);
            await service.Delete(key);
        }

        public string Get(string key)
        {
            return Request.Cookies[key];
        }

        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }

        public void Remove(string key)
        {
            Response.Cookies.Delete(key);
        }
    }
}
