﻿using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class UserOrderController : Controller
    {
        private readonly IOrderService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserOrderController(IOrderService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }
        // GET: UserOrderController

        [Authorize(Roles = "User")]
        public async Task<ActionResult> IndexAsync()
        {
            return View();
        }

        //[HttpGet]
        //public async Task<JsonResult> GetByEmployee(String employee)
        //{
        //    var entrys = await service.GetUnderEmployee(employee);
        //    var models = OrderHelper.Covert(entrys);

        //    return Json(models);
        //}

        [HttpGet]
        public async Task<object> GetByEmployee(DataSourceLoadOptions loadOptions)
        {
            //string token = HttpContext.Session.GetString("Token");
            //string role = HttpContext.Session.GetString("Role");
            //if (token != null)
            //{
            //    if (role == "User")
            //    {
            //        return RedirectToAction("Index", "UserHome");
            //    }
            //    else
            //    {
            //        return RedirectToAction("Index", "AdminHome");
            //    }

            //}
            var entrys = await service.GetUnderEmployee(Request.Query["employee"].ToString());
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpPut("UserOrder/Cancel/{Orderid}")]
        public async Task<IActionResult> CancelOrder([FromRoute] String OrderID, [FromQuery] String Reason)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await service.CancelOrder(OrderID, Reason);

            return new NoContentResult();
        }

        [HttpPut("UserOrder/UpdateCurrentPoint/{orderid}")]
        public async Task<IActionResult> UpdateCurrentPoint([FromRoute] string orderid, [FromQuery] string currentpoint)
        {
            var result = await service.UpdateCurrentPoint(orderid, currentpoint);
            return Json(result);
        }

        [HttpPut("UserOrder/UpdateMass/{orderid}")]
        public async Task<IActionResult> UpdateMass([FromRoute] string orderid, [FromQuery] decimal mass)
        {
            var result = await service.UpdateMass(orderid, mass);
            return Json(result);
        }


        [HttpPut("UserOrder/UpdateEndPoint/{orderid}")]
        public async Task<IActionResult> UpdateEndPoint([FromRoute] string orderid, [FromQuery] string endpoint, [FromQuery] string adress)
        {
            var result = await service.UpdateEndPoint(orderid,endpoint,adress);
            return Json(result);
        }

        [HttpPut("UserOrder/Start/{orderid}")]
        public async Task<IActionResult> Start([FromRoute] string orderid, [FromQuery] string point, [FromQuery] string adress, [FromQuery] decimal kilometersOrg)
        {
            var result = await service.Start(orderid, point,adress, kilometersOrg);
            return Json(result);
        }

        [HttpPut("UserOrder/Pause/{orderid}")]
        public async Task<IActionResult> Pause([FromRoute] string orderid, [FromQuery] string point, [FromQuery] decimal kilometers)
        {
            var result = await service.Pause(orderid, point, kilometers);
            return Json(result);
        }

        [HttpPut("UserOrder/Continue/{orderid}")]
        public async Task<IActionResult> Continue([FromRoute] string orderid, [FromQuery] string point)
        {
            var result = await service.Continue(orderid, point);
            return Json(result);
        }

        [HttpPut("UserOrder/End/{orderid}")]
        public async Task<IActionResult> End([FromRoute] string orderid, [FromQuery] decimal kilometers)
        {
            var result = await service.End(orderid, kilometers);
            return Json(result);
        }
[HttpGet("UserOrder/GetCurrentPoint/{orderID}")]
        public async Task<IActionResult> getCurrentPoint(string orderID)
        {
            string currentPoint= await service.GetCurrentPoint(orderID);
            return Json(currentPoint);
        }
        

        // GET: UserOrderController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: UserOrderController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserOrderController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(IndexAsync));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserOrderController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: UserOrderController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(IndexAsync));
            }
            catch
            {
                return View();
            }
        }

        // GET: UserOrderController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: UserOrderController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(IndexAsync));
            }
            catch
            {
                return View();
            }
        }
    }
}
