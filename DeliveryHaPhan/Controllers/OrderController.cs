﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Newtonsoft.Json;
using OfficeOpenXml;
using PusherServer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public OrderController(IOrderService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }


        // GET: OrderController
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.Upload = "No";
            return View();
        }


        [Authorize(Roles = "Admin")]
        public ActionResult Log()
        {
            return View();
        }

        public ActionResult Upload()
        {
            return View();
        }

        public ActionResult Map()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(string firstName, string lastName, IFormFile photo)
        {
            // Learn to use the entire functionality of the dxFileUploader widget.
            // http://js.devexpress.com/Documentation/Guide/UI_Widgets/UI_Widgets_-_Deep_Dive/dxFileUploader/

            ViewBag.FirstName = firstName;
            ViewBag.LastName = lastName;
            ViewBag.Photo = "[No photo]";

            if (photo != null)
            {
                SaveFile(photo);
                ViewBag.Photo = photo.FileName;
                ViewBag.Url = SaveFile(photo);
            }

            return View("SubmissionResult");
        }

        public string SaveFile(IFormFile file)
        {
            try
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "avatar");

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                using (var fileStream = System.IO.File.Create(Path.Combine(path, file.FileName)))
                {
                    file.CopyToAsync(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }
            return "../avatar/" + file.FileName;
        }

        [HttpPost("Order/UploadExcelAsync")]
        public async Task<ActionResult> UploadExcelAsync(IFormFile file)
        {
            if (file != null)
            {
                SaveFileExcel(file);
                //List<Order> orders = await Import(file.FileName);
                //foreach (Order order in orders)
                //{
                //    if (await Create(order) == true)
                //    {
                //        await Export(orders, file.FileName);
                //        await DowloadFileAsync(file.FileName);
                //        await DeleteFile(file.FileName);
                //    }
                //}
                return View("SubmissionResult");
            }

            return RedirectToAction("Index");
        }



        public string SaveFileExcel(IFormFile file)
        {
            try
            {
                var path = Path.Combine(_hostingEnvironment.WebRootPath, "ExcelFile");

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                using (var fileStream = System.IO.File.Create(Path.Combine(path, file.FileName)))
                {
                    file.CopyToAsync(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }
            return "../ExcelFile/" + file.FileName;
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            List<Order> orders = new List<Order>();
            using (var httpClient = new HttpClient())
            {
                var form = new MultipartFormDataContent();
                using (var fileStream = file.OpenReadStream())
                {
                    form.Add(new StreamContent(fileStream), "file", file.FileName);
                    var result = await service.UploadFile(file);
                    orders = await Import(file.FileName);
                    foreach (Order order in orders)
                    {
                        await Create(order);
                    }

                    if (await Export(orders, file.FileName) == true)
                    {
                        //return Redirect("DownloadFile/" + file.FileName);
                    }
                    //DowloadFileAsync(file.FileName);

                    //await DeleteFile(file.FileName);
                }
            }
            return Json(orders);
        }

        [HttpGet("Order/DownloadFile/{fileName}")]
        public async Task<FileResult> DowloadFileAsync(string fileName)
        {
            string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile/");
            IFileProvider provider = new PhysicalFileProvider(filePath);
            IFileInfo fileInfo = provider.GetFileInfo(fileName);
            var readStream = fileInfo.CreateReadStream();
            var mimeType = "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet";
            return File(readStream, mimeType, fileName);
        }

        [HttpDelete]
        public async Task<bool> DeleteFile(string fileName)
        {
            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), "wwwroot/ExcelFile",
                        fileName);
            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }
            else
            {
            }
            return true;
        }

        public async Task<List<Order>> Import(String fileName)
        {
            string rootFolder = _hostingEnvironment.ContentRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            
            List<Order> orders = new List<Order>();

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                int orderId = await getOrderIDAsync();
                for (int i = 5; i <= totalRows; i++)
                {
                    String selected = workSheet.Cells[i, 1].Text.Trim();
                    if (selected.Equals("x"))
                    {
                        Thread.Sleep(500);
                        orders.Add(new Order
                        {
                            OrderID = $"{orderId:D10}",
                            RequiredDate = Convert.ToDateTime(workSheet.Cells[i, 3].Text.Trim()),
                            WarehouseID = workSheet.Cells[i, 6].Text.Trim(),
                            ExportAdress = "",
                            ShippingMethod = workSheet.Cells[i, 11].Text.Trim(),
                            OrderValue = Decimal.Parse(workSheet.Cells[i, 12].Text.Trim()),
                            BussinesStaffID = workSheet.Cells[i, 13].Text.Trim(),
                            Note = workSheet.Cells[i, 14].Text.Trim(),
                            ShippingType = "",
                            ShippedDate = null,
                            Employee1 = "",
                            Employee2 = "",
                            Vehice="",
                            OrderFee = "Không",
                            StartTime = null,
                            StopTime = null,
                            Mass = 0,
                            Surcharge=0,
                            Kilometers = 0,
                            KilometersOrg = 0,
                            Result = "",
                            Reason = "",
                            CustomerAdress = workSheet.Cells[i, 8].Text.Trim(),
                            CustomerName = workSheet.Cells[i, 7].Text.Trim(),
                            CustomerPhone = workSheet.Cells[i, 10].Text.Trim(),
                            CustomerContact = workSheet.Cells[i, 9].Text.Trim(),
                            StartPoint = "",
                            EndPoint = await GetLngLat(workSheet.Cells[i, 8].Text.Trim()),
                            Status = "New",
                        });
                        orderId++;
                    }
                }
            }
            return orders;
        }

        public async Task<bool> Export(List<Order> orders, String fileName)
        {
            string rootFolder = _hostingEnvironment.ContentRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));


            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = orders.Count;


                int i = 0;
                for (int row = 5; row <= totalRows + 4; row++)
                {
                    worksheet.Cells[row, 2].Value = orders[i].OrderID;
                    i++;
                }
                package.Save();

            }
            
            return true;
        }

        public async Task<bool> Create(Order order)
        {
            bool flag = false;
            flag = await service.Create(order);
            
            return flag;
        }

        public async Task<String> GetLngLat(String adress)
        {
            var lnglat = await service.GetLatLng(adress);
            return lnglat;
        }


        public async Task<int> getOrderIDAsync()
        {
            String lastID = "";
            int orderID = 1;
            lastID = await service.GetLastID();
            if (lastID != null)
            {
                orderID = int.Parse(lastID) + 1;
            }
            return orderID;
        }

        //public async Task<IActionResult> Map()
        //{
        //    var entrys = await service.GetAll();
        //    var models = OrderHelper.Covert(entrys);
        //    return View(models);
        //}

        [HttpGet]
        public async Task<JsonResult> Get2()
        {
            var entrys = await service.GetAll();
            var models = OrderHelper.Covert(entrys);
            return Json(models);
        }

        [HttpGet]
        public async Task<JsonResult> GetByID([FromQuery]String id)
        {
            var entrys = await service.GetUnderId(id);
            var models = OrderHelper.Covert(entrys);

            return Json(models[0]);
        }

        [HttpGet]
        public async Task<object> Get(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        public async Task<object> GetUsers(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUsers();
            var models = UserHelper.CovertUsers(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpPost]
        public async Task<IActionResult> Post(string values)
        {
            var newOrder = new Order();
            JsonConvert.PopulateObject(values, newOrder);

            if (!TryValidateModel(newOrder))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Create2(newOrder);

            return Ok(newOrder);
        }

        [HttpPut]
        public async Task<IActionResult> Put(string key, string values)
        {
            var order = await service.GetUnderId(key);
            JsonConvert.PopulateObject(values, order[0]);

            if (!TryValidateModel(order[0]))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Update(key, order[0]);

            return Ok(order);
        }

        [HttpPut]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> UpdateOrder([FromQuery]string orderid,[FromBody]OrderModel orderModel)
        {
            await service.Update2(orderid,orderModel);
            return Ok(orderModel);
        }

        [HttpDelete]
        public async Task Delete(string key)
        {
            var order = await service.GetUnderId(key);
            await service.Delete(key);
        }

        [HttpDelete]
        public async Task Delete2([FromQuery] string key)
        {
            await service.Delete(key);
        }

        [HttpDelete]
        public async Task DeleteMore([FromQuery] string key)
        {
            await service.DeleteMore(key);
        }



        // GET: OrderController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: OrderController/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // POST: OrderController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: OrderController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: OrderController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: OrderController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<FileResult> UploadAndDownloadFile(IFormFile file, string fileName)
        {
            var entrys = await service.UploadFile(file);

            string filePath = service.getFilePath();
            IFileProvider provider = new PhysicalFileProvider(filePath);
            IFileInfo fileInfo = provider.GetFileInfo(fileName);
            var readStream = fileInfo.CreateReadStream();
            var mimeType = "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet";
            return File(readStream, mimeType, fileName);
        }

        [HttpGet]
        public async Task<JsonResult> GetLog([FromQuery] DateTime date, [FromQuery] string filterString)
        {
            var entrys = await service.GetLog(date, filterString);
            return Json(entrys);
        }

        //[HttpGet]
        //public async Task<JsonResult> GetLog2()
        //{
        //    var entrys = await service.GetLog2(DateTime.Now);
        //    return Json(entrys);
        //}

        [HttpGet]
        public async Task<object> GetLog2(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetLog2(DateTime.Now);
            return DataSourceLoader.Load(entrys, loadOptions);
        }

        [HttpPut]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignTo([FromBody] AssignOrder assignOrder, [FromQuery] string orderid)
        {
            //var options = new PusherOptions
            //{
            //    Cluster = "ap1",
            //    Encrypted = true
            //};
            //var pusher = new Pusher(
            //  "1355900",
            //  "ec1c83173397d553f3e1",
            //  "def1e8a713a8672bdf89",
            //  options);
            //var result = await pusher.TriggerAsync(
            //  "my-channel-" + assignOrder.Employee1,
            //  "my-event-" + assignOrder.Employee1,
            //  new { message = "Bạn vừa được phân công đơn hàng " + orderid}) ;

            var result1 = await service.Assign(orderid, assignOrder);
            return Json(result1);
        }

        [HttpGet]
        //[Authorize(Roles = "Admin")]
        public async Task<object> GetByEmployee(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderEmployee(Request.Query["employee"].ToString());
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpPut("Order/UpdateSurcharge/{orderid}")]
        public async Task<IActionResult> UpdateSurcharge([FromRoute] string orderid, [FromQuery] decimal surcharge)
        {
            var result = await service.UpdateSurcharge(orderid,surcharge);
            return Json(result);
        }

        [HttpGet]
        public async Task<JsonResult> GetEndPoint([FromQuery] String id)
        {
            var entrys = await service.GetEndPoint(id);
           
            return Json(entrys);
        }

    }
}
