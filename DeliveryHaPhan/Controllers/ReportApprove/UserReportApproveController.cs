﻿using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.ReportApprove
{
    public class UserReportApproveController : Controller
    {
        private readonly IUserNewService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserReportApproveController(IUserNewService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public IActionResult UserManager()
        {
            return View("../ReportApprove/UserManager");
        }

        [HttpGet]
        public IActionResult Login()
        {
            bool rememberMe;
            if (Request.Cookies["rememberme"] != null)
            {
                rememberMe = Boolean.Parse(Request.Cookies["rememberme"]);
            }
            else
            {
                rememberMe = false;
            }

            return View("../ReportApprove/Login", new LoginRequest()
            {
                Username = Request.Cookies["username"],
                Password = Request.Cookies["password"],
                RememberMe = rememberMe
            });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            if (loginRequest.RememberMe == true)
            {
                Set("username", loginRequest.Username, 10000);
                Set("password", loginRequest.Password, 10000);
                Set("rememberme", loginRequest.RememberMe.ToString(), 10000);
            }
            else
            {
                Remove("username");
                Remove("password");
                Remove("rememberme");
            }

            var loginResponse = await service.Login(loginRequest);

            if (loginResponse == null)
            {

                ViewBag.R = "Thông tin đăng nhập không đúng";
                return View("../ReportApprove/Login",loginRequest);
                //return BadRequest($"Invalid credentials");
            }
            else
            {
                if (loginResponse.Blocked == true)
                {
                    ModelState.AddModelError("CustomError", "Tài khoản này đang bị khóa");
                    return View();
                }
                if (ModelState.IsValid)
                {
                    HttpContext.Session.SetString("Token", loginResponse.Token);
                    HttpContext.Session.SetString("Role", loginResponse.Role);
                    HttpContext.Session.SetString("UserID", loginResponse.UserID);
                    HttpContext.Session.SetString("Avatar", loginResponse.Avatar);
                    HttpContext.Session.SetString("Username", loginResponse.Username);
                    HttpContext.Session.SetString("Email", loginResponse.Email);
                    HttpContext.Session.SetString("Fullname", loginResponse.FullName);
                    HttpContext.Session.SetString("Position", loginResponse.Position);
                    HttpContext.Session.SetString("Department", loginResponse.Department);
                    HttpContext.Session.SetInt32("Rank", loginResponse.Rank);
                    ViewBag.Username = HttpContext.Session.GetString("Username");
                    if (loginResponse.Role.Equals("Admin"))
                    {
                        return RedirectToAction("Index2", "ReportApprove");
                    }
                    else
                    {
                        return RedirectToAction("Index2", "ReportApprove");
                    }
                }
            }
            return View(loginRequest);
        }
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Remove("Token");
            HttpContext.Session.Remove("Username");
            HttpContext.Session.Remove("Role");
            HttpContext.Session.Remove("UserID");
            HttpContext.Session.Remove("Avatar");
            HttpContext.Session.Remove("Email");
            HttpContext.Session.Remove("Fullname");
            HttpContext.Session.Remove("Position");
            HttpContext.Session.Remove("Department");
            HttpContext.Session.Remove("Rank");
            return RedirectToAction("Login", "UserReportApprove");
        }


        public string Get(string key)
        {
            return Request.Cookies[key];
        }

        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }

        public void Remove(string key)
        {
            Response.Cookies.Delete(key);
        }
    }
}
