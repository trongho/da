﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryHaPhan.Controllers.ReportApprove
{
    public class FileSystemController : Controller
    {
        public IActionResult FileSystem()
        {
            return View("../ReportApprove/FileSystem");
        }
    }
}
