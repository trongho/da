﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.ReportApprove.api
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserNewApiController : Controller
    {
        private readonly IUserNewService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserNewApiController(IUserNewService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [Route("GetByID")]
        public async Task<IActionResult> GetUnderId([FromQuery] string id)
        {
            var entrys = await service.GetUnderId(id);
            return Json(entrys);
        }

        [HttpGet]
        [Route("GetByUsername")]
        public async Task<IActionResult> GetUnderUsername()
        {
            var entrys = await service.GetUnderUserName(HttpContext.Session.GetString("Username"));
            return Json(entrys);
        }

        [HttpGet]
        [Route("GetApprovers")]
        public async Task<IActionResult> GetApprovers()
        {
            var entrys = await service.GetApprovers(HttpContext.Session.GetString("Department"),(int)HttpContext.Session.GetInt32("Rank"));
            return Json(entrys);
        }

        [HttpGet]
        [Route("GetStaffUnderDepartmentAndRanks")]
        public async Task<IActionResult> GetStaffUnderDepartmentAndRanks([FromQuery]string department,[FromQuery]string rank)
        {
            var entrys = await service.GetStaffUnderDepartmentAndRanks(department,int.Parse(rank));
            return Json(entrys);
        }

        [HttpGet]
        [Route("GetFollowers")]
        public async Task<IActionResult> GetFollowers()
        {
            var entrys = await service.GetFollowers((int)HttpContext.Session.GetInt32("Rank"));
            return Json(entrys);
        }


        [HttpGet]
        [Route("Get")]
        public async Task<object> Get(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = UserNewHelper.Coverts(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }


        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromBody] UserNew userNew)
        {
            var entry = await service.Create2(userNew);

            return CreatedAtAction(
                 nameof(Get), new { UserID = userNew.UpdatedUserID }, entry);
        }

        [HttpPost]
        public async Task<IActionResult> Post(string values)
        {
            var entry = new UserNew();
            JsonConvert.PopulateObject(values, entry);

            if (!TryValidateModel(entry))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Create(entry);

            return Ok(entry);
        }

        [HttpPut]
        [Route("Put")]
        public async Task<IActionResult> Put(string key, string values)
        {
            string updateid = HttpContext.Session.GetString("UserID");
            var user = await service.GetUnderId(key);
            JsonConvert.PopulateObject(values, user[0]);

            if (!TryValidateModel(user[0]))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Update(key, updateid, user[0]);

            return Ok(user);
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task Delete(string key)
        {
            var order = await service.GetUnderId(key);
            await service.Delete(key);
        }

        [HttpPost]
        [Route("UploadFile")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            List<UserNew> userNews = new List<UserNew>();
           
            string path = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile/" + file.FileName);
            var form = new MultipartFormDataContent();
            using (var fileStream = file.OpenReadStream())
            {
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }
                form.Add(new StreamContent(fileStream), "file", file.FileName);

                userNews = await ImportUserNewAsync(file.FileName);

                await DeleteFile(file.FileName);
            }

            return Json(userNews);
        }

        [HttpDelete]
        public async Task<bool> DeleteFile(string fileName)
        {
            var path = Path.Combine(
                        _hostingEnvironment.ContentRootPath, "ExcelFile/",
                        fileName);
            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }
            else
            {
            }
            return true;
        }

        public async Task<List<UserNew>> ImportUserNewAsync(string fileName)
        {
            string rootFolder = _hostingEnvironment.ContentRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            List<UserNew> userNews = new List<UserNew>();

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                for (int i = 2; i <= totalRows; i++)
                {
                    if(!workSheet.Cells[i, 6].Text.Trim().Equals(""))
                    {
                        userNews.Add(new UserNew
                        {
                            UserID = workSheet?.Cells[i, 6].Text.Trim(),
                            FullName = workSheet?.Cells[i, 2].Text.Trim(),
                            Email = workSheet?.Cells[i, 4].Text.Trim(),
                            Position = workSheet?.Cells[i,3].Text.Trim(),
                            UserName = workSheet?.Cells[i, 4].Text.Trim().Substring(0, workSheet.Cells[i, 4].Text.Trim().IndexOf("@")),
                            Password ="123qwe!@#",
                            Role="User",
                            Avatar="noimage.png",
                            Active=true,
                            Blocked=false,
                            Rank=0,
                        });
                    }
                }
            }
            return userNews;
        }
    }
}
