﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.ReportApprove.api
{
    [ApiController]
    [Route("api/[controller]")]
    public class RequestSuppliesApiController : Controller
    {
        private readonly IRequestSuppliesService service;

        public RequestSuppliesApiController(IRequestSuppliesService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID")]
        public async Task<object> GetByID(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderId(Request.Query["id"].ToString());
            var models = RequestSuppliesHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        [Route("Gets")]
        public async Task<object> Gets(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = RequestSuppliesHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        [Route("GetByParams")]
        public async Task<object> GetByParams(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderParams(Request.Query["id"].ToString(), int.Parse(Request.Query["ordinal"]));
            var models = RequestSuppliesHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpPost]
        [Route("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody] RequestSupplies entry)
        {
            var result = await service.Create(entry);
            return Ok(result);
        }

        [HttpPut]
        [Route("Update")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update([FromQuery] string id, [FromQuery] string ordinal, [FromBody] RequestSupplies entry)
        {
            var result = await service.Update(id, int.Parse(ordinal), entry);
            return Ok(result);
        }

        [HttpDelete]
        [Route("DeleteRequest")]
        public async Task DeleteRequest([FromQuery] string id)
        {
            await service.Delete(id);
        }
    }
}
