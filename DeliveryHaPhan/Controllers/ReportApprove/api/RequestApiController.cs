﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.ReportApprove.api
{
    [ApiController]
    [Route("api/[controller]")]
    public class RequestApiController : Controller
    {
        private readonly IRequestService service;

        public RequestApiController(IRequestService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<object> Get(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpGet]
        [Route("GetQuantityRequest")]
        public async Task<object> GetQuantityRequest()
        {
            var entrys = await service.GetQuantityRequest(HttpContext.Session.GetString("Username"), Request.Query["text"].ToString());
            return Json(entrys);
        }

        [HttpGet]
        [Route("GetByID")]
        public async Task<object> GetByID(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderId(Request.Query["id"].ToString());
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [Route("GetOverdueRequest")]
        [HttpGet]
        public async Task<object> GetOverdueRequest(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetOverdueRequest(Request.Query["approvers"].ToString());
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [Route("GetByApprovers")]
        [HttpGet]
        public async Task<object> GetByApprovers(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderApprovers(Request.Query["approvers"].ToString());
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [Route("GetByFollowers")]
        [HttpGet]
        public async Task<object> GetByFollowers(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderFollowers(Request.Query["followers"].ToString());
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [Route("GetByCreateName")]
        [HttpGet]
        public async Task<object> GetByCreateName(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetUnderCreateName(Request.Query["createname"].ToString());
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }


        [HttpGet]
        [Route("GetLastRequestApproving")]
        public async Task<object> GetLastRequestApproving(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetLastRequestApproving(HttpContext.Session.GetString("Username"));
            var models = RequestHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpPost]
        [Route("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody] Request entry)
        {
            var result = await service.Create(entry);
            return CreatedAtAction(
                 nameof(Get), new {RequestID = entry.RequestID}, entry);
        }

        [HttpPut]
        [Route("Update")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update([FromQuery] string id, [FromBody] Request entry)
        {
            var result = await service.Update(id, entry);
            return Ok(result);
        }

        [HttpDelete]
        [Route("DeleteRequest")]
        public async Task DeleteRequest([FromQuery] string id)
        {
            await service.Delete(id);
        }

        [HttpGet]
        [Route("CheckExist")]
        public async Task<IActionResult> CheckExist()
        {
            var result = await service.checkExist(Request.Query["id"].ToString());
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [Route("Approve")]
        [HttpPut]
        public async Task<IActionResult> Approve([FromQuery] string id)
        {
            var result = await service.Approve(id);
            return Ok(result);
        }

        [Route("Forward")]
        [HttpPut]
        public async Task<IActionResult> Forward([FromQuery] string id, [FromQuery] string approver)
        {
            var result = await service.Forward(id, approver);
            return Ok(result);
        }

        [Route("Refuse")]
        [HttpPut]
        public async Task<IActionResult> Refuse([FromQuery] string id)
        {
            var result = await service.Refuse(id);
            return Ok(result);
        }

        [Route("Recycle")]
        [HttpDelete]
        public async Task<IActionResult> Recycle(string key)
        {
            var result = await service.Recycle(key);
            return Ok(result);
        }
    }
}
