﻿using DeliveryHaPhan.Entites;
//using DeliveryHaPhan.Utils;
using DevExpress.AspNetCore.Spreadsheet;
using DevExpress.ClipboardSource.SpreadsheetML;
using DevExpress.Spreadsheet;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace DeliveryHaPhan.Controllers.ReportApprove
{
    public class ReportApproveController : Controller
    {


        [IgnoreAntiforgeryToken]
        public IActionResult Index()
        {

            return View();
        }

        [IgnoreAntiforgeryToken]
        public IActionResult Index2()
        {

            return View();
        }

        public IActionResult MyReport()
        {
            
            return View();
        }

        public IActionResult ReportManager()
        {
            return View();
        }

        [IgnoreAntiforgeryToken]
        public IActionResult Spreadsheet()
        {        
            return PartialView("Spreadsheet");
        }

        [IgnoreAntiforgeryToken]
        public IActionResult RichTextCV()
        {
            return PartialView("RichTextCV");
        }

        [IgnoreAntiforgeryToken]
        [Route("RequestDetail/{requestid}")]
        public IActionResult RequestDetail()
        {
            return PartialView("RequestDetail");
        }


        [IgnoreAntiforgeryToken]
        public IActionResult _PreviewPopup()
        {
            return PartialView("_PreviewPopup");
        }

        //public IActionResult Export([FromQuery] string requestid)
        //{
        //    string username = HttpContext.Session.GetString("Username");
        //    string rootFolder = _hostingEnvironment.ContentRootPath + "/App_Data/Documents/SampleDocuments/";
        //    string excelName = requestid + "_" + username + ".xlsx";
        //    string downloadUrl = string.Format("{0}://{1}/{2}", Request.Scheme, Request.Host, "App_Data/Documents/SampleDocuments/" + excelName);
        //    FileInfo file = new FileInfo(Path.Combine(rootFolder, excelName));

        //    if (file.Exists)
        //    {
        //        file.Delete();
        //        file = new FileInfo(Path.Combine(rootFolder, excelName));
        //    }


        //    using (ExcelPackage package = new ExcelPackage(file))
        //    {

        //        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Request");
        //        worksheet.Cells["A1:C1,C3"].Style.Font.Bold = true;
        //        using (var range = worksheet.Cells[1, 1, 1, 5])
        //        {
        //            range.Value = excelName;
        //            range.Style.Font.Bold = true;
        //            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
        //            range.Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);
        //            range.Style.Font.Color.SetColor(Color.White);
        //        }
        //        package.Save();

        //    }
        //    return Json(downloadUrl);
        //}

    }
}
