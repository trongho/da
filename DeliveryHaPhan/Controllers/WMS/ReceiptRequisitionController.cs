﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Menu;
//using DeliveryHaPhan.Utils;
using DevExpress.DataAccess.Native.Json;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class ReceiptRequisitionController : Controller
    {
        private readonly IGoodsDataService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ReceiptRequisitionController(IGoodsDataService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public object GetMenu(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(MenuDataRR.Menus, loadOptions);
        }

        public IActionResult Index()
        {
            return View("../WMS/ReceiptRequisition/Index");
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            List<WRRDetail> wRRDetails = new List<WRRDetail>();
            WRRHeader wRRHeader = new WRRHeader();

            string path = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile/" + file.FileName);
            var form = new MultipartFormDataContent();
            using (var fileStream = file.OpenReadStream())
            {
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }
                form.Add(new StreamContent(fileStream), "file", file.FileName);

                wRRDetails = await ImportWRRDetailAsync(file.FileName);
                wRRHeader = ImportWRRHeader(file.FileName);

                await DeleteFile(file.FileName);
            }

            return Json(wRRDetails);
        }

        [HttpDelete]
        public async Task<bool> DeleteFile(string fileName)
        {
            var path = Path.Combine(
                        _hostingEnvironment.ContentRootPath, "ExcelFile/",
                        fileName);
            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }
            else
            {
            }
            return true;
        }

        public async Task<List<WRRDetail>> ImportWRRDetailAsync(string fileName)
        {
            //FileInfo file = new FileInfo(Path.Combine(fileName));

            string rootFolder = _hostingEnvironment.ContentRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            List<WRRDetail> wRRDetails = new List<WRRDetail>();

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                int j = 1;
                for (int i = 16; i <= totalRows; i++)
                {
                    wRRDetails.Add(new WRRDetail
                    {
                        WRRNumber= workSheet?.Cells[16,1].Text.Trim(),
                        GoodsID = workSheet?.Cells[i, 28].Text.Trim().Replace("-", "").Replace(" ", ""),
                        TotalQuantity = Decimal.Parse(workSheet?.Cells[i, 30].Text.Trim()),
                        QuantityByItem = Decimal.Parse(workSheet?.Cells[i, 30].Text.Trim()),
                        QuantityByPack = Decimal.Parse(workSheet.Cells[i, 31].Text.Trim()),
                        PackingVolume = Math.Floor(Decimal.Parse(workSheet?.Cells[i, 30].Text.Trim()) / Decimal.Parse(workSheet.Cells[i, 31].Text.Trim())),
                        SupplierCode = workSheet?.Cells[i, 27].Text.Trim(),
                        ASNNumber = workSheet?.Cells[i, 25].Text.Trim(),
                        PackingSlip = workSheet?.Cells[i, 26].Text.Trim(),
                        QuantityReceived = 0m,
                        ReceiptStatus = workSheet?.Cells[i, 34].Text.Trim(),
                        ScanOption=1,
                        Ordinal=j,
                    });
                    j++;
                }
            }

            //Get goodsname from metadata
            List<GoodsData> goodsDatas = await service.GetAll();
            for (int i = 0; i < wRRDetails.Count; i++)
            {
                foreach (GoodsData goodsData in goodsDatas)
                {
                    if (goodsData.GoodsID.Equals(wRRDetails[i].GoodsID) || goodsData.ECNPart.Equals(wRRDetails[i].GoodsID))
                    {
                        if (goodsData.ECNPart.Equals(""))
                        {
                            wRRDetails[i].GoodsName = goodsData.GoodsName;
                            wRRDetails[i].SLPart = goodsData.SLPart;
                        }
                        else
                        {
                            wRRDetails[i].GoodsName = goodsData.GoodsName;
                            wRRDetails[i].SLPart = goodsData.SLPart;
                        }

                    }
                }
            }
            return wRRDetails;
        }

        public WRRHeader ImportWRRHeader(string fileName)
        {
            string rootFolder = _hostingEnvironment.ContentRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            WRRHeader wRRHeader = new WRRHeader();

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                wRRHeader.WRRNumber = workSheet?.Cells[16, 1].Text.Trim();
                wRRHeader.WRRDate = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd"));
                wRRHeader.HandlingStatusID ="0";
            }
            return wRRHeader;
        }
    }
}
