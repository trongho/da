﻿using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.WMS
{
    public class UserWMSController : Controller
    {
        private readonly IUserService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserWMSController(IUserService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View("../WMS/User/Login");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginRequest loginRequest)
        {
            if (loginRequest == null || string.IsNullOrEmpty(loginRequest.Username) || string.IsNullOrEmpty(loginRequest.Password))
            {
                return BadRequest("Missing login details");
            }

            if (loginRequest.RememberMe == true)
            {
                Set("username", loginRequest.Username, 10000);
                Set("password", loginRequest.Password, 10000);
                Set("rememberme", loginRequest.RememberMe.ToString(), 10000);
            }
            else
            {
                Remove("username");
                Remove("password");
                Remove("rememberme");
            }

            var loginResponse = await service.Login(loginRequest);

            if (loginResponse == null)
            {

                ViewBag.R = "Thông tin đăng nhập không đúng";
                return View(loginRequest);
                //return BadRequest($"Invalid credentials");
            }
            else
            {
                if (loginResponse.Blocked == true)
                {
                    ModelState.AddModelError("CustomError", "Tài khoản này đang bị khóa");
                    return View();
                }
                if (ModelState.IsValid)
                {
                    HttpContext.Session.SetString("Token", loginResponse.Token);
                    HttpContext.Session.SetString("Role", loginResponse.Role);
                    HttpContext.Session.SetString("UserID",loginResponse.UserID+"");
                    HttpContext.Session.SetString("Avatar", loginResponse.Avatar);
                    HttpContext.Session.SetString("Username", loginRequest.Username);
                    ViewBag.Username = HttpContext.Session.GetString("Username");
                    return RedirectToAction("Index", "ReceiptRequisition");
                }
            }
            return View(loginRequest);
        }

        public void Set(string key, string value, int? expireTime)
        {
            CookieOptions option = new CookieOptions();
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddMilliseconds(10);
            Response.Cookies.Append(key, value, option);
        }

        public void Remove(string key)
        {
            Response.Cookies.Delete(key);
        }
    }
}
