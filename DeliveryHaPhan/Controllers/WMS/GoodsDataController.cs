﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.WMS
{
    public class GoodsDataController : Controller
    {
        private readonly IGoodsDataService service;
        private readonly IHostingEnvironment _hostingEnvironment;
        //private readonly IEmailSender _emailSender;

        public GoodsDataController(IGoodsDataService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
           
        }
        public IActionResult Index()
        {
            return View("../WMS/GoodsData/Index");
        }

        [HttpGet]
        public async Task<object> Get(DataSourceLoadOptions loadOptions)
        {
            var entrys = await service.GetAll();
            var models = GoodsDataHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }

        [HttpPost]
        public async Task<IActionResult> Post(string values)
        {
            var entry = new GoodsData();
            JsonConvert.PopulateObject(values,entry);

            if (!TryValidateModel(entry))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Create(entry);

            return Ok(entry);
        }

        [HttpPut]
        public async Task<IActionResult> Put(string key, string values)
        {
            var entry = await service.GetUnderId(key);
            JsonConvert.PopulateObject(values,entry[0]);

            if (!TryValidateModel(entry[0]))
                return BadRequest(ModelState.GetFullErrorMessage());

            await service.Update(key, entry[0]);

            return Ok(entry);
        }

        [HttpDelete]
        public async Task Delete(string key)
        {
            var order = await service.GetUnderId(key);
            await service.Delete(key);
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            //var callbackUrl = Url.Page(
            //            "/User/Login",
            //            pageHandler: null,
            //            values: new { area = "Identity", userId = 1, code ="123", returnUrl = "~/" },
            //            protocol: Request.Scheme);


            //await _emailSender.SendEmailAsync("wankimjun@gmail.com", "Xác nhận địa chỉ email",
            //           $"Hãy xác nhận địa chỉ email bằng cách <a href='{callbackUrl}'>Bấm vào đây</a>.");

            List<GoodsData> goodsDatas = new List<GoodsData>();

            string path = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filePath = Path.Combine(_hostingEnvironment.ContentRootPath, "ExcelFile/" + file.FileName);
            var form = new MultipartFormDataContent();
            using (var fileStream = file.OpenReadStream())
            {
                using (var stream = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    await file.CopyToAsync(stream);
                }
                form.Add(new StreamContent(fileStream), "file", file.FileName);

                goodsDatas = Import(file.FileName);
            }

            return Json(goodsDatas);
        }

        public List<GoodsData> Import(string fileName)
        {
            string rootFolder = _hostingEnvironment.ContentRootPath;
            FileInfo file = new FileInfo(Path.Combine(rootFolder, "ExcelFile/" + fileName));
            List<GoodsData> goodsDatas = new List<GoodsData>();

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet workSheet = package.Workbook.Worksheets.FirstOrDefault();
                int totalRows = workSheet.Dimension.Rows;
                for (int i = 2; i <= totalRows; i++)
                {
                    goodsDatas.Add(new GoodsData
                    {
                        GoodsID = workSheet?.Cells[i, 1].Text.Trim().Replace("-", "").Replace(" ", ""),
                        ECNPart = workSheet?.Cells[i, 2].Text.Trim().Replace("-", "").Replace(" ", ""),
                        GoodsName = workSheet?.Cells[i, 3].Text.Trim(),
                        SLPart = workSheet?.Cells[i, 4].Text.Trim(),
                    });
                }
            }
            return goodsDatas;
        }
    }
}
