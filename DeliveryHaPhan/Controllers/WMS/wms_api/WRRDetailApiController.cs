﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.WMS.wms_api
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRRDetailApiController : Controller
    {
        private readonly IWRRDetailService service;

        public WRRDetailApiController(IWRRDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRRDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRRNumber}")]
        public async Task<IActionResult> GetWRRDetailUnderWRRNumber(String wRRNumber)
        {
            var entrys = await service.GetUnderId(wRRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WRRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wRRNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(wRRNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRRDetail wRRDetail)
        {
            var entry = await service.Create(wRRDetail);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wRRDetail.WRRNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRRNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wRRNumber, String goodsID, int ordinal, [FromBody] WRRDetail wRRDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRRNumber != wRRDetail.WRRNumber)
            {
                return BadRequest();
            }

            await service.Update(wRRNumber, goodsID, ordinal, wRRDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRRNumber}")]
        public async Task<IActionResult> Delete(String wRRNumber)
        {
            var entry = await service.Delete(wRRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
