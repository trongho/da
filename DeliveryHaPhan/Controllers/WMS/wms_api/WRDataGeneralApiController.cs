﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.WMS.wms_api
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataGeneralApiController : Controller
    {
        private readonly IWRDataGeneralService service;

        public WRDataGeneralApiController(IWRDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWRDNumberAndGoodsID/{WRDNumber}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(String wRDNumber, String goodsID)
        {
            var entrys = await service.GetUnderId(wRDNumber, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataGeneral WRDataGeneral)
        {
            var entry = await service.Create(WRDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = WRDataGeneral.WRDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wRDNumber, String goodsID, int ordinal, [FromBody] WRDataGeneral WRDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != WRDataGeneral.WRDNumber)
            {
                return BadRequest();
            }

            await service.Update(wRDNumber, goodsID, ordinal, WRDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await service.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckQuantity/{wrdNumber}/{editedDateTime}")]
        public async Task<IActionResult> CheckQuantity(String wrdNumber, DateTime editedDateTime)
        {
            var entry = await service.checkQuantity(wrdNumber, editedDateTime);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
