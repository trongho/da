﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.WMS.wms_api
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataHeaderApiController : Controller
    {
        private readonly IWRDataHeaderService service;

        public WRDataHeaderApiController(IWRDataHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wRDNumber}")]
        public async Task<IActionResult> GetWRDataHeaderUnderWRDNumber(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataHeader wRDataHeader)
        {
            var entry = await service.Create(wRDataHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wRDataHeader.WRDNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WRDNumber}")]
        public async Task<IActionResult> Update(String wRDNumber, [FromBody] WRDataHeader wRDataHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != wRDataHeader.WRDNumber)
            {
                return BadRequest();
            }

            await service.Update(wRDNumber, wRDataHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await service.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRDNumber}/")]
        public async Task<IActionResult> checkExist(String wRDNumber)
        {
            var result = await service.checkExist(wRDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRDNumber")]
        public async Task<IActionResult> GetLastWRDNumber()
        {
            var WRDNumber = await service.GetLastId();
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }

        [HttpGet]
        [Route("CheckTotalQuantity/{wRDNumber}")]
        public async Task<IActionResult> CheckTotalQuantity(String id)
        {
            var entry = await service.checkTotalQuantity(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

    }
}
