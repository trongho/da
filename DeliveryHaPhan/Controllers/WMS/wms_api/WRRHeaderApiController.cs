﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers.WMS.wms_api
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRRHeaderApiController : Controller
    {
        private readonly IWRRHeaderService service;

        public WRRHeaderApiController(IWRRHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("Get")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRRHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{wRRNumber}")]
        public async Task<IActionResult> GetWRRHeaderUnderWRRNumber(String wRRNumber)
        {
            var entrys = await service.GetUnderId(wRRNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWRRHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await service.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWRRHeaderUnderBranchID(String branchID)
        {
            var entrys = await service.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await service.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRRHeader wRRHeader)
        {
            var entry = await service.Create(wRRHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = wRRHeader.WRRNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{WRRNumber}")]
        public async Task<IActionResult> Update(String wRRNumber, [FromBody] WRRHeader wRRHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRRNumber != wRRHeader.WRRNumber)
            {
                return BadRequest();
            }

            await service.Update(wRRNumber, wRRHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRRNumber}")]
        public async Task<IActionResult> Delete(String wRRNumber)
        {
            var entry = await service.Delete(wRRNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRRNumber}")]
        public async Task<IActionResult> checkExist(String wRRNumber)
        {
            var result = await service.checkExist(wRRNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("LastWRRNumber")]
        public async Task<IActionResult> GetLastWRRNumber()
        {
            var WRRNumber = await service.GetLastId();
            if (WRRNumber == null)
            {
                return NotFound();
            }

            return Ok(WRRNumber);
        }

        [HttpGet]
        [Route("Filter/{branchID}/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> Filter(String branchID, DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await service.Filter(branchID, fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndHandlingStatus/{fromDate}/{toDate}/{handlingStatusID}")]
        public async Task<IActionResult> FilterByDateAndHandlingStatus(DateTime fromDate, DateTime toDate, String handlingStatusID)
        {
            var entrys = await service.FilterByDateAndHandlingStatus(fromDate, toDate, handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByDateAndBranch/{fromDate}/{toDate}/{branchID}")]
        public async Task<IActionResult> FilterByDateAndBranch(DateTime fromDate, DateTime toDate, String branchID)
        {
            var entrys = await service.FilterByDateAndBranch(fromDate, toDate, branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRRHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

    }
}
