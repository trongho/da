﻿using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class MapController : Controller
    {
        private readonly IMapService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public MapController(IMapService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }
        // GET: MapController
        public IActionResult Index()
        {
            string endPoint=service.GetLatLng("108/1 Phan Huy Ích, Phường 15, Quận Tân Bình, Hồ Chí Minh");
            string startPoint = service.GetLatLng("758/25/2B Xô Viết Nghệ Tĩnh, Phường 25, Quận Bình Thạnh, Hồ Chí Minh");
            string vehice = "bike";
            decimal kilometers = service.GetKilometers(startPoint,endPoint,vehice);
            ViewBag.EndPoint =kilometers;
            return View();
        }

        public IActionResult FormDemo([FromForm] OrderModel orderModel,String coordinateString)
        {
            orderModel.OrderID = "0000000002";
            orderModel.RequiredDate = DateTime.Now;
            orderModel.EndPoint = "10.8310595329999,106.633337642997";
            orderModel.StartPoint = "10.8210753,106.7116815";
            return View(orderModel);
        }

        [HttpGet("Map/GetAdress/{coordinateString}")]
        public IActionResult GetAdress([FromRoute] string coordinateString)
        {
            string adress = service.GetAdress(coordinateString);

            return Json(adress);
        }

        [AllowAnonymous]
        [HttpGet("Map/GetKilometers/{startpoint}/{endpoint}/{vehice}")]
        public IActionResult GetKilometers(string startpoint,string endpoint,string vehice)
        {
            decimal kilometers = service.GetKilometers(startpoint,endpoint,vehice);
            return Json(kilometers);
        }


        //[HttpGet("Map/GetDirections")]
        //public async Task<JsonResult> GetDirections()
        //{
        //    var entrys = await service.GetDirections(Request.Query["startpoint"].ToString(), Request.Query["endpoint"].ToString(), Request.Query["vehice"].ToString());
        //    return Json(entrys);
        //}

        [HttpGet]
        public async Task<object> GetDirections(DataSourceLoadOptions options)
        {
            var entrys = await service.GetDirections(Request.Query["startpoint"].ToString(), Request.Query["endpoint"].ToString(), Request.Query["vehice"].ToString());
            return DataSourceLoader.Load(entrys, options);
        }

        [HttpPut("Map/UpdateCurrentLocation/{orderid}/{currentpoint}")]
        public async Task<IActionResult> UpdateCurrentLocationAsync([FromRoute]string orderid,[FromRoute] string currentpoint)
        {
            var result = await service.UpdateCurrentLocation(orderid, currentpoint);
            return Json(result);
        }

        [HttpGet("Map/Download/{fileName}")]
        public async Task<FileResult> DowloadFileAsync(string fileName)
        {
            string filePath = System.IO.Path.Combine(_hostingEnvironment.WebRootPath,"ExcelFile/");
            IFileProvider provider = new PhysicalFileProvider(filePath);
            IFileInfo fileInfo = provider.GetFileInfo(fileName);
            var readStream = fileInfo.CreateReadStream();
            var mimeType = "application/vnd.openxmlformatsofficedocument.spreadsheetml.sheet";
            return File(readStream, mimeType,fileName);
        }

        [HttpGet]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(service.GetAll().Result, loadOptions);
        }

        // GET: MapController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: MapController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MapController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: MapController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: MapController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: MapController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: MapController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
