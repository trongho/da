﻿using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class ValidationController : Controller
    {
        private readonly IUserService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ValidationController(IUserService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CheckOldPassword([FromForm]ChangePassword changePassword)
        {
            string id = HttpContext.Session.GetString("UserID");
            if (changePassword.OldPassword == null || changePassword.OldPassword.Equals(""))
            {
                return Json(false);
            }
            var entrys = service.CheckOldPassword(id, changePassword);
            return Json(entrys);
        }
    }
}
