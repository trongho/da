﻿using DeliveryHaPhan.Helpers;
using DeliveryHaPhan.Interfaces;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Controllers
{
    public class UserHomeController : Controller
    {
        private readonly IOrderService service;
        private readonly IHostingEnvironment _hostingEnvironment;

        public UserHomeController(IOrderService service, IHostingEnvironment hostingEnvironment)
        {
            this.service = service;
            this._hostingEnvironment = hostingEnvironment;
        }

        [Authorize(Roles = "User")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("UserHome/GetCurrentYearKilometersOfEmployee")]
        public async Task<JsonResult> GetCurrentYearKilometersOfEmployee()
        {
            string employee1 = HttpContext.Session.GetString("Fullname");
            var entry = await service.GetCurrentYearKilometersOfEmployee(employee1);
            return Json(entry);
        }

        [HttpGet]
        public async Task<object> GetActiveOrders(DataSourceLoadOptions loadOptions)
        {
            string employee1 = HttpContext.Session.GetString("Fullname");
            var entrys = await service.GetOrdersByEmployeeAndStatus(employee1,Request.Query["status"].ToString());
            var models = OrderHelper.Covert(entrys);
            return DataSourceLoader.Load(models, loadOptions);
        }
    }
}
