﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.SignalR
{
    public class LiveUpdateWRDHub : Hub
    {
        private readonly WRDTicker _wRDTicker;

        public LiveUpdateWRDHub(WRDTicker wRDTicker)
        {
            _wRDTicker=wRDTicker;
        }

        public IEnumerable<WRDataGeneral> GetAllWRD()
        {
            return _wRDTicker.GetAllWRD();
        }
    }
}
