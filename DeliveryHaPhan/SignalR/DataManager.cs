﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.SignalR
{
    public static class DataManager
    {
        private static readonly IWRDataGeneralService service;
        public static List<WRDataGeneral> GetData()
        {
            var r = new Random();
            return new List<WRDataGeneral>()
            {
               new WRDataGeneral {WRDNumber="AA",GoodsID="BB",Ordinal=1,Quantity = r.Next(1, 40)},
               new WRDataGeneral {WRDNumber="AA",GoodsID="CC",Ordinal=2,Quantity = r.Next(1, 40)},
            };
            //return service.GetUnderIdSignalR("M164-5");
        }
    }
}
