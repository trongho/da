﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Interfaces;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryHaPhan.SignalR
{
    public class WRDTicker
    {
        private readonly IEnumerable<WRDataGeneral> _wRDataGenerals;
        private IHubContext<LiveUpdateWRDHub> _hubContext { get; set; }
        private readonly Random _updateOrNotRandom = new Random();

        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(500);

        private readonly Timer _timer;

        private readonly object _updateElectionVotesLock = new object();


        public WRDTicker(IHubContext<LiveUpdateWRDHub> hubContext, IWRDataGeneralService service)
        {
            _hubContext = hubContext;

            _wRDataGenerals = GeneralData();

            _timer = new Timer(UpdateWRD, null, _updateInterval, _updateInterval);

        }

        public IEnumerable<WRDataGeneral> GetAllWRD()
        {
            return _wRDataGenerals;
        }

        public IEnumerable<WRDataGeneral> GeneralData()
        {
            return DataManager.GetData();
        }

       

        private void UpdateWRD(object state)
        {
            lock (_updateElectionVotesLock)
            {
                foreach (var election in _wRDataGenerals)
                {
                    if (TryUpdateElectionVotes())
                    {
                        BroadcastElectionVotes(election);
                    }
                }
            }
        }

        private bool TryUpdateElectionVotes()
        {
            DataManager.GetData();
            return true;
        }



        private void BroadcastElectionVotes(WRDataGeneral wRDataGeneral)
        {
            _hubContext.Clients.All.SendAsync("updateWRD",wRDataGeneral);
        }
    }
}
