﻿using DeliveryHaPhan.Entites;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IProductService
    {
        Task<List<Product>> GetAll();
        Task<List<Product>> GetUnderId(String id);
        Task<Boolean> Create(Product product,string photo);
        Task<Boolean> Update(String id, Product product,String images);
        Task<Boolean> Delete(String id);
        int GetID();
        Task<bool> UploadFile(IFormFile file);
    }
}
