﻿using DeliveryHaPhan.Entites;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IGoodsDataService
    {
        Task<List<GoodsData>> GetAll();
        Task<Boolean> Create(GoodsData entry);
        Task<Boolean> CreateRange(List<GoodsData> entrys);
        Task<List<GoodsData>> GetUnderId(String id);
        Task<Boolean> Update(String id, GoodsData entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
        Task<Boolean> checkExist(String id, String goodsName, String slPart);
    }
}
