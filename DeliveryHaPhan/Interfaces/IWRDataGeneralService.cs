﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IWRDataGeneralService
    {
        Task<List<WRDataGeneral>> GetAll();
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber);
        IEnumerable<WRDataGeneral> GetUnderIdSignalR(String wRDNumber);
        IEnumerable<WRDataGeneral> GetUnderIdSignalR(String wRDNumber, String goodsID);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String goodsID);
        Task<Boolean> Create(WRDataGeneral wRDataGeneral);
        Task<Boolean> Update(String wRDNumber, String goodsID, int Ordinal, WRDataGeneral wRDataGeneral);
        Task<Boolean> Delete(String wRDNumber);       
        Task<String> checkQuantity(String wRDNumber, DateTime editedDateTime);
    }
}
