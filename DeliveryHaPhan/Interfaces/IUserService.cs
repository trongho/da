﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace DeliveryHaPhan.Interfaces
{
    public interface IUserService
    {
        Task<LoginResponse> Login(LoginRequest loginRequest);
        Task<Boolean> changePassword(string id,ChangePassword changePassword);
        Boolean CheckOldPassword(string id, ChangePassword changePassword);
        Task<List<User>> GetAll();
        Task<List<User>> GetUnderId(string id);
        Task<Boolean> Create(User user);
        Task<Boolean> Update(string id, string updateid, User user);
        Task<Boolean> Update2(string id, string updateid, UserModel userModel);
        Task<Boolean> ChangeAvatar(string id,String imageUrl,User user);
        Task<Boolean> Delete(string id);
        Task<string> GetLastID();
    }
}
