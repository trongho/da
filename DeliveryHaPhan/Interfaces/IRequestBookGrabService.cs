﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IRequestBookGrabService
    {
        Task<Boolean> Create(RequestBookGrab entry);
        Task<List<RequestBookGrab>> GetUnderParams(String id, int Ordinal);
        Task<List<RequestBookGrab>> GetUnderId(String id);
        Task<List<RequestBookGrab>> GetAll();
        Task<Boolean> Update(String id, int Ordinal, RequestBookGrab entry);
        Task<Boolean> Delete(String id);

    }
}
