﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IRequestLendGoodsDemoService
    {
        Task<Boolean> Create(RequestLendGoodsDemo entry);
        Task<List<RequestLendGoodsDemo>> GetUnderParams(String id, int Ordinal);
        Task<List<RequestLendGoodsDemo>> GetUnderId(String id);
        Task<List<RequestLendGoodsDemo>> GetAll();
        Task<Boolean> Update(String id, int Ordinal, RequestLendGoodsDemo entry);
        Task<Boolean> Delete(String id);
    }
}
