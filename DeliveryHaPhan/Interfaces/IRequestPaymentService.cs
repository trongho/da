﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IRequestPaymentService
    {
        Task<Boolean> Create(RequestPayment entry);
        Task<List<RequestPayment>> GetUnderParams(String id, int Ordinal);
        Task<List<RequestPayment>> GetUnderId(String id);
        Task<List<RequestPayment>> GetAll();
        Task<Boolean> Update(String id, int Ordinal, RequestPayment entry);
        Task<Boolean> Delete(String id);
    }
}
