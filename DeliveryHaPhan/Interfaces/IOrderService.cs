﻿
using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IOrderService
    {
        string getFilePath();
        Task<bool> UploadFile(IFormFile file);
        
        Task<List<Order>> Import(string fileName);
        Task<bool> Export(List<Order> orders,string fileName);
        Task<List<Order>> GetAll();
 
        Task<List<Order>> GetUnderId(String id);
        Task<List<Order>> GetUnderEmployee(String employee);
        Task<Boolean> Create(Order order);
        Task<Boolean> Create2(Order order);
        Task<Boolean> Update(String id,Order order);
        Task<Boolean> Update2(String id, OrderModel orderModel);
        Task<Boolean> Assign(String id,AssignOrder assignOrder);
        Task<Boolean> Ended(String id);
        Task<Boolean> CancelOrder(String id,String reason);
        Task<Boolean> Start(String id, String point,string adress,decimal kilometersOrg);
        Task<Boolean> Continue(String id, String point);
        Task<Boolean> Pause(String id,String point,decimal kilometers);
        Task<Boolean> End(String id, decimal kilometers);
        Task<Boolean> Delete(String id);
        Task<Boolean> DeleteMore(String idString);
        Task<String> GetLastID();
        Task<String> GetCurrentPoint(String id);
        Task<Boolean> UpdateStartPoint(String id,String startPoint);
        Task<bool> UpdateCurrentPoint(string id, string currentpoint);
        Task<bool> UpdateEndPoint(string id, string endpoint,string adress);
        Task<bool> UpdateMass(string id,decimal mass);

        Task<bool> UpdateSurcharge(string id, decimal surcharge);
        Task<String> GetLatLng(String adress);
        Task<List<User>> GetUsers();
        int CountOrdersUnderStatus(String status);
        Task<List<Order>> GetActiveOrders(String status);
        Task<List<Order>> GetActiveOrdersByThisMonth(String status);
        Task<List<Order>> GetNewOrdersByThisMonth(String status);
        Task<List<Order>> GetOrdersByEmployeeAndStatus(String employee1,String status);
        Task<Decimal?> GetSumKilometersByDate(DateTime date);
        Task<Decimal> GetSumKilometersByCurrentMonth();
        Task<List<KilometersData>> GetKilometers();
        Task<List<KilometersData>> GetCurrentYearKilometers();
        Task<List<KilometersData>> GetCurrentYearKilometersWithEmployee();

        Task<List<KilometersData>> GetCurrentYearKilometersOfEmployee(string employee1);

        Task<List<StatusStatistic>> GetCurrentYearStatusStatistic();

        Task<String> GetLog(DateTime date,string filterString);
        Task<List<OrderLog>> GetLog2(DateTime date);

        Task<String> GetEndPoint(string orderID);
        Task<List<YearData>> GetYearData();
    }
}
