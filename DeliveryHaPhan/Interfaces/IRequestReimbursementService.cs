﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IRequestReimbursementService
    {
        Task<Boolean> Create(RequestReimbursement entry);
        Task<List<RequestReimbursement>> GetUnderParams(String id, int Ordinal);
        Task<List<RequestReimbursement>> GetUnderId(String id);
        Task<List<RequestReimbursement>> GetAll();
        Task<Boolean> Update(String id, int Ordinal, RequestReimbursement entry);
        Task<Boolean> Delete(String id);
    }
}
