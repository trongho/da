﻿
using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IOrderResultService
    {
        Task<List<OrderResult>> GetAll();
    }
}
