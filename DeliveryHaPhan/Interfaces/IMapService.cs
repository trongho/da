﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IMapService
    {
        String GetLatLng(String adress);
        String GetAdress(String coordinateString);
        Decimal GetKilometers(String startPoint, String endPoint, String vehice);
        Decimal GetKilometersText(String startPoint, String endPoint, String vehice);
        Task<bool> UpdateCurrentLocation(string id, string currentpoint);
        Task<List<Order>> GetAll();
        Task<List<Direction>> GetDirections(String startPoint, String endPoint, String vehice);
    }
}
