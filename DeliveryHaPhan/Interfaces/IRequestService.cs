﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IRequestService
    {
        Task<List<Request>> GetAll();
        Task<Boolean> Create(Request entry);
        Task<List<Request>> GetUnderId(String id);
        Task<List<Request>> GetUnderApprovers(String approvers);
        Task<List<Request>> GetUnderCreateName(String createName);
        Task<List<Request>> GetUnderFollowers(String followers);
        Task<List<Request>> GetOverdueRequest(string approvers);
        Task<List<Request>> GetLastRequestApproving(string approver);
        Task<Boolean> Update(String id, Request entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
        Task<Boolean> Approve(String id);
        Task<Boolean> Forward(String id, String approver);
        Task<Boolean> Refuse(String id);
        Task<Boolean> Recycle(string id);
        Task<int> GetQuantityRequest(string createName, string text);
    }
}
