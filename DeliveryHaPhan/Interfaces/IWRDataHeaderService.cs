﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IWRDataHeaderService
    {
        Task<List<WRDataHeader>> GetAll();
        Task<List<WRDataHeader>> GetUnderId(String id);
        Task<Boolean> Create(WRDataHeader wRDataHeader);
        Task<Boolean> Update(String id, WRDataHeader wRDataHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);
        Task<String> checkTotalQuantity(String wRDNumber);
    }
}
