﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IRequestSuppliesService
    {
        Task<Boolean> Create(RequestSupplies entry);
        Task<List<RequestSupplies>> GetUnderParams(String id, int Ordinal);
        Task<List<RequestSupplies>> GetUnderId(String id);
        Task<List<RequestSupplies>> GetAll();
        Task<Boolean> Update(String id, int Ordinal, RequestSupplies entry);
        Task<Boolean> Delete(String id);
    }
}
