﻿using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IWRRDetailService
    {
        Task<List<WRRDetail>> GetAll();
        Task<List<WRRDetail>> GetUnderId(String wRRNumber, String goodsID, int Ordinal);
        Task<List<WRRDetail>> GetUnderId(String wRRNumber);
        Task<Boolean> Create(WRRDetail wRRDetail);
        Task<Boolean> Update(String wRRNumber, String goodsID, int Ordinal, WRRDetail wRRDetail);
        Task<Boolean> Delete(String wRRNumber);
    }
}
