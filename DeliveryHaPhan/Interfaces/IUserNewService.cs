﻿using DeliveryHaPhan.Entites;
using DeliveryHaPhan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IUserNewService
    {
        Task<LoginResponseNew> Login(LoginRequest loginRequest);
        Task<Boolean> changePassword(string id, ChangePassword changePassword);
        Task<List<UserNew>> GetAll();
        Task<List<UserNew>> GetApprovers(string loginDepartment,int loginRank);
        Task<List<UserNew>> GetFollowers(int loginRank);
        Task<List<UserNew>> GetStaffUnderDepartmentAndRanks(string loginDepartment, int loginRank);
        Task<List<UserNew>> GetUnderId(string id);
        Task<Boolean> Create(UserNew userNew);
        Task<Boolean> Create2(UserNew userNew);
        Task<Boolean> Update(string id, string updateid, UserNew userNew);
        Task<Boolean> Delete(string id);
        Task<List<UserNew>> GetUnderUserName(string username);
    }
}
