﻿
using DeliveryHaPhan.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryHaPhan.Interfaces
{
    public interface IWarehouseService
    {
        Task<List<Warehouse>> GetAll();
        Task<List<Warehouse>> GetUnderId(String id);
        Task<Boolean> Create(Warehouse warehouse);
        Task<Boolean> Update(String id,Warehouse warehouse);
        Task<Boolean> Delete(String id);
    }
}
